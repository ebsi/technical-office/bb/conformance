import { createHash, randomBytes, randomUUID } from "node:crypto";
import { jest, describe, beforeAll, afterAll, it, expect } from "@jest/globals";
import request from "supertest";
import { Test } from "@nestjs/testing";
import type { TestingModule } from "@nestjs/testing";
import { Logger } from "@nestjs/common";
import type { HttpServer } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import type { NestFastifyApplication } from "@nestjs/platform-fastify";
import type { FastifyInstance } from "fastify";
import { SignJWT, exportJWK, generateKeyPair } from "jose";
import { util as keyDidMethodHelpers } from "@cef-ebsi/key-did-resolver";
import qs from "qs";
import { verifyCredentialJwt } from "@cef-ebsi/verifiable-credential";
import type { VerifyCredentialOptions } from "@cef-ebsi/verifiable-credential";
import { base64url } from "multiformats/bases/base64";
import { AppModule } from "../../src/app.module";
import type { ApiConfig } from "../../src/config/configuration";
import { getServer } from "../utils/getServer";
import { configureApp } from "../utils/app";
import {
  ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_DEFERRED,
  ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_IN_TIME,
  ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_PRE_AUTHORISED,
  ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_DEFERRED,
  ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_IN_TIME,
  ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_PRE_AUTHORISED,
  REQUEST_CT_ISSUE_TO_HOLDER_QUALIFICATION_CREDENTIAL,
} from "../../src/shared/constants";
import { getKeyPair, getUserPin } from "../../src/shared/utils";
import type { AuthorizationDetails } from "../../src/shared/validators";
import type {
  IssueToHolderDeferredParams,
  IssueToHolderInTimeParams,
  IssueToHolderPreAuthorisedParams,
} from "../../src/modules/issuer-mock/validators";
import type { CredentialIssuerMetadata } from "../../src/shared/interfaces";
import type {
  GetAuthorizeHolderWallerDto,
  IdTokenRequest,
  OPMetadata,
  PostTokenPkceDto,
  TokenResponse,
} from "../../src/shared/auth-server";
import type { CredentialResponse } from "../../src/shared/credential-issuer";

jest.setTimeout(600_000);

/**
 * Actors:
 * - Credential Issuer: Subject under test (client mock)
 * - Holder Wallet: Issuer Mock (using its ES256 public key and the did:key method)
 */
describe("WCT v3 - Issue to Holder e2e tests", () => {
  let app: NestFastifyApplication;
  let server: HttpServer | string;
  let configService: ConfigService<ApiConfig, true>;
  let conformanceDomain: string;
  let apiUrlPrefix: string;
  let issuerMockUri: string;
  let ebsiAuthority: string;

  function getPathnameWithoutPrefix(uri: string) {
    return new URL(uri).pathname.replace(conformanceDomain, "");
  }

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    configService =
      moduleFixture.get<ConfigService<ApiConfig, true>>(ConfigService);

    conformanceDomain = configService.get<string>("conformanceDomain");
    apiUrlPrefix = configService.get<string>("apiUrlPrefix");
    issuerMockUri = `${conformanceDomain}${apiUrlPrefix}/issuer-mock`;
    ebsiAuthority = configService
      .get<string>("domain")
      .replace(/^https?:\/\//, ""); // remove http protocol scheme

    app = await configureApp(moduleFixture, { apiUrlPrefix });

    Logger.overrideLogger(false);

    await app.init();
    await (app.getHttpAdapter().getInstance() as FastifyInstance).ready();

    server = await getServer(app, configService, { conformanceDomain });
  });

  afterAll(async () => {
    // Avoid jest open handle error
    await new Promise<void>((resolve) => {
      setTimeout(() => resolve(), 500);
    });
    await app.close();
  });

  it("should follow the entire Issue to Holder flow", async () => {
    // The client requesting the credential is impersonated by Issuer Mock, using a did:key DID
    const issuerMockPrivateKeyHex = configService.get<string>(
      "issuerMockPrivateKey"
    );
    const keyPairES256 = await getKeyPair(issuerMockPrivateKeyHex, "ES256");
    const clientDid = keyDidMethodHelpers.createDid(keyPairES256.publicKeyJwk);
    const clientId = clientDid;

    /**
     * ------------------------------------------------------------------------
     * Prepare Credential Issuer's server
     * ------------------------------------------------------------------------
     */
    const credentialIssuerPrivateKey = (await generateKeyPair("ES256"))
      .privateKey;
    const credentialIssuerPrivateKeyJwk = await exportJWK(
      credentialIssuerPrivateKey
    );
    const credentialIssuerKeyPair = await getKeyPair(
      credentialIssuerPrivateKeyJwk
    );
    const credentialIssuerDid = keyDidMethodHelpers.createDid(
      credentialIssuerKeyPair.publicKeyJwk
    );
    const fragmentIdentifier = credentialIssuerDid.replace("did:key:", "");
    const credentialIssuerKid = `${credentialIssuerDid}#${fragmentIdentifier}`;
    const credentialIssuerPathname = `${apiUrlPrefix}/client-mock/${credentialIssuerDid}`;
    const credentialIssuer = `${conformanceDomain}${credentialIssuerPathname}`;

    const issuerState = "issuer-state";

    let response = await request(server)
      .post(`${apiUrlPrefix}/client-mock/initiate`)
      .send({
        did: credentialIssuerDid,
        keys: [credentialIssuerKeyPair.privateKeyJwk],
        issuerState,
      });

    expect(response.status).toBe(204);

    /**
     * ------------------------------------------------------------------------
     * Request CTWalletSameInTime
     * ------------------------------------------------------------------------
     */
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_IN_TIME,
        data: {
          credentialIssuer,
          credentialIssuerDid,
          issuerState,
        } satisfies IssueToHolderInTimeParams,
      });

    expect(response.body).toStrictEqual({
      success: true,
    });

    /**
     * ------------------------------------------------------------------------
     * Validate CTWalletSameInTime
     * ------------------------------------------------------------------------
     */
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_IN_TIME,
        data: {
          credentialIssuer,
          credentialIssuerDid,
          issuerState,
        } satisfies IssueToHolderInTimeParams,
      });

    expect(response.body).toStrictEqual({
      success: true,
    });

    /**
     * ------------------------------------------------------------------------
     * Request CTWalletSameDeferred
     * ------------------------------------------------------------------------
     */
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_DEFERRED,
        data: {
          credentialIssuer,
          credentialIssuerDid,
          issuerState,
        } satisfies IssueToHolderDeferredParams,
      });

    expect(response.body).toStrictEqual({
      success: true,
    });

    /**
     * ------------------------------------------------------------------------
     * Validate CTWalletSameDeferred
     * ------------------------------------------------------------------------
     */
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_DEFERRED,
        data: {
          credentialIssuer,
          credentialIssuerDid,
          issuerState,
        } satisfies IssueToHolderDeferredParams,
      });

    expect(response.body).toStrictEqual({
      success: true,
    });

    /**
     * ------------------------------------------------------------------------
     * Request CTWalletSamePreAuthorised
     * ------------------------------------------------------------------------
     */
    // In our case, the server expects the pre-authorized code to be a JWT
    // In practice, it can be anything as long as the client server accepts it
    const preAuthorizedCode = await new SignJWT({
      iss: credentialIssuer,
      sub: clientId,
      client_id: clientId,
      authorization_details: [
        {
          type: "openid_credential",
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "CTWalletSamePreAuthorised",
          ],
          locations: [credentialIssuer],
        },
      ] satisfies AuthorizationDetails,
    })
      .setProtectedHeader({
        alg: "ES256",
        kid: credentialIssuerKeyPair.publicKeyJwk.kid,
      })
      .sign(credentialIssuerPrivateKey);
    const userPin = getUserPin(clientId);

    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_PRE_AUTHORISED,
        data: {
          credentialIssuer,
          credentialIssuerDid,
          preAuthorizedCode,
          userPin,
        } satisfies IssueToHolderPreAuthorisedParams,
      });

    expect(response.body).toStrictEqual({
      success: true,
    });

    /**
     * ------------------------------------------------------------------------
     * Validate CTWalletSamePreAuthorised
     * ------------------------------------------------------------------------
     */
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_PRE_AUTHORISED,
        data: {
          credentialIssuer,
          credentialIssuerDid,
          preAuthorizedCode,
          userPin,
        } satisfies IssueToHolderPreAuthorisedParams,
      });

    expect(response.body).toStrictEqual({
      success: true,
    });

    /**
     * ------------------------------------------------------------------------
     * Request CTIssueQualificationCredential from Conformance Issuer
     * ------------------------------------------------------------------------
     */

    // Stop here if the tests are running locally.
    // Reason: Loki can't get the logs of the local instance
    if (process.env.TEST_ENV !== "remote") {
      // eslint-disable-next-line no-console
      console.warn("Can't execute all the Issue to Holder tests locally.");
      return;
    }

    // Wait ~10 seconds to let Loki retrieve the logs
    await new Promise<void>((resolve) => {
      setTimeout(() => resolve(), 10_000);
    });

    // GET /issuer-mock/.well-known/openid-credential-issuer
    response = await request(server).get(
      `${apiUrlPrefix}/issuer-mock/.well-known/openid-credential-issuer`
    );

    expect(response.status).toBe(200);

    const credentialIssuerConfig = response.body as CredentialIssuerMetadata;

    // GET AUthorization Server (Auth Mock) /.well-known/openid-configuration
    if (!credentialIssuerConfig.authorization_server)
      throw new Error("authorization_server not defined");
    const authorizationServerUri = credentialIssuerConfig.authorization_server;

    response = await request(server).get(
      getPathnameWithoutPrefix(
        `${authorizationServerUri}/.well-known/openid-configuration`
      )
    );

    const authorizationServerOpenIdConfig = response.body as OPMetadata;

    // GET AS /authorize
    const codeVerifier = randomBytes(50).toString("base64url");
    const codeChallenge = base64url.baseEncode(
      createHash("sha256").update(codeVerifier).digest()
    );

    const authenticationRequestParams = {
      scope: "openid",
      client_id: credentialIssuerDid,
      response_type: "code",
      redirect_uri: "openid://callback",
      code_challenge: codeChallenge,
      code_challenge_method: "S256",
      state: randomUUID(),
      client_metadata: JSON.stringify({
        authorization_endpoint: "openid:",
      }),
      authorization_details: JSON.stringify([
        {
          type: "openid_credential",
          format: "jwt_vc",
          locations: [issuerMockUri],
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "CTIssueQualificationCredential",
          ],
        },
      ]),
    } satisfies GetAuthorizeHolderWallerDto;

    response = await request(server)
      .get(
        `${getPathnameWithoutPrefix(
          authorizationServerOpenIdConfig.authorization_endpoint
        )}?${new URLSearchParams(authenticationRequestParams).toString()}`
      )
      .send();

    expect(response.status).toBe(302);

    let { location } = response.headers as { location: string };

    const idTokenRequestParams = qs.parse(
      new URL(location).search.substring(1)
    ) as unknown as IdTokenRequest;

    expect(idTokenRequestParams.request).toBeUndefined();
    expect(idTokenRequestParams.request_uri).toBeDefined();

    // POST /auth-mock/direct_post
    const idToken = await new SignJWT({
      nonce: idTokenRequestParams.nonce,
      sub: credentialIssuerDid,
    })
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: credentialIssuerKid,
      })
      .setIssuer(credentialIssuerDid)
      .setAudience(authorizationServerOpenIdConfig.issuer)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(credentialIssuerPrivateKey);

    response = await request(server)
      .post(`${getPathnameWithoutPrefix(idTokenRequestParams.redirect_uri)}`)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(
        new URLSearchParams({
          id_token: idToken,
          state: idTokenRequestParams.state,
        }).toString()
      );

    expect(response.status).toBe(302);
    location = (response.headers as { location: string }).location;
    expect(location).toStrictEqual(
      expect.stringContaining(authenticationRequestParams.redirect_uri)
    );

    const authenticationResponseQueryParams = qs.parse(
      new URL(location).search.substring(1)
    ) as unknown as { code: string; state: string };

    expect(authenticationResponseQueryParams.state).toBe(
      authenticationRequestParams.state
    );
    expect(authenticationResponseQueryParams.code).toBeTruthy();

    // POST /auth-mock/token
    const { code } = authenticationResponseQueryParams;
    const pkceTokenRequestQueryParams = {
      grant_type: "authorization_code",
      code,
      client_id: credentialIssuerDid,
      code_verifier: codeVerifier,
    } satisfies PostTokenPkceDto;

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          authorizationServerOpenIdConfig.token_endpoint
        )}`
      )
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(new URLSearchParams(pkceTokenRequestQueryParams).toString());

    expect(response.status).toBe(200);
    const { c_nonce: cNonce, access_token: accessToken } =
      response.body as TokenResponse;
    expect(accessToken).toBeDefined();

    // POST /issuer-mock/credential
    const proofJwt = await new SignJWT({
      nonce: cNonce,
    })
      .setProtectedHeader({
        typ: "openid4vci-proof+jwt",
        alg: "ES256",
        kid: credentialIssuerKid,
      })
      .setIssuer(credentialIssuerDid)
      .setAudience(issuerMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(credentialIssuerPrivateKey);

    const credentialRequestParams = {
      types: [
        "VerifiableCredential",
        "VerifiableAttestation",
        "CTIssueQualificationCredential",
      ],
      format: "jwt_vc",
      proof: {
        proof_type: "jwt",
        jwt: proofJwt,
      },
    };

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          credentialIssuerConfig.credential_endpoint
        )}`
      )
      .set("Authorization", `Bearer ${accessToken}`)
      .send(credentialRequestParams);

    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual({
      format: "jwt_vc",
      credential: expect.any(String),
    });

    // Check VC
    const { credential } = response.body as CredentialResponse;
    const options: VerifyCredentialOptions = { ebsiAuthority };
    const vcPayload = await verifyCredentialJwt(credential, options);

    expect(vcPayload).toStrictEqual({
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      id: expect.stringContaining("vc:ebsi:conformance#"),
      type: [
        "VerifiableCredential",
        "VerifiableAttestation",
        "CTIssueQualificationCredential",
      ],
      issuer: configService.get<string>("issuerMockKid").split("#")[0],
      issuanceDate: expect.any(String),
      issued: expect.any(String),
      validFrom: expect.any(String),
      expirationDate: expect.any(String),
      credentialSubject: {
        id: credentialIssuerDid,
      },
      credentialSchema: {
        id: configService.get<string>("authorisationCredentialSchema"),
        type: "FullJsonSchemaValidator2021",
      },
      termsOfUse: {
        id: configService.get<string>("issuerMockAccreditationUrl"),
        type: "IssuanceCertificate",
      },
    });

    // Verify that the event has been properly emitted
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: REQUEST_CT_ISSUE_TO_HOLDER_QUALIFICATION_CREDENTIAL,
        data: {
          did: credentialIssuerDid,
          clientId: credentialIssuer,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);
  });
});
