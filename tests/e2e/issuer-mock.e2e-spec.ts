import { randomBytes } from "node:crypto";
import { URLSearchParams } from "node:url";
import { jest, describe, beforeAll, afterAll, it, expect } from "@jest/globals";
import { Test, TestingModule } from "@nestjs/testing";
import { ConfigService } from "@nestjs/config";
import { Logger } from "@nestjs/common";
import type { INestApplication, HttpServer } from "@nestjs/common";
import type { FastifyInstance } from "fastify";
import request from "supertest";
import axios from "axios";
import qs from "qs";
import { decodeJwt, importJWK, jwtVerify, SignJWT } from "jose";
import type { JWK } from "jose";
import { verifyCredentialJwt } from "@cef-ebsi/verifiable-credential";
import type { VerifyCredentialOptions } from "@cef-ebsi/verifiable-credential";
import type { ApiConfig } from "../../src/config/configuration";
import { AppModule } from "../../src/app.module";
import type {
  TokenResponse,
  OPMetadata,
  AuthenticationRequest,
} from "../../src/shared/auth-server";
import type { CredentialResponse } from "../../src/shared/credential-issuer";
import { configureApp } from "../utils/app";
import { getServer } from "../utils/getServer";
import {
  createClient,
  createQueryParams,
  createTokenRequestParams,
} from "../utils/data";
import type { VALID_CREDENTIAL_TYPES } from "../../src/shared/constants";
import type { AuthorizationDetails } from "../../src/shared/validators";

jest.setTimeout(180_000);

describe("Issuer Mock Module", () => {
  let app: INestApplication;
  let server: HttpServer | string;
  let configService: ConfigService<ApiConfig, true>;
  let conformanceDomain: string;
  let authMockUri: string;
  let apiUrlPrefix: string;
  let issuerMockUri: string;
  let issuerMockDid: string;
  let testClientMockPrivateKey: string;
  let testClientMockKid: string;
  let testClientMockDid: string;
  let ebsiAuthority: string;
  let didRegistry: string;
  let trustedIssuersRegistry: string;
  let trustedPoliciesRegistry: string;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    configService =
      moduleFixture.get<ConfigService<ApiConfig, true>>(ConfigService);

    conformanceDomain = configService.get<string>("conformanceDomain");
    apiUrlPrefix = configService.get<string>("apiUrlPrefix");
    authMockUri = `${conformanceDomain}${apiUrlPrefix}/auth-mock`;
    issuerMockUri = `${conformanceDomain}${apiUrlPrefix}/issuer-mock`;
    [issuerMockDid] = configService.get<string>("issuerMockKid").split("#") as [
      string
    ];
    testClientMockPrivateKey = configService.get<string>(
      "testClientMockPrivateKey"
    );
    testClientMockKid = configService.get<string>("testClientMockKid");
    [testClientMockDid] = testClientMockKid.split("#") as [string];
    didRegistry = configService.get<string>("didRegistryApiUrl");
    trustedIssuersRegistry = configService.get<string>(
      "trustedIssuersRegistryApiUrl"
    );
    trustedPoliciesRegistry = configService.get<string>(
      "trustedPoliciesRegistryApiUrl"
    );
    ebsiAuthority = configService
      .get<string>("domain")
      .replace(/^https?:\/\//, ""); // remove http protocol scheme

    app = await configureApp(moduleFixture, { apiUrlPrefix });

    Logger.overrideLogger(false);

    await app.init();
    await (app.getHttpAdapter().getInstance() as FastifyInstance).ready();

    server = await getServer(app, configService, { conformanceDomain });
  });

  afterAll(async () => {
    // Avoid jest open handle error
    await new Promise((r) => {
      setTimeout(r, 500);
    });
    await app.close();
  });

  describe("GET /issuer-mock/.well-known/openid-credential-issuer", () => {
    it("should return the well-known OpenID Credential Issuer config", async () => {
      expect.assertions(2);

      const response = await request(server).get(
        `${apiUrlPrefix}/issuer-mock/.well-known/openid-credential-issuer`
      );

      expect(response.body).toStrictEqual({
        credential_issuer: issuerMockUri,
        authorization_server: authMockUri,
        credential_endpoint: `${issuerMockUri}/credential`,
        deferred_credential_endpoint: `${issuerMockUri}/credential_deferred`,
        credentials_supported: expect.arrayContaining([
          {
            format: "jwt_vc",
            types: expect.arrayContaining([
              "VerifiableCredential",
              "VerifiableAttestation",
            ]),
            trust_framework: {
              name: "ebsi",
              type: "Accreditation",
              uri: expect.any(String),
            },
            display: expect.arrayContaining([
              {
                name: expect.any(String),
                locale: expect.any(String),
              },
            ]),
          },
        ]),
      });

      expect(response.status).toBe(200);
    });
  });

  describe("POST /issuer-mock/credential", () => {
    it("should return an error if the content type is not application/json", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post(`${apiUrlPrefix}/issuer-mock/credential`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            types: JSON.stringify([
              "VerifiableCredential",
              "VerifiableAttestation",
              "VerifiableAuthorisationToOnboard",
            ]),
            format: "jwt_vc",
            proof: JSON.stringify({
              proof_type: "jwt",
              jwt: "",
            }),
          }).toString()
        );

      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description: "Content-type must be application/json",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the access token is missing", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post(`${apiUrlPrefix}/issuer-mock/credential`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: "Authorization header is missing",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the authorization header is not valid", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post(`${apiUrlPrefix}/issuer-mock/credential`)
        .set("Authorization", "invalid")
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: "Authorization header must contain a Bearer token",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the Bearer token is not a JWT", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post(`${apiUrlPrefix}/issuer-mock/credential`)
        .set("Authorization", "Bearer invalid")
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: "Invalid Access Token header. Parsing failed.",
      });
      expect(response.status).toBe(400);
    });
  });

  describe("POST /issuer-mock/credential_deferred", () => {
    it("should return an error if the access token is missing", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post(`${apiUrlPrefix}/issuer-mock/credential_deferred`)
        .send();

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: "Authorization header is missing",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the authorization header is not valid", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post(`${apiUrlPrefix}/issuer-mock/credential_deferred`)
        .set("Authorization", "invalid")
        .send();

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: "Authorization header must contain a Bearer token",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if no deferred credential is found for the given acceptance token", async () => {
      expect.assertions(2);

      const acceptanceToken = Buffer.from(randomBytes(32)).toString(
        "base64url"
      );

      const response = await request(server)
        .post(`${apiUrlPrefix}/issuer-mock/credential_deferred`)
        .set("Authorization", `Bearer ${acceptanceToken}`)
        .send();

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: "Deferred credential not found",
      });
      expect(response.status).toBe(400);
    });
  });

  describe.each([
    "VerifiableAccreditationToAttest",
    "VerifiableAccreditationToAccredit",
  ] as const)("requesting a %s", (requestedType) => {
    it(`should get a ${requestedType} if the request is valid`, async () => {
      const requestedTypes: (typeof VALID_CREDENTIAL_TYPES)[number][] = [
        "VerifiableAccreditation",
        requestedType,
      ];

      // 0. prepare client's server
      const client = await createClient({
        privateKey: testClientMockPrivateKey,
        domain: conformanceDomain,
        pathname: `${apiUrlPrefix}/client-mock/${testClientMockDid}`,
      });
      const respClientMockRegistration = await request(server)
        .post(`${apiUrlPrefix}/client-mock/initiate`)
        .send({
          did: testClientMockDid,
          keys: [client.privateKeyJwk],
        });
      expect(respClientMockRegistration.status).toBe(204);

      // 1. call /auth-mock/.well-known/openid-configuration
      const responseOpenIdConfigurationIssuer = await request(server).get(
        `${apiUrlPrefix}/issuer-mock/.well-known/openid-credential-issuer`
      );

      expect(responseOpenIdConfigurationIssuer.status).toBe(200);
      const authorizationServer = (
        responseOpenIdConfigurationIssuer.body as {
          authorization_server: string;
        }
      ).authorization_server;
      const localAuthorizationServer = authorizationServer.replace(
        conformanceDomain,
        ""
      );

      /* AUTH MOCK */

      // 2. call /auth-mock/.well-known/openid-configuration
      const responseOpenIdConfiguration = await request(server).get(
        `${localAuthorizationServer}/.well-known/openid-configuration`
      );

      expect(responseOpenIdConfiguration.body).toStrictEqual(
        expect.objectContaining({})
      );
      expect(responseOpenIdConfiguration.status).toBe(200);

      const opMetadata = responseOpenIdConfiguration.body as OPMetadata;
      const jwksUri = opMetadata.jwks_uri.replace(conformanceDomain, "");
      const authorizationEndpoint = opMetadata.authorization_endpoint.replace(
        conformanceDomain,
        ""
      );
      const redirectUri = (opMetadata.redirect_uris[0] as string).replace(
        conformanceDomain,
        ""
      );
      const tokenEndpoint = opMetadata.token_endpoint.replace(
        conformanceDomain,
        ""
      );

      // 3. call /jwks to get JWK of authorisation service
      const responseJwks = await request(server).get(jwksUri);

      expect(responseJwks.body).toStrictEqual(expect.objectContaining({}));
      expect(responseJwks.status).toBe(200);

      const jwkAuthService = (responseJwks.body as { keys: [JWK] }).keys[0];

      // 3. call /authorize
      const authorizationDetails: AuthorizationDetails = [
        {
          type: "openid_credential",
          format: "jwt_vc",
          locations: [issuerMockUri],
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            ...requestedTypes,
          ],
        },
      ];
      const authenticationRequestParams = await createQueryParams(
        client,
        authorizationServer,
        issuerMockUri,
        {
          customQueryParams: { redirect_uri: `${client.clientId}/code-cb` },
          customJwtPayload: {
            authorization_details: authorizationDetails,
          },
        }
      );
      const authenticationRequestJwt = authenticationRequestParams.request;
      const decodedAuthenticationRequestJwt = decodeJwt(
        authenticationRequestJwt
      ) as AuthenticationRequest;

      const responseAuthorize = await request(server).get(
        `${authorizationEndpoint}?${new URLSearchParams(
          authenticationRequestParams
        ).toString()}`
      );

      expect(responseAuthorize.status).toBe(302);

      let { location } = responseAuthorize.headers as { [x: string]: string };
      expect(location).toStrictEqual(
        expect.stringContaining(
          decodedAuthenticationRequestJwt.client_metadata
            .authorization_endpoint as string
        )
      );

      // 4. Verify response of /authorize
      const responseQueryParams = qs.parse(
        new URL(location as string).search.substring(1)
      );

      expect(responseQueryParams).toStrictEqual(
        expect.objectContaining({
          state: expect.any(String),
          client_id: expect.any(String),
          redirect_uri: expect.any(String),
          response_type: "id_token",
          response_mode: "direct_post",
          scope: "openid",
          nonce: expect.any(String),
          request_uri: expect.any(String),
        })
      );

      const requestUri = (responseQueryParams["request_uri"] as string).replace(
        conformanceDomain,
        ""
      );
      const requestJwt = await request(server).get(requestUri);

      const publicKeyAuthService = await importJWK(jwkAuthService);
      const verifyResult = await jwtVerify(
        requestJwt.text,
        publicKeyAuthService
      );

      // 5. call /direct_post
      const idTokenRequestState = verifyResult.payload["state"] as
        | string
        | undefined;
      const idTokenRequestNonce = verifyResult.payload["nonce"] as
        | string
        | undefined;
      const privateKey = await importJWK(client.privateKeyJwk);
      const idTokenDirectPost = await new SignJWT({
        nonce: idTokenRequestNonce,
        sub: testClientMockDid,
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: testClientMockKid,
        })
        .setIssuer(testClientMockDid)
        .setAudience(authorizationServer)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(privateKey);

      const responseDirectPost = await request(server)
        .post(redirectUri)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token: idTokenDirectPost,
            ...(idTokenRequestState && { state: idTokenRequestState }),
          }).toString()
        );

      expect(responseDirectPost.body).toStrictEqual({});
      expect(responseDirectPost.status).toBe(302);

      location = (responseDirectPost.headers as { location: string }).location;

      expect(location).toStrictEqual(
        expect.stringContaining(authenticationRequestParams.redirect_uri)
      );

      const authenticationResponseQueryParams = qs.parse(
        new URL(location).search.substring(1)
      );

      expect(authenticationResponseQueryParams).toStrictEqual(
        expect.objectContaining({
          state: authenticationRequestParams.state,
          code: expect.any(String),
        })
      );

      // 6. call /token
      const queryParams = await createTokenRequestParams(
        client,
        authorizationServer,
        authenticationResponseQueryParams["code"] as string
      );

      const responseToken = await request(server)
        .post(tokenEndpoint)
        .send(new URLSearchParams(queryParams).toString());

      expect(responseToken.status).toBe(200);
      expect(responseToken.body).toStrictEqual({
        access_token: expect.any(String),
        c_nonce: expect.any(String),
        c_nonce_expires_in: 86400,
        expires_in: 86400,
        id_token: expect.any(String),
        token_type: "Bearer",
      });

      const { c_nonce: cNonce, access_token: bearerToken } =
        responseToken.body as TokenResponse;

      /* ISSUER-MOCK */

      // 7. Request the credential
      const proofJwt = await new SignJWT({
        nonce: cNonce,
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
          kid: testClientMockKid,
        })
        .setIssuer(client.clientId)
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(await importJWK(client.privateKeyJwk));

      const responseCredential = await request(server)
        .post(`${apiUrlPrefix}/issuer-mock/credential`)
        .set("Authorization", `Bearer ${bearerToken}`)
        .send({
          types: authorizationDetails[0].types,
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: proofJwt,
          },
        });

      expect(responseCredential.status).toBe(200);
      expect(responseCredential.body).toStrictEqual({
        format: "jwt_vc",
        credential: expect.any(String),
      });

      // 8. Verify credential
      const { credential } = responseCredential.body as CredentialResponse;
      const options: VerifyCredentialOptions = {
        ebsiAuthority,
        ebsiEnvConfig: {
          didRegistry,
          trustedIssuersRegistry,
          trustedPoliciesRegistry,
        },
      };
      const vcPayload = await verifyCredentialJwt(credential, options);

      expect(vcPayload).toStrictEqual({
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: expect.stringContaining("vc:ebsi:conformance#"),
        type: authorizationDetails[0].types,
        issuer: issuerMockDid,
        issuanceDate: expect.any(String),
        issued: expect.any(String),
        validFrom: expect.any(String),
        expirationDate: expect.any(String),
        credentialSubject: {
          id: testClientMockDid,
          reservedAttributeId: expect.any(String),
          ...([
            "VerifiableAccreditationToAttest",
            "VerifiableAccreditationToAccredit",
          ].includes(requestedType) && {
            accreditedFor: [
              {
                schemaId: configService.get<string>(
                  "authorisationCredentialSchema"
                ),
                types: [
                  "VerifiableCredential",
                  "VerifiableAttestation",
                  "CTRevocable",
                ],
                limitJurisdiction:
                  "https://publications.europa.eu/resource/authority/atu/FIN",
              },
            ],
          }),
        },
        credentialSchema: {
          id: configService.get<string>("authorisationCredentialSchema"),
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: configService.get<string>("issuerMockAccreditationUrl"),
          type: "IssuanceCertificate",
        },
      });

      // 9. Check the attribute registered in the TIR
      const attributeId = (
        vcPayload as unknown as {
          credentialSubject: { reservedAttributeId: string };
        }
      ).credentialSubject.reservedAttributeId;
      const responseAttribute = await axios.get(
        `${trustedIssuersRegistry}/${testClientMockDid}/attributes/${attributeId}`,
        { validateStatus: () => true }
      );

      let expectedIssuerType: string;
      let expectedTao: string;
      let expectedRootTao: string;
      switch (requestedType) {
        case "VerifiableAccreditationToAttest": {
          expectedIssuerType = "TI";
          expectedTao = issuerMockDid;
          expectedRootTao = issuerMockDid;
          break;
        }
        case "VerifiableAccreditationToAccredit": {
          expectedIssuerType = "TAO";
          expectedTao = issuerMockDid;
          expectedRootTao = issuerMockDid;
          break;
        }
        default: {
          throw new Error("no case defined for this type");
        }
      }

      expect(responseAttribute.data).toStrictEqual({
        attribute: {
          body: "",
          hash: attributeId.replace("0x", ""),
          issuerType: expectedIssuerType,
          rootTao: expectedRootTao,
          tao: expectedTao,
        },
        did: testClientMockDid,
      });
      expect(responseAttribute.status).toBe(200);
    });
  });
});
