import { URLSearchParams } from "node:url";
import { jest, describe, beforeAll, afterAll, it, expect } from "@jest/globals";
import request from "supertest";
import { Test, TestingModule } from "@nestjs/testing";
import { HttpServer, Logger } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import type { NestFastifyApplication } from "@nestjs/platform-fastify";
import type { FastifyInstance } from "fastify";
import { SignJWT, generateKeyPair, exportJWK } from "jose";
import qs from "qs";
import { util as keyDidHelpers } from "@cef-ebsi/key-did-resolver";
import {
  verifyCredentialJwt,
  VerifyCredentialOptions,
} from "@cef-ebsi/verifiable-credential";
import type {
  CredentialOffer,
  CredentialOfferPayload,
} from "../../src/modules/issuer-mock/issuer-mock.interface";
import { AppModule } from "../../src/app.module";
import type { ApiConfig } from "../../src/config/configuration";
import { getServer } from "../utils/getServer";
import { configureApp } from "../utils/app";
import type {
  TokenResponse,
  OPMetadata,
  PostTokenPreAuthorizedCodeDto,
} from "../../src/shared/auth-server";
import type { CredentialResponse } from "../../src/shared/credential-issuer";
import type { CredentialIssuerMetadata } from "../../src/shared/interfaces";
import { getUserPin } from "../../src/shared/utils";

jest.setTimeout(120_000);

describe("WCT v3 e2e tests: PDA1", () => {
  let app: NestFastifyApplication;
  let server: HttpServer | string;
  let configService: ConfigService<ApiConfig, true>;
  let conformanceDomain: string;
  let apiUrlPrefix: string;
  let issuerMockUri: string;
  let ebsiAuthority: string;
  let didRegistry: string;
  let trustedIssuersRegistry: string;
  let trustedPoliciesRegistry: string;

  function getPathnameWithoutPrefix(uri: string) {
    return new URL(uri).pathname.replace(conformanceDomain, "");
  }

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    configService =
      moduleFixture.get<ConfigService<ApiConfig, true>>(ConfigService);

    conformanceDomain = configService.get<string>("conformanceDomain");
    apiUrlPrefix = configService.get<string>("apiUrlPrefix");
    issuerMockUri = `${conformanceDomain}${apiUrlPrefix}/issuer-mock`;
    didRegistry = configService.get<string>("didRegistryApiUrl");
    trustedIssuersRegistry = configService.get<string>(
      "trustedIssuersRegistryApiUrl"
    );
    trustedPoliciesRegistry = configService.get<string>(
      "trustedPoliciesRegistryApiUrl"
    );
    ebsiAuthority = configService
      .get<string>("domain")
      .replace(/^https?:\/\//, ""); // remove http protocol scheme

    app = await configureApp(moduleFixture, { apiUrlPrefix });

    Logger.overrideLogger(false);

    await app.init();
    await (app.getHttpAdapter().getInstance() as FastifyInstance).ready();

    server = await getServer(app, configService, { conformanceDomain });
  });

  afterAll(async () => {
    // Avoid jest open handle error
    await new Promise<void>((resolve) => {
      setTimeout(() => resolve(), 500);
    });
    await app.close();
  });

  it("should follow the entire Holder Wallet flow", async () => {
    const clientKeyPair = await generateKeyPair("ES256");
    const clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
    const clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
    const methodSpecificIdentifier = clientDid.replace("did:key:", "");
    const clientKid = `${clientDid}#${methodSpecificIdentifier}`;
    const { privateKey: clientPrivateKey } = clientKeyPair;

    // Initiate credential offer from Issuer Mock
    const requestParams = {
      credential_type: "VerifiablePortableDocumentA1",
      credential_offer_endpoint: "openid-credential-offer://",
      client_id: clientDid,
    } as const;

    let response = await request(server).get(
      `${apiUrlPrefix}/issuer-mock/initiate-credential-offer?${new URLSearchParams(
        requestParams
      ).toString()}`
    );

    expect(response.text).toStrictEqual(
      expect.stringContaining(requestParams.credential_offer_endpoint)
    );
    expect(response.status).toBe(200);

    const { search } = new URL(response.text);

    const parsedCredentialOffer = qs.parse(
      search.slice(1)
    ) as unknown as CredentialOffer;

    expect(parsedCredentialOffer).toStrictEqual({
      credential_offer_uri: expect.any(String),
    });

    // Get credential_offer_uri
    response = await request(server)
      .get(
        getPathnameWithoutPrefix(
          parsedCredentialOffer.credential_offer_uri as string
        )
      )
      .send();

    const credentialOfferPayload = response.body as CredentialOfferPayload;

    expect(credentialOfferPayload).toStrictEqual({
      credential_issuer: issuerMockUri,
      credentials: [
        {
          format: "jwt_vc",
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            requestParams.credential_type,
          ],
        },
      ],
      grants: {
        "urn:ietf:params:oauth:grant-type:pre-authorized_code": {
          "pre-authorized_code": expect.any(String),
          user_pin_required: true,
        },
      },
    });

    // GET /issuer-mock/.well-known/openid-credential-issuer
    response = await request(server).get(
      `${apiUrlPrefix}/issuer-mock/.well-known/openid-credential-issuer`
    );

    expect(response.status).toBe(200);
    const credentialIssuerConfig = response.body as CredentialIssuerMetadata;

    // GET AUthorization Server (Auth Mock) /.well-known/openid-configuration
    if (!credentialIssuerConfig.authorization_server)
      throw new Error("authorization_server not defined");
    const authorizationServerUri = getPathnameWithoutPrefix(
      credentialIssuerConfig.authorization_server
    );

    response = await request(server).get(
      `${authorizationServerUri}/.well-known/openid-configuration`
    );

    expect(response.status).toBe(200);
    const authMockConfig = response.body as OPMetadata;

    // POST /auth-mock/token
    const preAuthorizedCode = credentialOfferPayload.grants[
      "urn:ietf:params:oauth:grant-type:pre-authorized_code"
    ]?.["pre-authorized_code"] as string;

    const tokenRequestQueryParams = {
      grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
      "pre-authorized_code": preAuthorizedCode,
      user_pin: getUserPin(clientDid),
    } satisfies PostTokenPreAuthorizedCodeDto;

    response = await request(server)
      .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(new URLSearchParams(tokenRequestQueryParams).toString());

    expect(response.status).toBe(200);
    const { access_token: accessToken, c_nonce: cNonce } =
      response.body as TokenResponse;
    expect(accessToken).toBeDefined();

    // POST /issuer-mock/credential
    const proofJwt = await new SignJWT({
      nonce: cNonce,
    })
      .setProtectedHeader({
        typ: "openid4vci-proof+jwt",
        alg: "ES256",
        kid: clientKid,
      })
      .setIssuer(clientDid)
      .setAudience(issuerMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKey);

    const credentialRequestParams = {
      types: [
        "VerifiableCredential",
        "VerifiableAttestation",
        requestParams.credential_type,
      ],
      format: "jwt_vc",
      proof: {
        proof_type: "jwt",
        jwt: proofJwt,
      },
    };

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          credentialIssuerConfig.credential_endpoint
        )}`
      )
      .set("Authorization", `Bearer ${accessToken}`)
      .send(credentialRequestParams);

    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual({
      acceptance_token: expect.any(String),
    });

    const { acceptance_token: acceptanceToken } = response.body as {
      acceptance_token: string;
    };

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          credentialIssuerConfig.deferred_credential_endpoint
        )}`
      )
      .set("Authorization", `Bearer ${acceptanceToken}`)
      .send();

    expect(response.status).toBe(400);
    expect(response.body).toStrictEqual({
      error: "invalid_request",
      error_description: "Deferred credential not available yet",
    });

    // Wait 5.5 seconds (> 5 seconds)
    await new Promise((r) => {
      setTimeout(r, 5500);
    });

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          credentialIssuerConfig.deferred_credential_endpoint
        )}`
      )
      .set("Authorization", `Bearer ${acceptanceToken}`)
      .send();

    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual({
      format: "jwt_vc",
      credential: expect.any(String),
    });

    // Check VC
    const { credential } = response.body as CredentialResponse;
    const options: VerifyCredentialOptions = {
      ebsiAuthority,
      ebsiEnvConfig: {
        didRegistry,
        trustedIssuersRegistry,
        trustedPoliciesRegistry,
      },
    };
    const vcPayload = await verifyCredentialJwt(credential, options);

    expect(vcPayload).toStrictEqual({
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      id: expect.stringContaining("vc:ebsi:conformance#"),
      type: [
        "VerifiableCredential",
        "VerifiableAttestation",
        requestParams.credential_type,
      ],
      issuer: configService.get<string>("issuerMockKid").split("#")[0],
      issuanceDate: expect.any(String),
      issued: expect.any(String),
      validFrom: expect.any(String),
      expirationDate: expect.any(String),
      credentialSubject: {
        id: clientDid,
        section1: {
          personalIdentificationNumber: "1",
          sex: "01",
          surname: "Dalton",
          forenames: "Joe Jack William Averell",
          dateBirth: "1985-08-15",
          nationalities: ["BE"],
          stateOfResidenceAddress: {
            streetNo: "sss, nnn ",
            postCode: "ppp",
            town: "ccc",
            countryCode: "BE",
          },
          stateOfStayAddress: {
            streetNo: "sss, nnn ",
            postCode: "ppp",
            town: "ccc",
            countryCode: "BE",
          },
        },
        section2: {
          memberStateWhichLegislationApplies: "DE",
          startingDate: "2022-10-09",
          endingDate: "2022-10-29",
          certificateForDurationActivity: true,
          determinationProvisional: false,
          transitionRulesApplyAsEC8832004: false,
        },
        section3: {
          postedEmployedPerson: false,
          employedTwoOrMoreStates: false,
          postedSelfEmployedPerson: true,
          selfEmployedTwoOrMoreStates: true,
          civilServant: true,
          contractStaff: false,
          mariner: false,
          employedAndSelfEmployed: false,
          civilAndEmployedSelfEmployed: true,
          flightCrewMember: false,
          exception: false,
          exceptionDescription: "",
          workingInStateUnder21: false,
        },
        section4: {
          employee: false,
          selfEmployedActivity: true,
          nameBusinessName: "1",
          registeredAddress: {
            streetNo: "1, 1 1",
            postCode: "1",
            town: "1",
            countryCode: "DE",
          },
        },
        section5: {
          noFixedAddress: true,
        },
        section6: {
          name: "National Institute for the Social Security of the Self-employed (NISSE)",
          address: {
            streetNo: "Quai de Willebroeck 35",
            postCode: "1000",
            town: "Bruxelles",
            countryCode: "BE",
          },
          institutionID: "NSSIE/INASTI/RSVZ",
          officeFaxNo: "",
          officePhoneNo: "0800 12 018",
          email: "info@rsvz-inasti.fgov.be",
          date: "2022-10-28",
          signature: "Official signature",
        },
      },
      credentialSchema: {
        id: configService.get<string>("pda1CredentialSchema"),
        type: "FullJsonSchemaValidator2021",
      },
      termsOfUse: {
        id: configService.get<string>("issuerMockAccreditationUrl"),
        type: "IssuanceCertificate",
      },
    });
  });
});
