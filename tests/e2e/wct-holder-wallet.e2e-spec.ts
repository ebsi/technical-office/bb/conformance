import { createHash, randomBytes, randomUUID } from "node:crypto";
import { URLSearchParams } from "node:url";
import { jest, describe, beforeAll, afterAll, it, expect } from "@jest/globals";
import request from "supertest";
import { Test, TestingModule } from "@nestjs/testing";
import { HttpServer, Logger } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import type { NestFastifyApplication } from "@nestjs/platform-fastify";
import type { FastifyInstance } from "fastify";
import { SignJWT, generateKeyPair, exportJWK } from "jose";
import qs from "qs";
import { util as keyDidHelpers } from "@cef-ebsi/key-did-resolver";
import {
  EbsiIssuer,
  verifyCredentialJwt,
  VerifyCredentialOptions,
} from "@cef-ebsi/verifiable-credential";
import { base64url } from "multiformats/bases/base64";
import type { PresentationSubmission } from "@sphereon/pex-models";
import { createVerifiablePresentationJwt } from "@cef-ebsi/verifiable-presentation";
import type {
  CredentialOffer,
  CredentialOfferPayload,
} from "../../src/modules/issuer-mock/issuer-mock.interface";
import { AppModule } from "../../src/app.module";
import type { ApiConfig } from "../../src/config/configuration";
import { getServer } from "../utils/getServer";
import { configureApp } from "../utils/app";
import {
  IdTokenRequest,
  TokenResponse,
  OPMetadata,
  PostTokenPkceDto,
  PostTokenPreAuthorizedCodeDto,
  VpTokenRequest,
  HOLDER_WALLET_QUALIFICATION_PRESENTATION_DEFINITION,
  GetAuthorizeHolderWallerDto,
} from "../../src/shared/auth-server";
import type { CredentialResponse } from "../../src/shared/credential-issuer";
import type { CredentialIssuerMetadata } from "../../src/shared/interfaces";
import {
  CT_WALLET_CROSS_DEFERRED,
  CT_WALLET_CROSS_IN_TIME,
  CT_WALLET_SAME_DEFERRED,
  CT_WALLET_SAME_IN_TIME,
  REQUEST_CT_WALLET_QUALIFICATION_CREDENTIAL,
} from "../../src/shared/constants";
import { getUserPin } from "../../src/shared/utils";

jest.setTimeout(120_000);

describe("WCT v3 e2e tests: Holder Wallet", () => {
  let app: NestFastifyApplication;
  let server: HttpServer | string;
  let configService: ConfigService<ApiConfig, true>;
  let conformanceDomain: string;
  let authMockUri: string;
  let apiUrlPrefix: string;
  let issuerMockUri: string;
  let ebsiAuthority: string;
  let didRegistry: string;
  let trustedIssuersRegistry: string;
  let trustedPoliciesRegistry: string;

  function getPathnameWithoutPrefix(uri: string) {
    return new URL(uri).pathname.replace(conformanceDomain, "");
  }

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    configService =
      moduleFixture.get<ConfigService<ApiConfig, true>>(ConfigService);

    conformanceDomain = configService.get<string>("conformanceDomain");
    apiUrlPrefix = configService.get<string>("apiUrlPrefix");
    authMockUri = `${conformanceDomain}${apiUrlPrefix}/auth-mock`;
    issuerMockUri = `${conformanceDomain}${apiUrlPrefix}/issuer-mock`;
    didRegistry = configService.get<string>("didRegistryApiUrl");
    trustedIssuersRegistry = configService.get<string>(
      "trustedIssuersRegistryApiUrl"
    );
    trustedPoliciesRegistry = configService.get<string>(
      "trustedPoliciesRegistryApiUrl"
    );
    ebsiAuthority = configService
      .get<string>("domain")
      .replace(/^https?:\/\//, ""); // remove http protocol scheme

    app = await configureApp(moduleFixture, { apiUrlPrefix });

    Logger.overrideLogger(false);

    await app.init();
    await (app.getHttpAdapter().getInstance() as FastifyInstance).ready();

    server = await getServer(app, configService, { conformanceDomain });
  });

  afterAll(async () => {
    // Avoid jest open handle error
    await new Promise<void>((resolve) => {
      setTimeout(() => resolve(), 500);
    });
    await app.close();
  });

  it("should follow the entire Holder Wallet flow", async () => {
    const clientKeyPair = await generateKeyPair("ES256");
    const clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
    const clientPrivateKeyJwk = await exportJWK(clientKeyPair.privateKey);
    const clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
    const methodSpecificIdentifier = clientDid.replace("did:key:", "");
    const clientKid = `${clientDid}#${methodSpecificIdentifier}`;
    const { privateKey: clientPrivateKey } = clientKeyPair;
    const credentials: string[] = [];

    // Initiate credential offer from Issuer Mock - CTWalletCrossInTime
    let requestParams = {
      credential_type: "CTWalletCrossInTime",
      credential_offer_endpoint: "openid-credential-offer://",
      client_id: clientDid,
    };

    let response = await request(server).get(
      `${apiUrlPrefix}/issuer-mock/initiate-credential-offer?${new URLSearchParams(
        requestParams
      ).toString()}`
    );

    expect(response.text).toStrictEqual(
      expect.stringContaining(requestParams.credential_offer_endpoint)
    );
    expect(response.status).toBe(200);

    let { search } = new URL(response.text);

    let parsedCredentialOffer = qs.parse(
      search.slice(1)
    ) as unknown as CredentialOffer;

    expect(parsedCredentialOffer).toStrictEqual({
      credential_offer_uri: expect.any(String),
    });

    // Get credential_offer_uri
    response = await request(server)
      .get(
        getPathnameWithoutPrefix(
          parsedCredentialOffer.credential_offer_uri as string
        )
      )
      .send();

    let credentialOfferPayload = response.body as CredentialOfferPayload;

    expect(credentialOfferPayload).toStrictEqual({
      credential_issuer: issuerMockUri,
      credentials: [
        {
          format: "jwt_vc",
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            requestParams.credential_type,
          ],
        },
      ],
      grants: {
        authorization_code: {
          issuer_state: expect.any(String),
        },
      },
    });

    // GET /issuer-mock/.well-known/openid-credential-issuer
    response = await request(server).get(
      `${apiUrlPrefix}/issuer-mock/.well-known/openid-credential-issuer`
    );

    expect(response.status).toBe(200);
    const credentialIssuerConfig = response.body as CredentialIssuerMetadata;

    // GET AUthorization Server (Auth Mock) /.well-known/openid-configuration
    if (!credentialIssuerConfig.authorization_server)
      throw new Error("authorization_server not defined");
    const authorizationServerUri = getPathnameWithoutPrefix(
      credentialIssuerConfig.authorization_server
    );

    response = await request(server).get(
      `${authorizationServerUri}/.well-known/openid-configuration`
    );

    expect(response.status).toBe(200);
    const authMockConfig = response.body as OPMetadata;

    // GET /auth-mock/authorize
    // Code verifier: cryptographically random string using the characters A-Z, a-z, 0-9, and the
    // punctuation characters -._~ (hyphen, period, underscore, and tilde), between 43 and 128
    // characters long.
    // Example: https://github.com/jaredhanson/passport-oauth2/blob/master/lib/strategy.js#L239
    let codeVerifier = randomBytes(50).toString("base64url");
    let codeChallenge = base64url.baseEncode(
      createHash("sha256").update(codeVerifier).digest()
    );

    let authenticationRequestParams = {
      scope: "openid",
      client_id: clientDid,
      response_type: "code",
      redirect_uri: "openid://callback",
      code_challenge: codeChallenge,
      code_challenge_method: "S256",
      state: randomUUID(),
      ...(credentialOfferPayload.grants.authorization_code?.issuer_state && {
        issuer_state:
          credentialOfferPayload.grants.authorization_code.issuer_state,
      }),
      client_metadata: JSON.stringify({
        authorization_endpoint: "openid:",
      }),
      authorization_details: JSON.stringify([
        {
          type: "openid_credential",
          format: "jwt_vc",
          locations: [issuerMockUri],
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            requestParams.credential_type,
          ],
        },
      ]),
    } satisfies GetAuthorizeHolderWallerDto;

    response = await request(server)
      .get(
        `${getPathnameWithoutPrefix(
          authMockConfig.authorization_endpoint
        )}?${new URLSearchParams(authenticationRequestParams).toString()}`
      )
      .send();

    expect(response.status).toBe(302);

    let { location } = response.headers as { location: string };

    let idTokenRequestParams = qs.parse(
      new URL(location).search.substring(1)
    ) as unknown as IdTokenRequest;

    expect(idTokenRequestParams.request).toBeUndefined();
    expect(idTokenRequestParams.request_uri).toBeDefined();

    // POST /auth-mock/direct_post
    let idToken = await new SignJWT({
      nonce: idTokenRequestParams.nonce,
      sub: clientDid,
    })
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: clientKid,
      })
      .setIssuer(clientDid)
      .setAudience(authMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKey);

    response = await request(server)
      .post(`${getPathnameWithoutPrefix(idTokenRequestParams.redirect_uri)}`)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(
        new URLSearchParams({
          id_token: idToken,
          state: idTokenRequestParams.state,
        }).toString()
      );

    expect(response.status).toBe(302);
    location = (response.headers as { location: string }).location;
    expect(location).toStrictEqual(
      expect.stringContaining(authenticationRequestParams.redirect_uri)
    );

    let authenticationResponseQueryParams = qs.parse(
      new URL(location).search.substring(1)
    ) as unknown as { code: string; state: string };

    expect(authenticationResponseQueryParams.state).toBe(
      authenticationRequestParams.state
    );
    expect(authenticationResponseQueryParams.code).toBeTruthy();

    // POST /auth-mock/token
    let { code } = authenticationResponseQueryParams;
    let pkceTokenRequestQueryParams = {
      grant_type: "authorization_code",
      code,
      client_id: clientDid,
      code_verifier: codeVerifier,
    } satisfies PostTokenPkceDto;

    response = await request(server)
      .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(new URLSearchParams(pkceTokenRequestQueryParams).toString());

    expect(response.status).toBe(200);
    let { access_token: accessToken, c_nonce: cNonce } =
      response.body as TokenResponse;
    expect(accessToken).toBeDefined();

    // POST /issuer-mock/credential
    let proofJwt = await new SignJWT({
      nonce: cNonce,
    })
      .setProtectedHeader({
        typ: "openid4vci-proof+jwt",
        alg: "ES256",
        kid: clientKid,
      })
      .setIssuer(clientDid)
      .setAudience(issuerMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKey);

    let credentialRequestParams = {
      types: [
        "VerifiableCredential",
        "VerifiableAttestation",
        requestParams.credential_type,
      ],
      format: "jwt_vc",
      proof: {
        proof_type: "jwt",
        jwt: proofJwt,
      },
    };

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          credentialIssuerConfig.credential_endpoint
        )}`
      )
      .set("Authorization", `Bearer ${accessToken}`)
      .send(credentialRequestParams);

    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual({
      format: "jwt_vc",
      credential: expect.any(String),
    });

    // Check VC
    let { credential } = response.body as CredentialResponse;

    const options: VerifyCredentialOptions = {
      ebsiAuthority,
      ebsiEnvConfig: {
        didRegistry,
        trustedIssuersRegistry,
        trustedPoliciesRegistry,
      },
    };
    let vcPayload = await verifyCredentialJwt(credential, options);

    expect(vcPayload).toStrictEqual({
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      id: expect.stringContaining("vc:ebsi:conformance#"),
      type: [
        "VerifiableCredential",
        "VerifiableAttestation",
        requestParams.credential_type,
      ],
      issuer: configService.get<string>("issuerMockKid").split("#")[0],
      issuanceDate: expect.any(String),
      issued: expect.any(String),
      validFrom: expect.any(String),
      expirationDate: expect.any(String),
      credentialSubject: {
        id: clientDid,
      },
      credentialSchema: {
        id: configService.get<string>("authorisationCredentialSchema"),
        type: "FullJsonSchemaValidator2021",
      },
      termsOfUse: {
        id: configService.get<string>("issuerMockAccreditationUrl"),
        type: "IssuanceCertificate",
      },
    });

    // Check if the test is completed
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: CT_WALLET_CROSS_IN_TIME,
        data: {
          did: clientDid,
        },
      });

    expect(response.body).toStrictEqual({
      success: true,
    });

    // Store VC
    credentials.push(credential);

    // Initiate credential offer from Issuer Mock - CTWalletCrossDeferred
    requestParams = {
      credential_type: "CTWalletCrossDeferred",
      credential_offer_endpoint: "openid-credential-offer://",
      client_id: clientDid,
    };

    response = await request(server).get(
      `${apiUrlPrefix}/issuer-mock/initiate-credential-offer?${new URLSearchParams(
        requestParams
      ).toString()}`
    );

    expect(response.text).toStrictEqual(
      expect.stringContaining(requestParams.credential_offer_endpoint)
    );
    expect(response.status).toBe(200);

    search = new URL(response.text).search;

    parsedCredentialOffer = qs.parse(
      search.slice(1)
    ) as unknown as CredentialOffer;

    expect(parsedCredentialOffer).toStrictEqual({
      credential_offer_uri: expect.any(String),
    });

    // Get credential_offer_uri
    response = await request(server)
      .get(
        getPathnameWithoutPrefix(
          parsedCredentialOffer.credential_offer_uri as string
        )
      )
      .send();

    credentialOfferPayload = response.body as CredentialOfferPayload;

    expect(credentialOfferPayload).toStrictEqual({
      credential_issuer: issuerMockUri,
      credentials: [
        {
          format: "jwt_vc",
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            requestParams.credential_type,
          ],
        },
      ],
      grants: {
        authorization_code: {
          issuer_state: expect.any(String),
        },
      },
    });

    // GET /auth-mock/authorize
    // Code verifier: cryptographically random string using the characters A-Z, a-z, 0-9, and the
    // punctuation characters -._~ (hyphen, period, underscore, and tilde), between 43 and 128
    // characters long.
    // Example: https://github.com/jaredhanson/passport-oauth2/blob/master/lib/strategy.js#L239
    codeVerifier = randomBytes(50).toString("base64url");
    codeChallenge = base64url.baseEncode(
      createHash("sha256").update(codeVerifier).digest()
    );

    authenticationRequestParams = {
      scope: "openid",
      client_id: clientDid,
      response_type: "code",
      redirect_uri: "openid://callback",
      code_challenge: codeChallenge,
      code_challenge_method: "S256",
      state: randomUUID(),
      ...(credentialOfferPayload.grants.authorization_code?.issuer_state && {
        issuer_state:
          credentialOfferPayload.grants.authorization_code.issuer_state,
      }),
      client_metadata: JSON.stringify({
        authorization_endpoint: "openid:",
      }),
      authorization_details: JSON.stringify([
        {
          type: "openid_credential",
          format: "jwt_vc",
          locations: [issuerMockUri],
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            requestParams.credential_type,
          ],
        },
      ]),
    } satisfies GetAuthorizeHolderWallerDto;

    response = await request(server)
      .get(
        `${getPathnameWithoutPrefix(
          authMockConfig.authorization_endpoint
        )}?${new URLSearchParams(authenticationRequestParams).toString()}`
      )
      .send();

    expect(response.status).toBe(302);

    location = (response.headers as { location: string }).location;

    idTokenRequestParams = qs.parse(
      new URL(location).search.substring(1)
    ) as unknown as IdTokenRequest;

    expect(idTokenRequestParams.request).toBeUndefined();
    expect(idTokenRequestParams.request_uri).toBeDefined();

    // POST /auth-mock/direct_post
    idToken = await new SignJWT({
      nonce: idTokenRequestParams.nonce,
      sub: clientDid,
    })
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: clientKid,
      })
      .setIssuer(clientDid)
      .setAudience(authMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKey);

    response = await request(server)
      .post(`${getPathnameWithoutPrefix(idTokenRequestParams.redirect_uri)}`)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(
        new URLSearchParams({
          id_token: idToken,
          state: idTokenRequestParams.state,
        }).toString()
      );

    expect(response.status).toBe(302);
    location = (response.headers as { location: string }).location;
    expect(location).toStrictEqual(
      expect.stringContaining(authenticationRequestParams.redirect_uri)
    );

    authenticationResponseQueryParams = qs.parse(
      new URL(location).search.substring(1)
    ) as unknown as { code: string; state: string };

    expect(authenticationResponseQueryParams.state).toBe(
      authenticationRequestParams.state
    );
    expect(authenticationResponseQueryParams.code).toBeTruthy();

    // POST /auth-mock/token
    code = authenticationResponseQueryParams.code;
    pkceTokenRequestQueryParams = {
      grant_type: "authorization_code",
      code,
      client_id: clientDid,
      code_verifier: codeVerifier,
    } satisfies PostTokenPkceDto;

    response = await request(server)
      .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(new URLSearchParams(pkceTokenRequestQueryParams).toString());

    expect(response.status).toBe(200);
    accessToken = (response.body as TokenResponse).access_token;
    cNonce = (response.body as TokenResponse).c_nonce;
    expect(accessToken).toBeDefined();

    // POST /issuer-mock/credential
    proofJwt = await new SignJWT({
      nonce: cNonce,
    })
      .setProtectedHeader({
        typ: "openid4vci-proof+jwt",
        alg: "ES256",
        kid: clientKid,
      })
      .setIssuer(clientDid)
      .setAudience(issuerMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKey);

    credentialRequestParams = {
      types: [
        "VerifiableCredential",
        "VerifiableAttestation",
        requestParams.credential_type,
      ],
      format: "jwt_vc",
      proof: {
        proof_type: "jwt",
        jwt: proofJwt,
      },
    };

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          credentialIssuerConfig.credential_endpoint
        )}`
      )
      .set("Authorization", `Bearer ${accessToken}`)
      .send(credentialRequestParams);

    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual({
      acceptance_token: expect.any(String),
    });

    let { acceptance_token: acceptanceToken } = response.body as {
      acceptance_token: string;
    };

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          credentialIssuerConfig.deferred_credential_endpoint
        )}`
      )
      .set("Authorization", `Bearer ${acceptanceToken}`)
      .send();

    expect(response.status).toBe(400);
    expect(response.body).toStrictEqual({
      error: "invalid_request",
      error_description: "Deferred credential not available yet",
    });

    // Wait 5.5 seconds (> 5 seconds)
    await new Promise((r) => {
      setTimeout(r, 5500);
    });

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          credentialIssuerConfig.deferred_credential_endpoint
        )}`
      )
      .set("Authorization", `Bearer ${acceptanceToken}`)
      .send();

    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual({
      format: "jwt_vc",
      credential: expect.any(String),
    });

    // Check VC
    credential = (response.body as CredentialResponse).credential;
    vcPayload = await verifyCredentialJwt(credential, options);

    expect(vcPayload).toStrictEqual({
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      id: expect.stringContaining("vc:ebsi:conformance#"),
      type: [
        "VerifiableCredential",
        "VerifiableAttestation",
        requestParams.credential_type,
      ],
      issuer: configService.get<string>("issuerMockKid").split("#")[0],
      issuanceDate: expect.any(String),
      issued: expect.any(String),
      validFrom: expect.any(String),
      expirationDate: expect.any(String),
      credentialSubject: {
        id: clientDid,
      },
      credentialSchema: {
        id: configService.get<string>("authorisationCredentialSchema"),
        type: "FullJsonSchemaValidator2021",
      },
      termsOfUse: {
        id: configService.get<string>("issuerMockAccreditationUrl"),
        type: "IssuanceCertificate",
      },
    });

    // Check if the test is completed
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: CT_WALLET_CROSS_DEFERRED,
        data: {
          did: clientDid,
        },
      });

    expect(response.body).toStrictEqual({
      success: true,
    });

    // Store VC
    credentials.push(credential);

    // Initiate credential offer from Issuer Mock - CTWalletCrossPreAuthorised
    requestParams = {
      credential_type: "CTWalletCrossPreAuthorised",
      credential_offer_endpoint: "openid-credential-offer://",
      client_id: clientDid,
    };

    response = await request(server).get(
      `${apiUrlPrefix}/issuer-mock/initiate-credential-offer?${new URLSearchParams(
        requestParams
      ).toString()}`
    );

    expect(response.text).toStrictEqual(
      expect.stringContaining(requestParams.credential_offer_endpoint)
    );
    expect(response.status).toBe(200);

    search = new URL(response.text).search;

    parsedCredentialOffer = qs.parse(
      search.slice(1)
    ) as unknown as CredentialOffer;

    expect(parsedCredentialOffer).toStrictEqual({
      credential_offer_uri: expect.any(String),
    });

    // Get credential_offer_uri
    response = await request(server)
      .get(
        getPathnameWithoutPrefix(
          parsedCredentialOffer.credential_offer_uri as string
        )
      )
      .send();

    credentialOfferPayload = response.body as CredentialOfferPayload;

    expect(credentialOfferPayload).toStrictEqual({
      credential_issuer: issuerMockUri,
      credentials: [
        {
          format: "jwt_vc",
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            requestParams.credential_type,
          ],
        },
      ],
      grants: {
        "urn:ietf:params:oauth:grant-type:pre-authorized_code": {
          "pre-authorized_code": expect.any(String),
          user_pin_required: true,
        },
      },
    });

    // POST /auth-mock/token
    let preAuthorizedCode = credentialOfferPayload.grants[
      "urn:ietf:params:oauth:grant-type:pre-authorized_code"
    ]?.["pre-authorized_code"] as string;

    let preAuthorizedTokenRequestQueryParams = {
      grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
      "pre-authorized_code": preAuthorizedCode,
      user_pin: getUserPin(clientDid),
    } satisfies PostTokenPreAuthorizedCodeDto;

    response = await request(server)
      .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(
        new URLSearchParams(preAuthorizedTokenRequestQueryParams).toString()
      );

    expect(response.status).toBe(200);
    accessToken = (response.body as TokenResponse).access_token;
    cNonce = (response.body as TokenResponse).c_nonce;
    expect(accessToken).toBeDefined();

    // POST /issuer-mock/credential
    proofJwt = await new SignJWT({
      nonce: cNonce,
    })
      .setProtectedHeader({
        typ: "openid4vci-proof+jwt",
        alg: "ES256",
        kid: clientKid,
      })
      .setIssuer(clientDid)
      .setAudience(issuerMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKey);

    credentialRequestParams = {
      types: [
        "VerifiableCredential",
        "VerifiableAttestation",
        requestParams.credential_type,
      ],
      format: "jwt_vc",
      proof: {
        proof_type: "jwt",
        jwt: proofJwt,
      },
    };

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          credentialIssuerConfig.credential_endpoint
        )}`
      )
      .set("Authorization", `Bearer ${accessToken}`)
      .send(credentialRequestParams);

    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual({
      format: "jwt_vc",
      credential: expect.any(String),
    });

    // Check VC
    credential = (response.body as CredentialResponse).credential;
    vcPayload = await verifyCredentialJwt(credential, options);

    expect(vcPayload).toStrictEqual({
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      id: expect.stringContaining("vc:ebsi:conformance#"),
      type: [
        "VerifiableCredential",
        "VerifiableAttestation",
        requestParams.credential_type,
      ],
      issuer: configService.get<string>("issuerMockKid").split("#")[0],
      issuanceDate: expect.any(String),
      issued: expect.any(String),
      validFrom: expect.any(String),
      expirationDate: expect.any(String),
      credentialSubject: {
        id: clientDid,
      },
      credentialSchema: {
        id: configService.get<string>("authorisationCredentialSchema"),
        type: "FullJsonSchemaValidator2021",
      },
      termsOfUse: {
        id: configService.get<string>("issuerMockAccreditationUrl"),
        type: "IssuanceCertificate",
      },
    });

    // Check if the test is completed
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: CT_WALLET_CROSS_DEFERRED,
        data: {
          did: clientDid,
        },
      });

    expect(response.body).toStrictEqual({
      success: true,
    });

    // Store VC
    credentials.push(credential);

    // Initiate credential offer from Issuer Mock - CTWalletSameInTime
    requestParams = {
      credential_type: "CTWalletSameInTime",
      credential_offer_endpoint: "openid-credential-offer://",
      client_id: clientDid,
    };

    response = await request(server).get(
      `${apiUrlPrefix}/issuer-mock/initiate-credential-offer?${new URLSearchParams(
        requestParams
      ).toString()}`
    );

    expect(response.status).toBe(302);
    location = (response.headers as { location: string }).location;
    expect(location).toStrictEqual(
      expect.stringContaining(requestParams.credential_offer_endpoint)
    );

    search = new URL(location).search;

    parsedCredentialOffer = qs.parse(
      search.slice(1)
    ) as unknown as CredentialOffer;

    expect(parsedCredentialOffer).toStrictEqual({
      credential_offer_uri: expect.any(String),
    });

    // Get credential_offer_uri
    response = await request(server)
      .get(
        getPathnameWithoutPrefix(
          parsedCredentialOffer.credential_offer_uri as string
        )
      )
      .send();

    credentialOfferPayload = response.body as CredentialOfferPayload;

    expect(credentialOfferPayload).toStrictEqual({
      credential_issuer: issuerMockUri,
      credentials: [
        {
          format: "jwt_vc",
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            requestParams.credential_type,
          ],
        },
      ],
      grants: {
        authorization_code: {
          issuer_state: expect.any(String),
        },
      },
    });

    // GET /auth-mock/authorize
    // Code verifier: cryptographically random string using the characters A-Z, a-z, 0-9, and the
    // punctuation characters -._~ (hyphen, period, underscore, and tilde), between 43 and 128
    // characters long.
    // Example: https://github.com/jaredhanson/passport-oauth2/blob/master/lib/strategy.js#L239
    codeVerifier = randomBytes(50).toString("base64url");
    codeChallenge = base64url.baseEncode(
      createHash("sha256").update(codeVerifier).digest()
    );

    authenticationRequestParams = {
      scope: "openid",
      client_id: clientDid,
      response_type: "code",
      redirect_uri: "openid://callback",
      code_challenge: codeChallenge,
      code_challenge_method: "S256",
      state: randomUUID(),
      ...(credentialOfferPayload.grants.authorization_code?.issuer_state && {
        issuer_state:
          credentialOfferPayload.grants.authorization_code.issuer_state,
      }),
      client_metadata: JSON.stringify({
        authorization_endpoint: "openid:",
      }),
      authorization_details: JSON.stringify([
        {
          type: "openid_credential",
          format: "jwt_vc",
          locations: [issuerMockUri],
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            requestParams.credential_type,
          ],
        },
      ]),
    } satisfies GetAuthorizeHolderWallerDto;

    response = await request(server)
      .get(
        `${getPathnameWithoutPrefix(
          authMockConfig.authorization_endpoint
        )}?${new URLSearchParams(authenticationRequestParams).toString()}`
      )
      .send();

    expect(response.status).toBe(302);

    location = (response.headers as { location: string }).location;

    idTokenRequestParams = qs.parse(
      new URL(location).search.substring(1)
    ) as unknown as IdTokenRequest;

    expect(idTokenRequestParams.request).toBeUndefined();
    expect(idTokenRequestParams.request_uri).toBeDefined();

    // POST /auth-mock/direct_post
    idToken = await new SignJWT({
      nonce: idTokenRequestParams.nonce,
      sub: clientDid,
    })
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: clientKid,
      })
      .setIssuer(clientDid)
      .setAudience(authMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKey);

    response = await request(server)
      .post(`${getPathnameWithoutPrefix(idTokenRequestParams.redirect_uri)}`)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(
        new URLSearchParams({
          id_token: idToken,
          state: idTokenRequestParams.state,
        }).toString()
      );

    expect(response.status).toBe(302);
    location = (response.headers as { location: string }).location;
    expect(location).toStrictEqual(
      expect.stringContaining(authenticationRequestParams.redirect_uri)
    );

    authenticationResponseQueryParams = qs.parse(
      new URL(location).search.substring(1)
    ) as unknown as { code: string; state: string };

    expect(authenticationResponseQueryParams.state).toBe(
      authenticationRequestParams.state
    );
    expect(authenticationResponseQueryParams.code).toBeTruthy();

    // POST /auth-mock/token
    code = authenticationResponseQueryParams.code;
    pkceTokenRequestQueryParams = {
      grant_type: "authorization_code",
      code,
      client_id: clientDid,
      code_verifier: codeVerifier,
    } satisfies PostTokenPkceDto;

    response = await request(server)
      .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(new URLSearchParams(pkceTokenRequestQueryParams).toString());

    expect(response.status).toBe(200);
    accessToken = (response.body as TokenResponse).access_token;
    cNonce = (response.body as TokenResponse).c_nonce;
    expect(accessToken).toBeDefined();

    // POST /issuer-mock/credential
    proofJwt = await new SignJWT({
      nonce: cNonce,
    })
      .setProtectedHeader({
        typ: "openid4vci-proof+jwt",
        alg: "ES256",
        kid: clientKid,
      })
      .setIssuer(clientDid)
      .setAudience(issuerMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKey);

    credentialRequestParams = {
      types: [
        "VerifiableCredential",
        "VerifiableAttestation",
        requestParams.credential_type,
      ],
      format: "jwt_vc",
      proof: {
        proof_type: "jwt",
        jwt: proofJwt,
      },
    };

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          credentialIssuerConfig.credential_endpoint
        )}`
      )
      .set("Authorization", `Bearer ${accessToken}`)
      .send(credentialRequestParams);

    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual({
      format: "jwt_vc",
      credential: expect.any(String),
    });

    // Check VC
    credential = (response.body as CredentialResponse).credential;
    vcPayload = await verifyCredentialJwt(credential, options);

    expect(vcPayload).toStrictEqual({
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      id: expect.stringContaining("vc:ebsi:conformance#"),
      type: [
        "VerifiableCredential",
        "VerifiableAttestation",
        requestParams.credential_type,
      ],
      issuer: configService.get<string>("issuerMockKid").split("#")[0],
      issuanceDate: expect.any(String),
      issued: expect.any(String),
      validFrom: expect.any(String),
      expirationDate: expect.any(String),
      credentialSubject: {
        id: clientDid,
      },
      credentialSchema: {
        id: configService.get<string>("authorisationCredentialSchema"),
        type: "FullJsonSchemaValidator2021",
      },
      termsOfUse: {
        id: configService.get<string>("issuerMockAccreditationUrl"),
        type: "IssuanceCertificate",
      },
    });

    // Check if the test is completed
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: CT_WALLET_SAME_IN_TIME,
        data: {
          did: clientDid,
        },
      });

    expect(response.body).toStrictEqual({
      success: true,
    });

    // Store VC
    credentials.push(credential);

    // Initiate credential offer from Issuer Mock - CTWalletSameDeferred
    requestParams = {
      credential_type: "CTWalletSameDeferred",
      credential_offer_endpoint: "openid-credential-offer://",
      client_id: clientDid,
    };

    response = await request(server).get(
      `${apiUrlPrefix}/issuer-mock/initiate-credential-offer?${new URLSearchParams(
        requestParams
      ).toString()}`
    );

    expect(response.status).toBe(302);
    location = (response.headers as { location: string }).location;
    expect(location).toStrictEqual(
      expect.stringContaining(requestParams.credential_offer_endpoint)
    );

    search = new URL(location).search;

    parsedCredentialOffer = qs.parse(
      search.slice(1)
    ) as unknown as CredentialOffer;

    expect(parsedCredentialOffer).toStrictEqual({
      credential_offer_uri: expect.any(String),
    });

    // Get credential_offer_uri
    response = await request(server)
      .get(
        getPathnameWithoutPrefix(
          parsedCredentialOffer.credential_offer_uri as string
        )
      )
      .send();

    credentialOfferPayload = response.body as CredentialOfferPayload;

    expect(credentialOfferPayload).toStrictEqual({
      credential_issuer: issuerMockUri,
      credentials: [
        {
          format: "jwt_vc",
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            requestParams.credential_type,
          ],
        },
      ],
      grants: {
        authorization_code: {
          issuer_state: expect.any(String),
        },
      },
    });

    // GET /auth-mock/authorize
    // Code verifier: cryptographically random string using the characters A-Z, a-z, 0-9, and the
    // punctuation characters -._~ (hyphen, period, underscore, and tilde), between 43 and 128
    // characters long.
    // Example: https://github.com/jaredhanson/passport-oauth2/blob/master/lib/strategy.js#L239
    codeVerifier = randomBytes(50).toString("base64url");
    codeChallenge = base64url.baseEncode(
      createHash("sha256").update(codeVerifier).digest()
    );

    authenticationRequestParams = {
      scope: "openid",
      client_id: clientDid,
      response_type: "code",
      redirect_uri: "openid://callback",
      code_challenge: codeChallenge,
      code_challenge_method: "S256",
      state: randomUUID(),
      ...(credentialOfferPayload.grants.authorization_code?.issuer_state && {
        issuer_state:
          credentialOfferPayload.grants.authorization_code.issuer_state,
      }),
      client_metadata: JSON.stringify({
        authorization_endpoint: "openid:",
      }),
      authorization_details: JSON.stringify([
        {
          type: "openid_credential",
          format: "jwt_vc",
          locations: [issuerMockUri],
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            requestParams.credential_type,
          ],
        },
      ]),
    } satisfies GetAuthorizeHolderWallerDto;

    response = await request(server)
      .get(
        `${getPathnameWithoutPrefix(
          authMockConfig.authorization_endpoint
        )}?${new URLSearchParams(authenticationRequestParams).toString()}`
      )
      .send();

    expect(response.status).toBe(302);

    location = (response.headers as { location: string }).location;

    idTokenRequestParams = qs.parse(
      new URL(location).search.substring(1)
    ) as unknown as IdTokenRequest;

    expect(idTokenRequestParams.request).toBeUndefined();
    expect(idTokenRequestParams.request_uri).toBeDefined();

    // POST /auth-mock/direct_post
    idToken = await new SignJWT({
      nonce: idTokenRequestParams.nonce,
      sub: clientDid,
    })
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: clientKid,
      })
      .setIssuer(clientDid)
      .setAudience(authMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKey);

    response = await request(server)
      .post(`${getPathnameWithoutPrefix(idTokenRequestParams.redirect_uri)}`)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(
        new URLSearchParams({
          id_token: idToken,
          state: idTokenRequestParams.state,
        }).toString()
      );

    expect(response.status).toBe(302);
    location = (response.headers as { location: string }).location;
    expect(location).toStrictEqual(
      expect.stringContaining(authenticationRequestParams.redirect_uri)
    );

    authenticationResponseQueryParams = qs.parse(
      new URL(location).search.substring(1)
    ) as unknown as { code: string; state: string };

    expect(authenticationResponseQueryParams.state).toBe(
      authenticationRequestParams.state
    );
    expect(authenticationResponseQueryParams.code).toBeTruthy();

    // POST /auth-mock/token
    code = authenticationResponseQueryParams.code;
    pkceTokenRequestQueryParams = {
      grant_type: "authorization_code",
      code,
      client_id: clientDid,
      code_verifier: codeVerifier,
    } satisfies PostTokenPkceDto;

    response = await request(server)
      .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(new URLSearchParams(pkceTokenRequestQueryParams).toString());

    expect(response.status).toBe(200);
    accessToken = (response.body as TokenResponse).access_token;
    cNonce = (response.body as TokenResponse).c_nonce;
    expect(accessToken).toBeDefined();

    // POST /issuer-mock/credential
    proofJwt = await new SignJWT({
      nonce: cNonce,
    })
      .setProtectedHeader({
        typ: "openid4vci-proof+jwt",
        alg: "ES256",
        kid: clientKid,
      })
      .setIssuer(clientDid)
      .setAudience(issuerMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKey);

    credentialRequestParams = {
      types: [
        "VerifiableCredential",
        "VerifiableAttestation",
        requestParams.credential_type,
      ],
      format: "jwt_vc",
      proof: {
        proof_type: "jwt",
        jwt: proofJwt,
      },
    };

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          credentialIssuerConfig.credential_endpoint
        )}`
      )
      .set("Authorization", `Bearer ${accessToken}`)
      .send(credentialRequestParams);

    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual({
      acceptance_token: expect.any(String),
    });

    acceptanceToken = (
      response.body as {
        acceptance_token: string;
      }
    ).acceptance_token;

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          credentialIssuerConfig.deferred_credential_endpoint
        )}`
      )
      .set("Authorization", `Bearer ${acceptanceToken}`)
      .send();

    expect(response.status).toBe(400);
    expect(response.body).toStrictEqual({
      error: "invalid_request",
      error_description: "Deferred credential not available yet",
    });

    // Wait 5.5 seconds (> 5 seconds)
    await new Promise((r) => {
      setTimeout(r, 5500);
    });

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          credentialIssuerConfig.deferred_credential_endpoint
        )}`
      )
      .set("Authorization", `Bearer ${acceptanceToken}`)
      .send();

    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual({
      format: "jwt_vc",
      credential: expect.any(String),
    });

    // Check VC
    credential = (response.body as CredentialResponse).credential;
    vcPayload = await verifyCredentialJwt(credential, options);

    expect(vcPayload).toStrictEqual({
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      id: expect.stringContaining("vc:ebsi:conformance#"),
      type: [
        "VerifiableCredential",
        "VerifiableAttestation",
        requestParams.credential_type,
      ],
      issuer: configService.get<string>("issuerMockKid").split("#")[0],
      issuanceDate: expect.any(String),
      issued: expect.any(String),
      validFrom: expect.any(String),
      expirationDate: expect.any(String),
      credentialSubject: {
        id: clientDid,
      },
      credentialSchema: {
        id: configService.get<string>("authorisationCredentialSchema"),
        type: "FullJsonSchemaValidator2021",
      },
      termsOfUse: {
        id: configService.get<string>("issuerMockAccreditationUrl"),
        type: "IssuanceCertificate",
      },
    });

    // Check if the test is completed
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: CT_WALLET_SAME_DEFERRED,
        data: {
          did: clientDid,
        },
      });

    expect(response.body).toStrictEqual({
      success: true,
    });

    // Store VC
    credentials.push(credential);

    // Initiate credential offer from Issuer Mock - CTWalletSamePreAuthorised
    requestParams = {
      credential_type: "CTWalletSamePreAuthorised",
      credential_offer_endpoint: "openid-credential-offer://",
      client_id: clientDid,
    };

    response = await request(server).get(
      `${apiUrlPrefix}/issuer-mock/initiate-credential-offer?${new URLSearchParams(
        requestParams
      ).toString()}`
    );

    expect(response.status).toBe(302);
    location = (response.headers as { location: string }).location;
    expect(location).toStrictEqual(
      expect.stringContaining(requestParams.credential_offer_endpoint)
    );

    search = new URL(location).search;

    parsedCredentialOffer = qs.parse(
      search.slice(1)
    ) as unknown as CredentialOffer;

    expect(parsedCredentialOffer).toStrictEqual({
      credential_offer_uri: expect.any(String),
    });

    // Get credential_offer_uri
    response = await request(server)
      .get(
        getPathnameWithoutPrefix(
          parsedCredentialOffer.credential_offer_uri as string
        )
      )
      .send();

    credentialOfferPayload = response.body as CredentialOfferPayload;

    expect(credentialOfferPayload).toStrictEqual({
      credential_issuer: issuerMockUri,
      credentials: [
        {
          format: "jwt_vc",
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            requestParams.credential_type,
          ],
        },
      ],
      grants: {
        "urn:ietf:params:oauth:grant-type:pre-authorized_code": {
          "pre-authorized_code": expect.any(String),
          user_pin_required: true,
        },
      },
    });

    // POST /auth-mock/token
    preAuthorizedCode = credentialOfferPayload.grants[
      "urn:ietf:params:oauth:grant-type:pre-authorized_code"
    ]?.["pre-authorized_code"] as string;

    preAuthorizedTokenRequestQueryParams = {
      grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
      "pre-authorized_code": preAuthorizedCode,
      user_pin: getUserPin(clientDid),
    } satisfies PostTokenPreAuthorizedCodeDto;

    response = await request(server)
      .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(
        new URLSearchParams(preAuthorizedTokenRequestQueryParams).toString()
      );

    expect(response.status).toBe(200);
    accessToken = (response.body as TokenResponse).access_token;
    cNonce = (response.body as TokenResponse).c_nonce;
    expect(accessToken).toBeDefined();

    // POST /issuer-mock/credential
    proofJwt = await new SignJWT({
      nonce: cNonce,
    })
      .setProtectedHeader({
        typ: "openid4vci-proof+jwt",
        alg: "ES256",
        kid: clientKid,
      })
      .setIssuer(clientDid)
      .setAudience(issuerMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKey);

    credentialRequestParams = {
      types: [
        "VerifiableCredential",
        "VerifiableAttestation",
        requestParams.credential_type,
      ],
      format: "jwt_vc",
      proof: {
        proof_type: "jwt",
        jwt: proofJwt,
      },
    };

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          credentialIssuerConfig.credential_endpoint
        )}`
      )
      .set("Authorization", `Bearer ${accessToken}`)
      .send(credentialRequestParams);

    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual({
      format: "jwt_vc",
      credential: expect.any(String),
    });

    // Check VC
    credential = (response.body as CredentialResponse).credential;
    vcPayload = await verifyCredentialJwt(credential, options);

    expect(vcPayload).toStrictEqual({
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      id: expect.stringContaining("vc:ebsi:conformance#"),
      type: [
        "VerifiableCredential",
        "VerifiableAttestation",
        requestParams.credential_type,
      ],
      issuer: configService.get<string>("issuerMockKid").split("#")[0],
      issuanceDate: expect.any(String),
      issued: expect.any(String),
      validFrom: expect.any(String),
      expirationDate: expect.any(String),
      credentialSubject: {
        id: clientDid,
      },
      credentialSchema: {
        id: configService.get<string>("authorisationCredentialSchema"),
        type: "FullJsonSchemaValidator2021",
      },
      termsOfUse: {
        id: configService.get<string>("issuerMockAccreditationUrl"),
        type: "IssuanceCertificate",
      },
    });

    // Check if the test is completed
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: CT_WALLET_CROSS_DEFERRED,
        data: {
          did: clientDid,
        },
      });

    expect(response.body).toStrictEqual({
      success: true,
    });

    // Store VC
    credentials.push(credential);

    // Initiate credential offer from Issuer Mock - CTWalletQualificationCredential
    requestParams = {
      credential_type: "CTWalletQualificationCredential",
      credential_offer_endpoint: "openid-credential-offer://",
      client_id: clientDid,
    };

    response = await request(server).get(
      `${apiUrlPrefix}/issuer-mock/initiate-credential-offer?${new URLSearchParams(
        requestParams
      ).toString()}`
    );

    expect(response.text).toStrictEqual(
      expect.stringContaining(requestParams.credential_offer_endpoint)
    );
    expect(response.status).toBe(200);

    search = new URL(response.text).search;

    parsedCredentialOffer = qs.parse(
      search.slice(1)
    ) as unknown as CredentialOffer;

    expect(parsedCredentialOffer).toStrictEqual({
      credential_offer_uri: expect.any(String),
    });

    // Get credential_offer_uri
    response = await request(server)
      .get(
        getPathnameWithoutPrefix(
          parsedCredentialOffer.credential_offer_uri as string
        )
      )
      .send();

    credentialOfferPayload = response.body as CredentialOfferPayload;

    expect(credentialOfferPayload).toStrictEqual({
      credential_issuer: issuerMockUri,
      credentials: [
        {
          format: "jwt_vc",
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            requestParams.credential_type,
          ],
        },
      ],
      grants: {
        authorization_code: {
          issuer_state: expect.any(String),
        },
      },
    });

    // GET /auth-mock/authorize
    // Code verifier: cryptographically random string using the characters A-Z, a-z, 0-9, and the
    // punctuation characters -._~ (hyphen, period, underscore, and tilde), between 43 and 128
    // characters long.
    // Example: https://github.com/jaredhanson/passport-oauth2/blob/master/lib/strategy.js#L239
    codeVerifier = randomBytes(50).toString("base64url");
    codeChallenge = base64url.baseEncode(
      createHash("sha256").update(codeVerifier).digest()
    );

    authenticationRequestParams = {
      scope: "openid",
      client_id: clientDid,
      response_type: "code",
      redirect_uri: "openid://callback",
      code_challenge: codeChallenge,
      code_challenge_method: "S256",
      state: randomUUID(),
      ...(credentialOfferPayload.grants.authorization_code?.issuer_state && {
        issuer_state:
          credentialOfferPayload.grants.authorization_code.issuer_state,
      }),
      client_metadata: JSON.stringify({
        authorization_endpoint: "openid:",
      }),
      authorization_details: JSON.stringify([
        {
          type: "openid_credential",
          format: "jwt_vc",
          locations: [issuerMockUri],
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            requestParams.credential_type,
          ],
        },
      ]),
    } satisfies GetAuthorizeHolderWallerDto;

    response = await request(server)
      .get(
        `${getPathnameWithoutPrefix(
          authMockConfig.authorization_endpoint
        )}?${new URLSearchParams(authenticationRequestParams).toString()}`
      )
      .send();

    expect(response.status).toBe(302);

    location = (response.headers as { location: string }).location;

    const vpTokenRequestParams = qs.parse(
      new URL(location).search.substring(1)
    ) as unknown as VpTokenRequest;

    expect(vpTokenRequestParams.request).toBeDefined();
    expect(vpTokenRequestParams.request_uri).toBeUndefined();

    // POST /auth-mock/direct_post
    const vpPayload = {
      id: `urn:did:${randomUUID()}`,
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      type: ["VerifiablePresentation"],
      holder: clientDid,
      verifiableCredential: credentials,
    };

    const credentialSubject: EbsiIssuer = {
      did: clientDid,
      kid: clientKid,
      privateKeyJwk: clientPrivateKeyJwk,
      publicKeyJwk: clientPublicKeyJwk,
      alg: "ES256",
    };

    const vpToken = await createVerifiablePresentationJwt(
      vpPayload,
      credentialSubject,
      authMockUri,
      {
        ebsiAuthority: "example.net",
        skipValidation: true,
        nonce: randomUUID(),
        exp: Math.floor(Date.now() / 1000) + 100,
        nbf: Math.floor(Date.now() / 1000) - 100,
      }
    );

    const presentationSubmission: PresentationSubmission = {
      id: randomUUID(),
      definition_id: HOLDER_WALLET_QUALIFICATION_PRESENTATION_DEFINITION.id,
      descriptor_map: [
        {
          id: "same-device-in-time-credential",
          path: "$",
          format: "jwt_vp",
          path_nested: {
            id: "same-device-in-time-credential",
            format: "jwt_vc",
            path: "$.verifiableCredential[0]",
          },
        },
        {
          id: "cross-device-in-time-credential",
          path: "$",
          format: "jwt_vp",
          path_nested: {
            id: "cross-device-in-time-credential",
            format: "jwt_vc",
            path: "$.verifiableCredential[1]",
          },
        },
        {
          id: "same-device-deferred-credential",
          path: "$",
          format: "jwt_vp",
          path_nested: {
            id: "same-device-deferred-credential",
            format: "jwt_vc",
            path: "$.verifiableCredential[2]",
          },
        },
        {
          id: "cross-device-deferred-credential",
          path: "$",
          format: "jwt_vp",
          path_nested: {
            id: "cross-device-deferred-credential",
            format: "jwt_vc",
            path: "$.verifiableCredential[3]",
          },
        },
        {
          id: "same-device-pre_authorised-credential",
          path: "$",
          format: "jwt_vp",
          path_nested: {
            id: "same-device-pre_authorised-credential",
            format: "jwt_vc",
            path: "$.verifiableCredential[4]",
          },
        },
        {
          id: "cross-device-pre_authorised-credential",
          path: "$",
          format: "jwt_vp",
          path_nested: {
            id: "cross-device-pre_authorised-credential",
            format: "jwt_vc",
            path: "$.verifiableCredential[5]",
          },
        },
      ],
    };

    response = await request(server)
      .post(`${getPathnameWithoutPrefix(vpTokenRequestParams.redirect_uri)}`)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(
        new URLSearchParams({
          vp_token: vpToken,
          presentation_submission: JSON.stringify(presentationSubmission),
          state: vpTokenRequestParams.state,
        }).toString()
      );

    expect(response.status).toBe(302);
    location = (response.headers as { location: string }).location;
    expect(location).toStrictEqual(
      expect.stringContaining(authenticationRequestParams.redirect_uri)
    );

    authenticationResponseQueryParams = qs.parse(
      new URL(location).search.substring(1)
    ) as unknown as { code: string; state: string };

    expect(authenticationResponseQueryParams.state).toBe(
      authenticationRequestParams.state
    );
    expect(authenticationResponseQueryParams.code).toBeTruthy();

    // POST /auth-mock/token
    code = authenticationResponseQueryParams.code;
    pkceTokenRequestQueryParams = {
      grant_type: "authorization_code",
      code,
      client_id: clientDid,
      code_verifier: codeVerifier,
    } satisfies PostTokenPkceDto;

    response = await request(server)
      .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(new URLSearchParams(pkceTokenRequestQueryParams).toString());

    expect(response.status).toBe(200);
    accessToken = (response.body as TokenResponse).access_token;
    cNonce = (response.body as TokenResponse).c_nonce;
    expect(accessToken).toBeDefined();

    // POST /issuer-mock/credential
    proofJwt = await new SignJWT({
      nonce: cNonce,
    })
      .setProtectedHeader({
        typ: "openid4vci-proof+jwt",
        alg: "ES256",
        kid: clientKid,
      })
      .setIssuer(clientDid)
      .setAudience(issuerMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKey);

    credentialRequestParams = {
      types: [
        "VerifiableCredential",
        "VerifiableAttestation",
        requestParams.credential_type,
      ],
      format: "jwt_vc",
      proof: {
        proof_type: "jwt",
        jwt: proofJwt,
      },
    };

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          credentialIssuerConfig.credential_endpoint
        )}`
      )
      .set("Authorization", `Bearer ${accessToken}`)
      .send(credentialRequestParams);

    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual({
      format: "jwt_vc",
      credential: expect.any(String),
    });

    // Check VC
    credential = (response.body as CredentialResponse).credential;
    vcPayload = await verifyCredentialJwt(credential, options);

    expect(vcPayload).toStrictEqual({
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      id: expect.stringContaining("vc:ebsi:conformance#"),
      type: [
        "VerifiableCredential",
        "VerifiableAttestation",
        requestParams.credential_type,
      ],
      issuer: configService.get<string>("issuerMockKid").split("#")[0],
      issuanceDate: expect.any(String),
      issued: expect.any(String),
      validFrom: expect.any(String),
      expirationDate: expect.any(String),
      credentialSubject: {
        id: clientDid,
      },
      credentialSchema: {
        id: configService.get<string>("authorisationCredentialSchema"),
        type: "FullJsonSchemaValidator2021",
      },
      termsOfUse: {
        id: configService.get<string>("issuerMockAccreditationUrl"),
        type: "IssuanceCertificate",
      },
    });

    // Check if the Holder wallet flow is completed
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: REQUEST_CT_WALLET_QUALIFICATION_CREDENTIAL,
        data: {
          did: clientDid,
        },
      });

    expect(response.body).toStrictEqual({
      success: true,
    });
  });
});
