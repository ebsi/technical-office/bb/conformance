import { randomUUID } from "node:crypto";
import { URLSearchParams } from "node:url";
import { describe, beforeAll, afterAll, it, expect } from "@jest/globals";
import { Test, TestingModule } from "@nestjs/testing";
import { ConfigService } from "@nestjs/config";
import { Logger } from "@nestjs/common";
import type { INestApplication, HttpServer } from "@nestjs/common";
import type { FastifyInstance } from "fastify";
import request from "supertest";
import { calculateJwkThumbprint } from "jose";
import type { JWK } from "jose";
import type { ApiConfig } from "../../src/config/configuration";
import { AppModule } from "../../src/app.module";
import { VALID_CREDENTIAL_TYPES } from "../../src/shared/constants";
import { configureApp } from "../utils/app";
import { getServer } from "../utils/getServer";
import {
  createClient,
  createQueryParams,
  createTokenRequestParams,
} from "../utils/data";

describe("Auth Mock Module", () => {
  let app: INestApplication;
  let server: HttpServer | string;
  let configService: ConfigService<ApiConfig, true>;
  let conformanceDomain: string;
  let authMockUri: string;
  let apiUrlPrefix: string;
  let issuerMockUri: string;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    configService =
      moduleFixture.get<ConfigService<ApiConfig, true>>(ConfigService);

    conformanceDomain = configService.get<string>("conformanceDomain");
    apiUrlPrefix = configService.get<string>("apiUrlPrefix");
    authMockUri = `${conformanceDomain}${apiUrlPrefix}/auth-mock`;
    issuerMockUri = `${conformanceDomain}${apiUrlPrefix}/issuer-mock`;

    app = await configureApp(moduleFixture, { apiUrlPrefix });

    Logger.overrideLogger(false);

    await app.init();
    await (app.getHttpAdapter().getInstance() as FastifyInstance).ready();

    server = await getServer(app, configService, { conformanceDomain });
  });

  afterAll(async () => {
    // Avoid jest open handle error
    await new Promise((r) => {
      setTimeout(r, 500);
    });
    await app.close();
  });

  describe("GET /auth-mock/.well-known/openid-configuration", () => {
    it("should return the well-known OpenID configuration", async () => {
      expect.assertions(2);

      const response = await request(server).get(
        `${apiUrlPrefix}/auth-mock/.well-known/openid-configuration`
      );

      expect(response.body).toStrictEqual({
        redirect_uris: [`${authMockUri}/direct_post`],
        issuer: authMockUri,
        authorization_endpoint: `${authMockUri}/authorize`,
        token_endpoint: `${authMockUri}/token`,
        jwks_uri: `${authMockUri}/jwks`,
        scopes_supported: ["openid"],
        response_types_supported: ["vp_token", "id_token"],
        response_modes_supported: ["query"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
        request_object_signing_alg_values_supported: ["ES256"],
        request_parameter_supported: true,
        request_uri_parameter_supported: true,
        token_endpoint_auth_methods_supported: ["private_key_jwt"],
        request_authentication_methods_supported: {
          authorization_endpoint: ["request_object"],
        },
        vp_formats_supported: {
          jwt_vp: {
            alg_values_supported: ["ES256"],
          },
          jwt_vc: {
            alg_values_supported: ["ES256"],
          },
        },
        subject_syntax_types_supported: ["did:key", "did:ebsi"],
        subject_syntax_types_discriminations: [
          "did:key:jwk_jcs-pub",
          "did:ebsi:v1",
        ],
        subject_trust_frameworks_supported: ["ebsi"],
        id_token_types_supported: [
          "subject_signed_id_token",
          "attester_signed_id_token",
        ],
      });

      expect(response.status).toBe(200);
    });
  });

  describe("GET /auth-mock/jwks", () => {
    it("should return the OP's JWKS", async () => {
      expect.assertions(4);

      const response = await request(server).get(
        `${apiUrlPrefix}/auth-mock/jwks`
      );

      expect(response.body).toStrictEqual({
        keys: [
          {
            kty: "EC",
            crv: "P-256",
            alg: "ES256",
            x: expect.any(String),
            y: expect.any(String),
            kid: expect.any(String),
          },
        ],
      });

      expect(response.status).toBe(200);
      expect(
        (response.headers as Record<string, unknown>)["content-type"]
      ).toBe("application/jwk-set+json; charset=utf-8");

      const jwk = (response.body as { keys: [JWK] }).keys[0];
      const thumbprint = await calculateJwkThumbprint(jwk);

      expect(jwk.kid).toBe(thumbprint);
    });
  });

  describe("GET /auth-mock/authorize", () => {
    it("should reject a request with an invalid redirect_uri", async () => {
      expect.assertions(2);

      // Create mock client
      const client = await createClient();

      const queryParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri,
        {
          customQueryParams: { redirect_uri: "" },
        }
      );

      const response = await request(server)
        .get(
          `${apiUrlPrefix}/auth-mock/authorize?${new URLSearchParams(
            queryParams
          ).toString()}`
        )
        .send();

      // If redirect_uri is incorrect, the server can't redirect the user -> returns 400
      expect(response.status).toBe(400);
      expect(response.body).toStrictEqual({
        detail: '["redirect_uri should not be empty"]',
        status: 400,
        title: "Bad Request",
        type: "about:blank",
      });
    });

    it("should reject a request with an invalid scope", async () => {
      expect.assertions(2);

      // Create mock client
      const client = await createClient();

      const queryParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri,
        {
          customQueryParams: {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-expect-error
            scope: "bad scope",
          },
        }
      );

      const response = await request(server)
        .get(
          `${apiUrlPrefix}/auth-mock/authorize?${new URLSearchParams(
            queryParams
          ).toString()}`
        )
        .send();

      expect(response.status).toBe(302);
      expect((response.headers as { location: string }).location).toBe(
        `${queryParams.redirect_uri}?${new URLSearchParams({
          error: "invalid_scope",
          error_description:
            "scope must be one of the following values: openid, openid ver_test:id_token, openid ver_test:vp_token",
          state: queryParams.state,
        }).toString()}`
      );
    });

    it("should reject a request with an invalid client_id", async () => {
      expect.assertions(2);

      // Create mock client
      const client = await createClient();

      const queryParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri,
        {
          customQueryParams: { client_id: "not_an_url" },
        }
      );

      const response = await request(server)
        .get(
          `${apiUrlPrefix}/auth-mock/authorize?${new URLSearchParams(
            queryParams
          ).toString()}`
        )
        .send();

      expect(response.status).toBe(302);
      expect((response.headers as { location: string }).location).toBe(
        `${queryParams.redirect_uri}?${new URLSearchParams({
          error: "invalid_request",
          error_description:
            "client_id must either be a URL or a DID using the key DID method",
          state: queryParams.state,
        }).toString()}`
      );
    });

    it("should reject a request with an invalid response_type", async () => {
      expect.assertions(2);

      // Create mock client
      const client = await createClient();

      const queryParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri,
        {
          customQueryParams: {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-expect-error
            response_type: "not code",
          },
        }
      );

      const response = await request(server)
        .get(
          `${apiUrlPrefix}/auth-mock/authorize?${new URLSearchParams(
            queryParams
          ).toString()}`
        )
        .send();

      expect(response.status).toBe(302);
      expect((response.headers as { location: string }).location).toBe(
        `${queryParams.redirect_uri}?${new URLSearchParams({
          error: "invalid_request",
          error_description: "response_type must be equal to code",
          state: queryParams.state,
        }).toString()}`
      );
    });

    it("should reject a request with an invalid request parameter", async () => {
      expect.assertions(2);

      // Create mock client
      const client = await createClient();

      const queryParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri
      );
      queryParams.request = "not a jwt";

      const response = await request(server)
        .get(
          `${apiUrlPrefix}/auth-mock/authorize?${new URLSearchParams(
            queryParams
          ).toString()}`
        )
        .send();

      expect(response.status).toBe(302);
      expect((response.headers as { location: string }).location).toBe(
        `${queryParams.redirect_uri}?${new URLSearchParams({
          error: "invalid_request",
          error_description: "request must be a jwt string",
          state: queryParams.state,
        }).toString()}`
      );
    });

    it("should reject a request with an invalid request audience", async () => {
      expect.assertions(2);

      // Create mock client
      const client = await createClient();

      const queryParams = await createQueryParams(
        client,
        "not the expected aud",
        issuerMockUri
      );

      const response = await request(server)
        .get(
          `${apiUrlPrefix}/auth-mock/authorize?${new URLSearchParams(
            queryParams
          ).toString()}`
        )
        .send();

      expect(response.status).toBe(302);
      expect((response.headers as { location: string }).location).toBe(
        `${queryParams.redirect_uri}?${new URLSearchParams({
          error: "invalid_request",
          error_description: `request.aud must be equal to ${authMockUri}`,
          state: queryParams.state,
        }).toString()}`
      );
    });

    it("should reject a request where the request JWT parameters don't match the parent request", async () => {
      expect.assertions(2);

      // Create mock client
      const client = await createClient();

      const queryParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri,
        {
          customJwtPayload: {
            redirect_uri: "http://example.net/invalid",
          },
        }
      );

      const response = await request(server)
        .get(
          `${apiUrlPrefix}/auth-mock/authorize?${new URLSearchParams(
            queryParams
          ).toString()}`
        )
        .send();

      expect(response.status).toBe(302);
      expect((response.headers as { location: string }).location).toBe(
        `${queryParams.redirect_uri}?${new URLSearchParams({
          error: "invalid_request",
          error_description:
            "request.redirect_uri must be equal to redirect_uri",
          state: queryParams.state,
        }).toString()}`
      );
    });

    it("should reject a request where the request JWT authorization_details are not valid (missing required type)", async () => {
      expect.assertions(2);

      // Create mock client
      const client = await createClient();

      const queryParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri,
        {
          customJwtPayload: {
            authorization_details: [
              {
                // @ts-expect-error Invalid type
                type: "invalid",
                // @ts-expect-error Invalid format
                format: "jwt_vp",
                locations: [issuerMockUri],
                types: [
                  // "VerifiableCredential", // <- missing
                  "VerifiableAttestation",
                  "VerifiableAuthorisationToOnboard",
                ],
              },
            ],
          },
        }
      );

      const response = await request(server)
        .get(
          `${apiUrlPrefix}/auth-mock/authorize?${new URLSearchParams(
            queryParams
          ).toString()}`
        )
        .send();

      expect(response.status).toBe(302);
      expect((response.headers as { location: string }).location).toBe(
        `${queryParams.redirect_uri}?${new URLSearchParams({
          error: "invalid_request",
          error_description: `[invalid_literal] in 'request.authorization_details.0.type': Invalid literal value, expected "openid_credential"
[invalid_literal] in 'request.authorization_details.0.format': Invalid literal value, expected "jwt_vc"
[custom] in 'request.authorization_details.0.types': Array must include VerifiableCredential, VerifiableAttestation`,
          state: queryParams.state,
        }).toString()}`
      );
    });

    it("should reject a request where the request JWT authorization_details are not valid (unsupported type)", async () => {
      expect.assertions(2);

      // Create mock client
      const client = await createClient();

      const queryParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri,
        {
          customJwtPayload: {
            authorization_details: [
              {
                // @ts-expect-error Invalid type
                type: "invalid",
                // @ts-expect-error Invalid format
                format: "jwt_vp",
                locations: [issuerMockUri],
                types: [
                  "VerifiableCredential",
                  // @ts-expect-error Invalid type
                  "VerifiableAttestation2", // <- unsupported
                  "VerifiableAuthorisationToOnboard",
                ],
              },
            ],
          },
        }
      );

      const response = await request(server)
        .get(
          `${apiUrlPrefix}/auth-mock/authorize?${new URLSearchParams(
            queryParams
          ).toString()}`
        )
        .send();

      expect(response.status).toBe(302);
      expect((response.headers as { location: string }).location).toBe(
        `${queryParams.redirect_uri}?${new URLSearchParams({
          error: "invalid_request",
          error_description: `[invalid_literal] in 'request.authorization_details.0.type': Invalid literal value, expected "openid_credential"
[invalid_literal] in 'request.authorization_details.0.format': Invalid literal value, expected "jwt_vc"
[invalid_enum_value] in 'request.authorization_details.0.types.1': Invalid enum value. Expected '${VALID_CREDENTIAL_TYPES.join(
            "' | '"
          )}', received 'VerifiableAttestation2'`,
          state: queryParams.state,
        }).toString()}`
      );
    });
  });

  describe("GET /auth-mock/request_uri/{requestId}", () => {
    it("should reject a request with an invalid requestId (not a nonce)", async () => {
      expect.assertions(3);

      const requestId = "invalid-nonce";

      const response = await request(server)
        .get(`${apiUrlPrefix}/auth-mock/request_uri/${requestId}`)
        .send();

      expect(response.status).toBe(400);
      expect(response.body).toStrictEqual({
        detail: '["requestId must be a UUID"]',
        status: 400,
        title: "Bad Request",
        type: "about:blank",
      });
      expect(
        (response.headers as Record<string, unknown>)["content-type"]
      ).toBe("application/problem+json; charset=utf-8");
    });

    it("should reject a request with an unknown nonce", async () => {
      expect.assertions(3);

      const requestId = randomUUID();

      const response = await request(server)
        .get(`${apiUrlPrefix}/auth-mock/request_uri/${requestId}`)
        .send();

      expect(response.status).toBe(404);
      expect(response.body).toStrictEqual({
        detail: `No Authorization Request found with the ID ${requestId}`,
        status: 404,
        title: "Not Found",
        type: "about:blank",
      });
      expect(
        (response.headers as Record<string, unknown>)["content-type"]
      ).toBe("application/problem+json; charset=utf-8");
    });
  });

  describe("POST /auth-mock/direct_post", () => {
    it("should return an error if the content type is not application/x-www-form-urlencoded", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post(`${apiUrlPrefix}/auth-mock/direct_post`)
        // .set("Content-Type", "application/x-www-form-urlencoded")
        .send({
          id_token:
            "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6ImRpZDplYnNpOnpkUGoxR1BYamZFUlh4WFBFMVlUWWRKIzdqM1RwYU5kUE5UT3pPdG91T09rbmxPTFFrM0pQLXlrVGZyYVd0WTNHTUUifQ.eyJpc3MiOiJodHRwczovL215LWlzc3Vlci5ldS9zdWZmaXgveHl6Iiwic3ViIjoiaHR0cHM6Ly9teS1pc3N1ZXIuZXUvc3VmZml4L3h5eiIsImF1ZCI6Imh0dHBzOi8vYXBpLmNvbmZvcm1hbmNlLmVic2kuZXUvY29uZm9ybWFuY2UvdjMvYXV0aC1tb2NrIiwiZXhwIjoxNTg5Njk5MzYwLCJpYXQiOjE1ODk2OTkyNjAsIm5vbmNlIjoibi0wUzZfV3pBMk1qIn0.Ne2nUwjR_32wImigcTnHCqtB7BlOQ_WBiHjPoQ4UVrAEeZOMjdcGjuVJM1uY3mkLqn99kJXRFCVVDo13bEY4DQ",
          state: randomUUID(),
        });

      expect(response.body).toStrictEqual({
        detail: expect.stringMatching(
          "Content-type must be application/x-www-form-urlencoded"
        ),
        status: 400,
        title: "Bad Request",
        type: "about:blank",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the state param is missing", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post(`${apiUrlPrefix}/auth-mock/direct_post`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token:
              "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6ImRpZDplYnNpOnpkUGoxR1BYamZFUlh4WFBFMVlUWWRKIzdqM1RwYU5kUE5UT3pPdG91T09rbmxPTFFrM0pQLXlrVGZyYVd0WTNHTUUifQ.eyJpc3MiOiJodHRwczovL215LWlzc3Vlci5ldS9zdWZmaXgveHl6Iiwic3ViIjoiaHR0cHM6Ly9teS1pc3N1ZXIuZXUvc3VmZml4L3h5eiIsImF1ZCI6Imh0dHBzOi8vYXBpLmNvbmZvcm1hbmNlLmVic2kuZXUvY29uZm9ybWFuY2UvdjMvYXV0aC1tb2NrIiwiZXhwIjoxNTg5Njk5MzYwLCJpYXQiOjE1ODk2OTkyNjAsIm5vbmNlIjoibi0wUzZfV3pBMk1qIn0.Ne2nUwjR_32wImigcTnHCqtB7BlOQ_WBiHjPoQ4UVrAEeZOMjdcGjuVJM1uY3mkLqn99kJXRFCVVDo13bEY4DQ",
          }).toString()
        );

      expect(response.body).toStrictEqual({
        detail: "state must be a string",
        status: 400,
        title: "Bad Request",
        type: "about:blank",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the state doesn't match any existing authentication request", async () => {
      expect.assertions(2);

      const randomState = randomUUID();
      const response = await request(server)
        .post(`${apiUrlPrefix}/auth-mock/direct_post`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token:
              "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6ImRpZDplYnNpOnpkUGoxR1BYamZFUlh4WFBFMVlUWWRKIzdqM1RwYU5kUE5UT3pPdG91T09rbmxPTFFrM0pQLXlrVGZyYVd0WTNHTUUifQ.eyJpc3MiOiJodHRwczovL215LWlzc3Vlci5ldS9zdWZmaXgveHl6Iiwic3ViIjoiaHR0cHM6Ly9teS1pc3N1ZXIuZXUvc3VmZml4L3h5eiIsImF1ZCI6Imh0dHBzOi8vYXBpLmNvbmZvcm1hbmNlLmVic2kuZXUvY29uZm9ybWFuY2UvdjMvYXV0aC1tb2NrIiwiZXhwIjoxNTg5Njk5MzYwLCJpYXQiOjE1ODk2OTkyNjAsIm5vbmNlIjoibi0wUzZfV3pBMk1qIn0.Ne2nUwjR_32wImigcTnHCqtB7BlOQ_WBiHjPoQ4UVrAEeZOMjdcGjuVJM1uY3mkLqn99kJXRFCVVDo13bEY4DQ",
            state: randomState,
          }).toString()
        );

      expect(response.body).toStrictEqual({
        detail: `No Authentication Request bound to state ${randomState} found`,
        status: 400,
        title: "Bad Request",
        type: "about:blank",
      });
      expect(response.status).toBe(400);
    });
  });

  describe("POST /auth-mock/token", () => {
    it("should return an error if the content type is not application/x-www-form-urlencoded", async () => {
      expect.assertions(2);

      const client = await createClient();

      const code = randomUUID();
      const queryParams = await createTokenRequestParams(
        client,
        authMockUri,
        code
      );

      const response = await request(server)
        .post(`${apiUrlPrefix}/auth-mock/token`)
        // .set("Content-Type", "application/x-www-form-urlencoded")
        .send(queryParams);

      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description:
          "Content-type must be application/x-www-form-urlencoded",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the code is invalid", async () => {
      expect.assertions(2);

      const client = await createClient();

      const code = randomUUID();
      const queryParams = await createTokenRequestParams(
        client,
        authMockUri,
        code
      );

      const response = await request(server)
        .post(`${apiUrlPrefix}/auth-mock/token`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(new URLSearchParams(queryParams).toString());

      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description: "Invalid code",
      });
      expect(response.status).toBe(400);
    });
  });
});
