/* eslint-disable no-console */
import { createHash, randomBytes, randomUUID } from "node:crypto";
import { URLSearchParams } from "node:url";
import { jest, describe, beforeAll, afterAll, it, expect } from "@jest/globals";
import request from "supertest";
import { Test, TestingModule } from "@nestjs/testing";
import { HttpServer, Logger } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import type { NestFastifyApplication } from "@nestjs/platform-fastify";
import type { FastifyInstance } from "fastify";
import {
  importJWK,
  jwtVerify,
  SignJWT,
  decodeProtectedHeader,
  decodeJwt,
} from "jose";
import type { JWK } from "jose";
import qs from "qs";
import { util } from "@cef-ebsi/ebsi-did-resolver";
import {
  EbsiIssuer,
  verifyCredentialJwt,
  VerifyCredentialOptions,
} from "@cef-ebsi/verifiable-credential";
import { createVerifiablePresentationJwt } from "@cef-ebsi/verifiable-presentation";
import type { PresentationSubmission } from "@sphereon/pex-models";
import { ethers } from "ethers";
import axios from "axios";
import type { AxiosResponse } from "axios";
import { AppModule } from "../../src/app.module";
import type { ApiConfig } from "../../src/config/configuration";
import { getServer } from "../utils/getServer";
import { configureApp } from "../utils/app";
import {
  TokenResponse,
  OPMetadata,
  VpTokenRequest,
  IdTokenRequest,
  VA_TO_ONBOARD_PRESENTATION_DEFINITION,
  AuthenticationRequest,
} from "../../src/shared/auth-server";
import {
  CredentialResponse,
  signAndSendTransaction,
  waitToBeMined,
} from "../../src/shared/credential-issuer";
import {
  createClient,
  createQueryParams,
  createTokenRequestParams,
} from "../utils/data";
import type {
  CredentialIssuerMetadata,
  IdLink,
  PaginatedList,
  UnsignedTransaction,
} from "../../src/shared/interfaces";
import {
  REQUEST_CTAA_QUALIFICATION_CREDENTIAL,
  RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
  RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
  TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
  TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
  TAO_REVOKE_RIGHTS_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
  TI_REGISTER_DID,
  TI_REGISTER_VERIFIABLE_ACCREDITATION_TO_ATTEST,
  TI_REQUEST_CT_REVOCABLE,
  TI_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST,
  TI_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD,
  TI_REVOKE_CT_REVOCABLE,
  TI_VALIDATE_CT_REVOCABLE,
} from "../../src/shared/constants";
import { getKeyPair } from "../../src/shared/utils";
import { EBSIAuthorisationService } from "../../src/modules/ebsi/authorisation.service";
import { getSubAccountDid } from "../../src/modules/issuer-mock/issuer-mock.utils";
import type { AuthorizationDetails } from "../../src/shared/validators";

jest.setTimeout(600_000);

function prefixWith0x(str: string) {
  return str.startsWith("0x") ? str : `0x${str}`;
}

// The A&A test can only be fully run against the remote instance because TIR API v4 needs to communicate with the client mock
describe("WCT v3 - A&A e2e tests", () => {
  let app: NestFastifyApplication;
  let server: HttpServer | string;
  let configService: ConfigService<ApiConfig, true>;
  let conformanceDomain: string;
  let apiUrlPrefix: string;
  let issuerMockUri: string;
  let issuerMockDid: string;
  let ebsiAuthority: string;
  let ebsiAuthorisationService: EBSIAuthorisationService;
  let didRegistryApiJsonrpcUrl: string;
  let trustedIssuersRegistryApiUrl: string;
  let trustedIssuersRegistryApiJsonrpcUrl: string;
  let ledgerApiUrl: string;

  const logger = {
    debug: console.debug,
    verbose: console.log,
    error: console.error,
  } as unknown as Logger;

  function getPathnameWithoutPrefix(uri: string) {
    return new URL(uri).pathname.replace(conformanceDomain, "");
  }

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    configService =
      moduleFixture.get<ConfigService<ApiConfig, true>>(ConfigService);
    ebsiAuthorisationService = moduleFixture.get<EBSIAuthorisationService>(
      EBSIAuthorisationService
    );

    conformanceDomain = configService.get<string>("conformanceDomain");
    apiUrlPrefix = configService.get<string>("apiUrlPrefix");
    issuerMockUri = `${conformanceDomain}${apiUrlPrefix}/issuer-mock`;
    [issuerMockDid] = configService.get<string>("issuerMockKid").split("#") as [
      string
    ];
    didRegistryApiJsonrpcUrl = configService.get<string>(
      "didRegistryApiJsonrpcUrl"
    );
    trustedIssuersRegistryApiUrl = configService.get<string>(
      "trustedIssuersRegistryApiUrl"
    );
    trustedIssuersRegistryApiJsonrpcUrl = configService.get<string>(
      "trustedIssuersRegistryApiJsonrpcUrl"
    );
    ledgerApiUrl = configService.get<string>("ledgerApiUrl");

    ebsiAuthority = configService
      .get<string>("domain")
      .replace(/^https?:\/\//, ""); // remove http protocol scheme

    app = await configureApp(moduleFixture, { apiUrlPrefix });

    Logger.overrideLogger(false);

    await app.init();
    await (app.getHttpAdapter().getInstance() as FastifyInstance).ready();

    server = await getServer(app, configService, { conformanceDomain });
  });

  afterAll(async () => {
    // Avoid jest open handle error
    await new Promise<void>((resolve) => {
      setTimeout(() => resolve(), 500);
    });
    await app.close();
  });

  it("should follow the entire A&A flow", async () => {
    // Prepare client's server
    const clientES256KPrivateKeyHex = randomBytes(32).toString("hex");
    const clientJwkKeyPairES256K = await getKeyPair(
      clientES256KPrivateKeyHex,
      "ES256K"
    );
    const clientDid = util.createDid(randomBytes(16));
    const clientKidES256K = `${clientDid}#${clientJwkKeyPairES256K.publicKeyJwk.kid}`;
    const client = await createClient({
      privateKey: randomBytes(32).toString("hex"),
      domain: conformanceDomain,
      pathname: `${apiUrlPrefix}/client-mock/${clientDid}`,
    });
    const clientKidES256 = `${clientDid}#${client.publicKeyJwk.kid}`;

    let response = await request(server)
      .post(`${apiUrlPrefix}/client-mock/initiate`)
      .send({
        did: clientDid,
        keys: [clientJwkKeyPairES256K.privateKeyJwk, client.privateKeyJwk],
      });

    expect(response.status).toBe(204);

    /**
     * ------------------------------------------------------------------------
     * A&A - Trusted Issuer - Onboarding
     * Request VerifiableAuthorisationToOnboard
     * ------------------------------------------------------------------------
     */

    // GET /issuer-mock/.well-known/openid-credential-issuer
    response = await request(server).get(
      `${apiUrlPrefix}/issuer-mock/.well-known/openid-credential-issuer`
    );

    expect(response.status).toBe(200);

    const credentialIssuerConfig = response.body as CredentialIssuerMetadata;

    // GET AUthorization Server (Auth Mock) /.well-known/openid-configuration
    if (!credentialIssuerConfig.authorization_server)
      throw new Error("authorization_server not defined");
    const authorizationServerUri = credentialIssuerConfig.authorization_server;

    response = await request(server).get(
      getPathnameWithoutPrefix(
        `${authorizationServerUri}/.well-known/openid-configuration`
      )
    );

    expect(response.body).toStrictEqual({
      redirect_uris: [`${authorizationServerUri}/direct_post`],
      issuer: authorizationServerUri,
      authorization_endpoint: `${authorizationServerUri}/authorize`,
      token_endpoint: `${authorizationServerUri}/token`,
      jwks_uri: `${authorizationServerUri}/jwks`,
      scopes_supported: ["openid"],
      response_types_supported: ["vp_token", "id_token"],
      response_modes_supported: ["query"],
      grant_types_supported: ["authorization_code"],
      subject_types_supported: ["public"],
      id_token_signing_alg_values_supported: ["ES256"],
      request_object_signing_alg_values_supported: ["ES256"],
      request_parameter_supported: true,
      request_uri_parameter_supported: true,
      token_endpoint_auth_methods_supported: ["private_key_jwt"],
      request_authentication_methods_supported: {
        authorization_endpoint: ["request_object"],
      },
      vp_formats_supported: {
        jwt_vp: {
          alg_values_supported: ["ES256"],
        },
        jwt_vc: {
          alg_values_supported: ["ES256"],
        },
      },
      subject_syntax_types_supported: ["did:key", "did:ebsi"],
      subject_syntax_types_discriminations: [
        "did:key:jwk_jcs-pub",
        "did:ebsi:v1",
      ],
      subject_trust_frameworks_supported: ["ebsi"],
      id_token_types_supported: [
        "subject_signed_id_token",
        "attester_signed_id_token",
      ],
    });

    const authorizationServerOpenIdConfig = response.body as OPMetadata;

    // GET AS /jwks
    const jwksUri = getPathnameWithoutPrefix(
      authorizationServerOpenIdConfig.jwks_uri
    );

    response = await request(server).get(jwksUri);

    expect(response.body).toStrictEqual({
      keys: [
        {
          kty: "EC",
          crv: "P-256",
          alg: "ES256",
          x: expect.any(String),
          y: expect.any(String),
          kid: expect.any(String),
        },
      ],
    });
    expect(response.status).toBe(200);

    const jwkAuthService = (response.body as { keys: [JWK] }).keys[0];

    // GET AS /authorize
    const authorizationEndpoint = getPathnameWithoutPrefix(
      authorizationServerOpenIdConfig.authorization_endpoint
    );
    let authorizationDetails: AuthorizationDetails = [
      {
        type: "openid_credential",
        format: "jwt_vc",
        locations: [issuerMockUri],
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAuthorisationToOnboard",
        ],
      },
    ];
    let authenticationRequestParams = await createQueryParams(
      client,
      authorizationServerUri,
      issuerMockUri,
      {
        customQueryParams: { redirect_uri: `${client.clientId}/code-cb` },
        customJwtPayload: {
          authorization_details: authorizationDetails,
        },
      }
    );
    const authenticationRequestJwt = authenticationRequestParams.request;
    const decodedAuthenticationRequestJwt = decodeJwt(
      authenticationRequestJwt
    ) as AuthenticationRequest;

    response = await request(server).get(
      `${authorizationEndpoint}?${new URLSearchParams(
        authenticationRequestParams
      ).toString()}`
    );

    expect(response.status).toBe(302);

    let { location } = response.headers as { location: string };

    expect(location).toStrictEqual(
      expect.stringContaining(
        decodedAuthenticationRequestJwt.client_metadata
          .authorization_endpoint as string
      )
    );

    // Verify response of /authorize
    let responseQueryParams = qs.parse(
      new URL(location).search.substring(1)
    ) as unknown as IdTokenRequest;

    expect(responseQueryParams).toStrictEqual({
      state: expect.any(String),
      client_id: authorizationServerUri,
      redirect_uri: `${authorizationServerUri}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: expect.any(String),
      request_uri: expect.any(String),
    });

    // Get request_uri
    response = await request(server).get(
      getPathnameWithoutPrefix(responseQueryParams.request_uri as string)
    );

    const authServerPublicKeyJwk = await importJWK(jwkAuthService);
    let verifyResult = await jwtVerify(response.text, authServerPublicKeyJwk);

    expect(verifyResult.protectedHeader).toStrictEqual({
      alg: "ES256",
      typ: "JWT",
      kid: jwkAuthService.kid,
    });
    expect(verifyResult.payload).toStrictEqual({
      state: expect.any(String),
      client_id: authorizationServerUri,
      redirect_uri: `${authorizationServerUri}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: expect.any(String),
      iss: authorizationServerUri,
      aud: authenticationRequestParams.client_id,
    });

    // POST AS /direct_post
    const redirectUri = getPathnameWithoutPrefix(
      responseQueryParams.redirect_uri
    );
    let idTokenRequestState = verifyResult.payload["state"] as
      | string
      | undefined;
    let idTokenRequestNonce = verifyResult.payload["nonce"] as
      | string
      | undefined;
    const clientPrivateKeyES256 = await importJWK(client.privateKeyJwk);
    let idTokenDirectPost = await new SignJWT({
      nonce: idTokenRequestNonce,
      sub: clientDid,
    })
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: clientKidES256,
      })
      .setIssuer(clientDid)
      .setAudience(authorizationServerUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKeyES256);

    response = await request(server)
      .post(redirectUri)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(
        new URLSearchParams({
          id_token: idTokenDirectPost,
          ...(idTokenRequestState && { state: idTokenRequestState }),
        }).toString()
      );

    expect(response.body).toStrictEqual({});
    expect(response.status).toBe(302);

    location = (response.headers as { location: string }).location;

    expect(location).toStrictEqual(
      expect.stringContaining(authenticationRequestParams.redirect_uri)
    );

    let authenticationResponseQueryParams = qs.parse(
      new URL(location).search.substring(1)
    );

    expect(authenticationResponseQueryParams["state"]).toBe(
      authenticationRequestParams.state
    );

    expect(authenticationResponseQueryParams["code"]).toStrictEqual(
      expect.any(String)
    );

    // POST AS /token
    const tokenEndpoint = getPathnameWithoutPrefix(
      authorizationServerOpenIdConfig.token_endpoint
    );
    let queryParams = await createTokenRequestParams(
      client,
      authorizationServerUri,
      authenticationResponseQueryParams["code"] as string
    );

    response = await request(server)
      .post(tokenEndpoint)
      .send(new URLSearchParams(queryParams).toString());

    expect(response.body).toStrictEqual({
      access_token: expect.any(String),
      c_nonce: expect.any(String),
      c_nonce_expires_in: 86400,
      expires_in: 86400,
      id_token: expect.any(String),
      token_type: "Bearer",
    });
    expect(response.status).toBe(200);

    let {
      c_nonce: cNonce,
      c_nonce_expires_in: cNonceExpiresIn,
      id_token: idTokenResponseToken,
      access_token: accessToken,
    } = response.body as TokenResponse;

    // Verify response of Token Endpoint
    expect(decodeProtectedHeader(idTokenResponseToken)).toStrictEqual({
      alg: "ES256",
      kid: expect.any(String),
      typ: "JWT",
    });
    expect(decodeJwt(idTokenResponseToken)).toStrictEqual({
      iss: authorizationServerUri,
      aud: client.clientId,
      sub: clientDid,
      exp: expect.any(Number),
      iat: expect.any(Number),
      nonce: expect.any(String),
    });

    expect(decodeProtectedHeader(accessToken)).toStrictEqual({
      alg: "ES256",
      kid: expect.any(String),
      typ: "JWT",
    });
    expect(decodeJwt(accessToken)).toStrictEqual({
      iss: authorizationServerUri,
      aud: [issuerMockUri],
      sub: clientDid,
      exp: expect.any(Number),
      iat: expect.any(Number),
      claims: {
        authorization_details: authorizationDetails,
        c_nonce: cNonce,
        c_nonce_expires_in: cNonceExpiresIn,
        client_id: client.clientId,
      },
      nonce: expect.any(String),
    });

    // POST Issuer Mock /credential
    let proofJwt = await new SignJWT({
      nonce: cNonce,
    })
      .setProtectedHeader({
        typ: "openid4vci-proof+jwt",
        alg: "ES256",
        kid: clientKidES256,
      })
      .setIssuer(client.clientId)
      .setAudience(issuerMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKeyES256);

    let credentialRequestParams = {
      types: [
        "VerifiableCredential",
        "VerifiableAttestation",
        "VerifiableAuthorisationToOnboard",
      ],
      format: "jwt_vc",
      proof: {
        proof_type: "jwt",
        jwt: proofJwt,
      },
    };

    const credentialEndpoint = getPathnameWithoutPrefix(
      credentialIssuerConfig.credential_endpoint
    );

    response = await request(server)
      .post(credentialEndpoint)
      .set("Authorization", `Bearer ${accessToken}`)
      .send(credentialRequestParams);

    expect(response.body).toStrictEqual({
      format: "jwt_vc",
      credential: expect.any(String),
    });
    expect(response.status).toBe(200);

    // Check VC
    const verifiableAuthorisationToOnboardJwt = (
      response.body as CredentialResponse
    ).credential;
    const options: VerifyCredentialOptions = {
      ebsiAuthority,
    };
    const vcPayload = await verifyCredentialJwt(
      verifiableAuthorisationToOnboardJwt,
      options
    );

    expect(vcPayload).toStrictEqual({
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      id: expect.stringContaining("vc:ebsi:conformance#"),
      type: [
        "VerifiableCredential",
        "VerifiableAttestation",
        "VerifiableAuthorisationToOnboard",
      ],
      issuer: configService.get<string>("issuerMockKid").split("#")[0],
      issuanceDate: expect.any(String),
      issued: expect.any(String),
      validFrom: expect.any(String),
      expirationDate: expect.any(String),
      credentialSubject: { id: clientDid },
      credentialSchema: {
        id: configService.get<string>("authorisationCredentialSchema"),
        type: "FullJsonSchemaValidator2021",
      },
      termsOfUse: {
        id: configService.get<string>("issuerMockAccreditationUrl"),
        type: "IssuanceCertificate",
      },
    });

    // Verify that the event has been properly emitted
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: TI_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    /**
     * ------------------------------------------------------------------------
     * A&A - Trusted Issuer - Onboarding
     * Register your DID document into the DID Registry
     * ------------------------------------------------------------------------
     */

    // Use VerifiableAuthorisationToOnboard to get an access token and register the DID document
    const signerES256K: EbsiIssuer = {
      did: clientDid,
      kid: clientKidES256K,
      alg: "ES256K",
      publicKeyJwk: clientJwkKeyPairES256K.publicKeyJwk,
      privateKeyJwk: clientJwkKeyPairES256K.privateKeyJwk,
    };

    const didrInviteAccessToken = await ebsiAuthorisationService.getAccessToken(
      "didr_invite",
      signerES256K,
      [verifiableAuthorisationToOnboardJwt]
    );

    // Build unsigned transaction
    const wallet = new ethers.Wallet(clientES256KPrivateKeyHex);
    const now = Math.floor(Date.now() / 1000);
    const in6months = now + 6 * 30 * 24 * 3600;
    let responseBuild: AxiosResponse<{
      result: UnsignedTransaction;
    }>;

    try {
      responseBuild = await axios.post(
        didRegistryApiJsonrpcUrl,
        {
          jsonrpc: "2.0",
          method: "insertDidDocument",
          params: [
            {
              from: wallet.address,
              did: clientDid,
              baseDocument: JSON.stringify({
                "@context": [
                  "https://www.w3.org/ns/did/v1",
                  "https://w3id.org/security/suites/jws-2020/v1",
                ],
              }),
              vMethodId: clientJwkKeyPairES256K.publicKeyJwk.kid,
              publicKey: wallet.publicKey,
              isSecp256k1: true,
              notBefore: now,
              notAfter: in6months,
            },
          ],
          id: 1,
        },
        {
          headers: { authorization: `Bearer ${didrInviteAccessToken}` },
        }
      );
    } catch (e) {
      console.error(e);
      throw e;
    }

    let unsignedTransaction = responseBuild.data.result;

    let transactionResult = await signAndSendTransaction(
      unsignedTransaction,
      wallet,
      didRegistryApiJsonrpcUrl,
      logger,
      didrInviteAccessToken
    );

    if (!transactionResult.success) {
      throw new Error(
        "Unable to send the transaction to register the DID document"
      );
    }

    let { txId } = transactionResult;

    let miningResult = await waitToBeMined(ledgerApiUrl, logger, txId);

    if (!miningResult.success) {
      throw new Error("Tx not mined");
    }

    // Add ES256 verification method to DID document

    // Get "didr_write" access token
    const didrWriteAccessToken = await ebsiAuthorisationService.getAccessToken(
      "didr_write",
      signerES256K
    );

    try {
      responseBuild = await axios.post(
        didRegistryApiJsonrpcUrl,
        {
          jsonrpc: "2.0",
          method: "addVerificationMethod",
          params: [
            {
              from: wallet.address,
              did: clientDid,
              vMethodId: client.publicKeyJwk.kid,
              publicKey: `0x${Buffer.from(
                JSON.stringify(client.publicKeyJwk)
              ).toString("hex")}`,
              isSecp256k1: false,
            },
          ],
          id: 1,
        },
        {
          headers: { authorization: `Bearer ${didrWriteAccessToken}` },
        }
      );
    } catch (e) {
      console.error(e);
      throw e;
    }

    unsignedTransaction = responseBuild.data.result;

    transactionResult = await signAndSendTransaction(
      unsignedTransaction,
      wallet,
      didRegistryApiJsonrpcUrl,
      logger,
      didrWriteAccessToken
    );

    if (!transactionResult.success) {
      throw new Error(
        "Unable to send the transaction to add an ES256 verification method to the DID document"
      );
    }

    txId = transactionResult.txId;

    miningResult = await waitToBeMined(ledgerApiUrl, logger, txId);

    if (!miningResult.success) {
      throw new Error("Tx not mined");
    }

    // Register ES256 verification method as authentication method
    try {
      responseBuild = await axios.post(
        didRegistryApiJsonrpcUrl,
        {
          jsonrpc: "2.0",
          method: "addVerificationRelationship",
          params: [
            {
              from: wallet.address,
              did: clientDid,
              name: "authentication",
              vMethodId: client.publicKeyJwk.kid,
              notBefore: now,
              notAfter: in6months,
            },
          ],
          id: 1,
        },
        {
          headers: { authorization: `Bearer ${didrWriteAccessToken}` },
        }
      );
    } catch (e) {
      console.error(e);
      throw e;
    }

    unsignedTransaction = responseBuild.data.result;

    transactionResult = await signAndSendTransaction(
      unsignedTransaction,
      wallet,
      didRegistryApiJsonrpcUrl,
      logger,
      didrWriteAccessToken
    );

    if (!transactionResult.success) {
      throw new Error(
        "Unable to send the transaction to register the ES256 verification method as an authentication method"
      );
    }

    txId = transactionResult.txId;

    miningResult = await waitToBeMined(ledgerApiUrl, logger, txId);

    if (!miningResult.success) {
      throw new Error("Tx not mined");
    }

    // Register ES256 verification method as assertionMethod method
    try {
      responseBuild = await axios.post(
        didRegistryApiJsonrpcUrl,
        {
          jsonrpc: "2.0",
          method: "addVerificationRelationship",
          params: [
            {
              from: wallet.address,
              did: clientDid,
              name: "assertionMethod",
              vMethodId: client.publicKeyJwk.kid,
              notBefore: now,
              notAfter: in6months,
            },
          ],
          id: 1,
        },
        {
          headers: { authorization: `Bearer ${didrWriteAccessToken}` },
        }
      );
    } catch (e) {
      console.error(e);
      throw e;
    }

    unsignedTransaction = responseBuild.data.result;

    transactionResult = await signAndSendTransaction(
      unsignedTransaction,
      wallet,
      didRegistryApiJsonrpcUrl,
      logger,
      didrWriteAccessToken
    );

    if (!transactionResult.success) {
      throw new Error(
        "Unable to send the transaction to register the ES256 verification method as an assertion method"
      );
    }

    txId = transactionResult.txId;

    miningResult = await waitToBeMined(ledgerApiUrl, logger, txId);

    if (!miningResult.success) {
      throw new Error("Tx not mined");
    }

    // Check result
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: TI_REGISTER_DID,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    /**
     * ------------------------------------------------------------------------
     * A&A - Trusted Issuer - Get accredited as a Trusted Issuer
     * Request VerifiableAccreditationToAttest
     * ------------------------------------------------------------------------
     */

    // GET AS /authorize
    authorizationDetails = [
      {
        type: "openid_credential",
        format: "jwt_vc",
        locations: [issuerMockUri],
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
      },
    ];
    authenticationRequestParams = await createQueryParams(
      client,
      authorizationServerUri,
      issuerMockUri,
      {
        customQueryParams: { redirect_uri: `${client.clientId}/code-cb` },
        customJwtPayload: {
          authorization_details: authorizationDetails,
        },
      }
    );

    response = await request(server).get(
      `${authorizationEndpoint}?${new URLSearchParams(
        authenticationRequestParams
      ).toString()}`
    );

    expect(response.status).toBe(302);

    location = (response.headers as { location: string }).location;

    expect(location).toStrictEqual(
      expect.stringContaining(
        decodedAuthenticationRequestJwt.client_metadata
          .authorization_endpoint as string
      )
    );

    // Verify response of /authorize
    responseQueryParams = qs.parse(
      new URL(location).search.substring(1)
    ) as unknown as IdTokenRequest;

    expect(responseQueryParams).toStrictEqual({
      state: expect.any(String),
      client_id: authorizationServerUri,
      redirect_uri: `${authorizationServerUri}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: expect.any(String),
      request_uri: expect.any(String),
    });

    // Get request_uri
    response = await request(server).get(
      getPathnameWithoutPrefix(responseQueryParams.request_uri as string)
    );

    verifyResult = await jwtVerify(response.text, authServerPublicKeyJwk);

    expect(verifyResult.protectedHeader).toStrictEqual({
      alg: "ES256",
      typ: "JWT",
      kid: jwkAuthService.kid,
    });
    expect(verifyResult.payload).toStrictEqual({
      state: expect.any(String),
      client_id: authorizationServerUri,
      redirect_uri: `${authorizationServerUri}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: expect.any(String),
      iss: authorizationServerUri,
      aud: authenticationRequestParams.client_id,
    });

    // POST AS /direct_post
    idTokenRequestState = verifyResult.payload["state"] as string | undefined;
    idTokenRequestNonce = verifyResult.payload["nonce"] as string | undefined;
    idTokenDirectPost = await new SignJWT({
      nonce: idTokenRequestNonce,
      sub: clientDid,
    })
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: clientKidES256,
      })
      .setIssuer(clientDid)
      .setAudience(authorizationServerUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKeyES256);

    response = await request(server)
      .post(redirectUri)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(
        new URLSearchParams({
          id_token: idTokenDirectPost,
          ...(idTokenRequestState && { state: idTokenRequestState }),
        }).toString()
      );

    expect(response.body).toStrictEqual({});
    expect(response.status).toBe(302);

    location = (response.headers as { location: string }).location;

    expect(location).toStrictEqual(
      expect.stringContaining(authenticationRequestParams.redirect_uri)
    );

    authenticationResponseQueryParams = qs.parse(
      new URL(location).search.substring(1)
    );

    expect(authenticationResponseQueryParams["state"]).toBe(
      authenticationRequestParams.state
    );

    expect(authenticationResponseQueryParams["code"]).toStrictEqual(
      expect.any(String)
    );

    // POST AS /token
    queryParams = await createTokenRequestParams(
      client,
      authorizationServerUri,
      authenticationResponseQueryParams["code"] as string
    );

    response = await request(server)
      .post(tokenEndpoint)
      .send(new URLSearchParams(queryParams).toString());

    expect(response.body).toStrictEqual({
      access_token: expect.any(String),
      c_nonce: expect.any(String),
      c_nonce_expires_in: 86400,
      expires_in: 86400,
      id_token: expect.any(String),
      token_type: "Bearer",
    });
    expect(response.status).toBe(200);

    cNonce = (response.body as TokenResponse).c_nonce;
    cNonceExpiresIn = (response.body as TokenResponse).c_nonce_expires_in;
    idTokenResponseToken = (response.body as TokenResponse).id_token;
    accessToken = (response.body as TokenResponse).access_token;

    // Verify response of Token Endpoint
    expect(decodeProtectedHeader(idTokenResponseToken)).toStrictEqual({
      alg: "ES256",
      kid: expect.any(String),
      typ: "JWT",
    });
    expect(decodeJwt(idTokenResponseToken)).toStrictEqual({
      iss: authorizationServerUri,
      aud: client.clientId,
      sub: clientDid,
      exp: expect.any(Number),
      iat: expect.any(Number),
      nonce: expect.any(String),
    });

    expect(decodeProtectedHeader(accessToken)).toStrictEqual({
      alg: "ES256",
      kid: expect.any(String),
      typ: "JWT",
    });
    expect(decodeJwt(accessToken)).toStrictEqual({
      iss: authorizationServerUri,
      aud: [issuerMockUri],
      sub: clientDid,
      exp: expect.any(Number),
      iat: expect.any(Number),
      claims: {
        authorization_details: authorizationDetails,
        c_nonce: cNonce,
        c_nonce_expires_in: cNonceExpiresIn,
        client_id: client.clientId,
      },
      nonce: expect.any(String),
    });

    // POST Issuer Mock /credential
    proofJwt = await new SignJWT({
      nonce: cNonce,
    })
      .setProtectedHeader({
        typ: "openid4vci-proof+jwt",
        alg: "ES256",
        kid: clientKidES256,
      })
      .setIssuer(client.clientId)
      .setAudience(issuerMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKeyES256);

    credentialRequestParams = {
      types: [
        "VerifiableCredential",
        "VerifiableAttestation",
        "VerifiableAccreditation",
        "VerifiableAccreditationToAttest",
      ],
      format: "jwt_vc",
      proof: {
        proof_type: "jwt",
        jwt: proofJwt,
      },
    };

    response = await request(server)
      .post(credentialEndpoint)
      .set("Authorization", `Bearer ${accessToken}`)
      .send(credentialRequestParams);

    expect(response.body).toStrictEqual({
      format: "jwt_vc",
      credential: expect.any(String),
    });
    expect(response.status).toBe(200);

    // Check VC
    const verifiableAccreditationToAttestJwt = (
      response.body as CredentialResponse
    ).credential;
    const verifiableAccreditationToAttestPayload = await verifyCredentialJwt(
      verifiableAccreditationToAttestJwt,
      options
    );

    expect(verifiableAccreditationToAttestPayload).toStrictEqual({
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      id: expect.stringContaining("vc:ebsi:conformance#"),
      type: [
        "VerifiableCredential",
        "VerifiableAttestation",
        "VerifiableAccreditation",
        "VerifiableAccreditationToAttest",
      ],
      issuer: configService.get<string>("issuerMockKid").split("#")[0],
      issuanceDate: expect.any(String),
      issued: expect.any(String),
      validFrom: expect.any(String),
      expirationDate: expect.any(String),
      credentialSubject: {
        id: clientDid,
        reservedAttributeId: expect.any(String),
        accreditedFor: [
          {
            limitJurisdiction: expect.any(String),
            schemaId: expect.any(String),
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              "CTRevocable",
            ],
          },
        ],
      },
      credentialSchema: {
        id: configService.get<string>("authorisationCredentialSchema"),
        type: "FullJsonSchemaValidator2021",
      },
      termsOfUse: {
        id: configService.get<string>("issuerMockAccreditationUrl"),
        type: "IssuanceCertificate",
      },
    });

    // Verify that the event has been properly emitted
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: TI_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    /**
     * ------------------------------------------------------------------------
     * A&A - Trusted Issuer - Get accredited as a Trusted Issuer
     * Register VerifiableAccreditationToAttest into the Trusted Issuers Registry
     * ------------------------------------------------------------------------
     */

    const verifiableAccreditationToAttestReservedAttributeId =
      verifiableAccreditationToAttestPayload.credentialSubject[
        "reservedAttributeId"
      ] as string;

    const signerES256: EbsiIssuer = {
      did: clientDid,
      kid: clientKidES256,
      alg: "ES256",
      publicKeyJwk: client.publicKeyJwk,
      privateKeyJwk: client.privateKeyJwk,
    };

    const tirInviteAccessToken = await ebsiAuthorisationService.getAccessToken(
      "tir_invite",
      signerES256,
      [verifiableAccreditationToAttestJwt]
    );

    // Build unsigned transaction
    try {
      responseBuild = await axios.post(
        trustedIssuersRegistryApiJsonrpcUrl,
        {
          jsonrpc: "2.0",
          method: "setAttributeData",
          params: [
            {
              from: wallet.address,
              did: clientDid,
              attributeId: verifiableAccreditationToAttestReservedAttributeId,
              attributeData: `0x${Buffer.from(
                verifiableAccreditationToAttestJwt
              ).toString("hex")}`,
            },
          ],
          id: 1,
        },
        {
          headers: { authorization: `Bearer ${tirInviteAccessToken}` },
        }
      );
    } catch (e) {
      console.error(e);
      throw e;
    }

    unsignedTransaction = responseBuild.data.result;

    transactionResult = await signAndSendTransaction(
      unsignedTransaction,
      wallet,
      trustedIssuersRegistryApiJsonrpcUrl,
      logger,
      tirInviteAccessToken
    );

    if (!transactionResult.success) {
      throw new Error("Unable to send the transaction to set the attribute");
    }

    txId = transactionResult.txId;

    miningResult = await waitToBeMined(ledgerApiUrl, logger, txId);

    if (!miningResult.success) {
      throw new Error("Tx not mined");
    }

    // Check result
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: TI_REGISTER_VERIFIABLE_ACCREDITATION_TO_ATTEST,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    // Stop here if the tests are running locally.
    // Reason: TIR API won't allow the registration of the local proxy.
    if (process.env.TEST_ENV !== "remote") {
      console.warn("Can't execute all the A&A tests locally.");
      return;
    }

    /**
     * ------------------------------------------------------------------------
     * A&A - Trusted Issuer - Issue & Revoke
     * Issue CTRevocable Credential with credentialStatus StatusList2021
     * ------------------------------------------------------------------------
     */

    // Create status list "1"
    response = await request(server)
      .post(`${apiUrlPrefix}/client-mock/updateList`)
      .send({
        did: clientDid,
        id: "1",
        position: 0,
        value: 0,
      });
    expect(response.status).toBe(204);

    // Register proxy
    const proxyObject = {
      prefix: client.clientId,
      headers: {},
      testSuffix: "/credentials/status/1",
    };
    const proxyData = JSON.stringify(proxyObject);
    const proxyId = ethers.utils.sha256(Buffer.from(proxyData));

    const tirWriteAccessToken = await ebsiAuthorisationService.getAccessToken(
      "tir_write",
      signerES256
    );

    // Build unsigned transaction
    try {
      responseBuild = await axios.post(
        trustedIssuersRegistryApiJsonrpcUrl,
        {
          jsonrpc: "2.0",
          method: "addIssuerProxy",
          params: [
            {
              from: wallet.address,
              did: clientDid,
              proxyData,
            },
          ],
          id: 1,
        },
        {
          headers: { authorization: `Bearer ${tirWriteAccessToken}` },
        }
      );
    } catch (e) {
      console.error(JSON.stringify(e, null, 2));
      throw e;
    }

    unsignedTransaction = responseBuild.data.result;

    transactionResult = await signAndSendTransaction(
      unsignedTransaction,
      wallet,
      trustedIssuersRegistryApiJsonrpcUrl,
      logger,
      tirWriteAccessToken
    );

    if (!transactionResult.success) {
      throw new Error("Unable to send the transaction to set the attribute");
    }

    txId = transactionResult.txId;

    miningResult = await waitToBeMined(ledgerApiUrl, logger, txId);

    if (!miningResult.success) {
      throw new Error("Tx not mined");
    }

    // Update client mock
    response = await request(server)
      .post(`${apiUrlPrefix}/client-mock/initiate`)
      .send({
        did: clientDid,
        keys: [clientJwkKeyPairES256K.privateKeyJwk, client.privateKeyJwk],
        attributeUrl: `${trustedIssuersRegistryApiUrl}/${clientDid}/attributes/${verifiableAccreditationToAttestReservedAttributeId}`,
        proxyId,
      });

    expect(response.status).toBe(204);

    // Trigger CTRevocable request from Conformance Wallet
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: TI_REQUEST_CT_REVOCABLE,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    /**
     * ------------------------------------------------------------------------
     * A&A - Trusted Issuer - Issue & Revoke
     * Validate the issued credential details
     * ------------------------------------------------------------------------
     */

    // Trigger CTRevocable validation
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: TI_VALIDATE_CT_REVOCABLE,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    /**
     * ------------------------------------------------------------------------
     * A&A - Trusted Issuer - Issue & Revoke
     * Revoke the credential and confirm
     * ------------------------------------------------------------------------
     */

    // Revoke CTRevocable
    // Note: the index can be deterministically computed from the client DID (convention specific to Client Mock)
    // Here, client = Conformance Issuer
    const statusListIndex =
      createHash("sha256")
        .update(issuerMockDid, "utf8")
        .digest()
        .slice(0, 6)
        .readUInt32BE() % 131072;

    response = await request(server)
      .post(`${apiUrlPrefix}/client-mock/updateList`)
      .send({
        did: clientDid,
        id: "1",
        position: statusListIndex,
        value: 1, // Revoked
      });
    expect(response.status).toBe(204);

    // Trigger CTRevocable validation
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: TI_REVOKE_CT_REVOCABLE,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    /**
     * ------------------------------------------------------------------------
     * A&A - Trusted Accreditation Organisation
     * Request VerifiableAccreditationToAccredit
     * ------------------------------------------------------------------------
     */

    // GET AS /authorize
    authorizationDetails = [
      {
        type: "openid_credential",
        format: "jwt_vc",
        locations: [issuerMockUri],
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
      },
    ];
    authenticationRequestParams = await createQueryParams(
      client,
      authorizationServerUri,
      issuerMockUri,
      {
        customQueryParams: { redirect_uri: `${client.clientId}/code-cb` },
        customJwtPayload: {
          authorization_details: authorizationDetails,
        },
      }
    );

    response = await request(server).get(
      `${authorizationEndpoint}?${new URLSearchParams(
        authenticationRequestParams
      ).toString()}`
    );

    expect(response.status).toBe(302);

    location = (response.headers as { location: string }).location;

    expect(location).toStrictEqual(
      expect.stringContaining(
        decodedAuthenticationRequestJwt.client_metadata
          .authorization_endpoint as string
      )
    );

    // Verify response of /authorize
    responseQueryParams = qs.parse(
      new URL(location).search.substring(1)
    ) as unknown as IdTokenRequest;

    expect(responseQueryParams).toStrictEqual({
      state: expect.any(String),
      client_id: authorizationServerUri,
      redirect_uri: `${authorizationServerUri}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: expect.any(String),
      request_uri: expect.any(String),
    });

    // Get request_uri
    response = await request(server).get(
      getPathnameWithoutPrefix(responseQueryParams.request_uri as string)
    );

    verifyResult = await jwtVerify(response.text, authServerPublicKeyJwk);

    expect(verifyResult.protectedHeader).toStrictEqual({
      alg: "ES256",
      typ: "JWT",
      kid: jwkAuthService.kid,
    });
    expect(verifyResult.payload).toStrictEqual({
      state: expect.any(String),
      client_id: authorizationServerUri,
      redirect_uri: `${authorizationServerUri}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: expect.any(String),
      iss: authorizationServerUri,
      aud: authenticationRequestParams.client_id,
    });

    // POST AS /direct_post
    idTokenRequestState = verifyResult.payload["state"] as string | undefined;
    idTokenRequestNonce = verifyResult.payload["nonce"] as string | undefined;
    idTokenDirectPost = await new SignJWT({
      nonce: idTokenRequestNonce,
      sub: clientDid,
    })
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: clientKidES256,
      })
      .setIssuer(clientDid)
      .setAudience(authorizationServerUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKeyES256);

    response = await request(server)
      .post(redirectUri)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(
        new URLSearchParams({
          id_token: idTokenDirectPost,
          ...(idTokenRequestState && { state: idTokenRequestState }),
        }).toString()
      );

    expect(response.body).toStrictEqual({});
    expect(response.status).toBe(302);

    location = (response.headers as { location: string }).location;

    expect(location).toStrictEqual(
      expect.stringContaining(authenticationRequestParams.redirect_uri)
    );

    authenticationResponseQueryParams = qs.parse(
      new URL(location).search.substring(1)
    );

    expect(authenticationResponseQueryParams["state"]).toBe(
      authenticationRequestParams.state
    );

    expect(authenticationResponseQueryParams["code"]).toStrictEqual(
      expect.any(String)
    );

    // POST AS /token
    queryParams = await createTokenRequestParams(
      client,
      authorizationServerUri,
      authenticationResponseQueryParams["code"] as string
    );

    response = await request(server)
      .post(tokenEndpoint)
      .send(new URLSearchParams(queryParams).toString());

    expect(response.body).toStrictEqual({
      access_token: expect.any(String),
      c_nonce: expect.any(String),
      c_nonce_expires_in: 86400,
      expires_in: 86400,
      id_token: expect.any(String),
      token_type: "Bearer",
    });
    expect(response.status).toBe(200);

    cNonce = (response.body as TokenResponse).c_nonce;
    cNonceExpiresIn = (response.body as TokenResponse).c_nonce_expires_in;
    idTokenResponseToken = (response.body as TokenResponse).id_token;
    accessToken = (response.body as TokenResponse).access_token;

    // Verify response of Token Endpoint
    expect(decodeProtectedHeader(idTokenResponseToken)).toStrictEqual({
      alg: "ES256",
      kid: expect.any(String),
      typ: "JWT",
    });
    expect(decodeJwt(idTokenResponseToken)).toStrictEqual({
      iss: authorizationServerUri,
      aud: client.clientId,
      sub: clientDid,
      exp: expect.any(Number),
      iat: expect.any(Number),
      nonce: expect.any(String),
    });

    expect(decodeProtectedHeader(accessToken)).toStrictEqual({
      alg: "ES256",
      kid: expect.any(String),
      typ: "JWT",
    });
    expect(decodeJwt(accessToken)).toStrictEqual({
      iss: authorizationServerUri,
      aud: [issuerMockUri],
      sub: clientDid,
      exp: expect.any(Number),
      iat: expect.any(Number),
      claims: {
        authorization_details: authorizationDetails,
        c_nonce: cNonce,
        c_nonce_expires_in: cNonceExpiresIn,
        client_id: client.clientId,
      },
      nonce: expect.any(String),
    });

    // POST Issuer Mock /credential
    proofJwt = await new SignJWT({
      nonce: cNonce,
    })
      .setProtectedHeader({
        typ: "openid4vci-proof+jwt",
        alg: "ES256",
        kid: clientKidES256,
      })
      .setIssuer(client.clientId)
      .setAudience(issuerMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKeyES256);

    credentialRequestParams = {
      types: [
        "VerifiableCredential",
        "VerifiableAttestation",
        "VerifiableAccreditation",
        "VerifiableAccreditationToAccredit",
      ],
      format: "jwt_vc",
      proof: {
        proof_type: "jwt",
        jwt: proofJwt,
      },
    };

    response = await request(server)
      .post(credentialEndpoint)
      .set("Authorization", `Bearer ${accessToken}`)
      .send(credentialRequestParams);

    expect(response.body).toStrictEqual({
      format: "jwt_vc",
      credential: expect.any(String),
    });
    expect(response.status).toBe(200);

    // Check VC
    const verifiableAccreditationToAccreditJwt = (
      response.body as CredentialResponse
    ).credential;
    const verifiableAccreditationToAccreditPayload = await verifyCredentialJwt(
      verifiableAccreditationToAccreditJwt,
      options
    );

    expect(verifiableAccreditationToAccreditPayload).toStrictEqual({
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      id: expect.stringContaining("vc:ebsi:conformance#"),
      type: [
        "VerifiableCredential",
        "VerifiableAttestation",
        "VerifiableAccreditation",
        "VerifiableAccreditationToAccredit",
      ],
      issuer: configService.get<string>("issuerMockKid").split("#")[0],
      issuanceDate: expect.any(String),
      issued: expect.any(String),
      validFrom: expect.any(String),
      expirationDate: expect.any(String),
      credentialSubject: {
        id: clientDid,
        reservedAttributeId: expect.any(String),
        accreditedFor: [
          {
            limitJurisdiction: expect.any(String),
            schemaId: expect.any(String),
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              "CTRevocable",
            ],
          },
        ],
      },
      credentialSchema: {
        id: configService.get<string>("authorisationCredentialSchema"),
        type: "FullJsonSchemaValidator2021",
      },
      termsOfUse: {
        id: configService.get<string>("issuerMockAccreditationUrl"),
        type: "IssuanceCertificate",
      },
    });

    // Verify that the event has been properly emitted
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    /**
     * ------------------------------------------------------------------------
     * A&A - Trusted Accreditation Organisation
     * Register VerifiableAccreditationToAccredit into the Trusted Issuers Registry
     * ------------------------------------------------------------------------
     */

    const verifiableAccreditationToAccreditReservedAttributeId =
      verifiableAccreditationToAccreditPayload.credentialSubject[
        "reservedAttributeId"
      ] as string;

    // Build unsigned transaction
    try {
      responseBuild = await axios.post(
        trustedIssuersRegistryApiJsonrpcUrl,
        {
          jsonrpc: "2.0",
          method: "setAttributeData",
          params: [
            {
              from: wallet.address,
              did: clientDid,
              attributeId: verifiableAccreditationToAccreditReservedAttributeId,
              attributeData: `0x${Buffer.from(
                verifiableAccreditationToAccreditJwt
              ).toString("hex")}`,
            },
          ],
          id: 1,
        },
        {
          headers: { authorization: `Bearer ${tirWriteAccessToken}` },
        }
      );
    } catch (e) {
      console.error(e);
      throw e;
    }

    unsignedTransaction = responseBuild.data.result;

    transactionResult = await signAndSendTransaction(
      unsignedTransaction,
      wallet,
      trustedIssuersRegistryApiJsonrpcUrl,
      logger,
      tirWriteAccessToken
    );

    if (!transactionResult.success) {
      throw new Error("Unable to send the transaction to set the attribute");
    }

    txId = transactionResult.txId;

    miningResult = await waitToBeMined(ledgerApiUrl, logger, txId);

    if (!miningResult.success) {
      throw new Error("Tx not mined");
    }

    // Check result
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    // Update client mock
    response = await request(server)
      .post(`${apiUrlPrefix}/client-mock/initiate`)
      .send({
        did: clientDid,
        keys: [clientJwkKeyPairES256K.privateKeyJwk, client.privateKeyJwk],
        attributeUrl: `${trustedIssuersRegistryApiUrl}/${clientDid}/attributes/${verifiableAccreditationToAccreditReservedAttributeId}`,
        proxyId,
      });

    /**
     * ------------------------------------------------------------------------
     * A&A - Trusted Accreditation Organisation
     * Issue VerifiableAuthorisationToOnboard for Conformance Issuer's sub-account DID
     * ------------------------------------------------------------------------
     */

    // Trigger VerifiableAuthorisationToOnboard request from sub-account DID
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    // Client mock issues the VerifiableAuthorisationToOnboard credential

    // Validate the issuance was correct
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    /**
     * ------------------------------------------------------------------------
     * A&A - Trusted Accreditation Organisation
     * Issue VerifiableAccreditationToAttest for sub-account DID
     * ------------------------------------------------------------------------
     */

    // Trigger VerifiableAccreditationToAttest request from sub-account DID
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    // Client mock issues the VerifiableAccreditationToAttest credential

    // Validate the issuance was correct
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    /**
     * ------------------------------------------------------------------------
     * A&A - Trusted Accreditation Organisation
     * Issue VerifiableAccreditationToAccredit for sub-account DID
     * ------------------------------------------------------------------------
     */

    // Trigger VerifiableAccreditationToAccredit request from sub-account DID
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    // Client mock issues the VerifiableAccreditationToAccredit credential

    // Validate the issuance was correct
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    /**
     * ------------------------------------------------------------------------
     * A&A - Trusted Accreditation Organisation
     * Revoke accreditations from sub-account DID
     * ------------------------------------------------------------------------
     */
    const subAccountDid = getSubAccountDid(clientDid);

    // Get subaccount's attributes
    let attributesResponse: AxiosResponse<PaginatedList<IdLink>>;
    try {
      attributesResponse = await axios.get(
        `${trustedIssuersRegistryApiUrl}/${subAccountDid}/attributes`
      );
    } catch (error) {
      console.error(error);
      throw error;
    }

    const { total, items } = attributesResponse.data;

    if (total !== 2) {
      throw new Error("Unexpected number of attributes");
    }

    const attributesIds = items.map((item) => item.id);

    // Revoke attributes
    // eslint-disable-next-line no-restricted-syntax
    for (const attributeId of attributesIds) {
      try {
        // eslint-disable-next-line no-await-in-loop
        responseBuild = await axios.post(
          trustedIssuersRegistryApiJsonrpcUrl,
          {
            jsonrpc: "2.0",
            method: "setAttributeMetadata",
            params: [
              {
                from: wallet.address,
                did: subAccountDid,
                attributeId: prefixWith0x(attributeId),
                taoDid: clientDid,
                issuerType: 4, // 4 -> Revoked
                taoAttributeId: prefixWith0x(
                  verifiableAccreditationToAccreditReservedAttributeId
                ),
              },
            ],
            id: 1,
          },
          {
            headers: { authorization: `Bearer ${tirWriteAccessToken}` },
          }
        );
      } catch (e) {
        console.error(JSON.stringify(e, null, 2));
        throw e;
      }

      unsignedTransaction = responseBuild.data.result;

      // eslint-disable-next-line no-await-in-loop
      transactionResult = await signAndSendTransaction(
        unsignedTransaction,
        wallet,
        trustedIssuersRegistryApiJsonrpcUrl,
        logger,
        tirWriteAccessToken
      );

      if (!transactionResult.success) {
        throw new Error("Unable to send the transaction to set the attribute");
      }

      txId = transactionResult.txId;

      // eslint-disable-next-line no-await-in-loop
      miningResult = await waitToBeMined(ledgerApiUrl, logger, txId);

      if (!miningResult.success) {
        throw new Error("Tx not mined");
      }
    }

    // Validate the issuance was correct
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: TAO_REVOKE_RIGHTS_SUBACCOUNT,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    /**
     * ------------------------------------------------------------------------
     * A&A - Root Trusted Accreditation Organisation
     * Request VerifiableAuthorisationForTrustChain
     * ------------------------------------------------------------------------
     */

    // GET AS /authorize
    authorizationDetails = [
      {
        type: "openid_credential",
        format: "jwt_vc",
        locations: [issuerMockUri],
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAuthorisationForTrustChain",
        ],
      },
    ];
    authenticationRequestParams = await createQueryParams(
      client,
      authorizationServerUri,
      issuerMockUri,
      {
        customQueryParams: { redirect_uri: `${client.clientId}/code-cb` },
        customJwtPayload: {
          authorization_details: authorizationDetails,
        },
      }
    );

    response = await request(server).get(
      `${authorizationEndpoint}?${new URLSearchParams(
        authenticationRequestParams
      ).toString()}`
    );

    expect(response.status).toBe(302);

    location = (response.headers as { location: string }).location;

    expect(location).toStrictEqual(
      expect.stringContaining(
        decodedAuthenticationRequestJwt.client_metadata
          .authorization_endpoint as string
      )
    );

    // Verify response of /authorize
    const vpTokenRequestParams = qs.parse(
      new URL(location).search.substring(1),
      {
        depth: 50,
        parameterLimit: 1000,
      }
    ) as unknown as VpTokenRequest;

    expect(vpTokenRequestParams).toStrictEqual({
      state: expect.any(String),
      client_id: authorizationServerUri,
      redirect_uri: `${authorizationServerUri}/direct_post`,
      response_type: "vp_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: expect.any(String),
      presentation_definition: expect.any(String),
      request: expect.any(String),
    });

    expect(
      JSON.parse(vpTokenRequestParams.presentation_definition)
    ).toStrictEqual(
      expect.objectContaining({
        id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.id,
      })
    );

    verifyResult = await jwtVerify(
      vpTokenRequestParams.request as string,
      authServerPublicKeyJwk
    );

    expect(verifyResult.protectedHeader).toStrictEqual({
      alg: "ES256",
      typ: "JWT",
      kid: jwkAuthService.kid,
    });
    expect(verifyResult.payload).toStrictEqual({
      state: expect.any(String),
      client_id: authorizationServerUri,
      exp: expect.any(Number),
      redirect_uri: `${authorizationServerUri}/direct_post`,
      response_type: "vp_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: expect.any(String),
      iss: authorizationServerUri,
      aud: authenticationRequestParams.client_id,
      presentation_definition: expect.objectContaining({
        id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.id,
      }),
    });

    // POST AS /direct_post
    const vpPayload = {
      id: `urn:did:${randomUUID()}`,
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      type: ["VerifiablePresentation"],
      holder: clientDid,
      verifiableCredential: [verifiableAuthorisationToOnboardJwt],
    };

    const vpToken = await createVerifiablePresentationJwt(
      vpPayload,
      signerES256,
      authorizationServerUri,
      {
        ebsiAuthority,
        skipValidation: true,
        nonce: randomUUID(),
        nbf: now,
        exp: in6months,
      }
    );

    const presentationSubmission: PresentationSubmission = {
      id: randomUUID(),
      definition_id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.id,
      descriptor_map: [
        {
          id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.input_descriptors[0].id,
          format: "jwt_vp",
          path: "$",
          path_nested: {
            id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.input_descriptors[0].id,
            format: "jwt_vc",
            path: "$.verifiableCredential[0]",
          },
        },
      ],
    };

    response = await request(server)
      .post(redirectUri)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(
        new URLSearchParams({
          vp_token: vpToken,
          state: vpTokenRequestParams.state,
          presentation_submission: JSON.stringify(presentationSubmission),
        }).toString()
      );

    expect(response.body).toStrictEqual({});
    expect(response.status).toBe(302);

    location = (response.headers as { location: string }).location;

    expect(location).toStrictEqual(
      expect.stringContaining(authenticationRequestParams.redirect_uri)
    );

    authenticationResponseQueryParams = qs.parse(
      new URL(location).search.substring(1)
    );

    expect(authenticationResponseQueryParams["state"]).toBe(
      authenticationRequestParams.state
    );

    expect(authenticationResponseQueryParams["code"]).toStrictEqual(
      expect.any(String)
    );

    // POST AS /token
    queryParams = await createTokenRequestParams(
      client,
      authorizationServerUri,
      authenticationResponseQueryParams["code"] as string
    );

    response = await request(server)
      .post(tokenEndpoint)
      .send(new URLSearchParams(queryParams).toString());

    expect(response.body).toStrictEqual({
      access_token: expect.any(String),
      c_nonce: expect.any(String),
      c_nonce_expires_in: 86400,
      expires_in: 86400,
      id_token: expect.any(String),
      token_type: "Bearer",
    });
    expect(response.status).toBe(200);

    cNonce = (response.body as TokenResponse).c_nonce;
    cNonceExpiresIn = (response.body as TokenResponse).c_nonce_expires_in;
    idTokenResponseToken = (response.body as TokenResponse).id_token;
    accessToken = (response.body as TokenResponse).access_token;

    // Verify response of Token Endpoint
    expect(decodeProtectedHeader(idTokenResponseToken)).toStrictEqual({
      alg: "ES256",
      kid: expect.any(String),
      typ: "JWT",
    });
    expect(decodeJwt(idTokenResponseToken)).toStrictEqual({
      iss: authorizationServerUri,
      aud: client.clientId,
      sub: clientDid,
      exp: expect.any(Number),
      iat: expect.any(Number),
      nonce: expect.any(String),
    });

    expect(decodeProtectedHeader(accessToken)).toStrictEqual({
      alg: "ES256",
      kid: expect.any(String),
      typ: "JWT",
    });
    expect(decodeJwt(accessToken)).toStrictEqual({
      iss: authorizationServerUri,
      aud: [issuerMockUri],
      sub: clientDid,
      exp: expect.any(Number),
      iat: expect.any(Number),
      claims: {
        authorization_details: authorizationDetails,
        c_nonce: cNonce,
        c_nonce_expires_in: cNonceExpiresIn,
        client_id: client.clientId,
      },
      nonce: expect.any(String),
    });

    // POST Issuer Mock /credential
    proofJwt = await new SignJWT({
      nonce: cNonce,
    })
      .setProtectedHeader({
        typ: "openid4vci-proof+jwt",
        alg: "ES256",
        kid: clientKidES256,
      })
      .setIssuer(client.clientId)
      .setAudience(issuerMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKeyES256);

    credentialRequestParams = {
      types: [
        "VerifiableCredential",
        "VerifiableAttestation",
        "VerifiableAuthorisationForTrustChain",
      ],
      format: "jwt_vc",
      proof: {
        proof_type: "jwt",
        jwt: proofJwt,
      },
    };

    response = await request(server)
      .post(credentialEndpoint)
      .set("Authorization", `Bearer ${accessToken}`)
      .send(credentialRequestParams);

    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual({
      acceptance_token: expect.any(String),
    });

    const { acceptance_token: acceptanceToken } = response.body as {
      acceptance_token: string;
    };

    // Wait a bit more than 5 seconds for the deferred credential to become available
    await new Promise<void>((resolve) => {
      setTimeout(() => resolve(), 5_500);
    });

    response = await request(server)
      .post(
        `${getPathnameWithoutPrefix(
          credentialIssuerConfig.deferred_credential_endpoint
        )}`
      )
      .set("Authorization", `Bearer ${acceptanceToken}`)
      .send();

    expect(response.body).toStrictEqual({
      format: "jwt_vc",
      credential: expect.any(String),
    });
    expect(response.status).toBe(200);

    // Check VC
    const verifiableAuthorisationForTrustChainJwt = (
      response.body as CredentialResponse
    ).credential;
    const verifiableAuthorisationForTrustChainPayload =
      await verifyCredentialJwt(
        verifiableAuthorisationForTrustChainJwt,
        options
      );

    expect(verifiableAuthorisationForTrustChainPayload).toStrictEqual({
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      id: expect.stringContaining("vc:ebsi:conformance#"),
      type: [
        "VerifiableCredential",
        "VerifiableAttestation",
        "VerifiableAuthorisationForTrustChain",
      ],
      issuer: configService.get<string>("issuerMockKid").split("#")[0],
      issuanceDate: expect.any(String),
      issued: expect.any(String),
      validFrom: expect.any(String),
      expirationDate: expect.any(String),
      credentialSubject: {
        id: clientDid,
        reservedAttributeId: expect.any(String),
      },
      credentialSchema: {
        id: configService.get<string>("authorisationCredentialSchema"),
        type: "FullJsonSchemaValidator2021",
      },
      termsOfUse: {
        id: configService.get<string>("issuerMockAccreditationUrl"),
        type: "IssuanceCertificate",
      },
    });

    // Verify that the event has been properly emitted
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    /**
     * ------------------------------------------------------------------------
     * A&A - Root Trusted Accreditation Organisation
     * Register VerifiableAuthorisationForTrustChain into the Trusted Issuers Registry
     * ------------------------------------------------------------------------
     */

    const verifiableAuthorisationForTrustChainReservedAttributeId =
      verifiableAuthorisationForTrustChainPayload.credentialSubject[
        "reservedAttributeId"
      ] as string;

    // Build unsigned transaction
    try {
      responseBuild = await axios.post(
        trustedIssuersRegistryApiJsonrpcUrl,
        {
          jsonrpc: "2.0",
          method: "setAttributeData",
          params: [
            {
              from: wallet.address,
              did: clientDid,
              attributeId:
                verifiableAuthorisationForTrustChainReservedAttributeId,
              attributeData: `0x${Buffer.from(
                verifiableAuthorisationForTrustChainJwt
              ).toString("hex")}`,
            },
          ],
          id: 1,
        },
        {
          headers: { authorization: `Bearer ${tirWriteAccessToken}` },
        }
      );
    } catch (e) {
      console.error(e);
      throw e;
    }

    unsignedTransaction = responseBuild.data.result;

    transactionResult = await signAndSendTransaction(
      unsignedTransaction,
      wallet,
      trustedIssuersRegistryApiJsonrpcUrl,
      logger,
      tirWriteAccessToken
    );

    if (!transactionResult.success) {
      throw new Error("Unable to send the transaction to set the attribute");
    }

    txId = transactionResult.txId;

    miningResult = await waitToBeMined(ledgerApiUrl, logger, txId);

    if (!miningResult.success) {
      throw new Error("Tx not mined");
    }

    // Check result
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);

    /**
     * ------------------------------------------------------------------------
     * A&A - Request CTAAQualificationCredential from Conformance Issuer
     * ------------------------------------------------------------------------
     */

    // Wait ~10 seconds to let Loki retrieve the logs
    await new Promise<void>((resolve) => {
      setTimeout(() => resolve(), 10_000);
    });

    // GET AS /authorize
    authorizationDetails = [
      {
        type: "openid_credential",
        format: "jwt_vc",
        locations: [issuerMockUri],
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "CTAAQualificationCredential",
        ],
      },
    ];
    authenticationRequestParams = await createQueryParams(
      client,
      authorizationServerUri,
      issuerMockUri,
      {
        customQueryParams: { redirect_uri: `${client.clientId}/code-cb` },
        customJwtPayload: {
          authorization_details: authorizationDetails,
        },
      }
    );

    response = await request(server).get(
      `${authorizationEndpoint}?${new URLSearchParams(
        authenticationRequestParams
      ).toString()}`
    );

    expect(response.status).toBe(302);

    location = (response.headers as { location: string }).location;

    expect(location).toStrictEqual(
      expect.stringContaining(
        decodedAuthenticationRequestJwt.client_metadata
          .authorization_endpoint as string
      )
    );

    // Verify response of /authorize
    responseQueryParams = qs.parse(
      new URL(location).search.substring(1)
    ) as unknown as IdTokenRequest;

    expect(responseQueryParams).toStrictEqual({
      state: expect.any(String),
      client_id: authorizationServerUri,
      redirect_uri: `${authorizationServerUri}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: expect.any(String),
      request_uri: expect.any(String),
    });

    // Get request_uri
    response = await request(server).get(
      getPathnameWithoutPrefix(responseQueryParams.request_uri as string)
    );

    verifyResult = await jwtVerify(response.text, authServerPublicKeyJwk);

    expect(verifyResult.protectedHeader).toStrictEqual({
      alg: "ES256",
      typ: "JWT",
      kid: jwkAuthService.kid,
    });
    expect(verifyResult.payload).toStrictEqual({
      state: expect.any(String),
      client_id: authorizationServerUri,
      redirect_uri: `${authorizationServerUri}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: expect.any(String),
      iss: authorizationServerUri,
      aud: authenticationRequestParams.client_id,
    });

    // POST AS /direct_post
    idTokenRequestState = verifyResult.payload["state"] as string | undefined;
    idTokenRequestNonce = verifyResult.payload["nonce"] as string | undefined;
    idTokenDirectPost = await new SignJWT({
      nonce: idTokenRequestNonce,
      sub: clientDid,
    })
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: clientKidES256,
      })
      .setIssuer(clientDid)
      .setAudience(authorizationServerUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKeyES256);

    response = await request(server)
      .post(redirectUri)
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(
        new URLSearchParams({
          id_token: idTokenDirectPost,
          ...(idTokenRequestState && { state: idTokenRequestState }),
        }).toString()
      );

    expect(response.body).toStrictEqual({});
    expect(response.status).toBe(302);

    location = (response.headers as { location: string }).location;

    expect(location).toStrictEqual(
      expect.stringContaining(authenticationRequestParams.redirect_uri)
    );

    authenticationResponseQueryParams = qs.parse(
      new URL(location).search.substring(1)
    );

    expect(authenticationResponseQueryParams["state"]).toBe(
      authenticationRequestParams.state
    );

    expect(authenticationResponseQueryParams["code"]).toStrictEqual(
      expect.any(String)
    );

    // POST AS /token
    queryParams = await createTokenRequestParams(
      client,
      authorizationServerUri,
      authenticationResponseQueryParams["code"] as string
    );

    response = await request(server)
      .post(tokenEndpoint)
      .send(new URLSearchParams(queryParams).toString());

    expect(response.body).toStrictEqual({
      access_token: expect.any(String),
      c_nonce: expect.any(String),
      c_nonce_expires_in: 86400,
      expires_in: 86400,
      id_token: expect.any(String),
      token_type: "Bearer",
    });
    expect(response.status).toBe(200);

    cNonce = (response.body as TokenResponse).c_nonce;
    cNonceExpiresIn = (response.body as TokenResponse).c_nonce_expires_in;
    idTokenResponseToken = (response.body as TokenResponse).id_token;
    accessToken = (response.body as TokenResponse).access_token;

    // Verify response of Token Endpoint
    expect(decodeProtectedHeader(idTokenResponseToken)).toStrictEqual({
      alg: "ES256",
      kid: expect.any(String),
      typ: "JWT",
    });
    expect(decodeJwt(idTokenResponseToken)).toStrictEqual({
      iss: authorizationServerUri,
      aud: client.clientId,
      sub: clientDid,
      exp: expect.any(Number),
      iat: expect.any(Number),
      nonce: expect.any(String),
    });

    expect(decodeProtectedHeader(accessToken)).toStrictEqual({
      alg: "ES256",
      kid: expect.any(String),
      typ: "JWT",
    });
    expect(decodeJwt(accessToken)).toStrictEqual({
      iss: authorizationServerUri,
      aud: [issuerMockUri],
      sub: clientDid,
      exp: expect.any(Number),
      iat: expect.any(Number),
      claims: {
        authorization_details: authorizationDetails,
        c_nonce: cNonce,
        c_nonce_expires_in: cNonceExpiresIn,
        client_id: client.clientId,
      },
      nonce: expect.any(String),
    });

    // POST Issuer Mock /credential
    proofJwt = await new SignJWT({
      nonce: cNonce,
    })
      .setProtectedHeader({
        typ: "openid4vci-proof+jwt",
        alg: "ES256",
        kid: clientKidES256,
      })
      .setIssuer(client.clientId)
      .setAudience(issuerMockUri)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(clientPrivateKeyES256);

    credentialRequestParams = {
      types: [
        "VerifiableCredential",
        "VerifiableAttestation",
        "CTAAQualificationCredential",
      ],
      format: "jwt_vc",
      proof: {
        proof_type: "jwt",
        jwt: proofJwt,
      },
    };

    response = await request(server)
      .post(credentialEndpoint)
      .set("Authorization", `Bearer ${accessToken}`)
      .send(credentialRequestParams);

    expect(response.body).toStrictEqual({
      format: "jwt_vc",
      credential: expect.any(String),
    });
    expect(response.status).toBe(200);

    // Check VC
    const qualificationCredentialJwt = (response.body as CredentialResponse)
      .credential;
    const qualificationCredentialPayload = await verifyCredentialJwt(
      qualificationCredentialJwt,
      options
    );

    expect(qualificationCredentialPayload).toStrictEqual({
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      id: expect.stringContaining("vc:ebsi:conformance#"),
      type: [
        "VerifiableCredential",
        "VerifiableAttestation",
        "CTAAQualificationCredential",
      ],
      issuer: configService.get<string>("issuerMockKid").split("#")[0],
      issuanceDate: expect.any(String),
      issued: expect.any(String),
      validFrom: expect.any(String),
      expirationDate: expect.any(String),
      credentialSubject: {
        id: clientDid,
      },
      credentialSchema: {
        id: configService.get<string>("authorisationCredentialSchema"),
        type: "FullJsonSchemaValidator2021",
      },
      termsOfUse: {
        id: configService.get<string>("issuerMockAccreditationUrl"),
        type: "IssuanceCertificate",
      },
    });

    // Verify that the event has been properly emitted
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: REQUEST_CTAA_QUALIFICATION_CREDENTIAL,
        data: {
          did: clientDid,
          clientId: client.clientId,
        },
      });

    expect(response.body).toStrictEqual({ success: true });
    expect(response.status).toBe(200);
  });
});
