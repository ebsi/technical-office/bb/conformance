import { jest, describe, beforeAll, afterAll, it, expect } from "@jest/globals";
import request from "supertest";
import { Test } from "@nestjs/testing";
import type { TestingModule } from "@nestjs/testing";
import { Logger } from "@nestjs/common";
import type { HttpServer } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import type { NestFastifyApplication } from "@nestjs/platform-fastify";
import type { FastifyInstance } from "fastify";
import { exportJWK, generateKeyPair } from "jose";
import { util as keyDidMethodHelpers } from "@cef-ebsi/key-did-resolver";
import { AppModule } from "../../src/app.module";
import type { ApiConfig } from "../../src/config/configuration";
import { getServer } from "../utils/getServer";
import { configureApp } from "../utils/app";
import {
  VERIFIER_ID_TOKEN_EXCHANGE,
  VERIFIER_VP_VALID_VC,
  VERIFIER_VP_EXPIRED_VC,
  VERIFIER_VP_REVOKED_VC,
  VERIFIER_VP_NOT_YET_VALID_VC,
} from "../../src/shared/constants";
import { getKeyPair } from "../../src/shared/utils";
import type { VerifierParams } from "../../src/modules/issuer-mock/validators";

jest.setTimeout(600_000);

/**
 * Actors:
 * - Verifier: Subject under test. Authorization Server.
 * - Holder Wallet: Issuer Mock (using its ES256 public key and the did:key method)
 */
describe("WCT v3 - Verifier e2e tests", () => {
  let app: NestFastifyApplication;
  let server: HttpServer | string;
  let configService: ConfigService<ApiConfig, true>;
  let conformanceDomain: string;
  let apiUrlPrefix: string;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    configService =
      moduleFixture.get<ConfigService<ApiConfig, true>>(ConfigService);

    conformanceDomain = configService.get<string>("conformanceDomain");
    apiUrlPrefix = configService.get<string>("apiUrlPrefix");

    app = await configureApp(moduleFixture, { apiUrlPrefix });

    Logger.overrideLogger(false);

    await app.init();
    await (app.getHttpAdapter().getInstance() as FastifyInstance).ready();

    server = await getServer(app, configService, { conformanceDomain });
  });

  afterAll(async () => {
    // Avoid jest open handle error
    await new Promise<void>((resolve) => {
      setTimeout(() => resolve(), 500);
    });
    await app.close();
  });

  it("should follow the entire Verifier flow", async () => {
    /**
     * ------------------------------------------------------------------------
     * Prepare Verifier's server
     * ------------------------------------------------------------------------
     */
    const verifierPrivateKey = (await generateKeyPair("ES256")).privateKey;
    const verifierPrivateKeyJwk = await exportJWK(verifierPrivateKey);
    const verifierKeyPair = await getKeyPair(verifierPrivateKeyJwk);

    // Note: Verifier doesn't need a DID, but Client Mock does
    const verifierDid = keyDidMethodHelpers.createDid(
      verifierKeyPair.publicKeyJwk
    );
    const verifierPathname = `${apiUrlPrefix}/client-mock/${verifierDid}`;
    const verifierClientId = `${conformanceDomain}${verifierPathname}`;

    let response = await request(server)
      .post(`${apiUrlPrefix}/client-mock/initiate`)
      .send({
        did: verifierDid,
        keys: [verifierKeyPair.privateKeyJwk],
      });

    expect(response.status).toBe(204);

    /**
     * ------------------------------------------------------------------------
     * ID Token exchange
     * ------------------------------------------------------------------------
     */
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: VERIFIER_ID_TOKEN_EXCHANGE,
        data: { clientId: verifierClientId } satisfies VerifierParams,
      });

    expect(response.body).toStrictEqual({
      success: true,
    });

    /**
     * ------------------------------------------------------------------------
     * VP Token exchange
     * ------------------------------------------------------------------------
     */
    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: VERIFIER_VP_VALID_VC,
        data: { clientId: verifierClientId } satisfies VerifierParams,
      });

    expect(response.body).toStrictEqual({
      success: true,
    });

    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: VERIFIER_VP_EXPIRED_VC,
        data: { clientId: verifierClientId } satisfies VerifierParams,
      });

    expect(response.body).toStrictEqual({
      success: true,
    });

    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: VERIFIER_VP_REVOKED_VC,
        data: { clientId: verifierClientId } satisfies VerifierParams,
      });

    expect(response.body).toStrictEqual({
      success: true,
    });

    response = await request(server)
      .post(`${apiUrlPrefix}/check`)
      .send({
        intent: VERIFIER_VP_NOT_YET_VALID_VC,
        data: { clientId: verifierClientId } satisfies VerifierParams,
      });

    expect(response.body).toStrictEqual({
      success: true,
    });
  });
});
