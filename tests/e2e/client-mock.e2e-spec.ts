import { jest, describe, beforeAll, afterAll, it, expect } from "@jest/globals";
import { Test, TestingModule } from "@nestjs/testing";
import { ConfigService } from "@nestjs/config";
import { Logger } from "@nestjs/common";
import type { INestApplication, HttpServer } from "@nestjs/common";
import type { FastifyInstance } from "fastify";
import request from "supertest";
import { randomUUID } from "node:crypto";
import {
  createVerifiableCredentialJwt,
  verifyCredentialJwt,
} from "@cef-ebsi/verifiable-credential";
import type { ApiConfig } from "../../src/config/configuration";
import { AppModule } from "../../src/app.module";
import { configureApp } from "../utils/app";
import { getServer } from "../utils/getServer";
import { getKeyPair, KeyPair } from "../../src/shared/utils";

jest.setTimeout(120_000);

const describeOnlyRemote =
  process.env.TEST_ENV === "remote" ? describe : describe.skip;

describe("Client Mock Module", () => {
  let app: INestApplication;
  let server: HttpServer | string;
  let configService: ConfigService<ApiConfig, true>;
  let conformanceDomain: string;
  let apiUrlPrefix: string;
  let testClientMockPrivateKey: string;
  let testClientMockKid: string;
  let testClientMockDid: string;
  let testClientMockAccreditationUrl: string;
  let testClientMockProxyUrl: string;
  let ebsiAuthority: string;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    configService =
      moduleFixture.get<ConfigService<ApiConfig, true>>(ConfigService);

    conformanceDomain = configService.get<string>("conformanceDomain");
    apiUrlPrefix = configService.get<string>("apiUrlPrefix");
    testClientMockPrivateKey = configService.get<string>(
      "testClientMockPrivateKey"
    );
    testClientMockKid = configService.get<string>("testClientMockKid");
    [testClientMockDid] = testClientMockKid.split("#") as [string];
    testClientMockAccreditationUrl = configService.get<string>(
      "testClientMockAccreditationUrl"
    );
    testClientMockProxyUrl = configService.get<string>(
      "testClientMockProxyUrl"
    );
    ebsiAuthority = configService
      .get<string>("domain")
      .replace(/^https?:\/\//, ""); // remove http protocol scheme

    app = await configureApp(moduleFixture, { apiUrlPrefix });

    Logger.overrideLogger(false);

    await app.init();
    await (app.getHttpAdapter().getInstance() as FastifyInstance).ready();

    server = await getServer(app, configService, { conformanceDomain });
  });

  afterAll(async () => {
    // Avoid jest open handle error
    await new Promise((r) => {
      setTimeout(r, 500);
    });
    await app.close();
  });

  /**
   * Unlike the other e2e tests this set of tests must be done
   * using TEST_ENV=remote because the TIR API will query the proxy
   * of the client mock to get the StatusList. Then the client mock
   * must be exposed and it is not possible to test it locally.
   */
  describeOnlyRemote("CTRevocable and StatusList2021", () => {
    let payloadJwt: Record<string, unknown>;
    let vcJwt: string;
    let keyPair: KeyPair;

    const statusIndex = "1";
    const statusListIndex = "50";

    beforeAll(async () => {
      keyPair = await getKeyPair(testClientMockPrivateKey);

      const iat = Math.round(Date.now() / 1000);
      const exp = iat + 365 * 24 * 3600;
      const issuanceDate = `${new Date(iat * 1000)
        .toISOString()
        .slice(0, -5)}Z`;
      const expirationDate = `${new Date(exp * 1000)
        .toISOString()
        .slice(0, -5)}Z`;

      payloadJwt = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `urn:uuid:${randomUUID()}`,
        type: ["VerifiableCredential", "VerifiableAttestation", "CTRevocable"],
        issuanceDate,
        expirationDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        issuer: testClientMockDid,
        credentialSubject: {
          id: testClientMockDid,
        },
        credentialStatus: {
          id: `${testClientMockProxyUrl}/credentials/status/${statusIndex}`,
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex,
          statusListCredential: `${testClientMockProxyUrl}/credentials/status/${statusIndex}`,
        },
        credentialSchema: {
          id: configService.get<string>("authorisationCredentialSchema"),
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: [
          {
            id: testClientMockAccreditationUrl,
            type: "IssuanceCertificate",
          },
        ],
      };

      vcJwt = await createVerifiableCredentialJwt(
        payloadJwt,
        {
          did: testClientMockDid,
          kid: testClientMockKid,
          publicKeyJwk: keyPair.publicKeyJwk,
          privateKeyJwk: keyPair.privateKeyJwk,
          alg: "ES256",
        },
        {
          ebsiAuthority,
          skipAccreditationsValidation: true,
          skipStatusValidation: true,
          skipValidation: true,
        }
      );
    });

    it("should initiate the client mock", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post(`${apiUrlPrefix}/client-mock/initiate`)
        .send({
          did: testClientMockDid,
          keys: [keyPair.privateKeyJwk],
        });
      expect(response.body).toStrictEqual({});
      expect(response.status).toBe(204);
    });

    it("should get the client mock's jwks", async () => {
      expect.assertions(2);

      const response = await request(server).get(
        `${apiUrlPrefix}/client-mock/${testClientMockDid}/jwks`
      );
      expect(response.body).toStrictEqual({ keys: [keyPair.publicKeyJwk] });
      expect(response.status).toBe(200);
    });

    it("should issue a valid credential", async () => {
      expect.assertions(3);

      // set value 0 so the credential is valid
      const responseUpdateList = await request(server)
        .post(`${apiUrlPrefix}/client-mock/updateList`)
        .send({
          did: testClientMockDid,
          id: statusIndex,
          position: Number(statusListIndex),
          value: 0,
        });
      expect(responseUpdateList.body).toStrictEqual({});
      expect(responseUpdateList.status).toBe(204);

      // the credential should be valid
      await expect(
        verifyCredentialJwt(vcJwt, { ebsiAuthority })
      ).resolves.toStrictEqual(payloadJwt);
    });

    it("should revoke a credential", async () => {
      expect.assertions(3);

      // set value 1 so the credential is revoked
      const responseUpdateList = await request(server)
        .post(`${apiUrlPrefix}/client-mock/updateList`)
        .send({
          did: testClientMockDid,
          id: statusIndex,
          position: Number(statusListIndex),
          value: 1,
        });
      expect(responseUpdateList.body).toStrictEqual({});
      expect(responseUpdateList.status).toBe(204);

      // the credential should be revoked
      await expect(
        verifyCredentialJwt(vcJwt, { ebsiAuthority })
      ).rejects.toThrow("The credential is revoked");
    });
  });
});
