import { randomUUID, randomBytes } from "node:crypto";
import type { ConfigService } from "@nestjs/config";
import {
  importJWK,
  JWTHeaderParameters,
  SignJWT,
  calculateJwkThumbprint,
  exportJWK,
  generateKeyPair,
} from "jose";
import type { JWK } from "jose";
import { util } from "@cef-ebsi/ebsi-did-resolver";
import nock from "nock";
import type { DIDDocument, JsonWebKey } from "did-resolver";
import type { EbsiVerifiableAttestation } from "@cef-ebsi/verifiable-credential";
import { base64 } from "multiformats/bases/base64";
import { gzip } from "pako";
import type { WritableDeep } from "type-fest";
import type {
  AuthenticationRequest,
  GetAuthorizeServiceWalletDto,
  PostTokenClientAssertionDto,
} from "../../src/shared/auth-server";
import type { AuthorizationDetails } from "../../src/shared/validators/authorization-details.validator";
import {
  ApiConfig,
  DIDR_API_PATH,
  LEDGER_API_PATH,
  TIR_API_PATH,
  TIR_JSON_RPC_PATH,
} from "../../src/config/configuration";
import { JWKWithKid, getKeyPair } from "../../src/shared/utils";
import type { CheckResult } from "../../src/shared/interfaces";
import type {
  LokiLog,
  LokiLogResponse,
} from "../../src/modules/logs/logs.service";
import type { INTENTS_LIST } from "../../src/shared/constants";

/**
 * Client properties
 */
export type ClientProps = {
  domain: string;
  pathname: string;
  clientId: string;
  privateKeyJwk: JWKWithKid;
  publicKeyJwk: JWKWithKid;
  authorizationEndpoint: string;
};

/**
 * Helper function to create a customizable client
 */
export async function createClient({
  domain = "https://my-issuer.eu",
  pathname = "/suffix/xyz",
  privateKey = {
    kty: "EC",
    crv: "P-256",
    alg: "ES256",
    use: "sig",
    kid: "Jp-9bEI8u68-e95mTwnCFKKIgTnDuRHcoTHazpXCJt4",
    x: "Q7-hrO70pO0P0FwwsAg6sd3k122qTRt2SlmUL4rrtB0",
    y: "4TVHXRow-XELCyj8BFte_pX1twrI7dZqr4yM7UJYgPA",
    d: "XuUhHLen_9-dmD8nMV5MkWxeFXWNtPnePjgji5JS8j8",
  },
  authorizationEndpoint,
}: {
  domain?: string;
  pathname?: string;
  privateKey?: string | JWK;
  authorizationEndpoint?: string;
} = {}): Promise<ClientProps> {
  if (!authorizationEndpoint) {
    // eslint-disable-next-line no-param-reassign
    authorizationEndpoint = `${domain}${pathname}/authorize`;
  }

  const keyPair = await getKeyPair(privateKey);
  return {
    domain,
    pathname,
    clientId: `${domain}${pathname}`,
    ...keyPair,
    authorizationEndpoint,
  };
}

export function mockClientServer(client: ClientProps) {
  // Mock client's /jwks endpoint
  nock(client.domain)
    .get(`${client.pathname}/jwks`)
    .reply(200, {
      keys: [client.publicKeyJwk],
    })
    .persist();
}

/**
 * Helper function to create the Authentication Request query params (for Service Wallet)
 *
 * @param client - The client props
 * @param aud - The JWT audience
 * @param customQueryParams - The query params to include in the request JWT
 * @returns The query params for the /authorize endpoint
 */
export async function createQueryParams(
  client: ClientProps,
  aud: string,
  issuerMockUri: string,
  {
    customQueryParams,
    customJwtPayload,
    customJwtHeaderParameters,
    alg = "ES256",
    ignoreClientMetadata = false,
  }: {
    customQueryParams?: Partial<GetAuthorizeServiceWalletDto>;
    customJwtPayload?: Partial<AuthenticationRequest>;
    customJwtHeaderParameters?: Partial<JWTHeaderParameters>;
    alg?: string;
    ignoreClientMetadata?: boolean;
  } = {}
) {
  const defaultQueryParams = {
    scope: "openid",
    client_id: client.clientId,
    redirect_uri: `${client.clientId}/code-cb`,
    response_type: "code",
    state: randomUUID(),
  } satisfies Omit<GetAuthorizeServiceWalletDto, "request">;

  const queryParams = {
    ...defaultQueryParams,
    ...customQueryParams,
  };

  const jwtPayload = {
    ...queryParams,
    ...(!ignoreClientMetadata && {
      client_metadata: {
        jwks_uri: `${client.clientId}/jwks`,
        authorization_endpoint: client.authorizationEndpoint,
        ...(customJwtPayload?.client_metadata ?? {}),
      },
    }),
    authorization_details: [
      {
        type: "openid_credential",
        format: "jwt_vc",
        locations: [issuerMockUri],
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAuthorisationToOnboard",
        ],
      },
    ] satisfies AuthorizationDetails,
    ...customJwtPayload,
  };

  const clientPrivateKey = await importJWK(client.privateKeyJwk, alg);
  const requestParam = await new SignJWT(jwtPayload)
    .setProtectedHeader({
      typ: "JWT",
      alg,
      kid: client.publicKeyJwk.kid,
      ...customJwtHeaderParameters,
    })
    .setAudience(aud)
    .setIssuer(queryParams.client_id)
    .sign(clientPrivateKey);

  return {
    ...queryParams,
    request: requestParam,
  } satisfies GetAuthorizeServiceWalletDto;
}

/**
 * Helper function to create a basic DID document for the provided DID, kid and JWK.
 *
 * @param did - The DID document controller
 * @param kid - The verification method id
 * @param publicKeyJwk - The verification method public key JWK
 * @returns - The corresponding DID document
 */
export function createDidDocument(did: string, kid: string, publicKeyJwk: JWK) {
  return {
    "@context": [
      "https://www.w3.org/ns/did/v1",
      "https://w3id.org/security/suites/jws-2020/v1",
    ],
    id: did,
    verificationMethod: [
      {
        id: kid,
        type: "JsonWebKey2020",
        controller: did,
        publicKeyJwk: publicKeyJwk as JsonWebKey,
      },
    ],
    authentication: [kid],
    assertionMethod: [kid],
  } satisfies DIDDocument;
}

export async function createLegalEntity(alg: "ES256" | "ES256K" | "EdDSA") {
  const did = util.createDid(randomBytes(16));
  const keypair = await generateKeyPair(alg);
  const publicKeyJwk = await exportJWK(keypair.publicKey);
  const privateKeyJwk = await exportJWK(keypair.privateKey);
  const thumbprint = await calculateJwkThumbprint(publicKeyJwk);
  const kid = `${did}#${thumbprint}`;

  const didDocument = createDidDocument(did, kid, publicKeyJwk);

  return {
    publicKeyJwk,
    privateKeyJwk,
    alg,
    did,
    kid,
    didDocument,
  };
}

/**
 * Helper function to create the Token Request query params
 *
 * @param client - The client props
 * @param aud - The JWT audience
 * @param customQueryParams - The query params to include in the request JWT
 * @returns The query params for the /token endpoint
 */
export async function createTokenRequestParams(
  client: ClientProps,
  aud: string,
  code: string,
  {
    customQueryParams,
    customJwtPayload,
    customJwtHeaderParameters,
    alg = "ES256",
  }: {
    customQueryParams?: Partial<PostTokenClientAssertionDto>;
    customJwtPayload?: Record<string, unknown>;
    customJwtHeaderParameters?: Partial<JWTHeaderParameters>;
    alg?: string;
  } = {}
): Promise<WritableDeep<PostTokenClientAssertionDto>> {
  const defaultQueryParams: Omit<
    PostTokenClientAssertionDto,
    "client_assertion"
  > = {
    grant_type: "authorization_code",
    code,
    client_id: client.clientId,
    client_assertion_type:
      "urn:ietf:params:oauth:client-assertion-type:jwt-bearer",
  };

  const queryParams = {
    ...defaultQueryParams,
    ...customQueryParams,
  };

  const jwtPayload = {
    ...queryParams,
    ...customJwtPayload,
  };

  const clientPrivateKey = await importJWK(client.privateKeyJwk, alg);
  const clientAssertion = await new SignJWT(jwtPayload)
    .setProtectedHeader({
      typ: "JWT",
      alg,
      kid: client.publicKeyJwk.kid,
      ...customJwtHeaderParameters,
    })
    .setAudience(aud)
    .setIssuer(queryParams.client_id)
    .setSubject(queryParams.client_id)
    .setIssuedAt()
    .setExpirationTime("5m")
    .sign(clientPrivateKey);

  return {
    ...queryParams,
    client_assertion: clientAssertion,
  };
}

/**
 * Mock DIDR and TIR responses when requesting records related to the Mock Issuer.
 */
export async function mockConformanceIssuerRecords(
  configService: ConfigService<ApiConfig, true>
) {
  const issuerMockKid = configService.get<string>("issuerMockKid");
  const [issuerMockDid] = issuerMockKid.split("#") as [string];
  const issuerMockAccreditationUrl = new URL(
    configService.get<string>("issuerMockAccreditationUrl")
  );
  const issuerMockKeyPair = await getKeyPair(
    configService.get<string>("issuerMockPrivateKey")
  );

  const didDoc = createDidDocument(
    issuerMockDid,
    issuerMockKid,
    issuerMockKeyPair.publicKeyJwk
  );

  // DID document
  nock(configService.get<string>("domain"))
    .get(`${DIDR_API_PATH}/${issuerMockDid}`)
    .reply(200, didDoc)
    .persist();

  // TI record
  nock(configService.get<string>("domain"))
    .get(`${TIR_API_PATH}/${issuerMockDid}`)
    .reply(200, {})
    .persist();

  // PDA1 TSR record
  const pda1CredentialSchema = new URL(
    configService.get<string>("pda1CredentialSchema")
  );
  nock(pda1CredentialSchema.origin)
    .get(pda1CredentialSchema.pathname)
    .reply(200, {
      $schema: "https://json-schema.org/draft/2020-12/schema",
      title: "ESSP Portable Document A1",
      description: "Schema for the PDA1",
      type: "object",
      allOf: [
        {
          $ref: "0x23039e6356ea6b703ce672e7cfac0b42765b150f63df78e2bd18ae785787f6a2", // EBSI VAttestation ID
        },
        {
          properties: {
            credentialSubject: {
              description:
                "Defines additional information about the subject that is described by the Verifiable Accreditation",
              type: "object",
              properties: {
                id: {
                  description:
                    "Defines the DID of the subject that is described by the Verifiable Attestation",
                  type: "string",
                  format: "uri",
                },
                section1: {
                  type: "object",
                  unevaluatedProperties: false,
                  properties: {
                    personalIdentificationNumber: {
                      $ref: "#/$defs/PDA1PINType",
                    },
                    sex: { $ref: "#/$defs/PDA1SexType" },
                    surname: { $ref: "#/$defs/PDA1PersonNameType" },
                    forenames: { $ref: "#/$defs/PDA1PersonNameType" },
                    surnameAtBirth: { $ref: "#/$defs/PDA1PersonNameType" },
                    dateBirth: { $ref: "#/$defs/PDA1BusinessDateType" },
                    nationalities: {
                      type: "array",
                      items: { $ref: "#/$defs/PDA1NationalityType" },
                    },
                    placeBirth: { $ref: "#/$defs/PDA1PlaceBirthType" },
                    stateOfResidenceAddress: {
                      $ref: "#/$defs/PDA1AddressType",
                    },
                    stateOfStayAddress: { $ref: "#/$defs/PDA1AddressType" },
                  },
                  required: [],
                },
                section2: {
                  type: "object",
                  unevaluatedProperties: false,
                  properties: {
                    memberStateWhichLegislationApplies: {
                      $ref: "#/$defs/PDA1EUEFTACountryType",
                    },
                    startingDate: { $ref: "#/$defs/PDA1BusinessDateType" },
                    endingDate: { $ref: "#/$defs/PDA1BusinessDateType" },
                    certificateForDurationActivity: { type: "boolean" },
                    determinationProvisional: { type: "boolean" },
                    transitionRulesApplyAsEC8832004: { type: "boolean" },
                  },
                  required: [],
                },
                section3: {
                  type: "object",
                  unevaluatedProperties: false,
                  properties: {
                    postedEmployedPerson: { type: "boolean" },
                    employedTwoOrMoreStates: { type: "boolean" },
                    postedSelfEmployedPerson: { type: "boolean" },
                    selfEmployedTwoOrMoreStates: { type: "boolean" },
                    civilServant: { type: "boolean" },
                    contractStaff: { type: "boolean" },
                    mariner: { type: "boolean" },
                    employedAndSelfEmployed: { type: "boolean" },
                    civilAndEmployedSelfEmployed: { type: "boolean" },
                    flightCrewMember: { type: "boolean" },
                    exception: { type: "boolean" },
                    exceptionDescription: {
                      $ref: "#/$defs/PDA1MediumStringType",
                    },
                    workingInStateUnder21: { type: "boolean" },
                  },
                  required: [],
                },
                section4: {
                  type: "object",
                  unevaluatedProperties: false,
                  properties: {
                    employee: { type: "boolean" },
                    selfEmployedActivity: { type: "boolean" },
                    employerSelfEmployedActivityCodes: {
                      type: "array",
                      items: { $ref: "#/$defs/PDA1TinyStringType" },
                    },
                    nameBusinessName: { $ref: "#/$defs/PDA1MediumStringType" },
                    registeredAddress: { $ref: "#/$defs/PDA1AddressType" },
                  },
                  required: [],
                },
                section5: {
                  type: "object",
                  unevaluatedProperties: false,
                  properties: {
                    workPlaceNames: {
                      type: "array",
                      items: { $ref: "#/$defs/PDA1WorkPlaceNameType" },
                    },
                    workPlaceNamesBlob: {
                      $ref: "#/$defs/PDA1VeryLongStringType",
                    },
                    workPlaceAddresses: {
                      type: "array",
                      items: { $ref: "#/$defs/PDA1WorkPlaceAddressType" },
                    },
                    workPlaceAddressesBlob: {
                      $ref: "#/$defs/PDA1VeryLongStringType",
                    },
                    noFixedAddress: { type: "boolean" },
                    noFixedAddressDescription: {
                      $ref: "#/$defs/PDA1MediumStringType",
                    },
                  },
                  required: [],
                },
                section6: {
                  type: "object",
                  unevaluatedProperties: false,
                  properties: {
                    name: { $ref: "#/$defs/PDA1MediumStringType" },
                    address: { $ref: "#/$defs/PDA1AddressType" },
                    institutionID: { $ref: "#/$defs/PDA1PINType" },
                    officeFaxNo: { $ref: "#/$defs/PDA1ShortStringType" },
                    officePhoneNo: { $ref: "#/$defs/PDA1ShortStringType" },
                    email: { $ref: "#/$defs/PDA1MediumStringType" },
                    date: { $ref: "#/$defs/PDA1BusinessDateType" },
                    signature: { $ref: "#/$defs/PDA1MediumStringType" },
                  },
                  required: [],
                },
              },
              required: [
                "id",
                "section1",
                "section2",
                "section3",
                "section4",
                "section5",
                "section6",
              ],
            },
          },
          required: ["credentialSubject"],
        },
      ],
      $defs: {
        PDA1SexType: { enum: ["01", "02", "98", "99"] },
        PDA1BusinessDateType: { type: "string", format: "date" },
        PDA1NationalityType: {
          enum: [
            "AT",
            "BE",
            "BG",
            "HR",
            "CY",
            "CZ",
            "DK",
            "EE",
            "FI",
            "FR",
            "DE",
            "EL",
            "HU",
            "IS",
            "IE",
            "IT",
            "LV",
            "LI",
            "LT",
            "LU",
            "MT",
            "NL",
            "NO",
            "PL",
            "PT",
            "RO",
            "SK",
            "SI",
            "ES",
            "SE",
            "CH",
            "UK",
            "XR",
            "XS",
            "XU",
            "AF",
            "AL",
            "DZ",
            "AD",
            "AO",
            "AG",
            "AR",
            "AM",
            "AU",
            "AZ",
            "BS",
            "BH",
            "BD",
            "BB",
            "BY",
            "BZ",
            "BJ",
            "BT",
            "BO",
            "BA",
            "BW",
            "BR",
            "BN",
            "BF",
            "BI",
            "KH",
            "CM",
            "CA",
            "CV",
            "CF",
            "TD",
            "CL",
            "CN",
            "CO",
            "KM",
            "CG",
            "CD",
            "CR",
            "CI",
            "CU",
            "DJ",
            "DM",
            "DO",
            "EC",
            "EG",
            "SV",
            "GQ",
            "ER",
            "ET",
            "FJ",
            "GA",
            "GM",
            "GE",
            "GH",
            "GD",
            "GT",
            "GN",
            "GW",
            "GY",
            "HT",
            "VA",
            "HN",
            "IN",
            "ID",
            "IR",
            "IQ",
            "IL",
            "JM",
            "JP",
            "JO",
            "KZ",
            "KE",
            "KI",
            "KP",
            "KR",
            "KW",
            "KG",
            "LA",
            "LB",
            "LS",
            "LR",
            "LY",
            "MK",
            "MG",
            "MW",
            "MY",
            "MV",
            "ML",
            "MH",
            "MR",
            "MU",
            "MX",
            "FM",
            "MD",
            "MC",
            "MN",
            "ME",
            "MA",
            "MZ",
            "MM",
            "NA",
            "NR",
            "NP",
            "NZ",
            "NI",
            "NE",
            "NG",
            "OM",
            "PK",
            "PW",
            "PS",
            "PA",
            "PG",
            "PY",
            "PE",
            "PH",
            "QA",
            "RU",
            "RW",
            "KN",
            "LC",
            "VC",
            "WS",
            "SM",
            "ST",
            "SA",
            "SN",
            "RS",
            "SC",
            "SL",
            "SG",
            "SB",
            "SO",
            "ZA",
            "SS",
            "LK",
            "SD",
            "SR",
            "SZ",
            "SY",
            "TJ",
            "TZ",
            "TH",
            "TL",
            "TG",
            "TO",
            "TT",
            "TN",
            "TR",
            "TM",
            "TV",
            "UG",
            "UA",
            "AE",
            "US",
            "UY",
            "UZ",
            "VU",
            "VE",
            "VN",
            "YE",
            "ZM",
            "ZW",
            "BQAQ",
            "BUMM",
            "BYAA",
            "CTKI",
            "CSHH",
            "DYBJ",
            "NQAQ",
            "TPTL",
            "FXFR",
            "AIDJ",
            "FQHH",
            "DDDE",
            "GEHH",
            "JTUM",
            "MIUM",
            "ANHH",
            "NTHH",
            "NHVU",
            "PCHH",
            "PZPA",
            "CSXX",
            "SKIN",
            "RHZW",
            "HVBF",
            "PUUM",
            "SUHH",
            "VDVN",
            "WKUM",
            "YDYE",
            "YUCS",
            "ZRCD",
          ],
        },
        PDA1TinyStringType: { type: "string", minLength: 0, maxLength: 25 },
        PDA1ShortStringType: { type: "string", minLength: 0, maxLength: 65 },
        PDA1MediumStringType: { type: "string", minLength: 0, maxLength: 155 },
        PDA1VeryLongStringType: {
          type: "string",
          minLength: 0,
          maxLength: 500,
        },
        PDA1PINType: { $ref: "#/$defs/PDA1ShortStringType" },
        PDA1PersonNameType: { $ref: "#/$defs/PDA1MediumStringType" },
        PDA1EUEFTACountryType: {
          enum: [
            "AT",
            "BE",
            "BG",
            "HR",
            "CY",
            "CZ",
            "DK",
            "EE",
            "FI",
            "FR",
            "DE",
            "EL",
            "HU",
            "IS",
            "IE",
            "IT",
            "LV",
            "LI",
            "LT",
            "LU",
            "MT",
            "NL",
            "NO",
            "PL",
            "PT",
            "RO",
            "SK",
            "SI",
            "ES",
            "SE",
            "CH",
            "UK",
          ],
        },
        PDA1PlaceBirthType: {
          type: "object",
          unevaluatedProperties: false,
          properties: {
            town: { $ref: "#/$defs/PDA1ShortStringType" },
            region: { $ref: "#/$defs/PDA1ShortStringType" },
            countryCode: { $ref: "#/$defs/PDA1EUEFTACountryType" },
          },
          required: [],
        },
        PDA1AddressType: {
          type: "object",
          unevaluatedProperties: false,
          properties: {
            buildingName: { $ref: "#/$defs/PDA1MediumStringType" },
            streetNo: { $ref: "#/$defs/PDA1MediumStringType" },
            postCode: { $ref: "#/$defs/PDA1TinyStringType" },
            town: { $ref: "#/$defs/PDA1ShortStringType" },
            region: { $ref: "#/$defs/PDA1ShortStringType" },
            countryCode: { $ref: "#/$defs/PDA1EUEFTACountryType" },
          },
          required: [],
        },
        PDA1WorkPlaceNameType: {
          type: "object",
          unevaluatedProperties: false,
          properties: {
            seqno: { type: "number" },
            flagStatehomeBase: { $ref: "#/$defs/PDA1MediumStringType" },
            companyNameVesselName: { $ref: "#/$defs/PDA1MediumStringType" },
          },
          required: [],
        },
        PDA1WorkPlaceAddressType: {
          type: "object",
          unevaluatedProperties: false,
          properties: {
            seqno: { type: "number" },
            address: { $ref: "#/$defs/PDA1AddressType" },
          },
          required: [],
        },
      },
    })
    .persist();

  // Self-Accreditation
  const authorisationCredentialSchema = configService.get<string>(
    "authorisationCredentialSchema"
  );
  const iat = Math.round(Date.now() / 1000);
  const exp = iat + 365 * 24 * 3600;
  const jti = `urn:uuid:${randomUUID()}`;
  const issuanceDate = new Date(iat * 1000).toISOString();
  const expirationDate = new Date(exp * 1000).toISOString();
  const accreditation = {
    "@context": ["https://www.w3.org/2018/credentials/v1"],
    type: [
      "VerifiableCredential",
      "VerifiableAttestation",
      "VerifiableAccreditation",
      "VerifiableAccreditationToAttest",
    ],
    id: jti,
    issuanceDate,
    expirationDate,
    issued: issuanceDate,
    validFrom: issuanceDate,
    validUntil: expirationDate,
    issuer: issuerMockDid,
    credentialSubject: {
      id: issuerMockDid,
      accreditedFor: [
        {
          schemaId: authorisationCredentialSchema,
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationForTrustChain",
          ],
          limitJurisdiction:
            "https://publications.europa.eu/resource/authority/atu/FIN",
        },
      ],
    },
    credentialSchema: {
      id: authorisationCredentialSchema,
      type: "FullJsonSchemaValidator2021",
    },
  } satisfies EbsiVerifiableAttestation;

  const accreditationVcJwt = await new SignJWT({
    iat,
    jti,
    nbf: iat,
    exp,
    sub: accreditation.credentialSubject.id,
    iss: accreditation.issuer,
    vc: accreditation,
  })
    .setProtectedHeader({
      alg: "ES256",
      typ: "JWT",
      kid: issuerMockKid,
    })
    .sign(await importJWK(issuerMockKeyPair.privateKeyJwk));

  nock(issuerMockAccreditationUrl.origin)
    .get(issuerMockAccreditationUrl.pathname)
    .reply(200, {
      attribute: {
        body: accreditationVcJwt,
      },
    })
    .persist();

  // JSONRPC TIR
  nock(configService.get<string>("domain"))
    .post(
      TIR_JSON_RPC_PATH,
      (body: { method: string }) => body.method === "setAttributeMetadata"
    )
    .reply(200, {
      result: {
        to: "0x320fadf6AE3336AEf12e899cdFff486400C51C92",
        data: "0x00",
        value: "0x00",
        nonce: "0x01",
        chainId: "0x01",
        gasLimit: "0xffffff",
        gasPrice: "0x00",
      },
    })
    .persist();

  nock(configService.get<string>("domain"))
    .post(
      TIR_JSON_RPC_PATH,
      (body: { method: string }) => body.method === "sendSignedTransaction"
    )
    .reply(200, { result: "0x" })
    .persist();

  nock(configService.get<string>("domain"))
    .post(
      LEDGER_API_PATH,
      (body: { method: string }) => body.method === "eth_getTransactionReceipt"
    )
    .reply(200, { result: { status: "0x1" } })
    .persist();
}

// Context: "https://www.w3.org/ns/did/v1"
export const DID_DOCUMENT_CONTEXT = {
  "@context": {
    "@protected": true,
    id: "@id",
    type: "@type",
    alsoKnownAs: {
      "@id": "https://www.w3.org/ns/activitystreams#alsoKnownAs",
      "@type": "@id",
    },
    assertionMethod: {
      "@id": "https://w3id.org/security#assertionMethod",
      "@type": "@id",
      "@container": "@set",
    },
    authentication: {
      "@id": "https://w3id.org/security#authenticationMethod",
      "@type": "@id",
      "@container": "@set",
    },
    capabilityDelegation: {
      "@id": "https://w3id.org/security#capabilityDelegationMethod",
      "@type": "@id",
      "@container": "@set",
    },
    capabilityInvocation: {
      "@id": "https://w3id.org/security#capabilityInvocationMethod",
      "@type": "@id",
      "@container": "@set",
    },
    controller: {
      "@id": "https://w3id.org/security#controller",
      "@type": "@id",
    },
    keyAgreement: {
      "@id": "https://w3id.org/security#keyAgreementMethod",
      "@type": "@id",
      "@container": "@set",
    },
    service: {
      "@id": "https://www.w3.org/ns/did#service",
      "@type": "@id",
      "@context": {
        "@protected": true,
        id: "@id",
        type: "@type",
        serviceEndpoint: {
          "@id": "https://www.w3.org/ns/did#serviceEndpoint",
          "@type": "@id",
        },
      },
    },
    verificationMethod: {
      "@id": "https://w3id.org/security#verificationMethod",
      "@type": "@id",
    },
  },
};

// Context "https://w3id.org/security/suites/jws-2020/v1"
export const JWS_2020_CONTEXT = {
  "@context": {
    privateKeyJwk: {
      "@id": "https://w3id.org/security#privateKeyJwk",
      "@type": "@json",
    },
    JsonWebKey2020: {
      "@id": "https://w3id.org/security#JsonWebKey2020",
      "@context": {
        "@protected": true,
        id: "@id",
        type: "@type",
        publicKeyJwk: {
          "@id": "https://w3id.org/security#publicKeyJwk",
          "@type": "@json",
        },
      },
    },
    JsonWebSignature2020: {
      "@id": "https://w3id.org/security#JsonWebSignature2020",
      "@context": {
        "@protected": true,
        id: "@id",
        type: "@type",
        challenge: "https://w3id.org/security#challenge",
        created: {
          "@id": "http://purl.org/dc/terms/created",
          "@type": "http://www.w3.org/2001/XMLSchema#dateTime",
        },
        domain: "https://w3id.org/security#domain",
        expires: {
          "@id": "https://w3id.org/security#expiration",
          "@type": "http://www.w3.org/2001/XMLSchema#dateTime",
        },
        jws: "https://w3id.org/security#jws",
        nonce: "https://w3id.org/security#nonce",
        proofPurpose: {
          "@id": "https://w3id.org/security#proofPurpose",
          "@type": "@vocab",
          "@context": {
            "@protected": true,
            id: "@id",
            type: "@type",
            assertionMethod: {
              "@id": "https://w3id.org/security#assertionMethod",
              "@type": "@id",
              "@container": "@set",
            },
            authentication: {
              "@id": "https://w3id.org/security#authenticationMethod",
              "@type": "@id",
              "@container": "@set",
            },
            capabilityInvocation: {
              "@id": "https://w3id.org/security#capabilityInvocationMethod",
              "@type": "@id",
              "@container": "@set",
            },
            capabilityDelegation: {
              "@id": "https://w3id.org/security#capabilityDelegationMethod",
              "@type": "@id",
              "@container": "@set",
            },
            keyAgreement: {
              "@id": "https://w3id.org/security#keyAgreementMethod",
              "@type": "@id",
              "@container": "@set",
            },
          },
        },
        verificationMethod: {
          "@id": "https://w3id.org/security#verificationMethod",
          "@type": "@id",
        },
      },
    },
  },
};

/**
 * Mock JSON-LD and JSON schemas
 */
export function mockSchemas() {
  nock("https://www.w3.org")
    .get("/ns/did/v1")
    .reply(200, DID_DOCUMENT_CONTEXT)
    .persist();

  nock("https://w3id.org")
    .get("/security/suites/jws-2020/v1")
    .reply(200, JWS_2020_CONTEXT)
    .persist();
}

/**
 * Change 1 bit in a byte array at a specific position
 *
 * @param list - The byte array to modify
 * @param pos - The position of the bit to change
 * @param value - The value to set the bit to ("0" or "1")
 */
export function setBit(list: Uint8Array, pos: number, value: "0" | "1") {
  const bytePosition = Math.floor(pos / 8);
  const bitPosition = pos % 8;

  const byteValue = list[bytePosition];
  if (byteValue === undefined) {
    throw new Error(`Byte at position ${bytePosition} does not exist`);
  }

  const byteAsBitstring = byteValue.toString(2).padStart(8, "0");
  const bitstringStart = byteAsBitstring.substring(0, bitPosition);
  const bitstringEnd = byteAsBitstring.substring(bitPosition + 1);
  const updatedByteAsBitstring = `${bitstringStart}${value}${bitstringEnd}`;

  // eslint-disable-next-line no-param-reassign
  list[bytePosition] = parseInt(updatedByteAsBitstring, 2);
}

/**
 * Encode byte array list according to https://w3c.github.io/vc-status-list-2021/#statuslist2021credential
 *
 * @param list - the byte array to encode
 * @returns the encoded string
 */
export function encodeList(list: Uint8Array) {
  return base64.baseEncode(gzip(list));
}

/**
 * Create logs response as returned by Loki.
 */
export function createLokiLogsResponse(
  did: string,
  clientId: string,
  logs: {
    intent: (typeof INTENTS_LIST)[number];
    result: CheckResult;
  }[]
) {
  const formattedLogs = logs.map((log) => {
    const formattedLog = `{"intent": "${log.intent}", "data": ${JSON.stringify({
      did,
      clientId,
    })}, "result": ${JSON.stringify(log.result)} }`;

    return {
      values: [
        [
          "1660734188205405478",
          `\u001b[32m[Conformance API v3]\u001b[39m \u001b[33mInfo\u001b[39m\\t4/7/2023, 11:28:28 AM \u001b[33m[CheckService]\u001b[39m \u001b[32mTest Data ${formattedLog} End Test Data\u001b[39m - {}`,
        ],
      ],
    } satisfies LokiLog;
  });
  const lokiLogsResponse: LokiLogResponse = { data: { result: formattedLogs } };
  return lokiLogsResponse;
}
