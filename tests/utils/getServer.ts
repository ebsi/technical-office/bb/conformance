import type { HttpServer, INestApplication } from "@nestjs/common";
import type { ConfigService } from "@nestjs/config";
import type { ApiConfig } from "../../src/config/configuration";

export const getServer = async (
  app: INestApplication,
  configService: ConfigService<ApiConfig, true>,
  options?: { conformanceDomain: string }
) => {
  if (process.env.TEST_ENV === "remote") {
    await app.close();

    const conformanceDomain = configService.get<string>("conformanceDomain");
    return conformanceDomain;
  }

  if (options) {
    if (!/http:\/\/localhost:([0-9]+)$/g.test(options.conformanceDomain)) {
      throw new Error("CONFORMANCE_DOMAIN must be http://localhost:<PORT>");
    }
    const port = Number(options.conformanceDomain.split(":")[2]);
    await app.listen(port);
  }
  return app.getHttpServer() as HttpServer;
};

export default getServer;
