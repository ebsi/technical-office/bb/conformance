import type { TestingModule } from "@nestjs/testing";
import { FastifyAdapter } from "@nestjs/platform-fastify";
import type { NestFastifyApplication } from "@nestjs/platform-fastify";
import { ValidationPipe } from "@nestjs/common";
import { fastifyFormbody } from "@fastify/formbody";
import { fastifyHelmet } from "@fastify/helmet";
import qs from "qs";
import { AllExceptionsFilter } from "../../src/filters/http-exception.filter";

/**
 * Configure Nest Fastify app with all the parsers, filters, and validation pipes.
 * /!\ Must be aligned with src/main.ts.
 */
export async function configureApp(
  moduleFixture: TestingModule,
  options = { apiUrlPrefix: "" }
) {
  const querystringParser = (str: string) =>
    qs.parse(str, {
      // Parse up to 50 children deep
      depth: 50,
      // Parse up to 1000 parameters
      parameterLimit: 1000,
    });

  const fastifyAdapter = new FastifyAdapter({
    querystringParser, // Replace default querystring parser. See https://www.fastify.io/docs/latest/Reference/Server/#querystringparser
    maxParamLength: 300, // Increase max param length for key DIDs. See https://www.fastify.io/docs/latest/Reference/Server/#maxparamlength
  });

  fastifyAdapter.enableCors({ methods: "*" });

  // Register "application/x-www-form-urlencoded" parser
  // @ts-expect-error - some types mismatch
  await fastifyAdapter.register(fastifyFormbody, {
    parser: querystringParser,
  });

  const app = moduleFixture.createNestApplication<NestFastifyApplication>(
    fastifyAdapter,
    { bodyParser: false }
  );

  app.enableShutdownHooks();

  if (options.apiUrlPrefix) {
    app.setGlobalPrefix(options.apiUrlPrefix);
  }

  await app.register(fastifyHelmet);

  app.useGlobalFilters(new AllExceptionsFilter());
  app.useGlobalPipes(
    new ValidationPipe({ transform: true, stopAtFirstError: true })
  );

  return app;
}

export default configureApp;
