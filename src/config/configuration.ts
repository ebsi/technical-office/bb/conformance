import type { CacheManagerOptions } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import Joi from "joi";
import { getDockerTag } from "../shared/utils";

// List here all the values that will be returned by the config factory
export interface ApiConfig {
  apiPort: number;
  apiUrlPrefix: string;
  domain: string;
  conformanceDomain: string;
  localOrigin: string;
  logLevel: "silent" | "error" | "warn" | "info" | "verbose" | "debug";
  externalEbsiApiHealthCheck: string;
  requestTimeout: number;
  authMockPrivateKey: string;
  testClientMockPrivateKey: string;
  testClientMockKid: string;
  testClientMockAccreditationUrl: string;
  testClientMockProxyUrl: string;
  issuerMockPrivateKey: string;
  issuerMockKid: string;
  issuerMockAccreditationUrl: string;
  issuerMockProxyUrl: string;
  didRegistryApiUrl: string;
  didRegistryApiJsonrpcUrl: string;
  ledgerApiUrl: string;
  authorisationApiUrl: string;
  trustedIssuersRegistryApiUrl: string;
  trustedIssuersRegistryApiJsonrpcUrl: string;
  trustedPoliciesRegistryApiUrl: string;
  trustedAppsRegistryApiUrl: string;
  authorisationCredentialSchema: string;
  pda1CredentialSchema: string;
  lokiUrl: string;
  lokiAuthToken: string;
  lokiLogsLifetime: number;
  statusList2021CredentialSchemaUrl: string;
  dockerContainerTag: string;
}

const HEALTH_CHECK_PATH = "/docs/";

export const DIDR_API_PATH = "/did-registry/v4/identifiers";
export const DIDR_JSON_RPC_PATH = "/did-registry/v4/jsonrpc";
export const LEDGER_API_PATH = "/ledger/v3/blockchains/besu";
export const TIR_API_PATH = "/trusted-issuers-registry/v4/issuers";
export const TIR_JSON_RPC_PATH = "/trusted-issuers-registry/v4/jsonrpc";
export const TAR_API_PATH = "/trusted-apps-registry/v3/apps";
export const TPR_API_PATH = "/trusted-policies-registry/v2/users";
export const TSR_API_PATH = "/trusted-schemas-registry/v2/schemas";
export const AUTH_API_PATH = "/authorisation/v3";

// Config factory
// Note that process.env — for which provide typings in src/environment.d.ts —
// should have already been validated by Joi in src/app.module.ts
export const loadConfig = (): ApiConfig => {
  const { EBSI_ENV, DOMAIN } = process.env;
  const dockerContainerTag = getDockerTag(EBSI_ENV);

  return {
    apiPort: parseInt(process.env.API_PORT || "3000", 10),
    apiUrlPrefix: process.env.API_URL_PREFIX || "/conformance/v3",
    domain: DOMAIN,
    conformanceDomain: process.env.CONFORMANCE_DOMAIN || DOMAIN,
    localOrigin: process.env.LOCAL_ORIGIN || "",
    logLevel: process.env.LOG_LEVEL || "info",
    externalEbsiApiHealthCheck: DOMAIN + HEALTH_CHECK_PATH,
    requestTimeout: parseInt(process.env.REQUEST_TIMEOUT || "30000", 10),
    authMockPrivateKey: process.env.AUTH_MOCK_PRIVATE_KEY,
    testClientMockPrivateKey: process.env.TEST_CLIENT_MOCK_PRIVATE_KEY || "",
    testClientMockKid: process.env.TEST_CLIENT_MOCK_KID || "",
    testClientMockAccreditationUrl:
      process.env.TEST_CLIENT_MOCK_ACCREDITATION_URL || "",
    testClientMockProxyUrl: process.env.TEST_CLIENT_MOCK_PROXY_URL || "",
    issuerMockPrivateKey: process.env.ISSUER_MOCK_PRIVATE_KEY,
    issuerMockKid: process.env.ISSUER_MOCK_KID,
    issuerMockAccreditationUrl: process.env.ISSUER_MOCK_ACCREDITATION_URL,
    issuerMockProxyUrl: process.env.ISSUER_MOCK_PROXY_URL,
    authorisationApiUrl: DOMAIN + AUTH_API_PATH,
    didRegistryApiUrl: DOMAIN + DIDR_API_PATH,
    didRegistryApiJsonrpcUrl: DOMAIN + DIDR_JSON_RPC_PATH,
    ledgerApiUrl: DOMAIN + LEDGER_API_PATH,
    trustedIssuersRegistryApiUrl: DOMAIN + TIR_API_PATH,
    trustedIssuersRegistryApiJsonrpcUrl: DOMAIN + TIR_JSON_RPC_PATH,
    trustedPoliciesRegistryApiUrl: DOMAIN + TPR_API_PATH,
    trustedAppsRegistryApiUrl: DOMAIN + TAR_API_PATH,
    authorisationCredentialSchema: `${DOMAIN}${TSR_API_PATH}/${process.env.AUTHORISATION_CREDENTIAL_SCHEMA_ID}`,
    pda1CredentialSchema: `${DOMAIN}${TSR_API_PATH}/${process.env.PDA1_CREDENTIAL_SCHEMA_ID}`,
    lokiAuthToken: process.env.LOKI_AUTH_TOKEN,
    lokiUrl: process.env.LOKI_URL,
    lokiLogsLifetime: parseInt(process.env.LOKI_LOGS_LIFETIME, 10),
    statusList2021CredentialSchemaUrl: `${DOMAIN}${TSR_API_PATH}/${process.env.STATUSLIST2021_CREDENTIAL_SCHEMA_ID}`,
    dockerContainerTag,
  };
};

export const ApiConfigModule = ConfigModule.forRoot({
  envFilePath: [
    `.env.${process.env.NODE_ENV}.local`,
    `.env.${process.env.NODE_ENV}`,
    ".env.local",
    ".env",
  ],
  load: [loadConfig],
  validationSchema: Joi.object({
    // Common API variables
    NODE_ENV: Joi.string()
      .valid("development", "production", "test")
      .default("development"),
    EBSI_ENV: Joi.string().valid("test", "conformance").required(),
    API_PORT: Joi.string().default("3000"),
    API_URL_PREFIX: Joi.string(),
    LOG_LEVEL: Joi.string().valid(
      "silent",
      "error",
      "warn",
      "info",
      "verbose",
      "debug"
    ),
    DOMAIN: Joi.string().uri().required(),
    LOCAL_ORIGIN: Joi.string().uri(),
    HEALTH_CHECK: Joi.string(),
    REQUEST_TIMEOUT: Joi.string(),
    // Variables specific to Conformance API
    CONFORMANCE_DOMAIN: Joi.string().uri(),
    AUTH_MOCK_PRIVATE_KEY: Joi.string().required(),
    TEST_CLIENT_MOCK_PRIVATE_KEY: Joi.string(),
    TEST_CLIENT_MOCK_KID: Joi.string(),
    TEST_CLIENT_MOCK_ACCREDITATION_URL: Joi.string(),
    TEST_CLIENT_MOCK_PROXY_URL: Joi.string(),
    ISSUER_MOCK_PRIVATE_KEY: Joi.string().required(),
    ISSUER_MOCK_KID: Joi.string().required(),
    ISSUER_MOCK_ACCREDITATION_URL: Joi.string().uri().required(),
    ISSUER_MOCK_PROXY_URL: Joi.string().uri().required(),
    AUTHORISATION_CREDENTIAL_SCHEMA_ID: Joi.string().required(),
    PDA1_CREDENTIAL_SCHEMA_ID: Joi.string().required(),
    LOKI_AUTH_TOKEN: Joi.string().required(),
    LOKI_URL: Joi.string().required(),
    LOKI_LOGS_LIFETIME: Joi.string().required(),
    STATUSLIST2021_CREDENTIAL_SCHEMA_ID: Joi.string().required(),
  }),
});

export const cacheConfig: CacheManagerOptions = {
  ttl: 0,
  max: 10_000,
};
