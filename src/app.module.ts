import { Module } from "@nestjs/common";
import { APP_INTERCEPTOR } from "@nestjs/core";
import { ApiConfigModule } from "./config/configuration";
import { HealthModule } from "./modules/health/health.module";
import { AuthMockModule } from "./modules/auth-mock/auth-mock.module";
import { IssuerMockModule } from "./modules/issuer-mock/issuer-mock.module";
import { ClientMockModule } from "./modules/client-mock/client-mock.module";
import { LoggingInterceptor } from "./interceptors/logging.interceptor";
import { VersionInterceptor } from "./interceptors/version.interceptor";
import { CheckModule } from "./modules/check/check.module";
import { LogsModule } from "./modules/logs/logs.module";

@Module({
  imports: [
    ApiConfigModule,
    HealthModule,
    AuthMockModule,
    IssuerMockModule,
    ClientMockModule,
    CheckModule,
    LogsModule,
  ],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: VersionInterceptor,
    },
  ],
})
export class AppModule {}

export default AppModule;
