import { createHash, randomUUID } from "node:crypto";
import { URLSearchParams } from "node:url";
import { util } from "@cef-ebsi/ebsi-did-resolver";
import type { Logger } from "@nestjs/common";
import axios from "axios";
import type { AxiosResponse } from "axios";
import axiosRetry from "axios-retry";
import qs from "qs";
import { SignJWT, decodeProtectedHeader, importJWK, jwtVerify } from "jose";
import type { ProtectedHeaderParameters } from "jose";
import { logAxiosRequestError } from "../../shared/credential-issuer";
import { formatZodError, getErrorMessage } from "../../shared/utils";
import type { KeyPair } from "../../shared/utils";
import {
  credentialResponseSchema,
  idTokenRequestJwtHeaderSchema,
  idTokenRequestSchema,
  openidConfigurationSchema,
  openidCredentialIssuerConfigSchema,
  tokenResponseSchema,
} from "./validators";
import type {
  GetAuthorizeHolderWallerDto,
  GetAuthorizeServiceWalletDto,
  PostTokenAuthorizationCodeDto,
  PostTokenClientAssertionDto,
  PostTokenPkceDto,
  PostTokenPreAuthorizedCodeDto,
} from "../../shared/auth-server";
import { jwksSchema } from "../../shared/validators";

/**
 * Compute sub-account DID from the given DID.
 */
export function getSubAccountDid(did: string) {
  const subaccountMsiBytes = createHash("sha256")
    .update(did, "utf8")
    .digest()
    .slice(0, 16);
  return util.createDid(subaccountMsiBytes);
}

/**
 * Helper function to deterministically compute the credential ID (used as key in the cache)
 */
export function getCredentialId(
  { clientId, did }: { clientId: string; did: string },
  type:
    | "CTRevocable"
    | "VerifiableAuthorisationToOnboard"
    | "VerifiableAccreditationToAttest"
    | "VerifiableAccreditationToAccredit"
    | "CTWalletSameInTime"
    | "CTWalletSameDeferred"
    | "CTWalletSamePreAuthorised"
) {
  return `credential::${clientId}::${did}::${type}`;
}

/**
 * Helper function to deterministically compute the deferred credential ID (used as key in the cache)
 */
export function getDeferredCredentialId(accessToken: string): string {
  return `deferred-credential::${accessToken}`;
}

/**
 * Helper function to request a Verifiable Credential from the Credential Issuer.
 * Used in:
 * - TI_REQUEST_CT_REVOCABLE
 * - TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT
 * - TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT
 * - TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT
 * - ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_IN_TIME
 */
export async function requestCredential(config: {
  clientAuthorizationEndpoint: string;
  clientId: string;
  clientMetadata: Record<string, unknown>;
  codeChallenge?: string;
  codeVerifier?: string;
  credentialIssuer: string;
  issuerState?: string;
  preAuthorizedCode?: string;
  userPin?: string;
  keyPair: KeyPair;
  logger: Logger;
  redirectUri: string;
  requestedTypes: string[];
  subjectDid: string;
  subjectKid: string;
  timeout: number;
}): Promise<
  | {
      success: false;
      errors: string[];
    }
  | { success: true; credential: string }
> {
  const {
    clientAuthorizationEndpoint,
    clientId,
    clientMetadata,
    codeChallenge,
    codeVerifier,
    credentialIssuer,
    issuerState,
    preAuthorizedCode,
    userPin,
    keyPair,
    logger,
    redirectUri,
    requestedTypes,
    subjectDid,
    subjectKid,
    timeout,
  } = config;

  // Send GET ${credentialIssuer}/.well-known/openid-credential-issuer and validate configuration
  let response: AxiosResponse<unknown>;
  try {
    response = await axios.get(
      `${credentialIssuer}/.well-known/openid-credential-issuer`,
      { timeout }
    );
  } catch (err) {
    logAxiosRequestError(err, logger);

    return {
      success: false,
      errors: [
        `Couldn't load client's /.well-known/openid-credential-issuer configuration: ${getErrorMessage(
          err
        )}`,
      ],
    };
  }

  // Validate config
  const parsedOpenidCredentialIssuerConfig =
    openidCredentialIssuerConfigSchema.safeParse(response.data);

  if (!parsedOpenidCredentialIssuerConfig.success) {
    return {
      success: false,
      errors: [
        "Client's openid-credential-issuer configuration is invalid.",
        ...formatZodError(parsedOpenidCredentialIssuerConfig.error),
      ],
    };
  }

  const openidCredentialIssuerConfig = parsedOpenidCredentialIssuerConfig.data;

  // Find the metadata related to the requested types
  const credentialMetadata =
    openidCredentialIssuerConfig.credentials_supported.find(({ types }) =>
      requestedTypes.every((type) => types.includes(type))
    );

  if (!credentialMetadata) {
    return {
      success: false,
      errors: [
        `Client's openid-credential-issuer configuration doesn't support ${JSON.stringify(
          requestedTypes
        )}.`,
      ],
    };
  }

  // 2. Get Authorization Server configuration
  const authorizationServer =
    openidCredentialIssuerConfig.authorization_server ??
    openidCredentialIssuerConfig.credential_issuer;

  try {
    response = await axios.get(
      `${authorizationServer}/.well-known/openid-configuration`,
      { timeout }
    );
  } catch (err) {
    logAxiosRequestError(err, logger);

    return {
      success: false,
      errors: [
        `Couldn't load Authorization Server's /.well-known/openid-configuration: ${getErrorMessage(
          err
        )}`,
      ],
    };
  }

  // Validate config
  const parsedOpenidConfiguration = openidConfigurationSchema.safeParse(
    response.data
  );

  if (!parsedOpenidConfiguration.success) {
    return {
      success: false,
      errors: [
        "Authorization Server openid-configuration is invalid.",
        ...formatZodError(parsedOpenidConfiguration.error),
      ],
    };
  }

  const openidConfiguration = parsedOpenidConfiguration.data;

  // Check if "code" flow is supported
  if (
    openidConfiguration.grant_types_supported &&
    !openidConfiguration.grant_types_supported.includes("authorization_code")
  ) {
    return {
      success: false,
      errors: ["Authorization Server must support Code flow."],
    };
  }

  const signingKey = await importJWK(keyPair.privateKeyJwk, "ES256");
  const isPreAuthorized = !!preAuthorizedCode;
  let tokenRequestPayload;

  if (!isPreAuthorized) {
    // 3. Authorization Code Flow
    // See https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-11.html#section-5.1
    const authorizationDetails = [
      {
        type: "openid_credential",
        ...(openidCredentialIssuerConfig.authorization_server
          ? { locations: [openidCredentialIssuerConfig.credential_issuer] }
          : {}),
        format: "jwt_vc",
        types: requestedTypes,
      },
    ];

    let authenticationRequestParams;

    if (clientId.startsWith("did:key")) {
      authenticationRequestParams = {
        scope: "openid",
        client_id: clientId,
        client_metadata: JSON.stringify(clientMetadata),
        authorization_details: JSON.stringify(authorizationDetails),
        redirect_uri: redirectUri,
        response_type: "code",
        state: randomUUID(),
        ...(issuerState && { issuer_state: issuerState }),
        ...(codeChallenge && {
          code_challenge: codeChallenge,
          code_challenge_method: "S256",
        }),
      } satisfies GetAuthorizeHolderWallerDto;
    } else {
      const queryParams = {
        scope: "openid",
        client_id: clientId,
        redirect_uri: redirectUri,
        response_type: "code",
        state: randomUUID(),
        ...(issuerState && { issuer_state: issuerState }),
      } as const;

      const authorizationRequestJwt = await new SignJWT({
        ...queryParams,
        authorization_details: authorizationDetails,
        client_metadata: clientMetadata,
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: subjectDid.startsWith("did:ebsi:")
            ? keyPair.publicKeyJwk.kid
            : subjectKid,
        })
        .setIssuer(clientId)
        .setAudience(authorizationServer)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(signingKey);

      authenticationRequestParams = {
        ...queryParams,
        request: authorizationRequestJwt,
      } satisfies GetAuthorizeServiceWalletDto;
    }

    // If "authorization_endpoint" is just the scheme, add "//" to make it a valid URL
    const sanitizedAuthorizationEndpoint =
      openidConfiguration.authorization_endpoint.endsWith(":")
        ? `${openidConfiguration.authorization_endpoint}//`
        : openidConfiguration.authorization_endpoint;

    try {
      response = await axios.get(
        `${sanitizedAuthorizationEndpoint}?${new URLSearchParams(
          authenticationRequestParams
        ).toString()}`,
        {
          timeout,
          maxRedirects: 0, // Don't follow redirects
          validateStatus: function validateStatus(status) {
            return status === 302; // Expect a redirect
          },
        }
      );
    } catch (err) {
      logAxiosRequestError(err, logger);

      if (
        axios.isAxiosError(err) &&
        err.response &&
        err.response.status >= 200 &&
        err.response.status < 300
      ) {
        return {
          success: false,
          errors: ["Authorization endpoint must redirect."],
        };
      }

      return {
        success: false,
        errors: [
          `Authorization endpoint returned an error: ${getErrorMessage(err)}`,
        ],
      };
    }

    // Expect response to be ID Token request:
    // HTTP/1.1 302 Found
    // Location: <this.clientAuthorizationEndpoint>?<ID Token Request>

    // Parse response
    const { location: authenticationRequestUri } = response.headers;
    if (
      !authenticationRequestUri ||
      typeof authenticationRequestUri !== "string" ||
      !authenticationRequestUri.startsWith(clientAuthorizationEndpoint)
    ) {
      return {
        success: false,
        errors: [
          `Authorization endpoint's response doesn't redirect to the client authorization endpoint ${clientAuthorizationEndpoint}. Received ${
            authenticationRequestUri as string
          }`,
        ],
      };
    }

    const parsedIdTokenRequest = idTokenRequestSchema.safeParse(
      qs.parse(new URL(authenticationRequestUri).search.slice(1))
    );

    if (!parsedIdTokenRequest.success) {
      return {
        success: false,
        errors: [
          "Authorization endpoint's response doesn't contain a valid ID Token request",
          ...formatZodError(parsedIdTokenRequest.error),
        ],
      };
    }

    const idTokenRequest = parsedIdTokenRequest.data;

    if (!idTokenRequest.request && !idTokenRequest.request_uri) {
      return {
        success: false,
        errors: [
          "Authorization endpoint's response doesn't contain request nor request_uri",
        ],
      };
    }

    let idTokenRequestJwt = idTokenRequest.request;

    if (!idTokenRequestJwt) {
      // Fetch request from request_uri
      const idTokenRequestUri = idTokenRequest.request_uri as string;
      try {
        response = await axios.get(idTokenRequestUri, {
          timeout,
        });
      } catch (err) {
        logAxiosRequestError(err, logger);

        return {
          success: false,
          errors: [`Couldn't fetch ID Token request: ${getErrorMessage(err)}`],
        };
      }

      if (!response.data) {
        return {
          success: false,
          errors: ["Invalid empty ID Token request URI response."],
        };
      }

      if (typeof response.data !== "string") {
        return {
          success: false,
          errors: [
            `Invalid ID Token request URI response. Expected: JWT. Received: ${typeof response.data}`,
          ],
        };
      }

      idTokenRequestJwt = response.data;
    }

    let unsafeIdTokenRequestJwtHeader: ProtectedHeaderParameters;
    try {
      unsafeIdTokenRequestJwtHeader = decodeProtectedHeader(idTokenRequestJwt);
    } catch (err) {
      return {
        success: false,
        errors: [
          `Couldn't decode ID Token request header: ${
            err instanceof Error ? err.message : "Unknown error"
          }`,
        ],
      };
    }

    const parsedIdTokenRequestJwtHeader =
      idTokenRequestJwtHeaderSchema.safeParse(unsafeIdTokenRequestJwtHeader);

    if (!parsedIdTokenRequestJwtHeader.success) {
      return {
        success: false,
        errors: [
          "ID Token request doesn't contain a valid header",
          ...formatZodError(parsedIdTokenRequestJwtHeader.error),
        ],
      };
    }

    const idTokenRequestJwtHeader = parsedIdTokenRequestJwtHeader.data;

    // Get Authorization Server's public key
    try {
      response = await axios.get(openidConfiguration.jwks_uri, {
        timeout,
      });
    } catch (err) {
      logAxiosRequestError(err, logger);

      return {
        success: false,
        errors: [
          `Couldn't fetch Authorization Server's jwks_uri: ${getErrorMessage(
            err
          )}`,
        ],
      };
    }

    const parsedAuthorisationServerJwks = jwksSchema.safeParse(response.data);

    if (!parsedAuthorisationServerJwks.success) {
      return {
        success: false,
        errors: [
          "Authorization Server's jwks_uri doesn't contain a valid JWKS",
          ...formatZodError(parsedAuthorisationServerJwks.error),
        ],
      };
    }

    const authorisationServerJwks = parsedAuthorisationServerJwks.data;

    // Get AS' JWK corresponding to the request.header.kid
    const authorisationServerPublicKeyJwk = authorisationServerJwks.keys.find(
      (jwk) => jwk.kid === idTokenRequestJwtHeader.kid
    );

    if (!authorisationServerPublicKeyJwk) {
      return {
        success: false,
        errors: [
          "Authorization Server's jwks_uri doesn't contain a public key matching ID Token request header's kid",
        ],
      };
    }

    // Support only ["ES256"]
    if (
      authorisationServerPublicKeyJwk.kty !== "EC" ||
      authorisationServerPublicKeyJwk.crv !== "P-256"
    ) {
      return {
        success: false,
        errors: ["Invalid request signature: only ES256 is supported"],
      };
    }

    // Verify signature
    try {
      const publicKey = await importJWK(
        authorisationServerPublicKeyJwk,
        "ES256"
      );
      await jwtVerify(idTokenRequestJwt, publicKey);
    } catch (e) {
      return {
        success: false,
        errors: [
          `Invalid ID Token request: ${
            e instanceof Error ? e.message : "Unknown error"
          }`,
        ],
      };
    }

    // Create ID Token, send to direct_post endpoint
    const idToken = await new SignJWT({
      nonce: idTokenRequest.nonce,
      sub: subjectDid,
    })
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: subjectKid,
      })
      .setIssuer(subjectDid)
      .setAudience(authorizationServer)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(signingKey);

    const idTokenResponseQueryParams = new URLSearchParams({
      id_token: idToken,
      ...(idTokenRequest.state && { state: idTokenRequest.state }),
    }).toString();

    try {
      response = await axios.post(
        idTokenRequest.redirect_uri,
        idTokenResponseQueryParams,
        {
          timeout,
          headers: { "content-type": "application/x-www-form-urlencoded" },
          maxRedirects: 0, // Don't follow redirects
          validateStatus: function validateStatus(status) {
            return status === 302; // Expect a redirect
          },
        }
      );

      // Expect client server to
    } catch (err) {
      logAxiosRequestError(err, logger);

      if (
        axios.isAxiosError(err) &&
        err.response &&
        err.response.status >= 200 &&
        err.response.status < 300
      ) {
        return {
          success: false,
          errors: [
            "Authorization Server direct_post must return a redirection.",
          ],
        };
      }

      return {
        success: false,
        errors: [
          `Authorization Server direct_post returned an error: ${getErrorMessage(
            err
          )}`,
        ],
      };
    }

    // Expect response to be:
    // HTTP/1.1 302 Found
    // Location: ${redirectUri}?code=${code}
    const { location } = response.headers;
    if (
      !location ||
      typeof location !== "string" ||
      !location.startsWith(redirectUri)
    ) {
      return {
        success: false,
        errors: [
          "Authorization Server direct_post doesn't redirect to the client's redirect_uri",
        ],
      };
    }

    const code = new URL(location).searchParams.get("code");

    if (!code) {
      return {
        success: false,
        errors: [
          `Authorization Server direct_post's response doesn't contain a code. Received response ${location}`,
        ],
      };
    }

    const state = new URL(location).searchParams.get("state");

    if (state !== authenticationRequestParams.state) {
      return {
        success: false,
        errors: [
          `Authorization Server direct_post's response doesn't contain the state ${
            authenticationRequestParams.state ?? ""
          }. Received response ${location}`,
        ],
      };
    }

    // Prepare Token Request
    const partialTokenRequestPayload = {
      grant_type: "authorization_code",
      code,
      client_id: clientId,
    } as const satisfies PostTokenAuthorizationCodeDto;

    if (codeVerifier) {
      tokenRequestPayload = {
        ...partialTokenRequestPayload,
        code_verifier: codeVerifier,
      } satisfies PostTokenPkceDto;
    } else {
      const clientAssertion = await new SignJWT(partialTokenRequestPayload)
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: subjectDid.startsWith("did:ebsi:")
            ? keyPair.publicKeyJwk.kid
            : subjectKid,
        })
        .setIssuer(clientId)
        .setAudience(authorizationServer)
        .setSubject(clientId)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(signingKey);

      tokenRequestPayload = {
        ...partialTokenRequestPayload,
        client_assertion: clientAssertion,
        client_assertion_type:
          "urn:ietf:params:oauth:client-assertion-type:jwt-bearer",
      } satisfies PostTokenClientAssertionDto;
    }
  } else {
    if (!userPin) {
      return {
        success: false,
        errors: ["Missing userPin"],
      };
    }

    tokenRequestPayload = {
      grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
      "pre-authorized_code": preAuthorizedCode,
      user_pin: userPin,
    } satisfies PostTokenPreAuthorizedCodeDto;
  }

  // 4. Get Access Token
  // See https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-11.html#section-6.1
  try {
    response = await axios.post(
      openidConfiguration.token_endpoint,
      new URLSearchParams(tokenRequestPayload).toString(),
      {
        timeout,
        headers: { "content-type": "application/x-www-form-urlencoded" },
      }
    );
  } catch (err) {
    logAxiosRequestError(err, logger);

    return {
      success: false,
      errors: [`Token endpoint returned an error: ${getErrorMessage(err)}`],
    };
  }

  const parsedTokenResponse = tokenResponseSchema.safeParse(response.data);

  if (!parsedTokenResponse.success) {
    return {
      success: false,
      errors: [
        "Authorization Server Token endpoint response is invalid.",
        ...formatZodError(parsedTokenResponse.error),
      ],
    };
  }

  const tokenResponse = parsedTokenResponse.data;

  // 5. Call client's credential_endpoint, asking for the requested types
  // See https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-11.html#name-credential-request
  const proof = await new SignJWT({
    iss: clientId,
    aud: openidCredentialIssuerConfig.credential_issuer,
    nonce: tokenResponse.c_nonce,
  })
    .setProtectedHeader({
      typ: "openid4vci-proof+jwt",
      alg: "ES256",
      kid: subjectKid,
    })
    .setIssuedAt()
    .sign(signingKey);

  const credentialRequestPayload = {
    format: "jwt_vc",
    types: requestedTypes,
    proof: {
      proof_type: "jwt",
      jwt: proof,
    },
  };

  try {
    response = await axios.post(
      openidCredentialIssuerConfig.credential_endpoint,
      credentialRequestPayload,
      {
        timeout,
        headers: {
          Authorization: `Bearer ${tokenResponse.access_token}`,
        },
      }
    );
  } catch (err) {
    logAxiosRequestError(err, logger);

    return {
      success: false,
      errors: [`Credential request returned an error: ${getErrorMessage(err)}`],
    };
  }

  // If the response contains an acceptance_token, query the deferred credential endpoint
  if (
    response.data &&
    typeof response.data === "object" &&
    "acceptance_token" in response.data &&
    typeof response.data.acceptance_token === "string" &&
    openidCredentialIssuerConfig.deferred_credential_endpoint
  ) {
    try {
      // Retry 12 times, wait 5 seconds between each query
      const axiosClient = axios.create();
      axiosRetry(axiosClient, {
        retries: 12,
        retryDelay: () => 5000,
        retryCondition: () => true, // Ignore error response, retry anyway
      });

      response = await axiosClient.post(
        openidCredentialIssuerConfig.deferred_credential_endpoint,
        credentialRequestPayload,
        {
          timeout,
          headers: {
            Authorization: `Bearer ${response.data.acceptance_token}`,
          },
        }
      );
    } catch (err) {
      logAxiosRequestError(err, logger);

      return {
        success: false,
        errors: [
          `Credential request returned an error: ${getErrorMessage(err)}`,
        ],
      };
    }
  }

  // Validate response
  const parsedCredentialResponse = credentialResponseSchema.safeParse(
    response.data
  );

  if (!parsedCredentialResponse.success) {
    return {
      success: false,
      errors: [
        "Credential response is invalid.",
        ...formatZodError(parsedCredentialResponse.error),
      ],
    };
  }

  return {
    success: true,
    credential: parsedCredentialResponse.data.credential,
  };
}
