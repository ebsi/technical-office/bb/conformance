import {
  Body,
  Controller,
  Get,
  Header,
  Headers,
  HttpCode,
  Param,
  Post,
  Query,
  Response,
} from "@nestjs/common";
import type { FastifyReply } from "fastify";
import { OAuth2Error } from "../../shared/errors";
import { IssuerMockService } from "./issuer-mock.service";
import type {
  JsonWebKeySet,
  CredentialIssuerMetadata,
  HolderWalletCredentialType,
} from "../../shared/interfaces";
import {
  GetCredentialOfferParamsDto,
  GetInitiateCredentialOfferDto,
} from "./dto";
import type {
  CredentialResponse,
  DeferredCredentialResponse,
} from "../../shared/credential-issuer";
import type { CredentialOfferPayload } from "./issuer-mock.interface";

@Controller("/issuer-mock")
export class IssuerMockController {
  constructor(private issuerMockService: IssuerMockService) {}

  @HttpCode(200)
  @Get("/.well-known/openid-credential-issuer")
  getCredentialIssuerMetadata(): CredentialIssuerMetadata {
    return this.issuerMockService.getCredentialIssuerMetadata();
  }

  @HttpCode(200)
  @Post("/credential")
  postCredential(
    @Headers("content-type") contentType: string,
    @Headers("authorization") authorizationHeader: string,
    @Body() body: unknown
  ): Promise<CredentialResponse | DeferredCredentialResponse> {
    // Only accept application/json
    // https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-11.html#section-7.2
    if (!contentType.toLowerCase().includes("application/json")) {
      throw new OAuth2Error("invalid_request", {
        errorDescription: "Content-type must be application/json",
      });
    }

    return this.issuerMockService.postCredential(authorizationHeader, body);
  }

  @HttpCode(200)
  @Post("/credential_deferred")
  postCredentialDeferred(
    @Headers("authorization") authorizationHeader: string
  ): Promise<CredentialResponse> {
    return this.issuerMockService.postCredentialDeferred(authorizationHeader);
  }

  @HttpCode(200)
  @Get("/jwks")
  @Header("Content-type", "application/jwk-set+json")
  async getJwks(): Promise<JsonWebKeySet> {
    return this.issuerMockService.getJwks();
  }

  @Get("/initiate-credential-offer")
  @HttpCode(200)
  @Header("content-type", "text/plain; charset=utf-8")
  async getInitiateCredentialOffer(
    @Query() query: GetInitiateCredentialOfferDto,
    @Response({ passthrough: true }) res: FastifyReply
  ) {
    const location = await this.issuerMockService.initiateCredentialOffer(
      query
    );

    // Redirect if the requested type is one of the following:
    const redirectTypes = [
      "CTWalletSameDeferred",
      "CTWalletSameInTime",
      "CTWalletSamePreAuthorised",
    ] satisfies HolderWalletCredentialType[];
    if (
      redirectTypes.includes(query.credential_type) ||
      (query.credential_type === "CTWalletQualificationCredential" &&
        query.redirect === "true")
    ) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      res.code(302).header("Location", location);
      return undefined;
    }

    return location;
  }

  /**
   * Credential Offer by Reference
   * @see https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-11.html#section-4.1.3
   */
  @Get("/offers/:credentialOfferId")
  @HttpCode(201)
  async getCredentialOffer(
    @Param() params: GetCredentialOfferParamsDto
  ): Promise<CredentialOfferPayload> {
    return this.issuerMockService.getCredentialOffer(params.credentialOfferId);
  }

  /**
   * Get credential status list 2021. This endpoint is used in the
   * Verifier Flow in the test of issuing a revoked VC
   * @see https://api-conformance.ebsi.eu/docs/ct/verifier-functional-flows
   */
  @HttpCode(200)
  @Get("/credentials/status/:id")
  async getCredentialStatusListJwt(
    @Param() params: { id: string }
  ): Promise<string> {
    return this.issuerMockService.getCredentialStatusListJwt(params.id);
  }
}

export default IssuerMockController;
