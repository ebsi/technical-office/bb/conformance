import { CacheModule, Module } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { ApiConfigModule, cacheConfig } from "../../config/configuration";
import { IssuerMockController } from "./issuer-mock.controller";
import { IssuerMockService } from "./issuer-mock.service";
import { DataStoreModule } from "../data-store/data-store.module";
import { LogsModule } from "../logs/logs.module";
import { EbsiModule } from "../ebsi/ebsi.module";

@Module({
  imports: [
    ApiConfigModule,
    CacheModule.register(cacheConfig),
    DataStoreModule,
    EbsiModule,
    LogsModule,
  ],
  controllers: [IssuerMockController],
  providers: [ConfigService, IssuerMockService],
  exports: [IssuerMockService],
})
export class IssuerMockModule {}

export default IssuerMockModule;
