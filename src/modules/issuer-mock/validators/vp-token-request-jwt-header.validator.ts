import { z } from "zod";

export const vpTokenRequestJwtHeaderSchema = z.object({
  kid: z.string(),
  alg: z.literal("ES256"),
  typ: z.literal("JWT"),
});

export default vpTokenRequestJwtHeaderSchema;
