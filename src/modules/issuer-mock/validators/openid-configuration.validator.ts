import { z } from "zod";

/**
 * Minimal OpenID Provider Metadata Schema
 *
 * @see https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderMetadata
 */
export const openidConfigurationSchema = z.object({
  // REQUIRED. URL using the https scheme with no query or fragment component that the OP asserts as its Issuer Identifier.
  // This also MUST be identical to the iss Claim value in ID Tokens issued from this Issuer.
  issuer: z.string().url(),

  // REQUIRED. URL of the OP's OAuth 2.0 Authorization Endpoint.
  authorization_endpoint: z.string().url(),

  // URL of the OP's OAuth 2.0 Token Endpoint.
  token_endpoint: z.string().url(),

  // REQUIRED. URL of the OP's JSON Web Key Set document. This contains the signing key(s) the RP uses to validate signatures from the OP.
  jwks_uri: z.string().url(),

  // REQUIRED. JSON array containing a list of the OAuth 2.0 response_type values that this OP supports. Dynamic OpenID Providers MUST support the code, id_token, and the token id_token Response Type values.
  response_types_supported: z.array(z.string()),

  // OPTIONAL. JSON array containing a list of the OAuth 2.0 Grant Type values that this OP supports.
  // Dynamic OpenID Providers MUST support the authorization_code and implicit Grant Type values and MAY support other Grant Types.
  // If omitted, the default value is ["authorization_code", "implicit"].
  grant_types_supported: z.array(z.string()).optional(),

  // REQUIRED. JSON array containing a list of the Subject Identifier types that this OP supports. Valid types include pairwise and public.
  subject_types_supported: z.array(z.enum(["pairwise", "public"])),

  // REQUIRED. JSON array containing a list of the JWS signing algorithms (alg values) supported by the OP for the ID Token to encode the Claims in a JWT [JWT].
  // The algorithm RS256 MUST be included. The value none MAY be supported, but MUST NOT be used unless the Response Type used returns no ID Token from the Authorization Endpoint (such as when using the Authorization Code Flow).
  id_token_signing_alg_values_supported: z.array(z.string()),
});

export default openidConfigurationSchema;
