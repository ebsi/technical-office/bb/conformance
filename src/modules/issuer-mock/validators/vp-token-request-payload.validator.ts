import { z } from "zod";
import { presentationDefinitionVerifierSchema } from "./presentation-definition-verifier.validator";

export const vpTokenRequestPayloadSchema = z.object({
  client_id: z.string().url(),
  redirect_uri: z.string(),
  response_type: z.literal("vp_token"),
  response_mode: z.literal("direct_post"),
  scope: z.enum([
    "openid",
    "openid ver_test:id_token",
    "openid ver_test:vp_token",
  ]),
  iss: z.string(),
  aud: z.string(),
  exp: z.number(),
  nonce: z.string().optional(),
  state: z.string().optional(),
  // Either request or request_uri
  request: z.string().optional(),
  request_uri: z.string().url().optional(),
  presentation_definition: presentationDefinitionVerifierSchema,
});

export default vpTokenRequestPayloadSchema;
