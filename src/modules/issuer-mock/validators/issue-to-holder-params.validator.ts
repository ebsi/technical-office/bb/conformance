import { z } from "zod";

// Params used in the Issue to Holder flow (in-time credential)
export const issueToHolderInTimeParamsSchema = z.object({
  credentialIssuer: z.string().url(),
  credentialIssuerDid: z.string(),
  issuerState: z.string().optional(),
});

export type IssueToHolderInTimeParams = z.infer<
  typeof issueToHolderInTimeParamsSchema
>;

// Params used in the Issue to Holder flow (deferred credential)
export const issueToHolderDeferredParamsSchema =
  issueToHolderInTimeParamsSchema;

export type IssueToHolderDeferredParams = z.infer<
  typeof issueToHolderDeferredParamsSchema
>;

// Params used in the Issue to Holder flow (pre-authorised credential)
export const issueToHolderPreAuthorisedParamsSchema = z.object({
  credentialIssuer: z.string().url(),
  credentialIssuerDid: z.string(),
  preAuthorizedCode: z.string(),
  userPin: z.string(),
});

export type IssueToHolderPreAuthorisedParams = z.infer<
  typeof issueToHolderPreAuthorisedParamsSchema
>;
