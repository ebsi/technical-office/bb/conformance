import { isJWT } from "class-validator";
import { z } from "zod";

export const credentialResponseSchema = z.object({
  // REQUIRED. JSON string denoting the format of the issued Credential.
  format: z.literal("jwt_vc"),

  // REQUIRED. The credential issued by the Credential Issuer.
  credential: z.string().refine(isJWT, { message: "Not a valid JWT" }),
});

export default credentialResponseSchema;
