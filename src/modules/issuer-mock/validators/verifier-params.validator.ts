import { z } from "zod";

// Params used in the Verifier flow
export const verifierParamsSchema = z.object({
  clientId: z.string().url(),
});

export type VerifierParams = z.infer<typeof verifierParamsSchema>;
