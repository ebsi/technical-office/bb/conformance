import { z } from "zod";

/**
 * Minimal OpenID Credential Issuer Metadata Schema
 *
 * @see https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-11.html#section-10.2.3
 */
export const openidCredentialIssuerConfigSchema = z.object({
  // REQUIRED. The Credential Issuer's identifier.
  credential_issuer: z.string().url(),

  // OPTIONAL. Identifier of the OAuth 2.0 Authorization Server (as defined in [RFC8414]) the Credential Issuer relies on for authorization.
  // If this element is omitted, the entity providing the Credential Issuer is also acting as the AS, i.e. the Credential Issuer's identifier is used as the OAuth 2.0 Issuer value to obtain the Authorization Server metadata.
  authorization_server: z.string().url().optional(),

  // REQUIRED. URL of the Credential Issuer's Credential Endpoint. This URL MUST use the https scheme and MAY contain port, path and query parameter components.
  credential_endpoint: z.string().url(),

  // OPTIONAL. URL of the Credential Issuer's Deferred Credential Endpoint.
  deferred_credential_endpoint: z.string().url().optional(),

  // REQUIRED. A JSON array containing a list of JSON objects, each of them representing metadata about a separate credential type that the Credential Issuer can issue. The JSON objects in the array MUST conform to the structure of the Section 10.2.3.1.
  credentials_supported: z.array(
    z.object({
      // REQUIRED. A JSON string identifying the format of this credential, e.g. jwt_vc_json or ldp_vc. Depending on the format value, the object contains further elements defining the type and (optionally) particular claims the credential MAY contain, and information how to display the credential.
      // WARNING: https://github.com/Sphereon-Opensource/PEX does not support `jwt_vc_json`. For this reason, we use `jwt_vc` instead.
      format: z.literal("jwt_vc"),
      types: z.array(z.string()),
    })
  ),
});

export default openidCredentialIssuerConfigSchema;
