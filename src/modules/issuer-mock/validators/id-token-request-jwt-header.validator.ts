import { z } from "zod";

export const idTokenRequestJwtHeaderSchema = z.object({
  kid: z.string(),
  alg: z.literal("ES256"),
});

export default idTokenRequestJwtHeaderSchema;
