import { z } from "zod";

export const tokenResponseSchema = z.object({
  // REQUIRED. The access token issued by the authorization server.
  access_token: z.string(),

  // REQUIRED. The type of the token issued as described in Section 7.1. Value is case insensitive.
  token_type: z.string().refine((val) => val.toLowerCase() === "bearer"),

  // RECOMMENDED. The lifetime in seconds of the access token. For example, the value "3600" denotes that the access token will expire in one hour from the time the response was generated. If omitted, the authorization server SHOULD provide the expiration time via other means or document the default value.
  expires_in: z.number().positive(),

  // OPTIONAL. JSON string containing a nonce to be used to create a proof of possession of key material when requesting a Credential (see Section 7.2). When received, the Wallet MUST use this nonce value for its subsequent credential requests until the Credential Issuer provides a fresh nonce.
  c_nonce: z.string().optional(),

  // OPTIONAL. JSON integer denoting the lifetime in seconds of the c_nonce.
  c_nonce_expires_in: z.number().positive().optional(),
});

export default tokenResponseSchema;
