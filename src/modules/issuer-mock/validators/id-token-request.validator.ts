import { z } from "zod";

export const idTokenRequestSchema = z.object({
  client_id: z.string().url(),
  redirect_uri: z.string(),
  response_type: z.literal("id_token"),
  response_mode: z.literal("direct_post"),
  scope: z.literal("openid"),
  nonce: z.string().optional(),
  state: z.string().optional(),
  // Either request or request_uri
  request: z.string().optional(),
  request_uri: z.string().url().optional(),
});

export default idTokenRequestSchema;
