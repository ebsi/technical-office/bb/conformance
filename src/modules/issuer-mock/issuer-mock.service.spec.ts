import { randomBytes, randomUUID } from "node:crypto";
import {
  jest,
  describe,
  beforeAll,
  afterEach,
  afterAll,
  it,
  expect,
  beforeEach,
} from "@jest/globals";
import { Test, TestingModule } from "@nestjs/testing";
import { APP_INTERCEPTOR } from "@nestjs/core";
import { ConfigService } from "@nestjs/config";
import { CACHE_MANAGER, INestApplication, Logger } from "@nestjs/common";
import type { FastifyInstance } from "fastify";
import nock from "nock";
import {
  calculateJwkThumbprint,
  exportJWK,
  generateKeyPair,
  importJWK,
  SignJWT,
} from "jose";
import type { GenerateKeyPairResult, KeyLike } from "jose";
import { util } from "@cef-ebsi/ebsi-did-resolver";
import { util as keyDidMethodHelpers } from "@cef-ebsi/key-did-resolver";
import { createVerifiableCredentialJwt } from "@cef-ebsi/verifiable-credential";
import type {
  EbsiIssuer,
  EbsiVerifiableAttestation,
} from "@cef-ebsi/verifiable-credential";
import type { Cache } from "cache-manager";
import { IssuerMockModule } from "./issuer-mock.module";
import { IssuerMockService } from "./issuer-mock.service";
import {
  TI_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD,
  TI_REGISTER_DID,
  TI_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST,
  TI_REGISTER_VERIFIABLE_ACCREDITATION_TO_ATTEST,
  TI_REQUEST_CT_REVOCABLE,
  TI_VALIDATE_CT_REVOCABLE,
  TI_REVOKE_CT_REVOCABLE,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
  TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
  TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
  RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
  RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
  TAO_REVOKE_RIGHTS_SUBACCOUNT,
} from "../../shared/constants";
import { VERIFIER_TEST_PRESENTATION_DEFINITION } from "../../shared/auth-server";
import { getKeyPair } from "../../shared/utils";
import { LogsModule } from "../logs/logs.module";
import { LogsService } from "../logs/logs.service";
import {
  AUTH_API_PATH,
  DIDR_JSON_RPC_PATH,
  LEDGER_API_PATH,
  TIR_API_PATH,
  TIR_JSON_RPC_PATH,
} from "../../config/configuration";
import type { ApiConfig } from "../../config/configuration";
import { LoggingInterceptor } from "../../interceptors/logging.interceptor";
import { configureApp } from "../../../tests/utils/app";
import {
  createDidDocument,
  encodeList,
  mockSchemas,
  setBit,
} from "../../../tests/utils/data";
import * as issuerMockUtils from "./issuer-mock.utils";
import { getCredentialId, getSubAccountDid } from "./issuer-mock.utils";
import type {
  IssueToHolderDeferredParams,
  IssueToHolderInTimeParams,
  IssueToHolderPreAuthorisedParams,
} from "./validators";

describe("Issuer Mock Service", () => {
  let app: INestApplication;
  let configService: ConfigService<ApiConfig, true>;
  let issuerMockService: IssuerMockService;
  let logsService: LogsService;
  let issuerMockDid: string;
  let issuerMockKid: string;
  let issuerMockAccreditationUrl: string;
  let cacheManager: Cache;
  let authorisationCredentialSchema: string;
  let domain: string;
  let ebsiAuthority: string;

  beforeAll(async () => {
    // Disable external requests
    nock.disableNetConnect();
    // Allow localhost connections so we can test local routes and mock servers.
    nock.enableNetConnect("127.0.0.1");

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [IssuerMockModule, LogsModule],
      providers: [
        {
          provide: APP_INTERCEPTOR,
          useClass: LoggingInterceptor,
        },
      ],
    }).compile();

    app = await configureApp(moduleFixture);

    Logger.overrideLogger(false);

    await app.init();
    await (app.getHttpAdapter().getInstance() as FastifyInstance).ready();

    configService =
      moduleFixture.get<ConfigService<ApiConfig, true>>(ConfigService);

    cacheManager = moduleFixture.get<Cache>(CACHE_MANAGER);

    issuerMockService = moduleFixture.get<IssuerMockService>(IssuerMockService);
    logsService = moduleFixture.get<LogsService>(LogsService);

    domain = configService.get<string>("domain");
    issuerMockKid = configService.get<string>("issuerMockKid");
    issuerMockDid = issuerMockKid.split("#")[0] as string;
    issuerMockAccreditationUrl = configService.get<string>(
      "issuerMockAccreditationUrl"
    );
    authorisationCredentialSchema = configService.get<string>(
      "authorisationCredentialSchema"
    );
    ebsiAuthority = domain.replace(/^https?:\/\//, ""); // remove http protocol scheme
  });

  afterAll(async () => {
    nock.restore();

    // Avoid jest open handle error
    await new Promise((r) => {
      setTimeout(r, 500);
    });
    await app.close();
  });

  describe("processCredentialRequest", () => {
    const clientId = "https://client-mock.xyz";
    const clientDid = "did:ebsi:zgPs5MVWHwJJb4g9kZvYf3e";

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response = await issuerMockService.processCredentialRequest({
        // did, Missing did
        clientId: "not an url",
      });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Validation error. Path: 'data.clientId'. Reason: Invalid url",
          "Validation error. Path: 'data.did'. Reason: Required",
        ],
      });
    });

    it("should return the error reported by issuerMockService.requestCredential if there's an error", async () => {
      jest.spyOn(issuerMockUtils, "requestCredential").mockResolvedValue({
        success: false,
        errors: ["error"],
      });

      const response = await issuerMockService.processCredentialRequest({
        clientId,
        did: clientDid,
      });

      expect(response).toStrictEqual({ success: false, errors: ["error"] });
    });

    it("should store the credential in cache and return { success: true } if the request is valid", async () => {
      expect.assertions(2);

      jest.spyOn(issuerMockUtils, "requestCredential").mockResolvedValue({
        success: true,
        credential: "jwt",
      });

      const cacheSpy = jest.spyOn(cacheManager, "set");

      const response = await issuerMockService.processCredentialRequest({
        clientId,
        did: clientDid,
      });

      expect(cacheSpy).toHaveBeenCalledWith(
        getCredentialId({ clientId, did: clientDid }, "CTRevocable"),
        "jwt",
        300_000 // 5 minutes
      );
      expect(response).toStrictEqual({ success: true });
    });
  });

  describe("processCredentialValidation", () => {
    const clientId = "https://client-mock.xyz";
    let clientDid: string;
    const requestedTypes = [
      "VerifiableCredential",
      "VerifiableAttestation",
      "CTRevocable",
    ];

    beforeEach(() => {
      clientDid = util.createDid(randomBytes(16));
    });

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response = await issuerMockService.processCredentialValidation({
        // did, Missing did
        clientId: "not an url",
      });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Validation error. Path: 'data.clientId'. Reason: Invalid url",
          "Validation error. Path: 'data.did'. Reason: Required",
        ],
      });
    });

    it("should return an error if no credential is found", async () => {
      expect.assertions(1);

      const response = await issuerMockService.processCredentialValidation({
        clientId,
        did: clientDid,
      });

      expect(response).toStrictEqual({
        success: false,
        errors: ["No credential found for this client."],
      });
    });

    it("should return an error if the stored credential is not a string", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve({ credential: { foo: "bar" } });
      });

      const response = await issuerMockService.processCredentialValidation({
        clientId,
        did: clientDid,
      });

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential is not a string."],
      });
    });

    it("should return an error if the stored credential is not a valid JWT", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve("not a jwt");
      });

      const response = await issuerMockService.processCredentialValidation({
        clientId,
        did: clientDid,
      });

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential is not valid: Unable to decode JWT VC"],
      });
    });

    it("should return an error if the stored credential is not a valid Verifiable Attestation JWT", async () => {
      expect.assertions(1);

      const jwt = await new SignJWT({
        foo: "bar",
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: `${clientDid}#key-1`,
        })
        .sign((await generateKeyPair("ES256")).privateKey);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(jwt);
      });

      const response = await issuerMockService.processCredentialValidation({
        clientId,
        did: clientDid,
      });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: Invalid EBSI Verifiable Attestation",
          "Validation error. Path: 'vc'. Reason: Required",
        ],
      });
    });

    it("should return an error if the credential is not issued to the correct DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };
      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: util.createDid(randomBytes(16)),
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
        credentialStatus: {
          id: `${clientId}/creds/1`,
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "42",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      const response = await issuerMockService.processCredentialValidation({
        clientId,
        did: clientDid,
      });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential is not issued to the Issuer Mock DID: ${issuerMockDid}`,
        ],
      });
    });

    it("should return an error if the credential is not issued by the correct DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };
      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: util.createDid(randomBytes(16)), // Not the same DID as the client DID
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: issuerMockDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
        credentialStatus: {
          id: `${clientId}/creds/1`,
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "42",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      const response = await issuerMockService.processCredentialValidation({
        clientId,
        did: clientDid,
      });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential issuer ${vcPayload.issuer} is not the same as client's DID: ${clientDid}`,
        ],
      });
    });

    it("should return an error if the credential is missing the CTRevocable type", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;
      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };
      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: ["VerifiableCredential", "VerifiableAttestation"], // "CTRevocable" is missing
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: issuerMockDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
        credentialStatus: {
          id: `${clientId}/creds/1`,
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "42",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      const response = await issuerMockService.processCredentialValidation({
        clientId,
        did: clientDid,
      });

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential does not have the CTRevocable type."],
      });
    });

    it("should return an error if the issuer is not accredited to issue CTRevocable", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
          // The following accreditation is missing
          // accreditedFor: [
          //   {
          //     schemaId: authorisationCredentialSchema,
          //     types: [
          //       "VerifiableCredential",
          //       "VerifiableAttestation",
          //       "CTRevocable", // Is allowed to issue CTRevocable VCs
          //     ],
          //     limitJurisdiction:
          //       "https://publications.europa.eu/resource/authority/atu/FIN",
          //   },
          // ],
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };
      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: issuerMockDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
        credentialStatus: {
          id: `${clientId}/creds/1`,
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "42",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response = await issuerMockService.processCredentialValidation({
        clientId,
        did: clientDid,
      });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential is not valid: Issuer ${clientDid} is not accredited to issue credentials with type ["CTRevocable"]`,
        ],
      });
    });

    it("should return an error if the credentialStatus.statusPurpose is not 'revocation'", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
          accreditedFor: [
            {
              schemaId: authorisationCredentialSchema,
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "CTRevocable", // Is allowed to issue CTRevocable VCs
              ],
              limitJurisdiction:
                "https://publications.europa.eu/resource/authority/atu/FIN",
            },
          ],
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const clientProxyPathname = `${TIR_API_PATH}/${clientDid}/proxies/0x1234`;
      const statusListCredentialPathname = `${clientProxyPathname}/creds/1`;
      const statusListCredential = `${domain}${statusListCredentialPathname}`;

      // Create a 16384 bytes long Uint8Array (filled with 0)
      const list = new Uint8Array(16384);
      // GZip + base64 encode
      const encodedList = encodeList(list);

      const credentialStatusListPayload = {
        "@context": [
          "https://www.w3.org/2018/credentials/v1",
          "https://w3id.org/vc/status-list/2021/v1",
        ],
        id: statusListCredential,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "StatusList2021Credential",
        ],
        issuer: clientDid,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        validUntil: expirationDate,
        credentialSubject: {
          id: `${statusListCredential}#list`,
          type: "StatusList2021",
          statusPurpose: "suspension",
          encodedList,
        },
        credentialSchema: [
          {
            id: authorisationCredentialSchema,
            type: "FullJsonSchemaValidator2021",
          },
        ],
      };

      const credentialStatusListJwt = await createVerifiableCredentialJwt(
        credentialStatusListPayload,
        vcIssuer,
        { ebsiAuthority, skipValidation: true }
      );

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: issuerMockDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
        credentialStatus: {
          id: `${statusListCredential}#42`,
          type: "StatusList2021Entry",
          statusPurpose: "suspension",
          statusListIndex: "42",
          statusListCredential,
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(statusListCredentialPathname)
        .reply(200, credentialStatusListJwt)
        .persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response = await issuerMockService.processCredentialValidation({
        clientId,
        did: clientDid,
      });

      expect(response).toStrictEqual({
        errors: ["Credential statusPurpose must be 'revocation'."],
        success: false,
      });
    });

    it("should return an error if the credential is revoked", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
          accreditedFor: [
            {
              schemaId: authorisationCredentialSchema,
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "CTRevocable", // Is allowed to issue CTRevocable VCs
              ],
              limitJurisdiction:
                "https://publications.europa.eu/resource/authority/atu/FIN",
            },
          ],
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const clientProxyPathname = `${TIR_API_PATH}/${clientDid}/proxies/0x1234`;
      const statusListCredentialPathname = `${clientProxyPathname}/creds/1`;
      const statusListCredential = `${domain}${statusListCredentialPathname}`;

      // Create a 16384 bytes long Uint8Array (filled with 0)
      const list = new Uint8Array(16384);
      // Set value of the 42th bit to 1
      setBit(list, 42, "1");
      // GZip + base64 encode
      const encodedList = encodeList(list);

      const credentialStatusListPayload = {
        "@context": [
          "https://www.w3.org/2018/credentials/v1",
          "https://w3id.org/vc/status-list/2021/v1",
        ],
        id: statusListCredential,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "StatusList2021Credential",
        ],
        issuer: clientDid,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        validUntil: expirationDate,
        credentialSubject: {
          id: `${statusListCredential}#list`,
          type: "StatusList2021",
          statusPurpose: "revocation",
          encodedList,
        },
        credentialSchema: [
          {
            id: authorisationCredentialSchema,
            type: "FullJsonSchemaValidator2021",
          },
        ],
      };

      const credentialStatusListJwt = await createVerifiableCredentialJwt(
        credentialStatusListPayload,
        vcIssuer,
        { ebsiAuthority, skipValidation: true }
      );

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: issuerMockDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
        credentialStatus: {
          id: `${statusListCredential}#42`,
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "42",
          statusListCredential,
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(statusListCredentialPathname)
        .reply(200, credentialStatusListJwt)
        .persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response = await issuerMockService.processCredentialValidation({
        clientId,
        did: clientDid,
      });

      expect(response).toStrictEqual({
        errors: ["Credential is not valid: The credential is revoked"],
        success: false,
      });
    });

    it("should return { success: true } if the credential is valid", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
          accreditedFor: [
            {
              schemaId: authorisationCredentialSchema,
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "CTRevocable", // Is allowed to issue CTRevocable VCs
              ],
              limitJurisdiction:
                "https://publications.europa.eu/resource/authority/atu/FIN",
            },
          ],
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const clientProxyPathname = `${TIR_API_PATH}/${clientDid}/proxies/0x1234`;
      const statusListCredentialPathname = `${clientProxyPathname}/creds/1`;
      const statusListCredential = `${domain}${statusListCredentialPathname}`;

      // Create a 16384 bytes long Uint8Array (filled with 0)
      const list = new Uint8Array(16384);
      // GZip + base64 encode
      const encodedList = encodeList(list);

      const credentialStatusListPayload = {
        "@context": [
          "https://www.w3.org/2018/credentials/v1",
          "https://w3id.org/vc/status-list/2021/v1",
        ],
        id: statusListCredential,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "StatusList2021Credential",
        ],
        issuer: clientDid,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        validUntil: expirationDate,
        credentialSubject: {
          id: `${statusListCredential}#list`,
          type: "StatusList2021",
          statusPurpose: "revocation",
          encodedList,
        },
        credentialSchema: [
          {
            id: authorisationCredentialSchema,
            type: "FullJsonSchemaValidator2021",
          },
        ],
      };

      const credentialStatusListJwt = await createVerifiableCredentialJwt(
        credentialStatusListPayload,
        vcIssuer,
        { ebsiAuthority, skipValidation: true }
      );

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: issuerMockDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
        credentialStatus: {
          id: `${statusListCredential}#42`,
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "42",
          statusListCredential,
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(statusListCredentialPathname)
        .reply(200, credentialStatusListJwt)
        .persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response = await issuerMockService.processCredentialValidation({
        clientId,
        did: clientDid,
      });

      expect(response).toStrictEqual({
        success: true,
      });
    });
  });

  describe("processRevokedCredentialValidation", () => {
    const clientId = "https://client-mock.xyz";
    let clientDid: string;
    const requestedTypes = [
      "VerifiableCredential",
      "VerifiableAttestation",
      "CTRevocable",
    ];

    beforeEach(() => {
      clientDid = util.createDid(randomBytes(16));
    });

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processRevokedCredentialValidation({
          // did, Missing did
          clientId: "not an url",
        });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Validation error. Path: 'data.clientId'. Reason: Invalid url",
          "Validation error. Path: 'data.did'. Reason: Required",
        ],
      });
    });

    it("should return an error if no credential is found", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processRevokedCredentialValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        success: false,
        errors: ["No credential found for this client."],
      });
    });

    it("should return an error if the stored credential is not a string", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve({ credential: { foo: "bar" } });
      });

      const response =
        await issuerMockService.processRevokedCredentialValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential is not a string."],
      });
    });

    it("should return an error if the stored credential is not a valid JWT", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve("not a jwt");
      });

      const response =
        await issuerMockService.processRevokedCredentialValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential is not valid: Unable to decode JWT VC"],
      });
    });

    it("should return an error if the stored credential is not a valid Verifiable Attestation JWT", async () => {
      expect.assertions(1);

      const jwt = await new SignJWT({
        foo: "bar",
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: `${clientDid}#key-1`,
        })
        .sign((await generateKeyPair("ES256")).privateKey);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(jwt);
      });

      const response =
        await issuerMockService.processRevokedCredentialValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: Invalid EBSI Verifiable Attestation",
          "Validation error. Path: 'vc'. Reason: Required",
        ],
      });
    });

    it("should return an error if the credential is not issued to the correct DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };
      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: util.createDid(randomBytes(16)),
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
        credentialStatus: {
          id: `${clientId}/creds/1`,
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "42",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      const response =
        await issuerMockService.processRevokedCredentialValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential is not issued to the Issuer Mock DID: ${issuerMockDid}`,
        ],
      });
    });

    it("should return an error if the credential is not issued by the correct DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };
      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: util.createDid(randomBytes(16)), // Not the same DID as the client DID
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: issuerMockDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
        credentialStatus: {
          id: `${clientId}/creds/1`,
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "42",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      const response =
        await issuerMockService.processRevokedCredentialValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential issuer ${vcPayload.issuer} is not the same as client's DID: ${clientDid}`,
        ],
      });
    });

    it("should return an error if the credential is missing the CTRevocable type", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;
      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };
      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: ["VerifiableCredential", "VerifiableAttestation"], // "CTRevocable" is missing
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: issuerMockDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
        credentialStatus: {
          id: `${clientId}/creds/1`,
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "42",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      const response =
        await issuerMockService.processRevokedCredentialValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential does not have the CTRevocable type."],
      });
    });

    it("should return an error if the issuer is not accredited to issue CTRevocable", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
          // The following accreditation is missing
          // accreditedFor: [
          //   {
          //     schemaId: authorisationCredentialSchema,
          //     types: [
          //       "VerifiableCredential",
          //       "VerifiableAttestation",
          //       "CTRevocable", // Is allowed to issue CTRevocable VCs
          //     ],
          //     limitJurisdiction:
          //       "https://publications.europa.eu/resource/authority/atu/FIN",
          //   },
          // ],
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };
      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: issuerMockDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
        credentialStatus: {
          id: `${clientId}/creds/1`,
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "42",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processRevokedCredentialValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential is not valid: Issuer ${clientDid} is not accredited to issue credentials with type ["CTRevocable"]`,
        ],
      });
    });

    it("should return an error if the credentialStatus.statusPurpose is not 'revocation'", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
          accreditedFor: [
            {
              schemaId: authorisationCredentialSchema,
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "CTRevocable", // Is allowed to issue CTRevocable VCs
              ],
              limitJurisdiction:
                "https://publications.europa.eu/resource/authority/atu/FIN",
            },
          ],
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const clientProxyPathname = `${TIR_API_PATH}/${clientDid}/proxies/0x1234`;
      const statusListCredentialPathname = `${clientProxyPathname}/creds/1`;
      const statusListCredential = `${domain}${statusListCredentialPathname}`;

      // Create a 16384 bytes long Uint8Array (filled with 0)
      const list = new Uint8Array(16384);
      // GZip + base64 encode
      const encodedList = encodeList(list);

      const credentialStatusListPayload = {
        "@context": [
          "https://www.w3.org/2018/credentials/v1",
          "https://w3id.org/vc/status-list/2021/v1",
        ],
        id: statusListCredential,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "StatusList2021Credential",
        ],
        issuer: clientDid,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        validUntil: expirationDate,
        credentialSubject: {
          id: `${statusListCredential}#list`,
          type: "StatusList2021",
          statusPurpose: "suspension",
          encodedList,
        },
        credentialSchema: [
          {
            id: authorisationCredentialSchema,
            type: "FullJsonSchemaValidator2021",
          },
        ],
      };

      const credentialStatusListJwt = await createVerifiableCredentialJwt(
        credentialStatusListPayload,
        vcIssuer,
        { ebsiAuthority, skipValidation: true }
      );

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: issuerMockDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
        credentialStatus: {
          id: `${statusListCredential}#42`,
          type: "StatusList2021Entry",
          statusPurpose: "suspension",
          statusListIndex: "42",
          statusListCredential,
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(statusListCredentialPathname)
        .reply(200, credentialStatusListJwt)
        .persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processRevokedCredentialValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        errors: ["Credential statusPurpose must be 'revocation'."],
        success: false,
      });
    });

    it("should return an error if the credential is not revoked", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
          accreditedFor: [
            {
              schemaId: authorisationCredentialSchema,
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "CTRevocable", // Is allowed to issue CTRevocable VCs
              ],
              limitJurisdiction:
                "https://publications.europa.eu/resource/authority/atu/FIN",
            },
          ],
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const clientProxyPathname = `${TIR_API_PATH}/${clientDid}/proxies/0x1234`;
      const statusListCredentialPathname = `${clientProxyPathname}/creds/1`;
      const statusListCredential = `${domain}${statusListCredentialPathname}`;

      // Create a 16384 bytes long Uint8Array (filled with 0)
      const list = new Uint8Array(16384);
      // GZip + base64 encode
      const encodedList = encodeList(list);

      const credentialStatusListPayload = {
        "@context": [
          "https://www.w3.org/2018/credentials/v1",
          "https://w3id.org/vc/status-list/2021/v1",
        ],
        id: statusListCredential,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "StatusList2021Credential",
        ],
        issuer: clientDid,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        validUntil: expirationDate,
        credentialSubject: {
          id: `${statusListCredential}#list`,
          type: "StatusList2021",
          statusPurpose: "revocation",
          encodedList,
        },
        credentialSchema: [
          {
            id: authorisationCredentialSchema,
            type: "FullJsonSchemaValidator2021",
          },
        ],
      };

      const credentialStatusListJwt = await createVerifiableCredentialJwt(
        credentialStatusListPayload,
        vcIssuer,
        { ebsiAuthority, skipValidation: true }
      );

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: issuerMockDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
        credentialStatus: {
          id: `${statusListCredential}#42`,
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "42",
          statusListCredential,
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(statusListCredentialPathname)
        .reply(200, credentialStatusListJwt)
        .persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processRevokedCredentialValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        errors: ["The credential is not revoked."],
        success: false,
      });
    });

    it("should return { success: true } if the credential is revoked", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
          accreditedFor: [
            {
              schemaId: authorisationCredentialSchema,
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "CTRevocable", // Is allowed to issue CTRevocable VCs
              ],
              limitJurisdiction:
                "https://publications.europa.eu/resource/authority/atu/FIN",
            },
          ],
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const clientProxyPathname = `${TIR_API_PATH}/${clientDid}/proxies/0x1234`;
      const statusListCredentialPathname = `${clientProxyPathname}/creds/1`;
      const statusListCredential = `${domain}${statusListCredentialPathname}`;

      // Create a 16384 bytes long Uint8Array (filled with 0)
      const list = new Uint8Array(16384);
      // Set value of the 42th bit to 1
      setBit(list, 42, "1");
      // GZip + base64 encode
      const encodedList = encodeList(list);

      const credentialStatusListPayload = {
        "@context": [
          "https://www.w3.org/2018/credentials/v1",
          "https://w3id.org/vc/status-list/2021/v1",
        ],
        id: statusListCredential,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "StatusList2021Credential",
        ],
        issuer: clientDid,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        validUntil: expirationDate,
        credentialSubject: {
          id: `${statusListCredential}#list`,
          type: "StatusList2021",
          statusPurpose: "revocation",
          encodedList,
        },
        credentialSchema: [
          {
            id: authorisationCredentialSchema,
            type: "FullJsonSchemaValidator2021",
          },
        ],
      };

      const credentialStatusListJwt = await createVerifiableCredentialJwt(
        credentialStatusListPayload,
        vcIssuer,
        { ebsiAuthority, skipValidation: true }
      );

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: issuerMockDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
        credentialStatus: {
          id: `${statusListCredential}#42`,
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "42",
          statusListCredential,
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(statusListCredentialPathname)
        .reply(200, credentialStatusListJwt)
        .persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processRevokedCredentialValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        success: true,
      });
    });
  });

  describe("processVAToOnboardSubaccountRequest", () => {
    const clientId = "https://client-mock.xyz";
    const clientDid = "did:ebsi:zgPs5MVWHwJJb4g9kZvYf3e";

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processVAToOnboardSubaccountRequest({
          // did, Missing did
          clientId: "not an url",
        });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Validation error. Path: 'data.clientId'. Reason: Invalid url",
          "Validation error. Path: 'data.did'. Reason: Required",
        ],
      });
    });

    it("should return the error reported by issuerMockService.requestCredential if there's an error", async () => {
      jest.spyOn(issuerMockUtils, "requestCredential").mockResolvedValue({
        success: false,
        errors: ["error"],
      });

      const response =
        await issuerMockService.processVAToOnboardSubaccountRequest({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({ success: false, errors: ["error"] });
    });

    it("should store the credential in cache and return { success: true } if the request is valid", async () => {
      expect.assertions(2);

      jest.spyOn(issuerMockUtils, "requestCredential").mockResolvedValue({
        success: true,
        credential: "jwt",
      });

      const cacheSpy = jest.spyOn(cacheManager, "set");

      const response =
        await issuerMockService.processVAToOnboardSubaccountRequest({
          clientId,
          did: clientDid,
        });

      expect(cacheSpy).toHaveBeenCalledWith(
        getCredentialId(
          { clientId, did: clientDid },
          "VerifiableAuthorisationToOnboard"
        ),
        "jwt",
        300_000 // 5 minutes
      );
      expect(response).toStrictEqual({ success: true });
    });
  });

  describe("processVerifiableAuthorisationToOnboardValidation", () => {
    const clientId = "https://client-mock.xyz";
    let clientDid: string;
    const requestedTypes = [
      "VerifiableCredential",
      "VerifiableAttestation",
      "VerifiableAuthorisationToOnboard",
    ];
    let subAccountDid: string;

    beforeEach(() => {
      clientDid = util.createDid(randomBytes(16));
      subAccountDid = getSubAccountDid(clientDid);
    });

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            // did, Missing did
            clientId: "not an url",
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Validation error. Path: 'data.clientId'. Reason: Invalid url",
          "Validation error. Path: 'data.did'. Reason: Required",
        ],
      });
    });

    it("should return an error if no credential is found", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["No credential found for this client."],
      });
    });

    it("should return an error if the stored credential is not a string", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve({ credential: { foo: "bar" } });
      });

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential is not a string."],
      });
    });

    it("should return an error if the stored credential is not a valid JWT", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve("not a jwt");
      });

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential is not valid: Unable to decode JWT VC"],
      });
    });

    it("should return an error if the stored credential is not a valid Verifiable Attestation JWT", async () => {
      expect.assertions(1);

      const jwt = await new SignJWT({
        foo: "bar",
        vc: {},
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: `${clientDid}#key-1`,
        })
        .sign((await generateKeyPair("ES256")).privateKey);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(jwt);
      });

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: Invalid EBSI Verifiable Attestation",
          "Credential must have required property '@context'",
          "Credential must have required property 'id'",
          "Credential must have required property 'type'",
          "Credential must have required property 'issuer'",
          "Credential must have required property 'issuanceDate'",
          "Credential must have required property 'issued'",
          "Credential must have required property 'validFrom'",
          "Credential must have required property 'credentialSubject'",
          "Credential must have required property 'credentialSchema'",
        ],
      });
    });

    it("should return an error if the credential doesn't contain termsOfUse", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;
      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };
      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: Credential doesn't contain terms of use",
        ],
      });
    });

    it("should return an error if the credential is not issued by the client DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const wrongClientDid = util.createDid(randomBytes(16));
      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: wrongClientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential issuer ${vcIssuer.did} is not the same as client's DID: ${wrongClientDid}`,
        ],
      });
    });

    it("should return an error if the credential is not issued to the sub-account DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: issuerMockDid, // Issued to Issuer Mock instead of sub-account DID
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential is not valid: credentialSubject.id is not equal to the sub-account DID ${subAccountDid}`,
        ],
      });
    });

    it("should return an error if the credential is not a VerifiableAuthorisationToOnboard", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          // "VerifiableAuthorisationToOnboard", // Missing type
        ],
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: credential types don't contain 'VerifiableAuthorisationToOnboard'",
        ],
      });
    });

    it("should return an error if the Issuer Mock can't get an access token to register the DID document", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest
        .spyOn(cacheManager, "get")
        .mockImplementation(async (key: string) => {
          // Only mock the cache entry for the VerifiableAuthorisationToOnboard
          if (
            key ===
            getCredentialId(
              { clientId, did: clientDid },
              "VerifiableAuthorisationToOnboard"
            )
          ) {
            return Promise.resolve(vcJwt);
          }

          return Promise.resolve();
        });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock Auth API v3 /token endpoint
      nock(domain).post(`${AUTH_API_PATH}/token`).reply(500, {
        title: "Internal error",
        status: 500,
        detail: "Internal error",
      });

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        errors: [
          'Failed to get DID Registry invite access token: {"title":"Internal error","status":500,"detail":"Internal error"}',
        ],
        success: false,
      });
    });

    it("should return an error if the DIDR /jsonrpc insertDidDocument method returns an error", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock Auth API v3 /token endpoint
      nock(domain).post(`${AUTH_API_PATH}/token`).reply(200, {
        access_token: "jwt",
        id_token: "jwt",
        token_type: "Bearer",
        scope: "openid didr_invite",
        expires_in: 7200,
      });

      // Mock DIDR API v4 /jsonrpc endpoint
      nock(domain).post(DIDR_JSON_RPC_PATH).reply(400, {
        title: "Bad Request",
        status: 400,
        detail: "Invalid method",
        type: "about:blank",
      });

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Unable to build the transaction to register the DID document: {"title":"Bad Request","status":400,"detail":"Invalid method","type":"about:blank"}',
        ],
      });
    });

    it("should return an error if the DIDR /jsonrpc sendSignedTransaction method returns an error", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock Auth API v3 /token endpoint
      nock(domain).post(`${AUTH_API_PATH}/token`).reply(200, {
        access_token: "jwt",
        id_token: "jwt",
        token_type: "Bearer",
        scope: "openid didr_invite",
        expires_in: 7200,
      });

      // Mock DIDR API v4 /jsonrpc endpoint
      nock(domain)
        .persist()
        .post(DIDR_JSON_RPC_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          if (requestBody["method"] === "insertDidDocument") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "sendSignedTransaction") {
            return [
              400,
              {
                title: "Bad Request",
                status: 400,
                detail: "Error",
                type: "about:blank",
              },
            ];
          }

          return [
            400,
            {
              title: "Bad Request",
              status: 400,
              detail: "Invalid method",
              type: "about:blank",
            },
          ];
        });

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Unable to send the transaction to register the DID document: {"title":"Bad Request","status":400,"detail":"Error","type":"about:blank"}',
        ],
      });
    });

    it("should return an error if the Ledger API doesn't return the TX status", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock Auth API v3 /token endpoint
      nock(domain).post(`${AUTH_API_PATH}/token`).reply(200, {
        access_token: "jwt",
        id_token: "jwt",
        token_type: "Bearer",
        scope: "openid didr_invite",
        expires_in: 7200,
      });

      // Mock DIDR API v4 /jsonrpc endpoint
      nock(domain)
        .persist()
        .post(DIDR_JSON_RPC_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          if (requestBody["method"] === "insertDidDocument") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "sendSignedTransaction") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 1,
                result:
                  "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331",
              },
            ];
          }

          return [
            400,
            {
              title: "Bad Request",
              status: 400,
              detail: "Invalid method",
              type: "about:blank",
            },
          ];
        });

      // Mock Ledger API v3 /blockchains/besu endpoint
      nock(domain).post(LEDGER_API_PATH).reply(400, {
        title: "Bad Request",
        status: 400,
        detail: "Error",
        type: "about:blank",
      });

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Transaction not mined: {"title":"Bad Request","status":400,"detail":"Error","type":"about:blank"}',
        ],
      });
    });

    it("should return an error if the block was not mined", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock Auth API v3 /token endpoint
      nock(domain).post(`${AUTH_API_PATH}/token`).reply(200, {
        access_token: "jwt",
        id_token: "jwt",
        token_type: "Bearer",
        scope: "openid didr_invite",
        expires_in: 7200,
      });

      // Mock DIDR API v4 /jsonrpc endpoint
      nock(domain)
        .persist()
        .post(DIDR_JSON_RPC_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          if (requestBody["method"] === "insertDidDocument") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "sendSignedTransaction") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 1,
                result:
                  "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331",
              },
            ];
          }

          return [
            400,
            {
              title: "Bad Request",
              status: 400,
              detail: "Invalid method",
              type: "about:blank",
            },
          ];
        });

      // Mock Ledger API v3 /blockchains/besu endpoint
      nock(domain)
        .post(LEDGER_API_PATH)
        .reply(200, {
          jsonrpc: "2.0",
          id: 1,
          result: {
            status: "0x00",
            revertReason: "0x52657665727420726561736f6e",
          },
        });

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Transaction failed: Status 0x00. Revert reason: Revert reason",
        ],
      });
    });

    it("should return an error if the DIDR /jsonrpc addVerificationMethod method returns an error", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock Auth API v3 /token endpoint
      nock(domain).post(`${AUTH_API_PATH}/token`).reply(200, {
        access_token: "jwt",
        id_token: "jwt",
        token_type: "Bearer",
        scope: "openid didr_invite",
        expires_in: 7200,
      });

      // Mock DIDR API v4 /jsonrpc endpoint
      nock(domain)
        .persist()
        .post(DIDR_JSON_RPC_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          if (requestBody["method"] === "insertDidDocument") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "addVerificationMethod") {
            return [
              400,
              {
                title: "Bad Request",
                status: 400,
                detail: "Error",
                type: "about:blank",
              },
            ];
          }

          if (requestBody["method"] === "sendSignedTransaction") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 1,
                result:
                  "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331",
              },
            ];
          }

          return [
            400,
            {
              title: "Bad Request",
              status: 400,
              detail: "Invalid method",
              type: "about:blank",
            },
          ];
        });

      // Mock Ledger API v3 /blockchains/besu endpoint
      nock(domain)
        .post(LEDGER_API_PATH)
        .reply(200, {
          jsonrpc: "2.0",
          id: 1,
          result: {
            status: "0x01",
          },
        });

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Unable to build the transaction to add an ES256 verification method to the DID document: {"title":"Bad Request","status":400,"detail":"Error","type":"about:blank"}',
        ],
      });
    });

    it("should return an error if the DIDR /jsonrpc sendSignedTransaction method returns an error (for addVerificationMethod)", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock Auth API v3 /token endpoint
      nock(domain).post(`${AUTH_API_PATH}/token`).reply(200, {
        access_token: "jwt",
        id_token: "jwt",
        token_type: "Bearer",
        scope: "openid didr_invite",
        expires_in: 7200,
      });

      // Mock DIDR API v4 /jsonrpc endpoint
      let counter = 0;
      nock(domain)
        .persist()
        .post(DIDR_JSON_RPC_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          if (requestBody["method"] === "insertDidDocument") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "addVerificationMethod") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "sendSignedTransaction") {
            // The first request passes
            if (counter === 0) {
              counter += 1;
              return [
                200,
                {
                  jsonrpc: "2.0",
                  id: 1,
                  result:
                    "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331",
                },
              ];
            }

            // The second request fails
            return [
              400,
              {
                title: "Bad Request",
                status: 400,
                detail: "Error",
                type: "about:blank",
              },
            ];
          }

          return [
            400,
            {
              title: "Bad Request",
              status: 400,
              detail: "Invalid method",
              type: "about:blank",
            },
          ];
        });

      // Mock Ledger API v3 /blockchains/besu endpoint
      nock(domain)
        .post(LEDGER_API_PATH)
        .reply(200, {
          jsonrpc: "2.0",
          id: 1,
          result: {
            status: "0x01",
          },
        });

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Unable to send the transaction to add an ES256 verification method to the DID document: {"title":"Bad Request","status":400,"detail":"Error","type":"about:blank"}',
        ],
      });
    });

    it("should return an error if the transaction to add a new verification method is not mined", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock Auth API v3 /token endpoint
      nock(domain).post(`${AUTH_API_PATH}/token`).reply(200, {
        access_token: "jwt",
        id_token: "jwt",
        token_type: "Bearer",
        scope: "openid didr_invite",
        expires_in: 7200,
      });

      // Mock DIDR API v4 /jsonrpc endpoint
      let counter = 0;
      nock(domain)
        .persist()
        .post(DIDR_JSON_RPC_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          if (requestBody["method"] === "insertDidDocument") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "addVerificationMethod") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "sendSignedTransaction") {
            counter += 1;
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 1,
                result:
                  counter === 1
                    ? "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331"
                    : "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527332",
              },
            ];
          }

          return [
            400,
            {
              title: "Bad Request",
              status: 400,
              detail: "Invalid method",
              type: "about:blank",
            },
          ];
        });

      // Mock Ledger API v3 /blockchains/besu endpoint
      nock(domain)
        .persist()
        .post(LEDGER_API_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          // Mine first TX, but don't mine second TX
          return [
            200,
            {
              jsonrpc: "2.0",
              id: 1,
              result:
                (requestBody["params"] as string[])[0] ===
                "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331"
                  ? { status: "0x01" }
                  : {
                      status: "0x00",
                      revertReason: "0x52657665727420726561736f6e",
                    },
            },
          ];
        });

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Transaction failed: Status 0x00. Revert reason: Revert reason",
        ],
      });
    });

    it("should return an error if the DIDR /jsonrpc sendSignedTransaction method returns an error (for addVerificationRelationship)", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock Auth API v3 /token endpoint
      nock(domain).post(`${AUTH_API_PATH}/token`).reply(200, {
        access_token: "jwt",
        id_token: "jwt",
        token_type: "Bearer",
        scope: "openid didr_invite",
        expires_in: 7200,
      });

      // Mock DIDR API v4 /jsonrpc endpoint
      nock(domain)
        .persist()
        .post(DIDR_JSON_RPC_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          if (requestBody["method"] === "insertDidDocument") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "addVerificationMethod") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "addVerificationRelationship") {
            return [
              400,
              {
                title: "Bad Request",
                status: 400,
                detail: "Error",
                type: "about:blank",
              },
            ];
          }

          if (requestBody["method"] === "sendSignedTransaction") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 1,
                result:
                  "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331",
              },
            ];
          }

          return [
            400,
            {
              title: "Bad Request",
              status: 400,
              detail: "Invalid method",
              type: "about:blank",
            },
          ];
        });

      // Mock Ledger API v3 /blockchains/besu endpoint
      nock(domain)
        .persist()
        .post(LEDGER_API_PATH)
        .reply(200, {
          jsonrpc: "2.0",
          id: 1,
          result: {
            status: "0x01",
          },
        });

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Unable to build the transaction to register the ES256 verification method as an authentication method: {"title":"Bad Request","status":400,"detail":"Error","type":"about:blank"}',
        ],
      });
    });

    it("should return { success: true } if the credential is valid and the Issuer Mock has registered the sub-account DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock Auth API v3 /token endpoint
      nock(domain).post(`${AUTH_API_PATH}/token`).reply(200, {
        access_token: "jwt",
        id_token: "jwt",
        token_type: "Bearer",
        scope: "openid didr_invite",
        expires_in: 7200,
      });

      // Mock DIDR API v4 /jsonrpc endpoint
      nock(domain)
        .persist()
        .post(DIDR_JSON_RPC_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          if (requestBody["method"] === "insertDidDocument") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "addVerificationMethod") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "addVerificationRelationship") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "sendSignedTransaction") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 1,
                result:
                  "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331",
              },
            ];
          }

          return [
            400,
            {
              title: "Bad Request",
              status: 400,
              detail: "Invalid method",
              type: "about:blank",
            },
          ];
        });

      // Mock Ledger API v3 /blockchains/besu endpoint
      nock(domain)
        .persist()
        .post(LEDGER_API_PATH)
        .reply(200, {
          jsonrpc: "2.0",
          id: 1,
          result: {
            status: "0x01",
          },
        });

      const response =
        await issuerMockService.processVerifiableAuthorisationToOnboardValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: true,
      });
    });
  });

  describe("processVAToAttestSubaccountRequest", () => {
    const clientId = "https://client-mock.xyz";
    const clientDid = "did:ebsi:zgPs5MVWHwJJb4g9kZvYf3e";

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processVAToAttestSubaccountRequest({
          // did, Missing did
          clientId: "not an url",
        });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Validation error. Path: 'data.clientId'. Reason: Invalid url",
          "Validation error. Path: 'data.did'. Reason: Required",
        ],
      });
    });

    it("should return the error reported by issuerMockService.requestCredential if there's an error", async () => {
      jest.spyOn(issuerMockUtils, "requestCredential").mockResolvedValue({
        success: false,
        errors: ["error"],
      });

      const response =
        await issuerMockService.processVAToAttestSubaccountRequest({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({ success: false, errors: ["error"] });
    });

    it("should store the credential in cache and return { success: true } if the request is valid", async () => {
      expect.assertions(2);

      jest.spyOn(issuerMockUtils, "requestCredential").mockResolvedValue({
        success: true,
        credential: "jwt",
      });

      const cacheSpy = jest.spyOn(cacheManager, "set");

      const response =
        await issuerMockService.processVAToAttestSubaccountRequest({
          clientId,
          did: clientDid,
        });

      expect(cacheSpy).toHaveBeenCalledWith(
        getCredentialId(
          { clientId, did: clientDid },
          "VerifiableAccreditationToAttest"
        ),
        "jwt",
        300_000 // 5 minutes
      );
      expect(response).toStrictEqual({ success: true });
    });
  });

  describe("processVerifiableAccreditationToAttestValidation", () => {
    const clientId = "https://client-mock.xyz";
    let clientDid: string;
    const requestedTypes = [
      "VerifiableCredential",
      "VerifiableAttestation",
      "VerifiableAccreditation",
      "VerifiableAccreditationToAttest",
    ];
    let subAccountDid: string;

    beforeEach(() => {
      clientDid = util.createDid(randomBytes(16));
      subAccountDid = getSubAccountDid(clientDid);
    });

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            // did, Missing did
            clientId: "not an url",
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Validation error. Path: 'data.clientId'. Reason: Invalid url",
          "Validation error. Path: 'data.did'. Reason: Required",
        ],
      });
    });

    it("should return an error if no credential is found", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["No credential found for this client."],
      });
    });

    it("should return an error if the stored credential is not a string", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve({ credential: { foo: "bar" } });
      });

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential is not a string."],
      });
    });

    it("should return an error if the stored credential is not a valid JWT", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve("not a jwt");
      });

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential is not valid: Unable to decode JWT VC"],
      });
    });

    it("should return an error if the stored credential is not a valid Verifiable Attestation JWT", async () => {
      expect.assertions(1);

      const jwt = await new SignJWT({
        foo: "bar",
        vc: {},
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: `${clientDid}#key-1`,
        })
        .sign((await generateKeyPair("ES256")).privateKey);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(jwt);
      });

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: Invalid EBSI Verifiable Attestation",
          "Credential must have required property '@context'",
          "Credential must have required property 'id'",
          "Credential must have required property 'type'",
          "Credential must have required property 'issuer'",
          "Credential must have required property 'issuanceDate'",
          "Credential must have required property 'issued'",
          "Credential must have required property 'validFrom'",
          "Credential must have required property 'credentialSubject'",
          "Credential must have required property 'credentialSchema'",
        ],
      });
    });

    it("should return an error if the credential doesn't contain termsOfUse", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;
      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };
      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: Credential doesn't contain terms of use",
        ],
      });
    });

    it("should return an error if the credential is not issued by the client DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const wrongClientDid = util.createDid(randomBytes(16));
      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: wrongClientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential issuer ${vcIssuer.did} is not the same as client's DID: ${wrongClientDid}`,
        ],
      });
    });

    it("should return an error if the credential is not issued to the sub-account DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: issuerMockDid, // Issued to Issuer Mock instead of sub-account DID
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential is not valid: credentialSubject.id is not equal to the sub-account DID ${subAccountDid}`,
        ],
      });
    });

    it("should return an error if the credential is not a VerifiableAccreditationToAttest", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          // "VerifiableAccreditationToAttest", // Missing type
        ],
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: credential types don't contain 'VerifiableAccreditationToAttest'",
        ],
      });
    });

    it("should return an error if the credentialSubject doesn't contain a reservedAttributeId attribute", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: credentialSubject doesn't contain a 'reservedAttributeId' property",
        ],
      });
    });

    it("should return an error if the credentialSubject 'reservedAttributeId' attribute is not a string", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: 43, // Not a string
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: 'credentialSubject.reservedAttributeId' property is not a string",
        ],
      });
    });

    it("should return an error if the reserved attribute can't be found", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // The client did not pre-register the attribute
      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(404, {
          title: "Not found",
          status: 404,
          detail: "Attribute 0x00 not found",
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Unable to get pre-registered attribute 0x0000: {"title":"Not found","status":404,"detail":"Attribute 0x00 not found"}',
        ],
      });
    });

    it("should return an error if the reserved attribute 'did' property doesn't match the sub-account DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // The client did not correctly pre-register the attribute
      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: clientDid, // Should be subAccountDid
          attribute: {},
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [`Attribute 0x0000 is not registered for DID ${subAccountDid}`],
      });
    });

    it("should return an error if the reserved attribute 'issuerType' property is not 'TI'", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // The client did not correctly pre-register the attribute
      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "RootTAO", // Not "TI"
            tao: clientDid,
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Attribute 0x0000 \'issuerType\' property must be "TI", got "RootTAO"',
        ],
      });
    });

    it("should return an error if the reserved attribute 'tao' property doesn't match the client DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // The client did not correctly pre-register the attribute
      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "TI",
            tao: issuerMockDid, // Not the Client DID
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Attribute 0x0000 'tao' property must be ${clientDid}, got ${issuerMockDid}`,
        ],
      });
    });

    it("should return an error if the reserved attribute 'rootTao' property doesn't match the Issuer Mock DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // The client did not correctly pre-register the attribute
      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "TI",
            tao: clientDid,
            rootTao: clientDid, // Not the Issuer Mock DID
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Attribute 0x0000 'rootTao' property must be ${issuerMockDid}, got ${clientDid}`,
        ],
      });
    });

    it("should return an error if the reserved attribute 'body' property is not empty", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // The client did not correctly pre-register the attribute
      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "some text",
            issuerType: "TI",
            tao: clientDid,
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["Attribute 0x0000 'body' property must be empty"],
      });
    });

    it("should return an error if the Issuer Mock can't get an access token to register the accreditation", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest
        .spyOn(cacheManager, "get")
        .mockImplementation(async (key: string) => {
          // Only mock the cache entry for the VerifiableAccreditationToAttest
          if (
            key ===
            getCredentialId(
              { clientId, did: clientDid },
              "VerifiableAccreditationToAttest"
            )
          ) {
            return Promise.resolve(vcJwt);
          }

          return Promise.resolve();
        });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "TI",
            tao: clientDid,
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock Auth API v3 /token endpoint
      nock(domain).post(`${AUTH_API_PATH}/token`).reply(500, {
        title: "Internal error",
        status: 500,
        detail: "Internal error",
      });

      // Mock TIR API v4 /jsonrpc endpoint
      nock(domain).post(TIR_JSON_RPC_PATH).reply(400, {
        title: "Bad Request",
        status: 400,
        detail: "Invalid method",
        type: "about:blank",
      });

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Failed to get tir_invite access token: {"title":"Internal error","status":500,"detail":"Internal error"}',
        ],
      });
    });

    it("should return an error if the TIR API /jsonrpc endpoint returns an error", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "TI",
            tao: clientDid,
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock TIR API v4 /jsonrpc endpoint
      nock(domain).post(TIR_JSON_RPC_PATH).reply(400, {
        title: "Bad Request",
        status: 400,
        detail: "Invalid method",
        type: "about:blank",
      });

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Unable to build the transaction to register the accreditation: {"title":"Bad Request","status":400,"detail":"Invalid method","type":"about:blank"}',
        ],
      });
    });

    it("should return an error if the TIR API /jsonrpc sendSignedTransaction method returns an error", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "TI",
            tao: clientDid,
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock TIR API v4 /jsonrpc endpoint
      nock(domain)
        .persist()
        .post(TIR_JSON_RPC_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          if (requestBody["method"] === "setAttributeData") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "sendSignedTransaction") {
            return [
              400,
              {
                title: "Bad Request",
                status: 400,
                detail: "Error",
                type: "about:blank",
              },
            ];
          }

          return [
            400,
            {
              title: "Bad Request",
              status: 400,
              detail: "Invalid method",
              type: "about:blank",
            },
          ];
        });

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Unable to send the transaction to register the accreditation: {"title":"Bad Request","status":400,"detail":"Error","type":"about:blank"}',
        ],
      });
    });

    it("should return an error if the Ledger API doesn't return the TX status", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "TI",
            tao: clientDid,
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock TIR API v4 /jsonrpc endpoint
      nock(domain)
        .persist()
        .post(TIR_JSON_RPC_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          if (requestBody["method"] === "setAttributeData") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "sendSignedTransaction") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 1,
                result:
                  "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331",
              },
            ];
          }

          return [
            400,
            {
              title: "Bad Request",
              status: 400,
              detail: "Invalid method",
              type: "about:blank",
            },
          ];
        });

      // Mock Ledger API v3 /blockchains/besu endpoint
      nock(domain).post(LEDGER_API_PATH).reply(400, {
        title: "Bad Request",
        status: 400,
        detail: "Error",
        type: "about:blank",
      });

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Transaction not mined: {"title":"Bad Request","status":400,"detail":"Error","type":"about:blank"}',
        ],
      });
    });

    it("should return an error if the block was not mined", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "TI",
            tao: clientDid,
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock TIR API v4 /jsonrpc endpoint
      nock(domain)
        .persist()
        .post(TIR_JSON_RPC_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          if (requestBody["method"] === "setAttributeData") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "sendSignedTransaction") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 1,
                result:
                  "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331",
              },
            ];
          }

          return [
            400,
            {
              title: "Bad Request",
              status: 400,
              detail: "Invalid method",
              type: "about:blank",
            },
          ];
        });

      // Mock Ledger API v3 /blockchains/besu endpoint
      nock(domain)
        .post(LEDGER_API_PATH)
        .reply(200, {
          jsonrpc: "2.0",
          id: 1,
          result: {
            status: "0x00",
            revertReason: "0x52657665727420726561736f6e",
          },
        });

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Transaction failed: Status 0x00. Revert reason: Revert reason",
        ],
      });
    });

    it("should return { success: true } if the credential is valid and the Issuer Mock has registered the accreditation", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAttest",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "TI",
            tao: clientDid,
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock TIR API v4 /jsonrpc endpoint
      nock(domain)
        .persist()
        .post(TIR_JSON_RPC_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          if (requestBody["method"] === "setAttributeData") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "sendSignedTransaction") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 1,
                result:
                  "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331",
              },
            ];
          }

          return [
            400,
            {
              title: "Bad Request",
              status: 400,
              detail: "Invalid method",
              type: "about:blank",
            },
          ];
        });

      // Mock Ledger API v3 /blockchains/besu endpoint
      nock(domain)
        .post(LEDGER_API_PATH)
        .reply(200, {
          jsonrpc: "2.0",
          id: 1,
          result: {
            status: "0x01",
          },
        });

      const response =
        await issuerMockService.processVerifiableAccreditationToAttestValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: true,
      });
    });
  });

  describe("processVAToAccreditSubaccountRequest", () => {
    const clientId = "https://client-mock.xyz";
    const clientDid = "did:ebsi:zgPs5MVWHwJJb4g9kZvYf3e";

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processVAToAccreditSubaccountRequest({
          // did, Missing did
          clientId: "not an url",
        });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Validation error. Path: 'data.clientId'. Reason: Invalid url",
          "Validation error. Path: 'data.did'. Reason: Required",
        ],
      });
    });

    it("should return the error reported by issuerMockService.requestCredential if there's an error", async () => {
      jest.spyOn(issuerMockUtils, "requestCredential").mockResolvedValue({
        success: false,
        errors: ["error"],
      });

      const response =
        await issuerMockService.processVAToAccreditSubaccountRequest({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({ success: false, errors: ["error"] });
    });

    it("should store the credential in cache and return { success: true } if the request is valid", async () => {
      expect.assertions(2);

      jest.spyOn(issuerMockUtils, "requestCredential").mockResolvedValue({
        success: true,
        credential: "jwt",
      });

      const cacheSpy = jest.spyOn(cacheManager, "set");

      const response =
        await issuerMockService.processVAToAccreditSubaccountRequest({
          clientId,
          did: clientDid,
        });

      expect(cacheSpy).toHaveBeenCalledWith(
        getCredentialId(
          { clientId, did: clientDid },
          "VerifiableAccreditationToAccredit"
        ),
        "jwt",
        300_000 // 5 minutes
      );
      expect(response).toStrictEqual({ success: true });
    });
  });

  describe("processVerifiableAccreditationToAccreditValidation", () => {
    const clientId = "https://client-mock.xyz";
    let clientDid: string;
    const requestedTypes = [
      "VerifiableCredential",
      "VerifiableAttestation",
      "VerifiableAccreditation",
      "VerifiableAccreditationToAccredit",
    ];
    let subAccountDid: string;

    beforeEach(() => {
      clientDid = util.createDid(randomBytes(16));
      subAccountDid = getSubAccountDid(clientDid);
    });

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            // did, Missing did
            clientId: "not an url",
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Validation error. Path: 'data.clientId'. Reason: Invalid url",
          "Validation error. Path: 'data.did'. Reason: Required",
        ],
      });
    });

    it("should return an error if no credential is found", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["No credential found for this client."],
      });
    });

    it("should return an error if the stored credential is not a string", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve({ credential: { foo: "bar" } });
      });

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential is not a string."],
      });
    });

    it("should return an error if the stored credential is not a valid JWT", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve("not a jwt");
      });

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential is not valid: Unable to decode JWT VC"],
      });
    });

    it("should return an error if the stored credential is not a valid Verifiable Attestation JWT", async () => {
      expect.assertions(1);

      const jwt = await new SignJWT({
        foo: "bar",
        vc: {},
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: `${clientDid}#key-1`,
        })
        .sign((await generateKeyPair("ES256")).privateKey);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(jwt);
      });

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: Invalid EBSI Verifiable Attestation",
          "Credential must have required property '@context'",
          "Credential must have required property 'id'",
          "Credential must have required property 'type'",
          "Credential must have required property 'issuer'",
          "Credential must have required property 'issuanceDate'",
          "Credential must have required property 'issued'",
          "Credential must have required property 'validFrom'",
          "Credential must have required property 'credentialSubject'",
          "Credential must have required property 'credentialSchema'",
        ],
      });
    });

    it("should return an error if the credential doesn't contain termsOfUse", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;
      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };
      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: Credential doesn't contain terms of use",
        ],
      });
    });

    it("should return an error if the credential is not issued by the client DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const wrongClientDid = util.createDid(randomBytes(16));
      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: wrongClientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential issuer ${vcIssuer.did} is not the same as client's DID: ${wrongClientDid}`,
        ],
      });
    });

    it("should return an error if the credential is not issued to the sub-account DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: issuerMockDid, // Issued to Issuer Mock instead of sub-account DID
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential is not valid: credentialSubject.id is not equal to the sub-account DID ${subAccountDid}`,
        ],
      });
    });

    it("should return an error if the credential is not a VerifiableAccreditationToAccredit", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          // "VerifiableAccreditationToAccredit", // Missing type
        ],
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: credential types don't contain 'VerifiableAccreditationToAccredit'",
        ],
      });
    });

    it("should return an error if the credentialSubject doesn't contain a reservedAttributeId attribute", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: credentialSubject doesn't contain a 'reservedAttributeId' property",
        ],
      });
    });

    it("should return an error if the credentialSubject 'reservedAttributeId' attribute is not a string", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: 43, // Not a string
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: 'credentialSubject.reservedAttributeId' property is not a string",
        ],
      });
    });

    it("should return an error if the reserved attribute can't be found", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // The client did not pre-register the attribute
      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(404, {
          title: "Not found",
          status: 404,
          detail: "Attribute 0x00 not found",
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Unable to get pre-registered attribute 0x0000: {"title":"Not found","status":404,"detail":"Attribute 0x00 not found"}',
        ],
      });
    });

    it("should return an error if the reserved attribute 'did' property doesn't match the sub-account DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // The client did not correctly pre-register the attribute
      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: clientDid, // Should be subAccountDid
          attribute: {},
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [`Attribute 0x0000 is not registered for DID ${subAccountDid}`],
      });
    });

    it("should return an error if the reserved attribute 'issuerType' property is not 'TAO'", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // The client did not correctly pre-register the attribute
      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "RootTAO", // Not "TAO"
            tao: clientDid,
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Attribute 0x0000 \'issuerType\' property must be "TAO", got "RootTAO"',
        ],
      });
    });

    it("should return an error if the reserved attribute 'tao' property doesn't match the client DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // The client did not correctly pre-register the attribute
      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "TAO",
            tao: issuerMockDid, // Not the Client DID
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Attribute 0x0000 'tao' property must be ${clientDid}, got ${issuerMockDid}`,
        ],
      });
    });

    it("should return an error if the reserved attribute 'rootTao' property doesn't match the Issuer Mock DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // The client did not correctly pre-register the attribute
      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "TAO",
            tao: clientDid,
            rootTao: clientDid, // Not the Issuer Mock DID
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Attribute 0x0000 'rootTao' property must be ${issuerMockDid}, got ${clientDid}`,
        ],
      });
    });

    it("should return an error if the reserved attribute 'body' property is not empty", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      // The client did not correctly pre-register the attribute
      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "some text",
            issuerType: "TAO",
            tao: clientDid,
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["Attribute 0x0000 'body' property must be empty"],
      });
    });

    it("should return an error if the Issuer Mock can't get an access token to register the accreditation", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest
        .spyOn(cacheManager, "get")
        .mockImplementation(async (key: string) => {
          // Only mock the cache entry for the VerifiableAccreditationToAccredit
          if (
            key ===
            getCredentialId(
              { clientId, did: clientDid },
              "VerifiableAccreditationToAccredit"
            )
          ) {
            return Promise.resolve(vcJwt);
          }

          return Promise.resolve();
        });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "TAO",
            tao: clientDid,
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock Auth API v3 /token endpoint
      nock(domain).post(`${AUTH_API_PATH}/token`).reply(500, {
        title: "Internal error",
        status: 500,
        detail: "Internal error",
      });

      // Mock TIR API v4 /jsonrpc endpoint
      nock(domain).post(TIR_JSON_RPC_PATH).reply(400, {
        title: "Bad Request",
        status: 400,
        detail: "Invalid method",
        type: "about:blank",
      });

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Failed to get tir_write access token: {"title":"Internal error","status":500,"detail":"Internal error"}',
        ],
      });
    });

    it("should return an error if the TIR API /jsonrpc endpoint returns an error", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "TAO",
            tao: clientDid,
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock TIR API v4 /jsonrpc endpoint
      nock(domain).post(TIR_JSON_RPC_PATH).reply(400, {
        title: "Bad Request",
        status: 400,
        detail: "Invalid method",
        type: "about:blank",
      });

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Unable to build the transaction to register the accreditation: {"title":"Bad Request","status":400,"detail":"Invalid method","type":"about:blank"}',
        ],
      });
    });

    it("should return an error if the TIR API /jsonrpc sendSignedTransaction method returns an error", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "TAO",
            tao: clientDid,
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock TIR API v4 /jsonrpc endpoint
      nock(domain)
        .persist()
        .post(TIR_JSON_RPC_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          if (requestBody["method"] === "setAttributeData") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "sendSignedTransaction") {
            return [
              400,
              {
                title: "Bad Request",
                status: 400,
                detail: "Error",
                type: "about:blank",
              },
            ];
          }

          return [
            400,
            {
              title: "Bad Request",
              status: 400,
              detail: "Invalid method",
              type: "about:blank",
            },
          ];
        });

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Unable to send the transaction to register the accreditation: {"title":"Bad Request","status":400,"detail":"Error","type":"about:blank"}',
        ],
      });
    });

    it("should return an error if the Ledger API doesn't return the TX status", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "TAO",
            tao: clientDid,
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock TIR API v4 /jsonrpc endpoint
      nock(domain)
        .persist()
        .post(TIR_JSON_RPC_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          if (requestBody["method"] === "setAttributeData") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "sendSignedTransaction") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 1,
                result:
                  "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331",
              },
            ];
          }

          return [
            400,
            {
              title: "Bad Request",
              status: 400,
              detail: "Invalid method",
              type: "about:blank",
            },
          ];
        });

      // Mock Ledger API v3 /blockchains/besu endpoint
      nock(domain).post(LEDGER_API_PATH).reply(400, {
        title: "Bad Request",
        status: 400,
        detail: "Error",
        type: "about:blank",
      });

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          'Transaction not mined: {"title":"Bad Request","status":400,"detail":"Error","type":"about:blank"}',
        ],
      });
    });

    it("should return an error if the block was not mined", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "TAO",
            tao: clientDid,
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock TIR API v4 /jsonrpc endpoint
      nock(domain)
        .persist()
        .post(TIR_JSON_RPC_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          if (requestBody["method"] === "setAttributeData") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "sendSignedTransaction") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 1,
                result:
                  "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331",
              },
            ];
          }

          return [
            400,
            {
              title: "Bad Request",
              status: 400,
              detail: "Invalid method",
              type: "about:blank",
            },
          ];
        });

      // Mock Ledger API v3 /blockchains/besu endpoint
      nock(domain)
        .post(LEDGER_API_PATH)
        .reply(200, {
          jsonrpc: "2.0",
          id: 1,
          result: {
            status: "0x00",
            revertReason: "0x52657665727420726561736f6e",
          },
        });

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Transaction failed: Status 0x00. Revert reason: Revert reason",
        ],
      });
    });

    it("should return { success: true } if the credential is valid and the Issuer Mock has registered the accreditation", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const issuerMockKeyPair = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      const accreditationIssuer: EbsiIssuer = {
        kid: issuerMockKid,
        did: issuerMockDid,
        publicKeyJwk: issuerMockKeyPair.publicKeyJwk,
        privateKeyJwk: issuerMockKeyPair.privateKeyJwk,
        alg: "ES256",
      };
      const accreditationPayload: EbsiVerifiableAttestation = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAccreditation",
          "VerifiableAccreditationToAccredit",
        ],
        issuer: accreditationIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: issuerMockAccreditationUrl,
          type: "IssuanceCertificate",
        },
      };
      const accreditationJwt = await createVerifiableCredentialJwt(
        accreditationPayload,
        accreditationIssuer,
        {
          ebsiAuthority,
          skipValidation: true,
        }
      );

      const keyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(keyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(keyPair.privateKey);
      const clientPublicKeyJwkThumbprint = await calculateJwkThumbprint(
        clientPublicKeyJwk
      );
      const clientKid = `${clientDid}#${clientPublicKeyJwkThumbprint}`;
      const clientAccreditationUri = `${TIR_API_PATH}/${clientDid}/attributes/0x1234`;
      const vcIssuer: EbsiIssuer = {
        kid: clientKid,
        did: clientDid,
        publicKeyJwk: clientPublicKeyJwk,
        privateKeyJwk: clientPrivateKeyJwk,
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: requestedTypes,
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: subAccountDid,
          reservedAttributeId: "0x0000",
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: `${domain}${clientAccreditationUri}`,
          type: "IssuanceCertificate",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock Client's DID document and TIR entry
      nock(domain)
        .get(`/did-registry/v4/identifiers/${clientDid}`)
        .reply(200, createDidDocument(clientDid, clientKid, clientPublicKeyJwk))
        .persist();

      nock(domain).get(`${TIR_API_PATH}/${clientDid}`).reply(200, {}).persist();

      nock(domain)
        .get(clientAccreditationUri)
        .reply(200, {
          did: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          attribute: {
            hash: "312f8fd655a9533dde12b191856f0b38fbcb40e72902193c48b8f80b27983722",
            body: accreditationJwt,
            issuerType: "RootTAO",
            tao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
            rootTao: "did:ebsi:zz7XsC9ixAXuZecoD9sZEM1",
          },
        })
        .persist();

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x0000`)
        .reply(200, {
          did: subAccountDid,
          attribute: {
            hash: "70d764b0f55edf573985dbc34cf93c9afce086548a8ac32f30c5fb6f42a3f758",
            body: "",
            issuerType: "TAO",
            tao: clientDid,
            rootTao: issuerMockDid,
          },
        })
        .persist();

      // Mock JSON-LD context schemas
      mockSchemas();

      // Mock TIR API v4 /jsonrpc endpoint
      nock(domain)
        .persist()
        .post(TIR_JSON_RPC_PATH)
        .reply((_, requestBody) => {
          if (typeof requestBody === "string") {
            throw new Error("Unsupported body");
          }

          if (requestBody["method"] === "setAttributeData") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 482,
                result: {
                  from: "0x8445fC96e27ceB0d1794be16a1Ac8BF5D765AACB",
                  to: "0x823BBc0ceE3dE3B61AcfA0CEedb951AB9a013F05",
                  data: "0x250f4d9700000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000120fb33cccb447605de75846617385f6d2aac28010f53a89ecb5ce151751c53c8d400000000000000000000000000000000000000000000000000000000000000206469643a656273693a7a5a766970744568464e31723275313868686937486f66000000000000000000000000000000000000000000000000000000000000000865794a68622e2e2e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000216469643a656273693a7a323536617342576d4842736a325a4e567847354d686b7000000000000000000000000000000000000000000000000000000000000000",
                  value: "0x0",
                  nonce: "0xb1d3",
                  chainId: "0x181f",
                  gasLimit: "0x1000000",
                  gasPrice: "0x0",
                },
              },
            ];
          }

          if (requestBody["method"] === "sendSignedTransaction") {
            return [
              200,
              {
                jsonrpc: "2.0",
                id: 1,
                result:
                  "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331",
              },
            ];
          }

          return [
            400,
            {
              title: "Bad Request",
              status: 400,
              detail: "Invalid method",
              type: "about:blank",
            },
          ];
        });

      // Mock Ledger API v3 /blockchains/besu endpoint
      nock(domain)
        .post(LEDGER_API_PATH)
        .reply(200, {
          jsonrpc: "2.0",
          id: 1,
          result: {
            status: "0x01",
          },
        });

      const response =
        await issuerMockService.processVerifiableAccreditationToAccreditValidation(
          {
            clientId,
            did: clientDid,
          }
        );

      expect(response).toStrictEqual({
        success: true,
      });
    });
  });

  describe("checkAccreditAuthoriseTests", () => {
    const clientDid = "did:ebsi:zgPs5MVWHwJJb4g9kZvYf3e";

    afterEach(() => {
      jest.resetAllMocks();
    });

    it("should return false on empty logs", async () => {
      expect.assertions(1);
      jest.spyOn(logsService, "getParsedLogs").mockResolvedValue([]);

      const result = await issuerMockService.checkAccreditAuthoriseTests(
        clientDid
      );

      expect(result).toStrictEqual({
        error: `No logs found for DID ${clientDid}`,
        success: false,
      });
    });

    it("should return false when no successful tests have been found", async () => {
      expect.assertions(1);
      jest.spyOn(logsService, "getParsedLogs").mockResolvedValue([
        {
          timestamp: "1660734188205",
          testData: {
            intent: TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
            result: {
              errors: ["No events for intents registered!"],
              success: false,
            },
            log: "",
          },
        },
      ]);

      const result = await issuerMockService.checkAccreditAuthoriseTests(
        clientDid
      );

      expect(result).toStrictEqual({
        error: `No successful tests found for DID ${clientDid}`,
        success: false,
      });
    });

    it("should return false on problematic tests", async () => {
      expect.assertions(1);
      jest.spyOn(logsService, "getParsedLogs").mockResolvedValue([
        {
          timestamp: "1660734188205",
          testData: {
            intent: TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
            result: {
              errors: ["No events for intents registered!"],
              success: false,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
            result: {
              success: true,
            },
            log: "",
          },
        },
      ]);

      const result = await issuerMockService.checkAccreditAuthoriseTests(
        clientDid
      );

      const expectedFailedTests = [
        TI_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD,
        TI_REGISTER_DID,
        TI_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST,
        TI_REGISTER_VERIFIABLE_ACCREDITATION_TO_ATTEST,
        TI_REQUEST_CT_REVOCABLE,
        TI_VALIDATE_CT_REVOCABLE,
        TI_REVOKE_CT_REVOCABLE,
        TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
        TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
        TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
        TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
        TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
        TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
        TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
        TAO_REVOKE_RIGHTS_SUBACCOUNT,
        RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
        RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
      ];

      expect(result).toStrictEqual({
        error: `The client didn't pass all the tests required to get the CTAAQualificationCredential. Failed tests: ${expectedFailedTests.join(
          ", "
        )}`,
        success: false,
      });
    });

    it("should return true on all intents a having success result", async () => {
      expect.assertions(1);
      jest.spyOn(logsService, "getParsedLogs").mockResolvedValue([
        {
          timestamp: "1660734188205",
          testData: {
            intent: TI_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: TI_REGISTER_DID,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: TI_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: TI_REGISTER_VERIFIABLE_ACCREDITATION_TO_ATTEST,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: TI_REQUEST_CT_REVOCABLE,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: TI_VALIDATE_CT_REVOCABLE,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: TI_REVOKE_CT_REVOCABLE,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent:
              TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: TAO_REVOKE_RIGHTS_SUBACCOUNT,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
            result: {
              success: true,
            },
            log: "",
          },
        },
        {
          timestamp: "1660734188205",
          testData: {
            intent: RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
            result: {
              success: true,
            },
            log: "",
          },
        },
      ]);

      const result = await issuerMockService.checkAccreditAuthoriseTests(
        clientDid
      );

      expect(result).toStrictEqual({ success: true });
    });
  });

  describe("processSubAccountRevocationValidation", () => {
    const clientId = "https://client-mock.xyz";
    let clientDid: string;
    let subAccountDid: string;

    beforeEach(() => {
      clientDid = util.createDid(randomBytes(16));
      subAccountDid = getSubAccountDid(clientDid);
    });

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processSubAccountRevocationValidation({
          // did, Missing did
          clientId: "not an url",
        });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Validation error. Path: 'data.clientId'. Reason: Invalid url",
          "Validation error. Path: 'data.did'. Reason: Required",
        ],
      });
    });

    it("should return an error if the subaccount DID is not registered in the TIR", async () => {
      expect.assertions(1);

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes`)
        .reply(404, {
          title: "Issuer Not Found",
          status: 404,
          type: "about:blank",
          detail: `Issuer ${subAccountDid} not found`,
        });

      const response =
        await issuerMockService.processSubAccountRevocationValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Unable to get attributes of ${subAccountDid}: {"title":"Issuer Not Found","status":404,"type":"about:blank","detail":"Issuer ${subAccountDid} not found"}`,
        ],
      });
    });

    it("should return an error if the subaccount DID has no accreditation", async () => {
      expect.assertions(1);

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes`)
        .reply(200, {
          items: [],
          total: 0,
          pageSize: 10,
        });

      const response =
        await issuerMockService.processSubAccountRevocationValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        success: false,
        errors: [`The trusted issuer ${subAccountDid} has no accreditations.`],
      });
    });

    it("should return an error if the subaccount DID has more than 10 accreditations", async () => {
      expect.assertions(1);

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes`)
        .reply(200, {
          items: [],
          total: 12,
          pageSize: 10,
        });

      const response =
        await issuerMockService.processSubAccountRevocationValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `The trusted issuer ${subAccountDid} has more than 10 accreditations.`,
        ],
      });
    });

    it("should return an error if one of the accreditations fails to load", async () => {
      expect.assertions(1);

      const attributesUrl = `${domain}${TIR_API_PATH}/${subAccountDid}/attributes`;

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes`)
        .reply(200, {
          items: [
            { href: `${attributesUrl}/0x00` },
            { href: `${attributesUrl}/0x01` },
          ],
          total: 2,
          pageSize: 10,
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x00`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x01`)
        .reply(500, {
          title: "Internal error",
          status: 500,
          detail: "Internal error",
        });

      const response =
        await issuerMockService.processSubAccountRevocationValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Error while fetching an attribute of ${subAccountDid}: ${attributesUrl}/0x01 returned {"title":"Internal error","status":500,"detail":"Internal error"}`,
        ],
      });
    });

    it("should return an error if one of the accreditations is not revoked", async () => {
      expect.assertions(1);

      const attributesUrl = `${domain}${TIR_API_PATH}/${subAccountDid}/attributes`;

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes`)
        .reply(200, {
          items: [
            { href: `${attributesUrl}/0x00` },
            { href: `${attributesUrl}/0x01` },
          ],
          total: 2,
          pageSize: 10,
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x00`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x01`)
        .reply(200, {
          attribute: {
            issuerType: "TAO",
          },
        });

      const response =
        await issuerMockService.processSubAccountRevocationValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        success: false,
        errors: [`Attribute ${attributesUrl}/0x01 is not revoked`],
      });
    });

    it("should return an error if one of the accreditations is not revoked (with more than 3 accreditations)", async () => {
      expect.assertions(1);

      const attributesUrl = `${domain}${TIR_API_PATH}/${subAccountDid}/attributes`;

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes`)
        .reply(200, {
          items: [
            { href: `${attributesUrl}/0x00` },
            { href: `${attributesUrl}/0x01` },
            { href: `${attributesUrl}/0x02` },
            { href: `${attributesUrl}/0x03` },
            { href: `${attributesUrl}/0x04` },
            { href: `${attributesUrl}/0x05` },
            { href: `${attributesUrl}/0x06` },
            { href: `${attributesUrl}/0x07` },
            { href: `${attributesUrl}/0x08` },
          ],
          total: 9,
          pageSize: 10,
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x00`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x01`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x02`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x03`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x04`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x05`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x06`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x07`)
        .reply(200, {
          attribute: {
            issuerType: "TAO",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x08`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      const response =
        await issuerMockService.processSubAccountRevocationValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({
        success: false,
        errors: [`Attribute ${attributesUrl}/0x07 is not revoked`],
      });
    });

    it("should return { success: true } if all the accreditations are revoked", async () => {
      expect.assertions(1);

      const attributesUrl = `${domain}${TIR_API_PATH}/${subAccountDid}/attributes`;

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes`)
        .reply(200, {
          items: [
            { href: `${attributesUrl}/0x00` },
            { href: `${attributesUrl}/0x01` },
            { href: `${attributesUrl}/0x02` },
            { href: `${attributesUrl}/0x03` },
            { href: `${attributesUrl}/0x04` },
            { href: `${attributesUrl}/0x05` },
            { href: `${attributesUrl}/0x06` },
            { href: `${attributesUrl}/0x07` },
            { href: `${attributesUrl}/0x08` },
          ],
          total: 9,
          pageSize: 10,
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x00`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x01`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x02`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x03`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x04`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x05`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x06`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x07`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      nock(domain)
        .get(`${TIR_API_PATH}/${subAccountDid}/attributes/0x08`)
        .reply(200, {
          attribute: {
            issuerType: "Revoked",
          },
        });

      const response =
        await issuerMockService.processSubAccountRevocationValidation({
          clientId,
          did: clientDid,
        });

      expect(response).toStrictEqual({ success: true });
    });
  });

  describe("processIssueToHolderCTWalletSameInTimeRequest", () => {
    const credentialIssuer = "https://client-mock.xyz";
    const credentialIssuerDid =
      "did:key:z2dmzD81cgPx8Vki7JbuuMmFYrWPgYoytykUZ3eyqht1j9KbsDbVZXdb3jzCagESyY4EE2x7Yjx3gNwctoEuRCKKDrdNP3HPFtG8RTvBiYStT5ghBHhHizH2Dy6xQtW3Pd2SecizL9b2jzDCMr7Ka5cRAWZFwvqwAtwTT7xet769y9ERh6";
    const issuerState = "issuer-state";

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameInTimeRequest({
          // credentialIssuer, credentialIssuerDid are missing
        });

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Validation error. Path: 'data.credentialIssuer'. Reason: Required",
          "Validation error. Path: 'data.credentialIssuerDid'. Reason: Required",
        ],
      });
    });

    it("should return the error reported by issuerMockService.requestCredential if there's an error", async () => {
      jest.spyOn(issuerMockUtils, "requestCredential").mockResolvedValue({
        success: false,
        errors: ["error"],
      });

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameInTimeRequest({
          credentialIssuer,
          credentialIssuerDid,
          issuerState,
        } satisfies IssueToHolderInTimeParams);

      expect(response).toStrictEqual({ success: false, errors: ["error"] });
    });

    it("should store the credential in cache and return { success: true } if the request is valid", async () => {
      expect.assertions(2);

      jest.spyOn(issuerMockUtils, "requestCredential").mockResolvedValue({
        success: true,
        credential: "jwt",
      });

      const cacheSpy = jest.spyOn(cacheManager, "set");

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameInTimeRequest({
          credentialIssuer,
          credentialIssuerDid,
          issuerState,
        } satisfies IssueToHolderInTimeParams);

      expect(cacheSpy).toHaveBeenCalledWith(
        getCredentialId(
          { clientId: credentialIssuer, did: issuerMockDid },
          "CTWalletSameInTime"
        ),
        "jwt",
        300_000 // 5 minutes
      );
      expect(response).toStrictEqual({ success: true });
    });
  });

  describe("processIssueToHolderCTWalletSameInTimeValidation", () => {
    const credentialIssuer = "https://client-mock.xyz";
    let credentialIssuerKeyPair: GenerateKeyPairResult<KeyLike>;
    let credentialIssuerDid: string;
    let requesterDid: string;

    beforeAll(async () => {
      credentialIssuerKeyPair = await generateKeyPair("ES256");
      const credentialIssuerPublicKeyJwk = await exportJWK(
        credentialIssuerKeyPair.publicKey
      );
      credentialIssuerDid = keyDidMethodHelpers.createDid(
        credentialIssuerPublicKeyJwk
      );

      const keyPairES256 = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      requesterDid = keyDidMethodHelpers.createDid(keyPairES256.publicKeyJwk);
    });

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameInTimeValidation(
          {
            // credentialIssuer, credentialIssuerDid are missing
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Validation error. Path: 'data.credentialIssuer'. Reason: Required",
          "Validation error. Path: 'data.credentialIssuerDid'. Reason: Required",
        ],
      });
    });

    it("should return an error if no credential is found", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve();
      });

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameInTimeValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
          } satisfies IssueToHolderInTimeParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["No credential found for this client."],
      });
    });

    it("should return an error if the stored credential is not a string", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve({ credential: { foo: "bar" } });
      });

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameInTimeValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
          } satisfies IssueToHolderInTimeParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential is not a string."],
      });
    });

    it("should return an error if the stored credential is not a valid JWT", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve("not a jwt");
      });

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameInTimeValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
          } satisfies IssueToHolderInTimeParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential is not valid: Unable to decode JWT VC"],
      });
    });

    it("should return an error if the stored credential is not a valid Verifiable Attestation JWT", async () => {
      expect.assertions(1);

      const jwt = await new SignJWT({
        foo: "bar",
        vc: {},
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: `${credentialIssuerDid}#key-1`,
        })
        .sign((await generateKeyPair("ES256")).privateKey);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(jwt);
      });

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameInTimeValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
          } satisfies IssueToHolderInTimeParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: Invalid EBSI Verifiable Attestation",
          "Credential must have required property '@context'",
          "Credential must have required property 'id'",
          "Credential must have required property 'type'",
          "Credential must have required property 'issuer'",
          "Credential must have required property 'issuanceDate'",
          "Credential must have required property 'issued'",
          "Credential must have required property 'validFrom'",
          "Credential must have required property 'credentialSubject'",
          "Credential must have required property 'credentialSchema'",
        ],
      });
    });

    it("should return an error if the credential subject is not equal to the requester DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const fragmentIdentifier = credentialIssuerDid.replace("did:key:", "");
      const credentialIssuerKid = `${credentialIssuerDid}#${fragmentIdentifier}`;
      const vcIssuer: EbsiIssuer = {
        did: credentialIssuerDid,
        kid: credentialIssuerKid,
        publicKeyJwk: await exportJWK(credentialIssuerKeyPair.publicKey),
        privateKeyJwk: await exportJWK(credentialIssuerKeyPair.privateKey),
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "CTWalletSameInTime",
        ],
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: keyDidMethodHelpers.createDid(
            await exportJWK((await generateKeyPair("ES256")).publicKey)
          ), // not the requester DID
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameInTimeValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
          } satisfies IssueToHolderInTimeParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential is not valid: credentialSubject.id is not equal to ${requesterDid}`,
        ],
      });
    });

    it("should return an error if the credential is not a CTWalletSameInTime", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const fragmentIdentifier = credentialIssuerDid.replace("did:key:", "");
      const credentialIssuerKid = `${credentialIssuerDid}#${fragmentIdentifier}`;
      const vcIssuer: EbsiIssuer = {
        did: credentialIssuerDid,
        kid: credentialIssuerKid,
        publicKeyJwk: await exportJWK(credentialIssuerKeyPair.publicKey),
        privateKeyJwk: await exportJWK(credentialIssuerKeyPair.privateKey),
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          // "CTWalletSameInTime", // Missing type
        ],
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: requesterDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameInTimeValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
          } satisfies IssueToHolderInTimeParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: credential types don't contain 'CTWalletSameInTime'",
        ],
      });
    });

    it("should return an error if the credential issuer property doesn't match the expected value", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const fragmentIdentifier = credentialIssuerDid.replace("did:key:", "");
      const credentialIssuerKid = `${credentialIssuerDid}#${fragmentIdentifier}`;
      const vcIssuer: EbsiIssuer = {
        did: credentialIssuerDid,
        kid: credentialIssuerKid,
        publicKeyJwk: await exportJWK(credentialIssuerKeyPair.publicKey),
        privateKeyJwk: await exportJWK(credentialIssuerKeyPair.privateKey),
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "CTWalletSameInTime",
        ],
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: requesterDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock JSON-LD context schemas
      mockSchemas();

      const invalidCredentialIssuer =
        "did:key:z2dmzD81cgPx8Vki7JbuuMmFYrWPgYoytykUZ3eyqht1j9KbsDbVZXdb3jzCagESyY4EE2x7Yjx3gNwctoEuRCKKDrdNP3HPFtG8RTvBiYStT5ghBHhHizH2Dy6xQtW3Pd2SecizL9b2jzDCMr7Ka5cRAWZFwvqwAtwTT7xet769y9ERh6";
      const response =
        await issuerMockService.processIssueToHolderCTWalletSameInTimeValidation(
          {
            credentialIssuer,
            credentialIssuerDid: invalidCredentialIssuer, // different from "credentialIssuerDid"
          } satisfies IssueToHolderInTimeParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential is not valid: credential issuer ${credentialIssuerDid} is different from ${invalidCredentialIssuer}`,
        ],
      });
    });

    it("should return { success: true } if the credential is valid", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const fragmentIdentifier = credentialIssuerDid.replace("did:key:", "");
      const credentialIssuerKid = `${credentialIssuerDid}#${fragmentIdentifier}`;
      const vcIssuer: EbsiIssuer = {
        did: credentialIssuerDid,
        kid: credentialIssuerKid,
        publicKeyJwk: await exportJWK(credentialIssuerKeyPair.publicKey),
        privateKeyJwk: await exportJWK(credentialIssuerKeyPair.privateKey),
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "CTWalletSameInTime",
        ],
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: requesterDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameInTimeValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
          } satisfies IssueToHolderInTimeParams
        );

      expect(response).toStrictEqual({
        success: true,
      });
    });
  });

  describe("processIssueToHolderCTWalletSameDeferredRequest", () => {
    const credentialIssuer = "https://client-mock.xyz";
    const credentialIssuerDid =
      "did:key:z2dmzD81cgPx8Vki7JbuuMmFYrWPgYoytykUZ3eyqht1j9KbsDbVZXdb3jzCagESyY4EE2x7Yjx3gNwctoEuRCKKDrdNP3HPFtG8RTvBiYStT5ghBHhHizH2Dy6xQtW3Pd2SecizL9b2jzDCMr7Ka5cRAWZFwvqwAtwTT7xet769y9ERh6";
    const issuerState = "issuer-state";

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameDeferredRequest(
          {
            // credentialIssuer, credentialIssuerDid are missing
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Validation error. Path: 'data.credentialIssuer'. Reason: Required",
          "Validation error. Path: 'data.credentialIssuerDid'. Reason: Required",
        ],
      });
    });

    it("should return the error reported by issuerMockService.requestCredential if there's an error", async () => {
      jest.spyOn(issuerMockUtils, "requestCredential").mockResolvedValue({
        success: false,
        errors: ["error"],
      });

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameDeferredRequest(
          {
            credentialIssuer,
            credentialIssuerDid,
            issuerState,
          } satisfies IssueToHolderDeferredParams
        );

      expect(response).toStrictEqual({ success: false, errors: ["error"] });
    });

    it("should store the credential in cache and return { success: true } if the request is valid", async () => {
      expect.assertions(2);

      jest.spyOn(issuerMockUtils, "requestCredential").mockResolvedValue({
        success: true,
        credential: "jwt",
      });

      const cacheSpy = jest.spyOn(cacheManager, "set");

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameDeferredRequest(
          {
            credentialIssuer,
            credentialIssuerDid,
            issuerState,
          } satisfies IssueToHolderDeferredParams
        );

      expect(cacheSpy).toHaveBeenCalledWith(
        getCredentialId(
          { clientId: credentialIssuer, did: issuerMockDid },
          "CTWalletSameDeferred"
        ),
        "jwt",
        300_000 // 5 minutes
      );
      expect(response).toStrictEqual({ success: true });
    });
  });

  describe("processIssueToHolderCTWalletSameDeferredValidation", () => {
    const credentialIssuer = "https://client-mock.xyz";
    let credentialIssuerKeyPair: GenerateKeyPairResult<KeyLike>;
    let credentialIssuerDid: string;
    let requesterDid: string;

    beforeAll(async () => {
      credentialIssuerKeyPair = await generateKeyPair("ES256");
      const credentialIssuerPublicKeyJwk = await exportJWK(
        credentialIssuerKeyPair.publicKey
      );
      credentialIssuerDid = keyDidMethodHelpers.createDid(
        credentialIssuerPublicKeyJwk
      );

      const keyPairES256 = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      requesterDid = keyDidMethodHelpers.createDid(keyPairES256.publicKeyJwk);
    });

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameDeferredValidation(
          {
            // credentialIssuer, credentialIssuerDid are missing
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Validation error. Path: 'data.credentialIssuer'. Reason: Required",
          "Validation error. Path: 'data.credentialIssuerDid'. Reason: Required",
        ],
      });
    });

    it("should return an error if no credential is found", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve();
      });

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameDeferredValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
          } satisfies IssueToHolderDeferredParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["No credential found for this client."],
      });
    });

    it("should return an error if the stored credential is not a string", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve({ credential: { foo: "bar" } });
      });

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameDeferredValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
          } satisfies IssueToHolderDeferredParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential is not a string."],
      });
    });

    it("should return an error if the stored credential is not a valid JWT", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve("not a jwt");
      });

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameDeferredValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
          } satisfies IssueToHolderDeferredParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential is not valid: Unable to decode JWT VC"],
      });
    });

    it("should return an error if the stored credential is not a valid Verifiable Attestation JWT", async () => {
      expect.assertions(1);

      const jwt = await new SignJWT({
        foo: "bar",
        vc: {},
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: `${credentialIssuerDid}#key-1`,
        })
        .sign((await generateKeyPair("ES256")).privateKey);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(jwt);
      });

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameDeferredValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
          } satisfies IssueToHolderDeferredParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: Invalid EBSI Verifiable Attestation",
          "Credential must have required property '@context'",
          "Credential must have required property 'id'",
          "Credential must have required property 'type'",
          "Credential must have required property 'issuer'",
          "Credential must have required property 'issuanceDate'",
          "Credential must have required property 'issued'",
          "Credential must have required property 'validFrom'",
          "Credential must have required property 'credentialSubject'",
          "Credential must have required property 'credentialSchema'",
        ],
      });
    });

    it("should return an error if the credential subject is not equal to the requester DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const fragmentIdentifier = credentialIssuerDid.replace("did:key:", "");
      const credentialIssuerKid = `${credentialIssuerDid}#${fragmentIdentifier}`;
      const vcIssuer: EbsiIssuer = {
        did: credentialIssuerDid,
        kid: credentialIssuerKid,
        publicKeyJwk: await exportJWK(credentialIssuerKeyPair.publicKey),
        privateKeyJwk: await exportJWK(credentialIssuerKeyPair.privateKey),
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "CTWalletSameDeferred",
        ],
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: keyDidMethodHelpers.createDid(
            await exportJWK((await generateKeyPair("ES256")).publicKey)
          ), // not the requester DID
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameDeferredValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
          } satisfies IssueToHolderDeferredParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential is not valid: credentialSubject.id is not equal to ${requesterDid}`,
        ],
      });
    });

    it("should return an error if the credential is not a CTWalletSameDeferred", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const fragmentIdentifier = credentialIssuerDid.replace("did:key:", "");
      const credentialIssuerKid = `${credentialIssuerDid}#${fragmentIdentifier}`;
      const vcIssuer: EbsiIssuer = {
        did: credentialIssuerDid,
        kid: credentialIssuerKid,
        publicKeyJwk: await exportJWK(credentialIssuerKeyPair.publicKey),
        privateKeyJwk: await exportJWK(credentialIssuerKeyPair.privateKey),
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          // "CTWalletSameDeferred", // Missing type
        ],
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: requesterDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameDeferredValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
          } satisfies IssueToHolderDeferredParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: credential types don't contain 'CTWalletSameDeferred'",
        ],
      });
    });

    it("should return an error if the credential issuer property doesn't match the expected value", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const fragmentIdentifier = credentialIssuerDid.replace("did:key:", "");
      const credentialIssuerKid = `${credentialIssuerDid}#${fragmentIdentifier}`;
      const vcIssuer: EbsiIssuer = {
        did: credentialIssuerDid,
        kid: credentialIssuerKid,
        publicKeyJwk: await exportJWK(credentialIssuerKeyPair.publicKey),
        privateKeyJwk: await exportJWK(credentialIssuerKeyPair.privateKey),
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "CTWalletSameDeferred",
        ],
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: requesterDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock JSON-LD context schemas
      mockSchemas();

      const invalidCredentialIssuer =
        "did:key:z2dmzD81cgPx8Vki7JbuuMmFYrWPgYoytykUZ3eyqht1j9KbsDbVZXdb3jzCagESyY4EE2x7Yjx3gNwctoEuRCKKDrdNP3HPFtG8RTvBiYStT5ghBHhHizH2Dy6xQtW3Pd2SecizL9b2jzDCMr7Ka5cRAWZFwvqwAtwTT7xet769y9ERh6";
      const response =
        await issuerMockService.processIssueToHolderCTWalletSameDeferredValidation(
          {
            credentialIssuer,
            credentialIssuerDid: invalidCredentialIssuer, // different from "credentialIssuerDid"
          } satisfies IssueToHolderDeferredParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential is not valid: credential issuer ${credentialIssuerDid} is different from ${invalidCredentialIssuer}`,
        ],
      });
    });

    it("should return { success: true } if the credential is valid", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const fragmentIdentifier = credentialIssuerDid.replace("did:key:", "");
      const credentialIssuerKid = `${credentialIssuerDid}#${fragmentIdentifier}`;
      const vcIssuer: EbsiIssuer = {
        did: credentialIssuerDid,
        kid: credentialIssuerKid,
        publicKeyJwk: await exportJWK(credentialIssuerKeyPair.publicKey),
        privateKeyJwk: await exportJWK(credentialIssuerKeyPair.privateKey),
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "CTWalletSameDeferred",
        ],
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: requesterDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processIssueToHolderCTWalletSameDeferredValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
          } satisfies IssueToHolderDeferredParams
        );

      expect(response).toStrictEqual({
        success: true,
      });
    });
  });

  describe("processIssueToHolderCTWalletSamePreAuthorisedRequest", () => {
    const credentialIssuer = "https://client-mock.xyz";
    const credentialIssuerDid =
      "did:key:z2dmzD81cgPx8Vki7JbuuMmFYrWPgYoytykUZ3eyqht1j9KbsDbVZXdb3jzCagESyY4EE2x7Yjx3gNwctoEuRCKKDrdNP3HPFtG8RTvBiYStT5ghBHhHizH2Dy6xQtW3Pd2SecizL9b2jzDCMr7Ka5cRAWZFwvqwAtwTT7xet769y9ERh6";
    const preAuthorizedCode = "jwt";
    const userPin = "1234";

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processIssueToHolderCTWalletSamePreAuthorisedRequest(
          {
            // credentialIssuer, credentialIssuerDid, preAuthorizedCode and userPin are missing
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Validation error. Path: 'data.credentialIssuer'. Reason: Required",
          "Validation error. Path: 'data.credentialIssuerDid'. Reason: Required",
          "Validation error. Path: 'data.preAuthorizedCode'. Reason: Required",
          "Validation error. Path: 'data.userPin'. Reason: Required",
        ],
      });
    });

    it("should return the error reported by issuerMockService.requestCredential if there's an error", async () => {
      jest.spyOn(issuerMockUtils, "requestCredential").mockResolvedValue({
        success: false,
        errors: ["error"],
      });

      const response =
        await issuerMockService.processIssueToHolderCTWalletSamePreAuthorisedRequest(
          {
            credentialIssuer,
            credentialIssuerDid,
            preAuthorizedCode,
            userPin,
          } satisfies IssueToHolderPreAuthorisedParams
        );

      expect(response).toStrictEqual({ success: false, errors: ["error"] });
    });

    it("should store the credential in cache and return { success: true } if the request is valid", async () => {
      expect.assertions(2);

      jest.spyOn(issuerMockUtils, "requestCredential").mockResolvedValue({
        success: true,
        credential: "jwt",
      });

      const cacheSpy = jest.spyOn(cacheManager, "set");

      const response =
        await issuerMockService.processIssueToHolderCTWalletSamePreAuthorisedRequest(
          {
            credentialIssuer,
            credentialIssuerDid,
            preAuthorizedCode,
            userPin,
          } satisfies IssueToHolderPreAuthorisedParams
        );

      expect(cacheSpy).toHaveBeenCalledWith(
        getCredentialId(
          { clientId: credentialIssuer, did: issuerMockDid },
          "CTWalletSamePreAuthorised"
        ),
        "jwt",
        300_000 // 5 minutes
      );
      expect(response).toStrictEqual({ success: true });
    });
  });

  describe("processIssueToHolderCTWalletSamePreAuthorisedValidation", () => {
    const credentialIssuer = "https://client-mock.xyz";
    let credentialIssuerKeyPair: GenerateKeyPairResult<KeyLike>;
    let credentialIssuerDid: string;
    let requesterDid: string;
    const preAuthorizedCode = "jwt";
    const userPin = "1234";

    beforeAll(async () => {
      credentialIssuerKeyPair = await generateKeyPair("ES256");
      const credentialIssuerPublicKeyJwk = await exportJWK(
        credentialIssuerKeyPair.publicKey
      );
      credentialIssuerDid = keyDidMethodHelpers.createDid(
        credentialIssuerPublicKeyJwk
      );

      const keyPairES256 = await issuerMockService.getIssuerMockKeyPair(
        "ES256"
      );
      requesterDid = keyDidMethodHelpers.createDid(keyPairES256.publicKeyJwk);
    });

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response =
        await issuerMockService.processIssueToHolderCTWalletSamePreAuthorisedValidation(
          {
            // credentialIssuer, credentialIssuerDid, preAuthorizedCode, userPin are missing
          }
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Validation error. Path: 'data.credentialIssuer'. Reason: Required",
          "Validation error. Path: 'data.credentialIssuerDid'. Reason: Required",
          "Validation error. Path: 'data.preAuthorizedCode'. Reason: Required",
          "Validation error. Path: 'data.userPin'. Reason: Required",
        ],
      });
    });

    it("should return an error if no credential is found", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve();
      });

      const response =
        await issuerMockService.processIssueToHolderCTWalletSamePreAuthorisedValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
            preAuthorizedCode,
            userPin,
          } satisfies IssueToHolderPreAuthorisedParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["No credential found for this client."],
      });
    });

    it("should return an error if the stored credential is not a string", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve({ credential: { foo: "bar" } });
      });

      const response =
        await issuerMockService.processIssueToHolderCTWalletSamePreAuthorisedValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
            preAuthorizedCode,
            userPin,
          } satisfies IssueToHolderPreAuthorisedParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential is not a string."],
      });
    });

    it("should return an error if the stored credential is not a valid JWT", async () => {
      expect.assertions(1);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve("not a jwt");
      });

      const response =
        await issuerMockService.processIssueToHolderCTWalletSamePreAuthorisedValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
            preAuthorizedCode,
            userPin,
          } satisfies IssueToHolderPreAuthorisedParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: ["Credential is not valid: Unable to decode JWT VC"],
      });
    });

    it("should return an error if the stored credential is not a valid Verifiable Attestation JWT", async () => {
      expect.assertions(1);

      const jwt = await new SignJWT({
        foo: "bar",
        vc: {},
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: `${credentialIssuerDid}#key-1`,
        })
        .sign((await generateKeyPair("ES256")).privateKey);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(jwt);
      });

      const response =
        await issuerMockService.processIssueToHolderCTWalletSamePreAuthorisedValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
            preAuthorizedCode,
            userPin,
          } satisfies IssueToHolderPreAuthorisedParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: Invalid EBSI Verifiable Attestation",
          "Credential must have required property '@context'",
          "Credential must have required property 'id'",
          "Credential must have required property 'type'",
          "Credential must have required property 'issuer'",
          "Credential must have required property 'issuanceDate'",
          "Credential must have required property 'issued'",
          "Credential must have required property 'validFrom'",
          "Credential must have required property 'credentialSubject'",
          "Credential must have required property 'credentialSchema'",
        ],
      });
    });

    it("should return an error if the credential subject is not equal to the requester DID", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const fragmentIdentifier = credentialIssuerDid.replace("did:key:", "");
      const credentialIssuerKid = `${credentialIssuerDid}#${fragmentIdentifier}`;
      const vcIssuer: EbsiIssuer = {
        did: credentialIssuerDid,
        kid: credentialIssuerKid,
        publicKeyJwk: await exportJWK(credentialIssuerKeyPair.publicKey),
        privateKeyJwk: await exportJWK(credentialIssuerKeyPair.privateKey),
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "CTWalletSamePreAuthorised",
        ],
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: keyDidMethodHelpers.createDid(
            await exportJWK((await generateKeyPair("ES256")).publicKey)
          ), // not the requester DID
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processIssueToHolderCTWalletSamePreAuthorisedValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
            preAuthorizedCode,
            userPin,
          } satisfies IssueToHolderPreAuthorisedParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential is not valid: credentialSubject.id is not equal to ${requesterDid}`,
        ],
      });
    });

    it("should return an error if the credential is not a CTWalletSamePreAuthorised", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const fragmentIdentifier = credentialIssuerDid.replace("did:key:", "");
      const credentialIssuerKid = `${credentialIssuerDid}#${fragmentIdentifier}`;
      const vcIssuer: EbsiIssuer = {
        did: credentialIssuerDid,
        kid: credentialIssuerKid,
        publicKeyJwk: await exportJWK(credentialIssuerKeyPair.publicKey),
        privateKeyJwk: await exportJWK(credentialIssuerKeyPair.privateKey),
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          // "CTWalletSamePreAuthorised", // Missing type
        ],
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: requesterDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processIssueToHolderCTWalletSamePreAuthorisedValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
            preAuthorizedCode,
            userPin,
          } satisfies IssueToHolderPreAuthorisedParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          "Credential is not valid: credential types don't contain 'CTWalletSamePreAuthorised'",
        ],
      });
    });

    it("should return an error if the credential issuer property doesn't match the expected value", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const fragmentIdentifier = credentialIssuerDid.replace("did:key:", "");
      const credentialIssuerKid = `${credentialIssuerDid}#${fragmentIdentifier}`;
      const vcIssuer: EbsiIssuer = {
        did: credentialIssuerDid,
        kid: credentialIssuerKid,
        publicKeyJwk: await exportJWK(credentialIssuerKeyPair.publicKey),
        privateKeyJwk: await exportJWK(credentialIssuerKeyPair.privateKey),
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "CTWalletSamePreAuthorised",
        ],
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: requesterDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock JSON-LD context schemas
      mockSchemas();

      const invalidCredentialIssuer =
        "did:key:z2dmzD81cgPx8Vki7JbuuMmFYrWPgYoytykUZ3eyqht1j9KbsDbVZXdb3jzCagESyY4EE2x7Yjx3gNwctoEuRCKKDrdNP3HPFtG8RTvBiYStT5ghBHhHizH2Dy6xQtW3Pd2SecizL9b2jzDCMr7Ka5cRAWZFwvqwAtwTT7xet769y9ERh6";
      const response =
        await issuerMockService.processIssueToHolderCTWalletSamePreAuthorisedValidation(
          {
            credentialIssuer,
            credentialIssuerDid: invalidCredentialIssuer, // different from "credentialIssuerDid"
            preAuthorizedCode,
            userPin,
          } satisfies IssueToHolderPreAuthorisedParams
        );

      expect(response).toStrictEqual({
        success: false,
        errors: [
          `Credential is not valid: credential issuer ${credentialIssuerDid} is different from ${invalidCredentialIssuer}`,
        ],
      });
    });

    it("should return { success: true } if the credential is valid", async () => {
      expect.assertions(1);

      const issuedAt = new Date();
      const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
      const expiresAt = new Date(
        issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
      );
      const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

      const fragmentIdentifier = credentialIssuerDid.replace("did:key:", "");
      const credentialIssuerKid = `${credentialIssuerDid}#${fragmentIdentifier}`;
      const vcIssuer: EbsiIssuer = {
        did: credentialIssuerDid,
        kid: credentialIssuerKid,
        publicKeyJwk: await exportJWK(credentialIssuerKeyPair.publicKey),
        privateKeyJwk: await exportJWK(credentialIssuerKeyPair.privateKey),
        alg: "ES256",
      };

      const vcPayload = {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: `vc:ebsi:conformance#${randomUUID()}`,
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "CTWalletSamePreAuthorised",
        ],
        issuer: vcIssuer.did,
        issuanceDate,
        issued: issuanceDate,
        validFrom: issuanceDate,
        expirationDate,
        credentialSubject: {
          id: requesterDid,
        },
        credentialSchema: {
          id: authorisationCredentialSchema,
          type: "FullJsonSchemaValidator2021",
        },
      } satisfies EbsiVerifiableAttestation;

      const vcJwt = await createVerifiableCredentialJwt(vcPayload, vcIssuer, {
        ebsiAuthority,
        skipValidation: true,
      });

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve(vcJwt);
      });

      // Mock JSON-LD context schemas
      mockSchemas();

      const response =
        await issuerMockService.processIssueToHolderCTWalletSamePreAuthorisedValidation(
          {
            credentialIssuer,
            credentialIssuerDid,
            preAuthorizedCode,
            userPin,
          } satisfies IssueToHolderPreAuthorisedParams
        );

      expect(response).toStrictEqual({
        success: true,
      });
    });
  });

  describe("processVerifierIdTokenExchange", () => {
    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    it("should return an error if the data parameter is invalid", async () => {
      expect.assertions(1);

      const response = await issuerMockService.processVerifierIdTokenExchange({
        // clientId is missing
      });

      expect(response).toStrictEqual({
        success: false,
        errors: ["Validation error. Path: 'data.clientId'. Reason: Required"],
      });
    });
  });

  describe("Verifier - Verifiable Presentations", () => {
    const verifier = "https://my-verifier.xyz";
    async function setupMockVerifier(
      opts: {
        jwks_response?: { [x: string]: unknown };
        openid_configuration_response?: { [x: string]: unknown };
        basic_request_params?: { [x: string]: unknown };
        vp_token_request_payload?: { [x: string]: unknown };
        vp_token_request_header?: { [x: string]: unknown };
        verifier?: string;
        signerVerifier?: KeyLike | Uint8Array;
        authorize_status_response?: number;
        authorize_location_response?: string;
        direct_post_status_response?: number;
        direct_post_location_response?: (clientState: string | null) => string;
      } = {}
    ) {
      const verifierPrivateKeyHex = randomBytes(32).toString("hex");
      const verifierKeyPair = await getKeyPair(verifierPrivateKeyHex);

      nock(verifier)
        .get("/jwks")
        .reply(200, {
          keys: [verifierKeyPair.publicKeyJwk],
          ...opts.jwks_response,
        });

      nock(verifier)
        .get("/.well-known/openid-configuration")
        .reply(200, {
          redirect_uris: [`${verifier}/direct_post`],
          issuer: verifier,
          authorization_endpoint: `${verifier}/authorize`,
          token_endpoint: `${verifier}/token`,
          jwks_uri: `${verifier}/jwks`,
          scopes_supported: ["openid"],
          response_types_supported: ["vp_token", "id_token"],
          response_modes_supported: ["query"],
          grant_types_supported: ["authorization_code"],
          subject_types_supported: ["public"],
          id_token_signing_alg_values_supported: ["ES256"],
          request_object_signing_alg_values_supported: ["ES256"],
          request_parameter_supported: true,
          request_uri_parameter_supported: true,
          token_endpoint_auth_methods_supported: ["private_key_jwt"],
          request_authentication_methods_supported: {
            authorization_endpoint: ["request_object"],
          },
          vp_formats_supported: {
            jwt_vp: {
              alg_values_supported: ["ES256"],
            },
            jwt_vc: {
              alg_values_supported: ["ES256"],
            },
          },
          subject_syntax_types_supported: ["did:key", "did:ebsi"],
          subject_syntax_types_discriminations: [
            "did:key:jwk_jcs-pub",
            "did:ebsi:v1",
          ],
          subject_trust_frameworks_supported: ["ebsi"],
          id_token_types_supported: [
            "subject_signed_id_token",
            "attester_signed_id_token",
          ],
          ...opts.openid_configuration_response,
        });

      const signerVerifier = await importJWK(verifierKeyPair.privateKeyJwk);

      const basicRequestParams = {
        client_id: verifier,
        redirect_uri: `${verifier}/direct_post`,
        response_type: "vp_token",
        response_mode: "direct_post",
        scope: "openid",
        ...opts.basic_request_params,
      };

      const vpTokenRequestPayload = {
        ...basicRequestParams,
        state: randomUUID(),
        nonce: randomUUID(),
        presentation_definition: VERIFIER_TEST_PRESENTATION_DEFINITION,
        ...opts.vp_token_request_payload,
      };

      let clientState: string | null = "";
      let clientId: string | null = "";
      nock(verifier)
        .filteringPath((path) => {
          if (!path.startsWith("/authorize")) return path;
          const params = new URL(`${verifier}${path}`).searchParams;
          clientState = params.get("state");
          clientId = params.get("client_id");
          return path;
        })
        .get("/authorize")
        .query(true)
        .reply(async () => {
          const vpTokenRequestJwt = await new SignJWT(vpTokenRequestPayload)
            .setProtectedHeader({
              typ: "JWT",
              alg: "ES256",
              kid: verifierKeyPair.publicKeyJwk.kid,
              ...opts.vp_token_request_header,
            })
            .setIssuer(opts.verifier ?? verifier)
            .setAudience(clientId ?? "")
            .setExpirationTime("1h")
            .sign(opts.signerVerifier ?? signerVerifier);

          return [
            opts.authorize_status_response ?? 302,
            undefined,
            {
              Location:
                opts.authorize_location_response ??
                `openid://?${new URLSearchParams({
                  ...basicRequestParams,
                  request: vpTokenRequestJwt,
                }).toString()}`,
            },
          ];
        });

      nock(verifier)
        .post("/direct_post")
        .reply(() => {
          let location: string;
          if (opts.direct_post_location_response)
            location = opts.direct_post_location_response(clientState);
          else
            location = `openid://?${new URLSearchParams({
              code: randomUUID(),
              ...(clientState && { state: clientState }),
            }).toString()}`;

          return [
            opts.direct_post_status_response ?? 302,
            undefined,
            { Location: location },
          ];
        });
    }

    afterEach(() => {
      nock.cleanAll();
      jest.resetAllMocks();
    });

    describe("processVerifierVpValidVc", () => {
      it("should return an error if the data parameter is invalid", async () => {
        expect.assertions(1);

        const response = await issuerMockService.processVerifierVpValidVc({
          // clientId is missing
        });

        expect(response).toStrictEqual({
          success: false,
          errors: ["Validation error. Path: 'data.clientId'. Reason: Required"],
        });
      });

      it("should throw error for invalid jwk", async () => {
        expect.assertions(1);

        const otherKeys = await getKeyPair(randomBytes(32).toString("hex"));
        await setupMockVerifier({
          jwks_response: {
            keys: [otherKeys.publicKeyJwk],
          },
        });
        const response = await issuerMockService.processVerifierVpValidVc({
          clientId: verifier,
        });

        expect(response).toStrictEqual({
          success: false,
          errors: [
            "Authorization Server's jwks_uri doesn't contain a public key matching ID Token request header's kid",
          ],
        });
      });

      it("should throw error if the presentation definition doesn't request 3 VCs", async () => {
        expect.assertions(1);

        await setupMockVerifier({
          vp_token_request_payload: {
            presentation_definition: {
              id: "<any id, random or static>",
              format: {
                jwt_vc: { alg: ["ES256"] },
                jwt_vp: { alg: ["ES256"] },
              },
              input_descriptors: [
                {
                  id: "<any id, random or static>",
                  constraints: {
                    fields: [
                      {
                        path: ["$.type"],
                        filter: {
                          type: "array",
                          contains: { const: "VerifiableAttestation" },
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        });
        const response = await issuerMockService.processVerifierVpValidVc({
          clientId: verifier,
        });

        expect(response).toStrictEqual({
          success: false,
          errors: [
            "Authorization endpoint's response doesn't contain a valid JWT payload in the VP Token request",
            "Validation error. Path: 'presentation_definition.input_descriptors'. Reason: Array must contain exactly 3 element(s)",
          ],
        });
      });

      it("should verify a valid vc", async () => {
        expect.assertions(1);

        await setupMockVerifier();
        const response = await issuerMockService.processVerifierVpValidVc({
          clientId: verifier,
        });

        expect(response).toStrictEqual({ success: true });
      });
    });

    describe("processVerifierVpExpiredVc", () => {
      it("should reject a verifier that returns success", async () => {
        expect.assertions(1);

        await setupMockVerifier();
        const response = await issuerMockService.processVerifierVpExpiredVc({
          clientId: verifier,
        });

        expect(response).toStrictEqual({
          success: false,
          errors: [
            expect.stringContaining(
              "Authorization Server direct_post's response doesn't contain an 'invalid_request' error."
            ),
          ],
        });
      });

      it("should reject a verifier that returns an invalid error_description", async () => {
        expect.assertions(1);

        await setupMockVerifier({
          direct_post_location_response: (clientState) =>
            `openid://?${new URLSearchParams({
              error: "invalid_request",
              error_description: "invalid description",
              ...(clientState && { state: clientState }),
            }).toString()}`,
        });
        const response = await issuerMockService.processVerifierVpExpiredVc({
          clientId: verifier,
        });

        expect(response).toStrictEqual({
          success: false,
          errors: [
            expect.stringContaining(
              "Authorization Server direct_post's response doesn't mention in the error_description that the VC is expired."
            ),
          ],
        });
      });

      it("should accept a verifier that rejects an expired vc", async () => {
        expect.assertions(1);

        await setupMockVerifier({
          direct_post_location_response: (clientState) =>
            `openid://?${new URLSearchParams({
              error: "invalid_request",
              error_description: "is expired",
              ...(clientState && { state: clientState }),
            }).toString()}`,
        });
        const response = await issuerMockService.processVerifierVpExpiredVc({
          clientId: verifier,
        });

        expect(response).toStrictEqual({ success: true });
      });
    });

    describe("processVerifierVpRevokedVc", () => {
      it("should reject a verifier that returns success", async () => {
        expect.assertions(1);

        await setupMockVerifier();
        const response = await issuerMockService.processVerifierVpRevokedVc({
          clientId: verifier,
        });

        expect(response).toStrictEqual({
          success: false,
          errors: [
            expect.stringContaining(
              "Authorization Server direct_post's response doesn't contain an 'invalid_request' error."
            ),
          ],
        });
      });

      it("should reject a verifier that returns an invalid error_description", async () => {
        expect.assertions(1);

        await setupMockVerifier({
          direct_post_location_response: (clientState) =>
            `openid://?${new URLSearchParams({
              error: "invalid_request",
              error_description: "invalid description",
              ...(clientState && { state: clientState }),
            }).toString()}`,
        });
        const response = await issuerMockService.processVerifierVpRevokedVc({
          clientId: verifier,
        });

        expect(response).toStrictEqual({
          success: false,
          errors: [
            expect.stringContaining(
              "Authorization Server direct_post's response doesn't mention in the error_description that the VC is revoked."
            ),
          ],
        });
      });

      it("should accept a verifier that rejects a not yet valid vc", async () => {
        expect.assertions(1);

        await setupMockVerifier({
          direct_post_location_response: (clientState) =>
            `openid://?${new URLSearchParams({
              error: "invalid_request",
              error_description: "is revoked",
              ...(clientState && { state: clientState }),
            }).toString()}`,
        });
        const response = await issuerMockService.processVerifierVpRevokedVc({
          clientId: verifier,
        });

        expect(response).toStrictEqual({ success: true });
      });
    });

    describe("processVerifierVpNotYetValidVc", () => {
      it("should reject a verifier that returns success", async () => {
        expect.assertions(1);

        await setupMockVerifier();
        const response = await issuerMockService.processVerifierVpNotYetValidVc(
          {
            clientId: verifier,
          }
        );

        expect(response).toStrictEqual({
          success: false,
          errors: [
            expect.stringContaining(
              "Authorization Server direct_post's response doesn't contain an 'invalid_request' error."
            ),
          ],
        });
      });

      it("should reject a verifier that returns an invalid error_description", async () => {
        expect.assertions(1);

        await setupMockVerifier({
          direct_post_location_response: (clientState) =>
            `openid://?${new URLSearchParams({
              error: "invalid_request",
              error_description: "invalid description",
              ...(clientState && { state: clientState }),
            }).toString()}`,
        });
        const response = await issuerMockService.processVerifierVpNotYetValidVc(
          {
            clientId: verifier,
          }
        );

        expect(response).toStrictEqual({
          success: false,
          errors: [
            expect.stringContaining(
              "Authorization Server direct_post's response doesn't mention in the error_description that the VC is not yet valid."
            ),
          ],
        });
      });

      it("should accept a verifier that rejects a not yet valid vc", async () => {
        expect.assertions(1);

        await setupMockVerifier({
          direct_post_location_response: (clientState) =>
            `openid://?${new URLSearchParams({
              error: "invalid_request",
              error_description: "is not yet valid",
              ...(clientState && { state: clientState }),
            }).toString()}`,
        });
        const response = await issuerMockService.processVerifierVpNotYetValidVc(
          {
            clientId: verifier,
          }
        );

        expect(response).toStrictEqual({ success: true });
      });
    });
  });
});
