import { createHash, randomBytes, randomUUID } from "node:crypto";
import {
  CACHE_MANAGER,
  Inject,
  Injectable,
  Logger,
  OnModuleDestroy,
  OnModuleInit,
} from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import type { Cache } from "cache-manager";
import axios, { AxiosResponse } from "axios";
import { ethers } from "ethers";
import {
  decodeJwt,
  decodeProtectedHeader,
  importJWK,
  jwtVerify,
  ProtectedHeaderParameters,
  SignJWT,
} from "jose";
import type { JWTPayload } from "jose";
import qs from "qs";
import { Resolver } from "did-resolver";
import { getResolver as getEbsiDidResolver } from "@cef-ebsi/ebsi-did-resolver";
import {
  getResolver as getKeyDidResolver,
  util,
} from "@cef-ebsi/key-did-resolver";
import {
  createVerifiableCredentialJwt,
  ValidationError,
  verifyCredentialJwt,
} from "@cef-ebsi/verifiable-credential";
import type {
  EbsiIssuer,
  EbsiVerifiableAttestation,
} from "@cef-ebsi/verifiable-credential";
import {
  createVerifiablePresentationJwt,
  EbsiVerifiablePresentation,
} from "@cef-ebsi/verifiable-presentation";
import { Level } from "level";
import {
  InternalServerError,
  NotFoundError,
} from "@cef-ebsi/problem-details-errors";
import { z } from "zod";
import pLimit from "p-limit";
import { base64, base64url } from "multiformats/bases/base64";
import { gzip } from "pako";
import type { ApiConfig } from "../../config/configuration";
import type {
  ChecksMap,
  CheckParams,
  CredentialOffer,
  CredentialOfferPayload,
  VerifierFlowAuthorisationRequest,
} from "./issuer-mock.interface";
import {
  accreditAuthoriseParamsSchema,
  idTokenRequestJwtHeaderSchema,
  idTokenRequestSchema,
  issueToHolderDeferredParamsSchema,
  issueToHolderInTimeParamsSchema,
  issueToHolderPreAuthorisedParamsSchema,
  openidConfigurationSchema,
  verifierParamsSchema,
  vpTokenRequestJwtHeaderSchema,
  vpTokenRequestSchema,
  vpTokenRequestPayloadSchema,
} from "./validators";
import { CredentialError } from "../../shared/errors";
import {
  formatZodError,
  getErrorDetails,
  getErrorMessage,
  getKeyPair,
} from "../../shared/utils";
import type { KeyPair } from "../../shared/utils";
import { EBSIAuthorisationService } from "../ebsi/authorisation.service";
import { DataStoreService } from "../data-store/data-store.service";
import { LogsService } from "../logs/logs.service";
import {
  TI_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD,
  TI_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST,
  TI_REGISTER_VERIFIABLE_ACCREDITATION_TO_ATTEST,
  TI_REQUEST_CT_REVOCABLE,
  TI_VALIDATE_CT_REVOCABLE,
  TI_REVOKE_CT_REVOCABLE,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
  TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
  TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
  TAO_REVOKE_RIGHTS_SUBACCOUNT,
  RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
  RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
  REQUEST_CTAA_QUALIFICATION_CREDENTIAL,
  ACCREDIT_AUTHORISE_TESTS,
  CT_WALLET_CROSS_IN_TIME,
  CT_WALLET_CROSS_DEFERRED,
  CT_WALLET_CROSS_PRE_AUTHORISED,
  CT_WALLET_SAME_IN_TIME,
  CT_WALLET_SAME_DEFERRED,
  CT_WALLET_SAME_PRE_AUTHORISED,
  HOLDER_WALLET_AUTHORIZATION_CODE_CREDENTIAL_TYPES,
  REQUEST_CT_WALLET_QUALIFICATION_CREDENTIAL,
  ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_IN_TIME,
  ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_IN_TIME,
  ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_DEFERRED,
  ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_DEFERRED,
  ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_PRE_AUTHORISED,
  ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_PRE_AUTHORISED,
  ISSUE_TO_HOLDER_TESTS,
  REQUEST_CT_ISSUE_TO_HOLDER_QUALIFICATION_CREDENTIAL,
  VERIFIER_ID_TOKEN_EXCHANGE,
  VERIFIER_VP_VALID_VC,
  VERIFIER_VP_EXPIRED_VC,
  VERIFIER_VP_REVOKED_VC,
  VERIFIER_VP_NOT_YET_VALID_VC,
} from "../../shared/constants";
import type {
  CheckResult,
  JsonWebKeySet,
  UnsignedTransaction,
  CredentialIssuerMetadata,
  IssuerAttribute,
  IdLink,
  PaginatedList,
} from "../../shared/interfaces";
import {
  getCredentialId,
  getDeferredCredentialId,
  getSubAccountDid,
  requestCredential,
} from "./issuer-mock.utils";
import type { GetInitiateCredentialOfferDto } from "./dto";
import {
  AccessTokenPayload,
  issueCredential,
  logAxiosRequestError,
  preregisterAttribute,
  signAndSendTransaction,
  validatePostCredential,
  waitToBeMined,
} from "../../shared/credential-issuer";
import type {
  LevelDbKeyIssuer,
  LevelDbObjectIssuer,
  CredentialResponse,
  DeferredCredentialResponse,
} from "../../shared/credential-issuer";
import { jwksSchema } from "../../shared/validators";

@Injectable()
export class IssuerMockService implements OnModuleInit, OnModuleDestroy {
  private readonly logger = new Logger(IssuerMockService.name);

  private db: Level<LevelDbKeyIssuer, LevelDbObjectIssuer>;

  private readonly authMockUri: string;

  private readonly issuerMockUri: string;

  /**
   * Auth Mock ES256 private key (hex)
   */
  private readonly authMockPrivateKeyHex: string;

  /**
   * Auth Mock ES256 key pair (private + public key JWKs)
   */
  private authMockKeyPair?: KeyPair;

  /**
   * Issuer Mock ES256 private key (hex)
   */
  private readonly issuerMockPrivateKeyHex: string;

  /**
   * Issuer Mock ES256 and ES256K key pairs (private + public key JWKs)
   */
  private issuerMockKeyPair: Record<"ES256" | "ES256K", KeyPair | undefined>;

  /**
   * Issuer Mock DID (as registered in the DIDR and TIR)
   */
  private readonly issuerMockDid: string;

  /**
   * Issuer Mock kid (must refer to a verification method in the DID document)
   */
  private readonly issuerMockKid: string;

  /**
   * Issuer Mock accreditation (URL of the attribute in TIR v4)
   */
  private readonly issuerMockAccreditationUrl: string;

  /**
   * Issuer Mock proxy URL
   */
  private readonly issuerMockProxyUrl: string;

  /**
   * EBSI DID v1 Resolver
   */
  private readonly ebsiResolver: Resolver;

  /**
   * Key DID v1 Resolver
   */
  private readonly keyResolver: Resolver;

  /**
   * Request timeout
   */
  private readonly timeout: number;

  /**
   * EBSI Authority
   */
  private readonly ebsiAuthority: string;

  /**
   * EBSI VA schema URI
   */
  private readonly authorisationCredentialSchema: string;

  /**
   * PDA1 schema URI
   */
  private readonly pda1CredentialSchema: string;

  /**
   * Status List 2021 schema UIR
   */
  private readonly statusList2021CredentialSchemaUrl: string;

  /**
   * DIDR API /identifiers endpoint
   */
  private readonly didRegistryApiUrl: string;

  /**
   * DIDR API /jsonrpc endpoint
   */
  private readonly didRegistryApiJsonrpcUrl: string;

  /**
   * TIR API /issuers endpoint
   */
  private readonly trustedIssuersRegistryApiUrl: string;

  /**
   * TIR API /jsonrpc endpoint
   */
  private readonly trustedIssuersRegistryApiJsonrpcUrl: string;

  /**
   * TPR API /users endpoint
   */
  private readonly trustedPoliciesRegistryApiUrl: string;

  /**
   * Mapping between checks names and their implementation
   */
  public readonly checksMap: ChecksMap;

  /**
   * Ledger API /besu endpoint
   */
  private readonly ledgerApiUrl: string;

  /**
   * Issuer Mock's authorization_endpoint when acting as a client
   */
  private clientAuthorizationEndpoint: string;

  private cacheManagerLevel: {
    interval: ReturnType<typeof setInterval>;
    keys: { key: LevelDbKeyIssuer; expiration: number }[];
  };

  constructor(
    configService: ConfigService<ApiConfig, true>,
    private ebsiAuthorisationService: EBSIAuthorisationService,
    private dataStoreService: DataStoreService,
    private logsService: LogsService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache
  ) {
    const conformanceDomain = configService.get<string>("conformanceDomain");
    const apiUrlPrefix = configService.get<string>("apiUrlPrefix");
    this.authMockUri = `${conformanceDomain}${apiUrlPrefix}/auth-mock`;
    this.issuerMockUri = `${conformanceDomain}${apiUrlPrefix}/issuer-mock`;
    this.authMockPrivateKeyHex =
      configService.get<string>("authMockPrivateKey");
    this.issuerMockPrivateKeyHex = configService.get<string>(
      "issuerMockPrivateKey"
    );
    this.issuerMockKid = configService.get<string>("issuerMockKid");
    [this.issuerMockDid] = this.issuerMockKid.split("#") as [string];
    this.issuerMockAccreditationUrl = configService.get<string>(
      "issuerMockAccreditationUrl"
    );
    this.issuerMockProxyUrl = configService.get<string>("issuerMockProxyUrl");
    this.didRegistryApiUrl = configService.get<string>("didRegistryApiUrl");
    this.didRegistryApiJsonrpcUrl = configService.get<string>(
      "didRegistryApiJsonrpcUrl"
    );
    this.trustedIssuersRegistryApiUrl = configService.get<string>(
      "trustedIssuersRegistryApiUrl"
    );
    this.trustedIssuersRegistryApiJsonrpcUrl = configService.get<string>(
      "trustedIssuersRegistryApiJsonrpcUrl"
    );
    this.trustedPoliciesRegistryApiUrl = configService.get<string>(
      "trustedPoliciesRegistryApiUrl"
    );
    this.ledgerApiUrl = configService.get<string>("ledgerApiUrl");
    this.ebsiResolver = new Resolver(
      getEbsiDidResolver({ registry: this.didRegistryApiUrl })
    );
    this.keyResolver = new Resolver(getKeyDidResolver());
    this.timeout = configService.get<number>("requestTimeout");
    this.ebsiAuthority = configService
      .get<string>("domain")
      .replace(/^https?:\/\//, ""); // remove http protocol scheme
    this.authorisationCredentialSchema = configService.get<string>(
      "authorisationCredentialSchema"
    );
    this.pda1CredentialSchema = configService.get<string>(
      "pda1CredentialSchema"
    );
    this.statusList2021CredentialSchemaUrl = configService.get<string>(
      "statusList2021CredentialSchemaUrl"
    );
    this.clientAuthorizationEndpoint = "openid:";
    this.issuerMockKeyPair = {
      ES256: undefined,
      ES256K: undefined,
    };

    this.db = new Level<LevelDbKeyIssuer, LevelDbObjectIssuer>(
      "db/issuerMock",
      {
        keyEncoding: "json",
        valueEncoding: "json",
      }
    );
    this.cacheManagerLevel = {
      interval: setInterval(() => {
        this.manageCache().catch(() => {});
      }, 120_000),
      keys: [],
    };

    this.checksMap = {
      [TI_REQUEST_CT_REVOCABLE]: this.processCredentialRequest.bind(this),
      [TI_VALIDATE_CT_REVOCABLE]: this.processCredentialValidation.bind(this),
      [TI_REVOKE_CT_REVOCABLE]:
        this.processRevokedCredentialValidation.bind(this),
      [TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT]:
        this.processVAToOnboardSubaccountRequest.bind(this),
      [TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT]:
        this.processVerifiableAuthorisationToOnboardValidation.bind(this),
      [TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT]:
        this.processVAToAttestSubaccountRequest.bind(this),
      [TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT]:
        this.processVerifiableAccreditationToAttestValidation.bind(this),
      [TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT]:
        this.processVAToAccreditSubaccountRequest.bind(this),
      [TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT]:
        this.processVerifiableAccreditationToAccreditValidation.bind(this),
      [TAO_REVOKE_RIGHTS_SUBACCOUNT]:
        this.processSubAccountRevocationValidation.bind(this),
      [ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_IN_TIME]:
        this.processIssueToHolderCTWalletSameInTimeRequest.bind(this),
      [ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_IN_TIME]:
        this.processIssueToHolderCTWalletSameInTimeValidation.bind(this),
      [ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_DEFERRED]:
        this.processIssueToHolderCTWalletSameDeferredRequest.bind(this),
      [ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_DEFERRED]:
        this.processIssueToHolderCTWalletSameDeferredValidation.bind(this),
      [ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_PRE_AUTHORISED]:
        this.processIssueToHolderCTWalletSamePreAuthorisedRequest.bind(this),
      [ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_PRE_AUTHORISED]:
        this.processIssueToHolderCTWalletSamePreAuthorisedValidation.bind(this),
      [VERIFIER_ID_TOKEN_EXCHANGE]:
        this.processVerifierIdTokenExchange.bind(this),
      [VERIFIER_VP_VALID_VC]: this.processVerifierVpValidVc.bind(this),
      [VERIFIER_VP_EXPIRED_VC]: this.processVerifierVpExpiredVc.bind(this),
      [VERIFIER_VP_REVOKED_VC]: this.processVerifierVpRevokedVc.bind(this),
      [VERIFIER_VP_NOT_YET_VALID_VC]:
        this.processVerifierVpNotYetValidVc.bind(this),
    } as const;
  }

  async onModuleInit() {
    await this.db.open();
  }

  async onModuleDestroy() {
    clearInterval(this.cacheManagerLevel.interval);
    // remove data from cache
    try {
      await this.db.batch(
        this.cacheManagerLevel.keys.map((k) => ({
          type: "del",
          key: k.key,
        }))
      );
    } catch {
      // empty
    }
    await this.db.close();
  }

  /**
   * Expire key after TTL milliseconds
   * @param key - Key to remove from cache
   * @param ttl - Time to leave, in milliseconds
   */
  private addKeyToCacheManager(
    key: LevelDbKeyIssuer,
    ttl: number = Number.MAX_SAFE_INTEGER
  ) {
    this.cacheManagerLevel.keys.push({ key, expiration: Date.now() + ttl });
    // Sort by expiration: The first ones to expire are at the beginning of the list
    this.cacheManagerLevel.keys.sort((a, b) => a.expiration - b.expiration);
  }

  private async manageCache() {
    const now = Date.now();
    // search which ID has not expired yet
    const id = this.cacheManagerLevel.keys.findIndex((a) => now < a.expiration);

    if (!id) return;

    // remove the expired keys (from 0 until id-1)
    const expiredKeys = this.cacheManagerLevel.keys.splice(0, id);
    // remove data from database
    await this.db.batch(expiredKeys.map(({ key }) => ({ type: "del", key })));
  }

  getCredentialIssuerMetadata(): CredentialIssuerMetadata {
    return {
      credential_issuer: this.issuerMockUri,
      authorization_server: this.authMockUri,
      credential_endpoint: `${this.issuerMockUri}/credential`,
      deferred_credential_endpoint: `${this.issuerMockUri}/credential_deferred`,
      credentials_supported: [
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          display: [
            {
              name: "Verifiable Authorisation to onboard",
              locale: "en-GB",
            },
          ],
        },
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAccreditation",
            "VerifiableAccreditationToAttest",
          ],
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          display: [
            {
              name: "Verifiable Accreditation to attest",
              locale: "en-GB",
            },
          ],
        },
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAccreditation",
            "VerifiableAccreditationToAccredit",
          ],
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          display: [
            {
              name: "Verifiable Accreditation to accredit",
              locale: "en-GB",
            },
          ],
        },
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationForTrustChain",
          ],
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          display: [
            {
              name: "Verifiable Authorisation to issue verifiable tokens",
              locale: "en-GB",
            },
          ],
        },
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "CTAAQualificationCredential",
          ],
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          display: [
            {
              name: "Verifiable Attestation Conformance Qualification To Accredit & Authorise",
              locale: "en-GB",
            },
          ],
        },
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "CTWalletQualificationCredential",
          ],
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          display: [
            {
              name: "Verifiable Attestation Conformance Qualification Holder Wallet",
              locale: "en-GB",
            },
          ],
        },
      ],
    };
  }

  /**
   * Load Auth Mock's key pair from environment.
   *
   * @returns The private and public key JWKs (including "kid")
   */
  async getAuthMockKeyPair() {
    if (!this.authMockKeyPair) {
      this.authMockKeyPair = await getKeyPair(this.authMockPrivateKeyHex);
    }

    return this.authMockKeyPair;
  }

  /**
   * Load Issuer Mock's key pair from environment.
   *
   * @returns The private and public key JWKs (including "kid")
   */
  async getIssuerMockKeyPair(alg: "ES256" | "ES256K"): Promise<KeyPair> {
    let keyPair = this.issuerMockKeyPair[alg];
    if (keyPair === undefined) {
      keyPair = await getKeyPair(this.issuerMockPrivateKeyHex, alg);
      this.issuerMockKeyPair[alg] = keyPair;
    }
    return keyPair;
  }

  async preregisterAttribute(
    did: string,
    issuerType: number,
    attributeId: string
  ): Promise<void> {
    const subject: EbsiIssuer = {
      did: this.issuerMockDid,
      kid: this.issuerMockKid,
      alg: "ES256",
      ...(await this.getIssuerMockKeyPair("ES256")),
    };

    let accessToken: string;

    try {
      accessToken = await this.ebsiAuthorisationService.getAccessToken(
        "tir_write",
        subject
      );
    } catch (e) {
      throw new InternalServerError();
    }

    const issuerMockAttributeId = `0x${this.issuerMockAccreditationUrl.slice(
      this.issuerMockAccreditationUrl.lastIndexOf("/") + 1
    )}`;

    try {
      await preregisterAttribute(
        this.issuerMockDid,
        issuerMockAttributeId,
        this.issuerMockPrivateKeyHex,
        this.trustedIssuersRegistryApiJsonrpcUrl,
        this.ledgerApiUrl,
        this.logger,
        did,
        issuerType,
        attributeId,
        accessToken
      );
    } catch (e) {
      throw new InternalServerError(undefined, {
        detail: e instanceof Error ? e.message : "unknown error",
      });
    }
  }

  /**
   * Helper function to check if the client has passed all the conformance A&A tests
   */
  async checkAccreditAuthoriseTests(
    did: string
  ): Promise<{ success: true } | { success: false; error: string }> {
    const logs = await this.logsService.getParsedLogs(did);

    if (logs.length === 0) {
      return {
        success: false,
        error: `No logs found for DID ${did}`,
      };
    }

    const passedTests = logs.filter((log) => log.testData.result.success);

    if (passedTests.length === 0) {
      return {
        success: false,
        error: `No successful tests found for DID ${did}`,
      };
    }

    const unsuccessfulTests = ACCREDIT_AUTHORISE_TESTS.map((intent) =>
      passedTests.some((test) => test.testData.intent === intent)
        ? null
        : intent
    ).filter(Boolean);

    if (unsuccessfulTests.length > 0) {
      return {
        success: false,
        error: `The client didn't pass all the tests required to get the CTAAQualificationCredential. Failed tests: ${unsuccessfulTests.join(
          ", "
        )}`,
      };
    }

    return { success: true };
  }

  /**
   * Helper function to check if the client has passed all the Issue to Holder tests
   */
  async checkIssueToHolderTests(
    credentialIssuerDid: string
  ): Promise<{ success: true } | { success: false; error: string }> {
    const logs = await this.logsService.getParsedLogs(credentialIssuerDid);

    if (logs.length === 0) {
      return {
        success: false,
        error: `No logs found for Credential Issuer ${credentialIssuerDid}`,
      };
    }

    const passedTests = logs.filter((log) => log.testData.result.success);

    if (passedTests.length === 0) {
      return {
        success: false,
        error: `No successful tests found for Credential Issuer ${credentialIssuerDid}`,
      };
    }

    const unsuccessfulTests = ISSUE_TO_HOLDER_TESTS.map((intent) =>
      passedTests.some((log) => log.testData.intent === intent) ? null : intent
    ).filter(Boolean);

    if (unsuccessfulTests.length > 0) {
      return {
        success: false,
        error: `The client didn't pass all the tests required to get the CTIssueQualificationCredential. Failed tests: ${unsuccessfulTests.join(
          ", "
        )}`,
      };
    }

    return { success: true };
  }

  async postCredential(
    authorizationHeader: string,
    rawRequestBody: unknown
  ): Promise<CredentialResponse | DeferredCredentialResponse> {
    const { credentialRequest, accessTokenPayload } =
      await validatePostCredential(
        this.db,
        this.issuerMockDid,
        this.issuerMockUri,
        (
          await this.getAuthMockKeyPair()
        ).publicKeyJwk,
        this.ebsiResolver,
        this.keyResolver,
        this.timeout,
        authorizationHeader,
        rawRequestBody
      );

    if (credentialRequest.types.includes("CTAAQualificationCredential")) {
      const testsCompletion = await this.checkAccreditAuthoriseTests(
        accessTokenPayload.sub
      );

      if (!testsCompletion.success) {
        throw new CredentialError("invalid_request", {
          errorDescription: testsCompletion.error,
          statusCode: 401,
        });
      }
    }

    if (credentialRequest.types.includes("CTIssueQualificationCredential")) {
      const testsCompletion = await this.checkIssueToHolderTests(
        accessTokenPayload.sub
      );

      if (!testsCompletion.success) {
        throw new CredentialError("invalid_request", {
          errorDescription: testsCompletion.error,
          statusCode: 401,
        });
      }
    }

    // Store c_nonce to prevent replay attacks
    const dbKey = {
      did: this.issuerMockDid,
      nonceAccessToken: accessTokenPayload.claims.c_nonce,
    };
    await this.db.put(dbKey, { nonce: accessTokenPayload.claims.c_nonce });
    // `accessTokenPayload.exp` is expressed in seconds, convert to milliseconds
    const ttl = accessTokenPayload.exp * 1000 - Date.now();
    this.addKeyToCacheManager(dbKey, ttl);

    const keyPair = await this.getIssuerMockKeyPair("ES256");
    const { reservedAttributeId, vcJwt } = await issueCredential(
      this.issuerMockDid,
      this.issuerMockKid,
      keyPair.privateKeyJwk,
      this.issuerMockAccreditationUrl,
      credentialRequest.types.includes("VerifiablePortableDocumentA1")
        ? this.pda1CredentialSchema
        : this.authorisationCredentialSchema,
      {
        ebsiAuthority: this.ebsiAuthority,
        timeout: this.timeout,
        ebsiEnvConfig: {
          didRegistry: this.didRegistryApiUrl,
          trustedIssuersRegistry: this.trustedIssuersRegistryApiUrl,
          trustedPoliciesRegistry: this.trustedPoliciesRegistryApiUrl,
        },
      },
      {},
      credentialRequest,
      accessTokenPayload
    );

    // For issuers, make the registration in the Trusted Issuers Registry
    if (
      credentialRequest.types.includes("VerifiableAuthorisationForTrustChain")
    ) {
      await this.preregisterAttribute(
        accessTokenPayload.sub,
        1,
        reservedAttributeId
      );
      await this.dataStoreService.pushEvent(accessTokenPayload.sub, {
        success: true,
        timestamp: new Date().getTime(),
        intent: RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
      });
      await this.dataStoreService.setVCWithLinkedAttr(
        `${RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN}_${accessTokenPayload.sub}`,
        {
          vc: vcJwt,
          attributeId: reservedAttributeId,
        }
      );
    } else if (
      credentialRequest.types.includes("VerifiableAccreditationToAccredit")
    ) {
      await this.preregisterAttribute(
        accessTokenPayload.sub,
        2,
        reservedAttributeId
      );
      await this.dataStoreService.pushEvent(accessTokenPayload.sub, {
        success: true,
        timestamp: new Date().getTime(),
        intent: TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
      });
      await this.dataStoreService.setVCWithLinkedAttr(
        `${TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT}_${accessTokenPayload.sub}`,
        {
          vc: vcJwt,
          attributeId: reservedAttributeId,
        }
      );
    } else if (
      credentialRequest.types.includes("VerifiableAccreditationToAttest")
    ) {
      await this.preregisterAttribute(
        accessTokenPayload.sub,
        3,
        reservedAttributeId
      );
      await this.dataStoreService.pushEvent(accessTokenPayload.sub, {
        success: true,
        timestamp: new Date().getTime(),
        intent: TI_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST,
      });
      await this.dataStoreService.setVCWithLinkedAttr(
        `${TI_REGISTER_VERIFIABLE_ACCREDITATION_TO_ATTEST}_${accessTokenPayload.sub}`,
        {
          vc: vcJwt,
          attributeId: reservedAttributeId,
        }
      );
    } else if (
      credentialRequest.types.includes("VerifiableAuthorisationToOnboard")
    ) {
      await this.dataStoreService.pushEvent(accessTokenPayload.sub, {
        success: true,
        timestamp: new Date().getTime(),
        intent: TI_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD,
      });
    } else if (
      credentialRequest.types.includes("CTAAQualificationCredential")
    ) {
      await this.dataStoreService.pushEvent(accessTokenPayload.sub, {
        success: true,
        timestamp: new Date().getTime(),
        intent: REQUEST_CTAA_QUALIFICATION_CREDENTIAL,
      });
    } else if (credentialRequest.types.includes("CTWalletCrossInTime")) {
      await this.dataStoreService.pushEvent(accessTokenPayload.sub, {
        success: true,
        timestamp: new Date().getTime(),
        intent: CT_WALLET_CROSS_IN_TIME,
      });
    } else if (credentialRequest.types.includes("CTWalletSameInTime")) {
      await this.dataStoreService.pushEvent(accessTokenPayload.sub, {
        success: true,
        timestamp: new Date().getTime(),
        intent: CT_WALLET_SAME_IN_TIME,
      });
    } else if (credentialRequest.types.includes("CTWalletSamePreAuthorised")) {
      await this.dataStoreService.pushEvent(accessTokenPayload.sub, {
        success: true,
        timestamp: new Date().getTime(),
        intent: CT_WALLET_SAME_PRE_AUTHORISED,
      });
    } else if (credentialRequest.types.includes("CTWalletCrossPreAuthorised")) {
      await this.dataStoreService.pushEvent(accessTokenPayload.sub, {
        success: true,
        timestamp: new Date().getTime(),
        intent: CT_WALLET_CROSS_PRE_AUTHORISED,
      });
    } else if (
      credentialRequest.types.includes("CTWalletQualificationCredential")
    ) {
      await this.dataStoreService.pushEvent(accessTokenPayload.sub, {
        success: true,
        timestamp: new Date().getTime(),
        intent: REQUEST_CT_WALLET_QUALIFICATION_CREDENTIAL,
      });
    } else if (
      credentialRequest.types.includes("CTIssueQualificationCredential")
    ) {
      await this.dataStoreService.pushEvent(accessTokenPayload.sub, {
        success: true,
        timestamp: new Date().getTime(),
        intent: REQUEST_CT_ISSUE_TO_HOLDER_QUALIFICATION_CREDENTIAL,
      });
    }

    // Deferred issuance flows
    const deferredCredentials = [
      "VerifiableAuthorisationForTrustChain",
      "CTWalletCrossDeferred",
      "CTWalletSameDeferred",
      "VerifiablePortableDocumentA1",
    ] as const;
    if (
      credentialRequest.types.some((type) => deferredCredentials.includes(type))
    ) {
      // Create random acceptance token
      const acceptanceToken = Buffer.from(randomBytes(32)).toString(
        "base64url"
      );

      // Store VC JWT in cache
      const deferredCredentialId = getDeferredCredentialId(acceptanceToken);
      await this.cacheManager.set(
        deferredCredentialId,
        {
          vcJwt,
          notBefore: Date.now() + 5000, // Available in 5 seconds
        },
        300_000
      ); // 5 minutes (5 * 60 * 1000)

      // Return acceptance token
      return {
        acceptance_token: acceptanceToken,
      };
    }

    return {
      format: "jwt_vc",
      credential: vcJwt,
    };
  }

  async postCredentialDeferred(
    authorizationHeader: string
  ): Promise<CredentialResponse> {
    if (!authorizationHeader) {
      throw new CredentialError("invalid_token", {
        errorDescription: "Authorization header is missing",
      });
    }

    if (!authorizationHeader.startsWith("Bearer ")) {
      throw new CredentialError("invalid_token", {
        errorDescription: "Authorization header must contain a Bearer token",
      });
    }

    // Get the deferred credential from the cache
    const accessToken = authorizationHeader.replace("Bearer ", "");
    const deferredCredentialId = getDeferredCredentialId(accessToken);
    const deferredCredential = await this.cacheManager.get<{
      vcJwt: string;
      notBefore: number;
    }>(deferredCredentialId);

    if (!deferredCredential) {
      throw new CredentialError("invalid_token", {
        errorDescription: "Deferred credential not found",
      });
    }

    if (Date.now() < deferredCredential.notBefore) {
      throw new CredentialError("invalid_request", {
        errorDescription: "Deferred credential not available yet",
      });
    }

    await this.cacheManager.del(deferredCredentialId);

    const deferredCredentialPayload = decodeJwt(deferredCredential.vcJwt); // We assume the decoding won't fail
    const vc = deferredCredentialPayload["vc"] as EbsiVerifiableAttestation;

    if (vc.type.includes("CTWalletCrossDeferred")) {
      await this.dataStoreService.pushEvent(vc.credentialSubject.id as string, {
        success: true,
        timestamp: new Date().getTime(),
        intent: CT_WALLET_CROSS_DEFERRED,
      });
    } else if (vc.type.includes("CTWalletSameDeferred")) {
      await this.dataStoreService.pushEvent(vc.credentialSubject.id as string, {
        success: true,
        timestamp: new Date().getTime(),
        intent: CT_WALLET_SAME_DEFERRED,
      });
    }

    return {
      format: "jwt_vc",
      credential: deferredCredential.vcJwt,
    };
  }

  /**
   * Expose Issuer Mock's public keys.
   * Used in "Issue & Revoke" tests.
   *
   * @returns Issuer Mock's JWKS
   */
  async getJwks(): Promise<JsonWebKeySet> {
    const { publicKeyJwk } = await this.getIssuerMockKeyPair("ES256");

    // Return JWKS
    return {
      keys: [publicKeyJwk],
    };
  }

  /**
   * Trigger a credential offer from the Issuer Mock.
   *
   * @see https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-11.html#name-credential-offer
   */
  async initiateCredentialOffer({
    credential_type: credentialType,
    credential_offer_endpoint: credentialOfferEndpoint,
    client_id: clientId,
  }: GetInitiateCredentialOfferDto) {
    const offeredTypes = [
      "VerifiableCredential",
      "VerifiableAttestation",
      credentialType,
    ];

    // Define grants based on the flow related to the credential type
    let grants = {};
    const keyPair = await this.getIssuerMockKeyPair("ES256");
    const signingKey = await importJWK(keyPair.privateKeyJwk, "ES256");
    if (
      HOLDER_WALLET_AUTHORIZATION_CODE_CREDENTIAL_TYPES.includes(credentialType)
    ) {
      // Generate a signed JWT as the issuer state
      const issuerState = await new SignJWT({
        client_id: clientId,
        credential_types: offeredTypes,
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: keyPair.publicKeyJwk.kid,
        })
        .setIssuedAt()
        .setExpirationTime("5m")
        .setIssuer(this.issuerMockUri)
        .setAudience(this.authMockUri)
        .setSubject(clientId)
        .sign(signingKey);

      grants = {
        authorization_code: {
          issuer_state: issuerState,
        },
      };
    } else {
      // For pre-authorized flow
      const preAuthorizedCode = await new SignJWT({
        client_id: clientId,
        authorization_details: [
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [this.issuerMockUri],
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              credentialType,
            ],
          },
        ],
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: keyPair.publicKeyJwk.kid,
        })
        .setIssuedAt()
        .setExpirationTime("5m")
        .setIssuer(this.issuerMockUri)
        .setAudience(this.authMockUri)
        .setSubject(clientId)
        .sign(signingKey);

      grants = {
        "urn:ietf:params:oauth:grant-type:pre-authorized_code": {
          "pre-authorized_code": preAuthorizedCode,
          user_pin_required: true,
        },
      };
    }

    const credentialOffer = {
      credential_issuer: this.issuerMockUri,
      credentials: [
        {
          format: "jwt_vc",
          types: offeredTypes,
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
        },
      ],
      grants,
    } satisfies CredentialOfferPayload;

    const location = `${credentialOfferEndpoint}?${new URLSearchParams({
      credential_offer: JSON.stringify(credentialOffer),
    } satisfies CredentialOffer).toString()}`;

    // If the credential offer redirect URI is longer than 500 characters, use `credential_offer_uri` instead of `credential_offer`
    if (location.length > 500) {
      // Store request in cache
      const credentialOfferId = randomUUID();

      // Store credential offer in cache
      const cacheKey = { did: this.issuerMockDid, credentialOfferId };
      await this.db.put(cacheKey, credentialOffer);
      const ttl = 120_000; // Available for 2 minutes
      this.addKeyToCacheManager(cacheKey, ttl);

      return `${credentialOfferEndpoint}?${new URLSearchParams({
        credential_offer_uri: `${this.issuerMockUri}/offers/${credentialOfferId}`,
      } satisfies CredentialOffer).toString()}`;
    }

    return location;
  }

  /**
   * Get Credential Offer by reference
   * @see https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-11.html#section-4.1.3
   */
  async getCredentialOffer(
    credentialOfferId: string
  ): Promise<CredentialOfferPayload> {
    const cacheKey = { did: this.issuerMockDid, credentialOfferId };
    try {
      const credentialOffer = (await this.db.get(
        cacheKey
      )) as CredentialOfferPayload;

      return credentialOffer;
    } catch (error) {
      throw new NotFoundError(NotFoundError.defaultTitle, {
        detail: `No Credential Offer found with the ID ${credentialOfferId}`,
      });
    }
  }

  /**
   * ACTIONS
   */

  /**
   * A&A - TI_REQUEST_CT_REVOCABLE
   * The user asks Issuer Mock to request a CTRevocable credential.
   *
   * Instructions:
   * Conformance Wallet / Conformance Issuer can be same entity/DID in this case, there is no need to split these.
   * ”Conformance Wallet" must request ["VerifiableCredential", "VerifiableAttestation", "CTRevocable"] from the "client_id", this is expected to be returned with in-time flow.
   * The Client_id must host /.well-known/openid-credential-issuer to define where the authentication must be received from and where the credential endpoints are.
   * The request flow is normal VCI based, as all other cases are.
   */
  async processCredentialRequest(params: CheckParams): Promise<CheckResult> {
    const parsedParams = accreditAuthoriseParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { clientId } = parsedParams.data;

    // Get credential with the requested types
    const requestedTypes = [
      "VerifiableCredential",
      "VerifiableAttestation",
      "CTRevocable",
    ];

    const credentialRequest = await requestCredential({
      clientAuthorizationEndpoint: this.clientAuthorizationEndpoint,
      clientId: this.issuerMockUri,
      clientMetadata: {
        redirect_uris: [`${this.issuerMockUri}/code-cb`],
        jwks_uri: `${this.issuerMockUri}/jwks`,
        authorization_endpoint: this.clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair: await this.getIssuerMockKeyPair("ES256"),
      redirectUri: `${this.issuerMockUri}/redirect`,
      requestedTypes,
      subjectDid: this.issuerMockDid,
      subjectKid: this.issuerMockKid,
      logger: this.logger,
      timeout: this.timeout,
    });

    if (!credentialRequest.success) {
      return {
        success: false,
        errors: credentialRequest.errors,
      };
    }

    // Store VC (validation is done in another action)
    const { credential } = credentialRequest;
    const credentialId = getCredentialId(parsedParams.data, "CTRevocable");
    await this.cacheManager.set(
      credentialId,
      credential,
      300_000 // 5 minutes (5 * 60 * 1000)
    );

    return { success: true };
  }

  /**
   * Internal helper to perform a basic validation of a CTRevocable JWT credential.
   */
  private safeParseCTRevocable(
    credential: unknown,
    did: string
  ):
    | { success: true; credential: string }
    | { success: false; errors: string[] } {
    if (typeof credential !== "string") {
      return {
        success: false,
        errors: ["Credential is not a string."],
      };
    }

    // Try to decode VC JWT payload
    let rawCredentialPayload: JWTPayload;
    try {
      rawCredentialPayload = decodeJwt(credential);
    } catch (e) {
      return {
        success: false,
        errors: ["Credential is not valid: Unable to decode JWT VC"],
      };
    }

    // Most basic schema needed before doing the full VC validation
    const schema = z.object({
      vc: z.object({
        issuer: z.string(),
        type: z.array(z.string()),
        credentialSubject: z.object({
          id: z.string(),
        }),
        credentialStatus: z.object({
          statusPurpose: z.string(),
        }),
      }),
    });

    const parsedCredentialPayload = schema.safeParse(rawCredentialPayload);

    if (!parsedCredentialPayload.success) {
      return {
        success: false,
        errors: [
          "Credential is not valid: Invalid EBSI Verifiable Attestation",
          ...formatZodError(parsedCredentialPayload.error),
        ],
      };
    }

    const { vc } = parsedCredentialPayload.data;

    // Verify that the credential is issued to the Issuer Mock DID
    if (vc.credentialSubject.id !== this.issuerMockDid) {
      return {
        success: false,
        errors: [
          `Credential is not issued to the Issuer Mock DID: ${this.issuerMockDid}`,
        ],
      };
    }

    // Verify that the credential is issued by the client's DID
    if (vc.issuer !== did) {
      return {
        success: false,
        errors: [
          `Credential issuer ${vc.issuer} is not the same as client's DID: ${did}`,
        ],
      };
    }

    // Verify that the credential has the CTRevocable type
    if (!vc.type.includes("CTRevocable")) {
      return {
        success: false,
        errors: ["Credential does not have the CTRevocable type."],
      };
    }

    const { credentialStatus } = vc;

    // Verify that the credentialStatus.statusPurpose is "revocation"
    if (credentialStatus.statusPurpose !== "revocation") {
      return {
        success: false,
        errors: ["Credential statusPurpose must be 'revocation'."],
      };
    }

    // Return the credential (VC JWT string)
    return {
      success: true,
      credential,
    };
  }

  /**
   * A&A - TI_VALIDATE_CT_REVOCABLE
   * The user asks Issuer Mock to validate the CTRevocable credential issued earlier.
   */
  async processCredentialValidation(params: CheckParams): Promise<CheckResult> {
    const parsedParams = accreditAuthoriseParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { did } = parsedParams.data;

    const credentialId = getCredentialId(parsedParams.data, "CTRevocable");
    const unsafeCredential = await this.cacheManager.get(credentialId);

    if (!unsafeCredential) {
      return {
        success: false,
        errors: ["No credential found for this client."],
      };
    }

    const parsedCredential = this.safeParseCTRevocable(unsafeCredential, did);

    if (!parsedCredential.success) {
      await this.cacheManager.del(credentialId);
      return {
        success: false,
        errors: parsedCredential.errors,
      };
    }

    const { credential } = parsedCredential;

    try {
      await verifyCredentialJwt(credential, {
        ebsiAuthority: this.ebsiAuthority,
        timeout: this.timeout,
      });
    } catch (err) {
      await this.cacheManager.del(credentialId);
      return {
        success: false,
        errors: [
          `Credential is not valid: ${getErrorMessage(err)}`,
          ...getErrorDetails(err, "Credential"),
        ],
      };
    }

    return { success: true };
  }

  /**
   * A&A - TI_REVOKE_CT_REVOCABLE
   * The user asks Issuer Mock to validate the CTRevocable credential issued earlier.
   * The credential status must be revoked.
   */
  async processRevokedCredentialValidation(
    params: CheckParams
  ): Promise<CheckResult> {
    const parsedParams = accreditAuthoriseParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { did } = parsedParams.data;

    const credentialId = getCredentialId(parsedParams.data, "CTRevocable");
    const unsafeCredential = await this.cacheManager.get(credentialId);

    if (!unsafeCredential) {
      return {
        success: false,
        errors: ["No credential found for this client."],
      };
    }

    const parsedCredential = this.safeParseCTRevocable(unsafeCredential, did);

    if (!parsedCredential.success) {
      await this.cacheManager.del(credentialId);
      return {
        success: false,
        errors: parsedCredential.errors,
      };
    }

    const { credential } = parsedCredential;

    // Verify credential
    try {
      await verifyCredentialJwt(credential, {
        ebsiAuthority: this.ebsiAuthority,
        timeout: this.timeout,
      });
    } catch (err) {
      // If credential is revoked, return success
      if (
        err instanceof ValidationError &&
        err.message === "The credential is revoked"
      ) {
        return { success: true };
      }

      await this.cacheManager.del(credentialId);

      return {
        success: false,
        errors: [`Credential is not valid: ${getErrorMessage(err)}`],
      };
    }

    return {
      success: false,
      errors: ["The credential is not revoked."],
    };
  }

  /**
   * A&A - TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT
   * The user asks Issuer Mock to request a VerifiableAuthorisationToOnboard credential for its sub-account.
   */
  async processVAToOnboardSubaccountRequest(
    params: CheckParams
  ): Promise<CheckResult> {
    const parsedParams = accreditAuthoriseParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { did, clientId } = parsedParams.data;

    // We assume that "did" is a valid EBSI DID
    const subaccountDid = getSubAccountDid(did);

    const keyPairES256 = await this.getIssuerMockKeyPair("ES256");
    const subaccountKid = `${subaccountDid}#${keyPairES256.publicKeyJwk.kid}`;

    // Get credential with the requested types
    const requestedTypes = [
      "VerifiableCredential",
      "VerifiableAttestation",
      "VerifiableAuthorisationToOnboard",
    ];

    const credentialRequest = await requestCredential({
      clientAuthorizationEndpoint: this.clientAuthorizationEndpoint,
      clientId: this.issuerMockUri,
      clientMetadata: {
        redirect_uris: [`${this.issuerMockUri}/code-cb`],
        jwks_uri: `${this.issuerMockUri}/jwks`,
        authorization_endpoint: this.clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair: keyPairES256,
      redirectUri: `${this.issuerMockUri}/redirect`,
      requestedTypes,
      subjectDid: subaccountDid,
      subjectKid: subaccountKid,
      logger: this.logger,
      timeout: this.timeout,
    });

    if (!credentialRequest.success) {
      return {
        success: false,
        errors: credentialRequest.errors,
      };
    }

    // Store VC (validation is done in another action)
    const { credential } = credentialRequest;
    const credentialId = getCredentialId(
      parsedParams.data,
      "VerifiableAuthorisationToOnboard"
    );
    await this.cacheManager.set(
      credentialId,
      credential,
      300_000 // 5 minutes (5 * 60 * 1000)
    );

    return { success: true };
  }

  /**
   * A&A - TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT
   * The user asks Issuer Mock to validate the VerifiableAuthorisationToOnboard credential issued earlier.
   * The Issuer Mock then registers the new sub-account DID into the DID Registry.
   */
  async processVerifiableAuthorisationToOnboardValidation(
    params: CheckParams
  ): Promise<CheckResult> {
    const parsedParams = accreditAuthoriseParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { did } = parsedParams.data;

    const credentialId = getCredentialId(
      parsedParams.data,
      "VerifiableAuthorisationToOnboard"
    );

    const vcJwt = await this.cacheManager.get(credentialId);

    if (!vcJwt) {
      return {
        success: false,
        errors: ["No credential found for this client."],
      };
    }

    if (typeof vcJwt !== "string") {
      return {
        success: false,
        errors: ["Credential is not a string."],
      };
    }

    let verifiedCredential: EbsiVerifiableAttestation;
    try {
      verifiedCredential = await verifyCredentialJwt(vcJwt, {
        ebsiAuthority: this.ebsiAuthority,
        timeout: this.timeout,
        validateAccreditationWithoutTermsOfUse: true,
      });
    } catch (err) {
      await this.cacheManager.del(credentialId);
      return {
        success: false,
        errors: [
          `Credential is not valid: ${getErrorMessage(err)}`,
          ...getErrorDetails(err, "Credential"),
        ],
      };
    }

    // Verify that the credential is issued by the client's DID
    if (verifiedCredential.issuer !== did) {
      return {
        success: false,
        errors: [
          `Credential issuer ${verifiedCredential.issuer} is not the same as client's DID: ${did}`,
        ],
      };
    }

    // We assume that "did" is a valid EBSI DID
    const subaccountDid = getSubAccountDid(did);

    // Check that the credential subject is equal to the sub-account DID
    if (verifiedCredential.credentialSubject.id !== subaccountDid) {
      return {
        success: false,
        errors: [
          `Credential is not valid: credentialSubject.id is not equal to the sub-account DID ${subaccountDid}`,
        ],
      };
    }

    // Verify that the credential types contain "VerifiableAuthorisationToOnboard"
    if (!verifiedCredential.type.includes("VerifiableAuthorisationToOnboard")) {
      return {
        success: false,
        errors: [
          "Credential is not valid: credential types don't contain 'VerifiableAuthorisationToOnboard'",
        ],
      };
    }

    // Onboard the sub-account DID into the DID Registry (using Issuer Mock keys)
    const keyPairES256 = await this.getIssuerMockKeyPair("ES256");
    const keyPairES256K = await this.getIssuerMockKeyPair("ES256K");
    const subAccountKid = `${subaccountDid}#${keyPairES256K.publicKeyJwk.kid}`;
    const subject: EbsiIssuer = {
      did: subaccountDid,
      kid: subAccountKid,
      alg: "ES256K",
      ...keyPairES256K,
    };
    let accessToken: string;
    try {
      accessToken = await this.ebsiAuthorisationService.getAccessToken(
        "didr_invite",
        subject,
        [vcJwt]
      );
    } catch (err) {
      return {
        success: false,
        errors: [
          `Failed to get DID Registry invite access token: ${getErrorMessage(
            err
          )}`,
        ],
      };
    }

    // Build unsigned transaction
    const wallet = new ethers.Wallet(this.issuerMockPrivateKeyHex);
    const now = Math.floor(Date.now() / 1000);
    const in6months = now + 6 * 30 * 24 * 3600;
    let responseBuild: AxiosResponse<{
      result: UnsignedTransaction;
    }>;

    try {
      responseBuild = await axios.post(
        this.didRegistryApiJsonrpcUrl,
        {
          jsonrpc: "2.0",
          method: "insertDidDocument",
          params: [
            {
              from: wallet.address,
              did: subaccountDid,
              baseDocument: JSON.stringify({
                "@context": [
                  "https://www.w3.org/ns/did/v1",
                  "https://w3id.org/security/suites/jws-2020/v1",
                ],
              }),
              vMethodId: keyPairES256K.publicKeyJwk.kid,
              publicKey: wallet.publicKey,
              isSecp256k1: true,
              notBefore: now,
              notAfter: in6months,
            },
          ],
          id: 1,
        },
        {
          headers: { authorization: `Bearer ${accessToken}` },
        }
      );
    } catch (error) {
      logAxiosRequestError(error, this.logger);
      return {
        success: false,
        errors: [
          `Unable to build the transaction to register the DID document: ${getErrorMessage(
            error
          )}`,
        ],
      };
    }

    let unsignedTransaction = responseBuild.data.result;

    let transactionResult = await signAndSendTransaction(
      unsignedTransaction,
      wallet,
      this.didRegistryApiJsonrpcUrl,
      this.logger,
      accessToken
    );

    if (!transactionResult.success) {
      return {
        success: false,
        errors: [
          `Unable to send the transaction to register the DID document: ${getErrorMessage(
            transactionResult.error
          )}`,
        ],
      };
    }

    let { txId } = transactionResult;

    let miningResult = await waitToBeMined(
      this.ledgerApiUrl,
      this.logger,
      txId
    );

    if (!miningResult.success) {
      return {
        success: false,
        errors: [miningResult.error.message],
      };
    }

    // Add ES256 verification method to DID document

    // Get "didr_write" access token
    try {
      accessToken = await this.ebsiAuthorisationService.getAccessToken(
        "didr_write",
        subject
      );
    } catch (err) {
      return {
        success: false,
        errors: [
          `Failed to get DID Registry write access token: ${getErrorMessage(
            err
          )}`,
        ],
      };
    }

    try {
      responseBuild = await axios.post(
        this.didRegistryApiJsonrpcUrl,
        {
          jsonrpc: "2.0",
          method: "addVerificationMethod",
          params: [
            {
              from: wallet.address,
              did: subaccountDid,
              vMethodId: keyPairES256.publicKeyJwk.kid,
              publicKey: `0x${Buffer.from(
                JSON.stringify(keyPairES256.publicKeyJwk)
              ).toString("hex")}`,
              isSecp256k1: false,
            },
          ],
          id: 1,
        },
        {
          headers: { authorization: `Bearer ${accessToken}` },
        }
      );
    } catch (error) {
      logAxiosRequestError(error, this.logger);
      return {
        success: false,
        errors: [
          `Unable to build the transaction to add an ES256 verification method to the DID document: ${getErrorMessage(
            error
          )}`,
        ],
      };
    }

    unsignedTransaction = responseBuild.data.result;

    transactionResult = await signAndSendTransaction(
      unsignedTransaction,
      wallet,
      this.didRegistryApiJsonrpcUrl,
      this.logger,
      accessToken
    );

    if (!transactionResult.success) {
      return {
        success: false,
        errors: [
          `Unable to send the transaction to add an ES256 verification method to the DID document: ${getErrorMessage(
            transactionResult.error
          )}`,
        ],
      };
    }

    txId = transactionResult.txId;

    miningResult = await waitToBeMined(this.ledgerApiUrl, this.logger, txId);

    if (!miningResult.success) {
      return {
        success: false,
        errors: [miningResult.error.message],
      };
    }

    // Register ES256 verification method as authentication method
    try {
      responseBuild = await axios.post(
        this.didRegistryApiJsonrpcUrl,
        {
          jsonrpc: "2.0",
          method: "addVerificationRelationship",
          params: [
            {
              from: wallet.address,
              did: subaccountDid,
              name: "authentication",
              vMethodId: keyPairES256.publicKeyJwk.kid,
              notBefore: now,
              notAfter: in6months,
            },
          ],
          id: 1,
        },
        {
          headers: { authorization: `Bearer ${accessToken}` },
        }
      );
    } catch (error) {
      logAxiosRequestError(error, this.logger);
      return {
        success: false,
        errors: [
          `Unable to build the transaction to register the ES256 verification method as an authentication method: ${getErrorMessage(
            error
          )}`,
        ],
      };
    }

    unsignedTransaction = responseBuild.data.result;

    transactionResult = await signAndSendTransaction(
      unsignedTransaction,
      wallet,
      this.didRegistryApiJsonrpcUrl,
      this.logger,
      accessToken
    );

    if (!transactionResult.success) {
      return {
        success: false,
        errors: [
          `Unable to send the transaction to register the ES256 verification method as an authentication method: ${getErrorMessage(
            transactionResult.error
          )}`,
        ],
      };
    }

    txId = transactionResult.txId;

    miningResult = await waitToBeMined(this.ledgerApiUrl, this.logger, txId);

    if (!miningResult.success) {
      return {
        success: false,
        errors: [miningResult.error.message],
      };
    }

    // Register ES256 verification method as assertionMethod method
    try {
      responseBuild = await axios.post(
        this.didRegistryApiJsonrpcUrl,
        {
          jsonrpc: "2.0",
          method: "addVerificationRelationship",
          params: [
            {
              from: wallet.address,
              did: subaccountDid,
              name: "assertionMethod",
              vMethodId: keyPairES256.publicKeyJwk.kid,
              notBefore: now,
              notAfter: in6months,
            },
          ],
          id: 1,
        },
        {
          headers: { authorization: `Bearer ${accessToken}` },
        }
      );
    } catch (error) {
      logAxiosRequestError(error, this.logger);
      return {
        success: false,
        errors: [
          `Unable to build the transaction to register the ES256 verification method as an assertion method: ${getErrorMessage(
            error
          )}`,
        ],
      };
    }

    unsignedTransaction = responseBuild.data.result;

    transactionResult = await signAndSendTransaction(
      unsignedTransaction,
      wallet,
      this.didRegistryApiJsonrpcUrl,
      this.logger,
      accessToken
    );

    if (!transactionResult.success) {
      return {
        success: false,
        errors: [
          `Unable to send the transaction to register the ES256 verification method as an assertion method: ${getErrorMessage(
            transactionResult.error
          )}`,
        ],
      };
    }

    txId = transactionResult.txId;

    miningResult = await waitToBeMined(this.ledgerApiUrl, this.logger, txId);

    if (!miningResult.success) {
      return {
        success: false,
        errors: [miningResult.error.message],
      };
    }

    await this.cacheManager.del(credentialId);

    return { success: true };
  }

  /**
   * A&A - TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT
   * The user asks Issuer Mock to request a VerifiableAccreditationToAttest credential for its sub-account.
   */
  async processVAToAttestSubaccountRequest(
    params: CheckParams
  ): Promise<CheckResult> {
    const parsedParams = accreditAuthoriseParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { did, clientId } = parsedParams.data;

    // We assume that "did" is a valid EBSI DID
    const subaccountDid = getSubAccountDid(did);

    const keyPairES256 = await this.getIssuerMockKeyPair("ES256");
    const subaccountKid = `${subaccountDid}#${keyPairES256.publicKeyJwk.kid}`;

    // Get credential with the requested types
    const requestedTypes = [
      "VerifiableCredential",
      "VerifiableAttestation",
      "VerifiableAccreditation",
      "VerifiableAccreditationToAttest",
    ];

    const credentialRequest = await requestCredential({
      clientAuthorizationEndpoint: this.clientAuthorizationEndpoint,
      clientId: this.issuerMockUri,
      clientMetadata: {
        redirect_uris: [`${this.issuerMockUri}/code-cb`],
        jwks_uri: `${this.issuerMockUri}/jwks`,
        authorization_endpoint: this.clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair: keyPairES256,
      redirectUri: `${this.issuerMockUri}/redirect`,
      requestedTypes,
      subjectDid: subaccountDid,
      subjectKid: subaccountKid,
      logger: this.logger,
      timeout: this.timeout,
    });

    if (!credentialRequest.success) {
      return {
        success: false,
        errors: credentialRequest.errors,
      };
    }

    // Store VC (validation is done in another action)
    const { credential } = credentialRequest;
    const credentialId = getCredentialId(
      parsedParams.data,
      "VerifiableAccreditationToAttest"
    );
    await this.cacheManager.set(
      credentialId,
      credential,
      300_000 // 5 minutes (5 * 60 * 1000)
    );

    return { success: true };
  }

  /**
   * Helper function to validate an accreditation issued by the client.
   */
  async validateAccreditationJwt(
    vcJwt: unknown,
    clientDid: string,
    requiredType:
      | "VerifiableAccreditationToAttest"
      | "VerifiableAccreditationToAccredit",
    issuerType: "TI" | "TAO"
  ): Promise<
    | {
        success: true;
        data: {
          subaccountDid: string;
          reservedAttributeId: string;
          vcJwt: string;
        };
      }
    | { success: false; errors: string[] }
  > {
    if (typeof vcJwt !== "string") {
      return {
        success: false,
        errors: ["Credential is not a string."],
      };
    }

    let verifiedCredential: EbsiVerifiableAttestation;
    try {
      verifiedCredential = await verifyCredentialJwt(vcJwt, {
        ebsiAuthority: this.ebsiAuthority,
        timeout: this.timeout,
        validateAccreditationWithoutTermsOfUse: true,
      });
    } catch (err) {
      return {
        success: false,
        errors: [
          `Credential is not valid: ${getErrorMessage(err)}`,
          ...getErrorDetails(err, "Credential"),
        ],
      };
    }

    // Verify that the credential is issued by the client's DID
    if (verifiedCredential.issuer !== clientDid) {
      return {
        success: false,
        errors: [
          `Credential issuer ${verifiedCredential.issuer} is not the same as client's DID: ${clientDid}`,
        ],
      };
    }

    // We assume that "clientDid" is a valid EBSI DID
    const subaccountDid = getSubAccountDid(clientDid);

    // Check that the credential subject is equal to the sub-account DID
    if (verifiedCredential.credentialSubject.id !== subaccountDid) {
      return {
        success: false,
        errors: [
          `Credential is not valid: credentialSubject.id is not equal to the sub-account DID ${subaccountDid}`,
        ],
      };
    }

    // Verify that the credential types contain the required type
    if (!verifiedCredential.type.includes(requiredType)) {
      return {
        success: false,
        errors: [
          `Credential is not valid: credential types don't contain '${requiredType}'`,
        ],
      };
    }

    // Verify that the credential credentialSubject contains a "reservedAttributeId" property
    if (!verifiedCredential.credentialSubject["reservedAttributeId"]) {
      return {
        success: false,
        errors: [
          "Credential is not valid: credentialSubject doesn't contain a 'reservedAttributeId' property",
        ],
      };
    }

    if (
      typeof verifiedCredential.credentialSubject["reservedAttributeId"] !==
      "string"
    ) {
      return {
        success: false,
        errors: [
          "Credential is not valid: 'credentialSubject.reservedAttributeId' property is not a string",
        ],
      };
    }

    // Verify pre-registered attribute
    const { reservedAttributeId } = verifiedCredential.credentialSubject;

    // Note: we assume the response is correctly formatted
    let attributeResponse: AxiosResponse<IssuerAttribute>;
    try {
      attributeResponse = await axios.get(
        `${this.trustedIssuersRegistryApiUrl}/${subaccountDid}/attributes/${reservedAttributeId}`,
        {
          timeout: this.timeout,
        }
      );
    } catch (error) {
      logAxiosRequestError(error, this.logger);
      return {
        success: false,
        errors: [
          `Unable to get pre-registered attribute ${reservedAttributeId}: ${getErrorMessage(
            error
          )}`,
        ],
      };
    }

    if (attributeResponse.data.did !== subaccountDid) {
      return {
        success: false,
        errors: [
          `Attribute ${reservedAttributeId} is not registered for DID ${subaccountDid}`,
        ],
      };
    }

    const { attribute } = attributeResponse.data;

    // "issuerType" must be "TI" or "TAO"
    if (attribute.issuerType !== issuerType) {
      return {
        success: false,
        errors: [
          `Attribute ${reservedAttributeId} 'issuerType' property must be "${issuerType}", got "${attribute.issuerType}"`,
        ],
      };
    }

    // "tao" must be the client DID
    if (attribute.tao !== clientDid) {
      return {
        success: false,
        errors: [
          `Attribute ${reservedAttributeId} 'tao' property must be ${clientDid}, got ${attribute.tao}`,
        ],
      };
    }

    // "rootTao" must be the Conformance Issuer DID
    if (attribute.rootTao !== this.issuerMockDid) {
      return {
        success: false,
        errors: [
          `Attribute ${reservedAttributeId} 'rootTao' property must be ${this.issuerMockDid}, got ${attribute.rootTao}`,
        ],
      };
    }

    // "body" must be an empty string
    if (attribute.body !== "") {
      return {
        success: false,
        errors: [
          `Attribute ${reservedAttributeId} 'body' property must be empty`,
        ],
      };
    }

    return {
      success: true,
      data: {
        subaccountDid,
        reservedAttributeId,
        vcJwt,
      },
    };
  }

  /**
   * Helper function to register a credential into the TIR.
   */
  async registerCredential(
    subaccountDid: string,
    reservedAttributeId: string,
    vcJwt: string,
    accessTokenScope: "tir_invite" | "tir_write"
  ): Promise<{ success: true } | { success: false; error: string }> {
    const keyPairES256 = await this.getIssuerMockKeyPair("ES256");
    const subAccountKid = `${subaccountDid}#${keyPairES256.publicKeyJwk.kid}`;
    const subject: EbsiIssuer = {
      did: subaccountDid,
      kid: subAccountKid,
      alg: "ES256",
      ...keyPairES256,
    };
    let accessToken: string;
    try {
      accessToken = await this.ebsiAuthorisationService.getAccessToken(
        accessTokenScope,
        subject,
        accessTokenScope === "tir_invite" ? [vcJwt] : []
      );
    } catch (err) {
      return {
        success: false,
        error: `Failed to get ${accessTokenScope} access token: ${getErrorMessage(
          err
        )}`,
      };
    }

    // Build unsigned transaction
    const wallet = new ethers.Wallet(this.issuerMockPrivateKeyHex);
    let responseBuild: AxiosResponse<{
      result: UnsignedTransaction;
    }>;

    try {
      responseBuild = await axios.post(
        this.trustedIssuersRegistryApiJsonrpcUrl,
        {
          jsonrpc: "2.0",
          method: "setAttributeData",
          params: [
            {
              from: wallet.address,
              did: subaccountDid,
              attributeId: reservedAttributeId,
              attributeData: `0x${Buffer.from(vcJwt).toString("hex")}`,
            },
          ],
          id: 1,
        },
        {
          headers: { authorization: `Bearer ${accessToken}` },
        }
      );
    } catch (error) {
      logAxiosRequestError(error, this.logger);
      return {
        success: false,
        error: `Unable to build the transaction to register the accreditation: ${getErrorMessage(
          error
        )}`,
      };
    }

    const unsignedTransaction = responseBuild.data.result;

    const transactionResult = await signAndSendTransaction(
      unsignedTransaction,
      wallet,
      this.trustedIssuersRegistryApiJsonrpcUrl,
      this.logger,
      accessToken
    );

    if (!transactionResult.success) {
      return {
        success: false,
        error: `Unable to send the transaction to register the accreditation: ${getErrorMessage(
          transactionResult.error
        )}`,
      };
    }

    const { txId } = transactionResult;

    const miningResult = await waitToBeMined(
      this.ledgerApiUrl,
      this.logger,
      txId
    );

    if (!miningResult.success) {
      return {
        success: false,
        error: miningResult.error.message,
      };
    }

    return { success: true };
  }

  /**
   * A&A - TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT
   * The user asks Issuer Mock to validate the VerifiableAccreditationToAttest credential issued earlier.
   * The Issuer Mock then registers the new attribute into the TIR.
   */
  async processVerifiableAccreditationToAttestValidation(
    params: CheckParams
  ): Promise<CheckResult> {
    const parsedParams = accreditAuthoriseParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { did } = parsedParams.data;

    // Get credential from cache
    const credentialId = getCredentialId(
      parsedParams.data,
      "VerifiableAccreditationToAttest"
    );

    const unverifiedCredentialJwt = await this.cacheManager.get(credentialId);

    if (!unverifiedCredentialJwt) {
      return {
        success: false,
        errors: ["No credential found for this client."],
      };
    }

    // Delete credential from cache
    await this.cacheManager.del(credentialId);

    // Verify credential
    const accreditationValidation = await this.validateAccreditationJwt(
      unverifiedCredentialJwt,
      did,
      "VerifiableAccreditationToAttest",
      "TI"
    );

    if (!accreditationValidation.success) {
      return {
        success: false,
        errors: accreditationValidation.errors,
      };
    }

    const { vcJwt, subaccountDid, reservedAttributeId } =
      accreditationValidation.data;

    // Register credential into the TIR
    const credentialRegistration = await this.registerCredential(
      subaccountDid,
      reservedAttributeId,
      vcJwt,
      "tir_invite"
    );

    if (!credentialRegistration.success) {
      return {
        success: false,
        errors: [credentialRegistration.error],
      };
    }

    return { success: true };
  }

  /**
   * A&A - TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT
   * The user asks Issuer Mock to request a VerifiableAccreditationToAccredit credential for its sub-account.
   */
  async processVAToAccreditSubaccountRequest(
    params: CheckParams
  ): Promise<CheckResult> {
    const parsedParams = accreditAuthoriseParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { did, clientId } = parsedParams.data;

    // We assume that "did" is a valid EBSI DID
    const subaccountDid = getSubAccountDid(did);

    const keyPairES256 = await this.getIssuerMockKeyPair("ES256");
    const subaccountKid = `${subaccountDid}#${keyPairES256.publicKeyJwk.kid}`;

    // Get credential with the requested types
    const requestedTypes = [
      "VerifiableCredential",
      "VerifiableAttestation",
      "VerifiableAccreditation",
      "VerifiableAccreditationToAccredit",
    ];

    const credentialRequest = await requestCredential({
      clientAuthorizationEndpoint: this.clientAuthorizationEndpoint,
      clientId: this.issuerMockUri,
      clientMetadata: {
        redirect_uris: [`${this.issuerMockUri}/code-cb`],
        jwks_uri: `${this.issuerMockUri}/jwks`,
        authorization_endpoint: this.clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair: keyPairES256,
      redirectUri: `${this.issuerMockUri}/redirect`,
      requestedTypes,
      subjectDid: subaccountDid,
      subjectKid: subaccountKid,
      logger: this.logger,
      timeout: this.timeout,
    });

    if (!credentialRequest.success) {
      return {
        success: false,
        errors: credentialRequest.errors,
      };
    }

    // Store VC (validation is done in another action)
    const { credential } = credentialRequest;
    const credentialId = getCredentialId(
      parsedParams.data,
      "VerifiableAccreditationToAccredit"
    );
    await this.cacheManager.set(
      credentialId,
      credential,
      300_000 // 5 minutes (5 * 60 * 1000)
    );

    return { success: true };
  }

  /**
   * A&A - TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT
   * The user asks Issuer Mock to validate the VerifiableAccreditationToAccredit credential issued earlier.
   * The Issuer Mock then registers the new attribute into the TIR.
   */
  async processVerifiableAccreditationToAccreditValidation(
    params: CheckParams
  ): Promise<CheckResult> {
    const parsedParams = accreditAuthoriseParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { did } = parsedParams.data;

    // Get credential from cache
    const credentialId = getCredentialId(
      parsedParams.data,
      "VerifiableAccreditationToAccredit"
    );

    const unverifiedCredentialJwt = await this.cacheManager.get(credentialId);

    if (!unverifiedCredentialJwt) {
      return {
        success: false,
        errors: ["No credential found for this client."],
      };
    }

    // Delete credential from cache
    await this.cacheManager.del(credentialId);

    // Verify credential
    const accreditationValidation = await this.validateAccreditationJwt(
      unverifiedCredentialJwt,
      did,
      "VerifiableAccreditationToAccredit",
      "TAO"
    );

    if (!accreditationValidation.success) {
      return {
        success: false,
        errors: accreditationValidation.errors,
      };
    }

    const { vcJwt, subaccountDid, reservedAttributeId } =
      accreditationValidation.data;

    // Register credential into the TIR
    const credentialRegistration = await this.registerCredential(
      subaccountDid,
      reservedAttributeId,
      vcJwt,
      "tir_write"
    );

    if (!credentialRegistration.success) {
      return {
        success: false,
        errors: [credentialRegistration.error],
      };
    }

    return { success: true };
  }

  /**
   * A&A - TAO_REVOKE_RIGHTS_SUBACCOUNT
   * The user asks Issuer Mock to validate the subaccount DID is revoked.
   */
  async processSubAccountRevocationValidation(
    params: CheckParams
  ): Promise<CheckResult> {
    const parsedParams = accreditAuthoriseParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { did } = parsedParams.data;

    const subaccountDid = getSubAccountDid(did);

    // Retrieve all attributes from subaccount DID
    let attributesResponse: AxiosResponse<PaginatedList<IdLink>>;
    try {
      attributesResponse = await axios.get(
        `${this.trustedIssuersRegistryApiUrl}/${subaccountDid}/attributes`,
        {
          timeout: this.timeout,
        }
      );
    } catch (error) {
      logAxiosRequestError(error, this.logger);
      return {
        success: false,
        errors: [
          `Unable to get attributes of ${subaccountDid}: ${getErrorMessage(
            error
          )}`,
        ],
      };
    }

    const { total, pageSize, items } = attributesResponse.data;

    if (total === 0) {
      return {
        success: false,
        errors: [`The trusted issuer ${subaccountDid} has no accreditations.`],
      };
    }

    // Current limitation: limit the number of accreditations to pageSize (10)
    if (total > pageSize) {
      return {
        success: false,
        errors: [
          `The trusted issuer ${subaccountDid} has more than ${pageSize} accreditations.`,
        ],
      };
    }

    // Limit concurrent promises to 3
    const limit = pLimit(3);

    // For each attribute, check if it is revoked
    const requests = items.map(({ href }) =>
      limit(() =>
        axios.get<IssuerAttribute>(href, {
          timeout: this.timeout,
        })
      )
    );

    try {
      const results = await Promise.all(requests);

      const errors = results
        .map((result) => {
          const { attribute } = result.data;
          if (attribute.issuerType !== "Revoked") {
            const { url } = result.config;
            return `Attribute ${url || "unknown"} is not revoked`;
          }

          return null;
        })
        .filter(Boolean);

      if (errors.length > 0) {
        return {
          success: false,
          errors,
        };
      }
    } catch (error) {
      logAxiosRequestError(error, this.logger);

      let errorMessage = getErrorMessage(error);

      // Add URL to error message
      if (axios.isAxiosError(error) && error.response?.config.url) {
        errorMessage = `${error.response?.config.url} returned ${errorMessage}`;
      }

      return {
        success: false,
        errors: [
          `Error while fetching an attribute of ${subaccountDid}: ${errorMessage}`,
        ],
      };
    }

    return { success: true };
  }

  /**
   * Issue to Holder - ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_IN_TIME
   * The user asks Issuer Mock to request a CTWalletSameInTime credential.
   */
  async processIssueToHolderCTWalletSameInTimeRequest(
    params: CheckParams
  ): Promise<CheckResult> {
    const parsedParams = issueToHolderInTimeParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { credentialIssuer, issuerState } = parsedParams.data;

    // In this test, the credential requester must use the `did:key` method.
    // We use the Issuer Mock public key JWK for the DID.
    const keyPairES256 = await this.getIssuerMockKeyPair("ES256");
    const requesterDid = util.createDid(keyPairES256.publicKeyJwk);
    const methodSpecificIdentifier = requesterDid.replace("did:key:", "");
    const requesterKid = `${requesterDid}#${methodSpecificIdentifier}`;

    // Get credential with the requested types
    const requestedTypes = [
      "VerifiableCredential",
      "VerifiableAttestation",
      "CTWalletSameInTime",
    ];

    const codeVerifier = randomBytes(50).toString("base64url");
    const codeChallenge = base64url.baseEncode(
      createHash("sha256").update(codeVerifier).digest()
    );

    const credentialRequest = await requestCredential({
      clientAuthorizationEndpoint: this.clientAuthorizationEndpoint,
      clientId: requesterDid,
      clientMetadata: {
        redirect_uris: ["openid://redirect"],
        jwks_uri: `${this.issuerMockUri}/jwks`,
        authorization_endpoint: this.clientAuthorizationEndpoint,
      },
      codeChallenge,
      codeVerifier,
      credentialIssuer,
      keyPair: keyPairES256,
      redirectUri: "openid://redirect",
      requestedTypes,
      subjectDid: requesterDid,
      subjectKid: requesterKid,
      logger: this.logger,
      timeout: this.timeout,
      issuerState,
    });

    if (!credentialRequest.success) {
      return {
        success: false,
        errors: credentialRequest.errors,
      };
    }

    // Store VC (validation is done in another action)
    const { credential } = credentialRequest;
    const credentialId = getCredentialId(
      { clientId: credentialIssuer, did: this.issuerMockDid },
      "CTWalletSameInTime"
    );

    await this.cacheManager.set(
      credentialId,
      credential,
      300_000 // 5 minutes (5 * 60 * 1000)
    );

    return { success: true };
  }

  /**
   * Issue to Holder - Validate credential
   */
  private async validateIssueToHolderCredential(
    credentialIssuer: string,
    credentialIssuerDid: string,
    type:
      | "CTWalletSameInTime"
      | "CTWalletSameDeferred"
      | "CTWalletSamePreAuthorised"
  ): Promise<CheckResult> {
    // Get credential from cache
    const credentialId = getCredentialId(
      { clientId: credentialIssuer, did: this.issuerMockDid },
      type
    );

    const unverifiedCredentialJwt = await this.cacheManager.get(credentialId);

    if (!unverifiedCredentialJwt) {
      return {
        success: false,
        errors: ["No credential found for this client."],
      };
    }

    // Delete credential from cache
    await this.cacheManager.del(credentialId);

    // Verify credential
    if (typeof unverifiedCredentialJwt !== "string") {
      return {
        success: false,
        errors: ["Credential is not a string."],
      };
    }

    let verifiedCredential: EbsiVerifiableAttestation;
    try {
      verifiedCredential = await verifyCredentialJwt(unverifiedCredentialJwt, {
        ebsiAuthority: this.ebsiAuthority,
        timeout: this.timeout,
      });
    } catch (err) {
      return {
        success: false,
        errors: [
          `Credential is not valid: ${getErrorMessage(err)}`,
          ...getErrorDetails(err, "Credential"),
        ],
      };
    }

    // Check that the credential subject is equal to the requester DID
    // In this test, the credential requester must use the `did:key` method.
    // We use the Issuer Mock public key JWK for the DID.
    const keyPairES256 = await this.getIssuerMockKeyPair("ES256");
    const requesterDid = util.createDid(keyPairES256.publicKeyJwk);

    if (verifiedCredential.credentialSubject.id !== requesterDid) {
      return {
        success: false,
        errors: [
          `Credential is not valid: credentialSubject.id is not equal to ${requesterDid}`,
        ],
      };
    }

    // Verify that the credential types contain the required type
    if (!verifiedCredential.type.includes(type)) {
      return {
        success: false,
        errors: [
          `Credential is not valid: credential types don't contain '${type}'`,
        ],
      };
    }

    // Verify that the credential has been issued by credentialIssuerDid
    if (verifiedCredential.issuer !== credentialIssuerDid) {
      return {
        success: false,
        errors: [
          `Credential is not valid: credential issuer ${verifiedCredential.issuer} is different from ${credentialIssuerDid}`,
        ],
      };
    }

    return { success: true };
  }

  /**
   * Issue to Holder - ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_IN_TIME
   * The user asks Issuer Mock to validate the CTWalletSameInTime credential issued earlier.
   */
  async processIssueToHolderCTWalletSameInTimeValidation(
    params: CheckParams
  ): Promise<CheckResult> {
    const parsedParams = issueToHolderInTimeParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { credentialIssuer, credentialIssuerDid } = parsedParams.data;

    return this.validateIssueToHolderCredential(
      credentialIssuer,
      credentialIssuerDid,
      "CTWalletSameInTime"
    );
  }

  /**
   * Issue to Holder - ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_DEFERRED
   * The user asks Issuer Mock to request a CTWalletSameInTime credential.
   */
  async processIssueToHolderCTWalletSameDeferredRequest(
    params: CheckParams
  ): Promise<CheckResult> {
    const parsedParams = issueToHolderDeferredParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { credentialIssuer, issuerState } = parsedParams.data;

    // In this test, the credential requester must use the `did:key` method.
    // We use the Issuer Mock public key JWK for the DID.
    const keyPairES256 = await this.getIssuerMockKeyPair("ES256");
    const requesterDid = util.createDid(keyPairES256.publicKeyJwk);
    const methodSpecificIdentifier = requesterDid.replace("did:key:", "");
    const requesterKid = `${requesterDid}#${methodSpecificIdentifier}`;

    // Get credential with the requested types
    const requestedTypes = [
      "VerifiableCredential",
      "VerifiableAttestation",
      "CTWalletSameDeferred",
    ];

    const codeVerifier = randomBytes(50).toString("base64url");
    const codeChallenge = base64url.baseEncode(
      createHash("sha256").update(codeVerifier).digest()
    );

    const credentialRequest = await requestCredential({
      clientAuthorizationEndpoint: this.clientAuthorizationEndpoint,
      clientId: requesterDid,
      clientMetadata: {
        redirect_uris: ["openid://redirect"],
        jwks_uri: `${this.issuerMockUri}/jwks`,
        authorization_endpoint: this.clientAuthorizationEndpoint,
      },
      codeChallenge,
      codeVerifier,
      credentialIssuer,
      keyPair: keyPairES256,
      redirectUri: "openid://redirect",
      requestedTypes,
      subjectDid: requesterDid,
      subjectKid: requesterKid,
      logger: this.logger,
      timeout: this.timeout,
      issuerState,
    });

    if (!credentialRequest.success) {
      return {
        success: false,
        errors: credentialRequest.errors,
      };
    }

    // Store VC (validation is done in another action)
    const { credential } = credentialRequest;
    const credentialId = getCredentialId(
      { clientId: credentialIssuer, did: this.issuerMockDid },
      "CTWalletSameDeferred"
    );

    await this.cacheManager.set(
      credentialId,
      credential,
      300_000 // 5 minutes (5 * 60 * 1000)
    );

    return { success: true };
  }

  /**
   * Issue to Holder - ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_DEFERRED
   * The user asks Issuer Mock to validate the CTWalletSameDeferred credential issued earlier.
   */
  async processIssueToHolderCTWalletSameDeferredValidation(
    params: CheckParams
  ): Promise<CheckResult> {
    const parsedParams = issueToHolderDeferredParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { credentialIssuer, credentialIssuerDid } = parsedParams.data;

    return this.validateIssueToHolderCredential(
      credentialIssuer,
      credentialIssuerDid,
      "CTWalletSameDeferred"
    );
  }

  /**
   * Issue to Holder - ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_PRE_AUTHORISED
   * The user asks Issuer Mock to request a CTWalletSamePreAuthorised credential.
   */
  async processIssueToHolderCTWalletSamePreAuthorisedRequest(
    params: CheckParams
  ): Promise<CheckResult> {
    const parsedParams =
      issueToHolderPreAuthorisedParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { credentialIssuer, preAuthorizedCode, userPin } = parsedParams.data;

    // In this test, the credential requester must use the `did:key` method.
    // We use the Issuer Mock public key JWK for the DID.
    const keyPairES256 = await this.getIssuerMockKeyPair("ES256");
    const requesterDid = util.createDid(keyPairES256.publicKeyJwk);
    const methodSpecificIdentifier = requesterDid.replace("did:key:", "");
    const requesterKid = `${requesterDid}#${methodSpecificIdentifier}`;

    // Get credential with the requested types
    const requestedTypes = [
      "VerifiableCredential",
      "VerifiableAttestation",
      "CTWalletSamePreAuthorised",
    ];

    const codeVerifier = randomBytes(50).toString("base64url");
    const codeChallenge = base64url.baseEncode(
      createHash("sha256").update(codeVerifier).digest()
    );

    const credentialRequest = await requestCredential({
      clientAuthorizationEndpoint: this.clientAuthorizationEndpoint,
      clientId: requesterDid,
      clientMetadata: {
        redirect_uris: ["openid://redirect"],
        jwks_uri: `${this.issuerMockUri}/jwks`,
        authorization_endpoint: this.clientAuthorizationEndpoint,
      },
      codeChallenge,
      codeVerifier,
      credentialIssuer,
      keyPair: keyPairES256,
      redirectUri: "openid://redirect",
      requestedTypes,
      subjectDid: requesterDid,
      subjectKid: requesterKid,
      logger: this.logger,
      timeout: this.timeout,
      preAuthorizedCode,
      userPin,
    });

    if (!credentialRequest.success) {
      return {
        success: false,
        errors: credentialRequest.errors,
      };
    }

    // Store VC (validation is done in another action)
    const { credential } = credentialRequest;
    const credentialId = getCredentialId(
      { clientId: credentialIssuer, did: this.issuerMockDid },
      "CTWalletSamePreAuthorised"
    );

    await this.cacheManager.set(
      credentialId,
      credential,
      300_000 // 5 minutes (5 * 60 * 1000)
    );

    return { success: true };
  }

  /**
   * Issue to Holder - ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_PRE_AUTHORISED
   * The user asks Issuer Mock to validate the CTWalletSamePreAuthorised credential issued earlier.
   */
  async processIssueToHolderCTWalletSamePreAuthorisedValidation(
    params: CheckParams
  ): Promise<CheckResult> {
    const parsedParams =
      issueToHolderPreAuthorisedParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { credentialIssuer, credentialIssuerDid } = parsedParams.data;

    return this.validateIssueToHolderCredential(
      credentialIssuer,
      credentialIssuerDid,
      "CTWalletSamePreAuthorised"
    );
  }

  /**
   * Verifier - VERIFIER_ID_TOKEN_EXCHANGE
   * An ID Token test is requested with the scope `openid ver_test:id_token`.
   */
  async processVerifierIdTokenExchange(
    params: CheckParams
  ): Promise<CheckResult> {
    const parsedParams = verifierParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { clientId } = parsedParams.data;

    const authorizationServer = clientId;

    // In this test, the credential requester must use the `did:key` method.
    // We use the Issuer Mock public key JWK for the DID.
    const keyPairES256 = await this.getIssuerMockKeyPair("ES256");
    const subjectDid = util.createDid(keyPairES256.publicKeyJwk);
    const methodSpecificIdentifier = subjectDid.replace("did:key:", "");
    const subjectKid = `${subjectDid}#${methodSpecificIdentifier}`;
    const signingKey = await importJWK(keyPairES256.privateKeyJwk);

    let response: AxiosResponse<unknown>;

    // Get AS config
    try {
      response = await axios.get(
        `${authorizationServer}/.well-known/openid-configuration`,
        { timeout: this.timeout }
      );
    } catch (err) {
      logAxiosRequestError(err, this.logger);

      return {
        success: false,
        errors: [
          `Couldn't load Authorization Server's /.well-known/openid-configuration: ${getErrorMessage(
            err
          )}`,
        ],
      };
    }

    // Validate config
    const parsedOpenidConfiguration = openidConfigurationSchema.safeParse(
      response.data
    );

    if (!parsedOpenidConfiguration.success) {
      return {
        success: false,
        errors: [
          "Authorization Server openid-configuration is invalid.",
          ...formatZodError(parsedOpenidConfiguration.error),
        ],
      };
    }

    const openidConfiguration = parsedOpenidConfiguration.data;

    // If "authorization_endpoint" is just the scheme, add "//" to make it a valid URL
    const sanitizedAuthorizationEndpoint =
      openidConfiguration.authorization_endpoint.endsWith(":")
        ? `${openidConfiguration.authorization_endpoint}//`
        : openidConfiguration.authorization_endpoint;

    // Check if "code" flow is supported
    if (
      openidConfiguration.grant_types_supported &&
      !openidConfiguration.grant_types_supported.includes("authorization_code")
    ) {
      return {
        success: false,
        errors: ["Authorization Server must support Code flow."],
      };
    }

    // Create Authorisation Request
    const clientMetadata = {
      vp_formats_supported: {
        jwt_vp: { alg: ["ES256"] },
        jwt_vc: { alg: ["ES256"] },
      },
      response_types_supported: ["vp_token", "id_token"],
      authorization_endpoint: "openid://",
    };

    const authorisationRequestParams = {
      scope: "openid ver_test:id_token",
      client_id: subjectDid,
      client_metadata: JSON.stringify(clientMetadata),
      redirect_uri: "openid://",
      response_type: "code",
      nonce: randomUUID(),
      state: randomUUID(),
    } satisfies VerifierFlowAuthorisationRequest;

    try {
      response = await axios.get(
        `${sanitizedAuthorizationEndpoint}?${new URLSearchParams(
          authorisationRequestParams
        ).toString()}`,
        {
          timeout: this.timeout,
          maxRedirects: 0, // Don't follow redirects
          validateStatus: function validateStatus(status) {
            return status === 302; // Expect a redirect
          },
        }
      );
    } catch (err) {
      logAxiosRequestError(err, this.logger);

      if (
        axios.isAxiosError(err) &&
        err.response &&
        err.response.status >= 200 &&
        err.response.status < 300
      ) {
        return {
          success: false,
          errors: ["Authorization endpoint must redirect."],
        };
      }

      return {
        success: false,
        errors: [
          `Authorization endpoint returned an error: ${getErrorMessage(err)}`,
        ],
      };
    }

    // Expect response to be ID Token request:
    // HTTP/1.1 302 Found
    // Location: openid://?<ID Token Request>

    // Parse response
    const { location: authenticationRequestUri } = response.headers;
    if (
      !authenticationRequestUri ||
      typeof authenticationRequestUri !== "string" ||
      !authenticationRequestUri.startsWith("openid://")
    ) {
      return {
        success: false,
        errors: [
          `Authorization endpoint's response doesn't redirect to the client authorization endpoint openid://. Received ${
            authenticationRequestUri as string
          }`,
        ],
      };
    }

    const parsedIdTokenRequest = idTokenRequestSchema.safeParse(
      qs.parse(new URL(authenticationRequestUri).search.slice(1))
    );

    if (!parsedIdTokenRequest.success) {
      return {
        success: false,
        errors: [
          "Authorization endpoint's response doesn't contain a valid ID Token request",
          ...formatZodError(parsedIdTokenRequest.error),
        ],
      };
    }

    const idTokenRequest = parsedIdTokenRequest.data;

    if (!idTokenRequest.request && !idTokenRequest.request_uri) {
      return {
        success: false,
        errors: [
          "Authorization endpoint's response doesn't contain request nor request_uri",
        ],
      };
    }

    let idTokenRequestJwt = idTokenRequest.request;

    if (!idTokenRequestJwt) {
      // Fetch request from request_uri
      const idTokenRequestUri = idTokenRequest.request_uri as string;
      try {
        response = await axios.get(idTokenRequestUri, {
          timeout: this.timeout,
        });
      } catch (err) {
        logAxiosRequestError(err, this.logger);

        return {
          success: false,
          errors: [`Couldn't fetch ID Token request: ${getErrorMessage(err)}`],
        };
      }

      if (!response.data) {
        return {
          success: false,
          errors: ["Invalid empty ID Token request URI response."],
        };
      }

      if (typeof response.data !== "string") {
        return {
          success: false,
          errors: [
            `Invalid ID Token request URI response. Expected: JWT. Received: ${typeof response.data}`,
          ],
        };
      }

      idTokenRequestJwt = response.data;
    }

    let unsafeIdTokenRequestJwtHeader: ProtectedHeaderParameters;
    try {
      unsafeIdTokenRequestJwtHeader = decodeProtectedHeader(idTokenRequestJwt);
    } catch (err) {
      return {
        success: false,
        errors: [
          `Couldn't decode ID Token request header: ${
            err instanceof Error ? err.message : "Unknown error"
          }`,
        ],
      };
    }

    const parsedIdTokenRequestJwtHeader =
      idTokenRequestJwtHeaderSchema.safeParse(unsafeIdTokenRequestJwtHeader);

    if (!parsedIdTokenRequestJwtHeader.success) {
      return {
        success: false,
        errors: [
          "ID Token request doesn't contain a valid header",
          ...formatZodError(parsedIdTokenRequestJwtHeader.error),
        ],
      };
    }

    const idTokenRequestJwtHeader = parsedIdTokenRequestJwtHeader.data;

    // Get Authorization Server's public key
    try {
      response = await axios.get(openidConfiguration.jwks_uri, {
        timeout: this.timeout,
      });
    } catch (err) {
      logAxiosRequestError(err, this.logger);

      return {
        success: false,
        errors: [
          `Couldn't fetch Authorization Server's jwks_uri: ${getErrorMessage(
            err
          )}`,
        ],
      };
    }

    const parsedAuthorisationServerJwks = jwksSchema.safeParse(response.data);

    if (!parsedAuthorisationServerJwks.success) {
      return {
        success: false,
        errors: [
          "Authorization Server's jwks_uri doesn't contain a valid JWKS",
          ...formatZodError(parsedAuthorisationServerJwks.error),
        ],
      };
    }

    const authorisationServerJwks = parsedAuthorisationServerJwks.data;

    // Get AS' JWK corresponding to the request.header.kid
    const authorisationServerPublicKeyJwk = authorisationServerJwks.keys.find(
      (jwk) => jwk.kid === idTokenRequestJwtHeader.kid
    );

    if (!authorisationServerPublicKeyJwk) {
      return {
        success: false,
        errors: [
          "Authorization Server's jwks_uri doesn't contain a public key matching ID Token request header's kid",
        ],
      };
    }

    // Support only ["ES256"]
    if (
      authorisationServerPublicKeyJwk.kty !== "EC" ||
      authorisationServerPublicKeyJwk.crv !== "P-256"
    ) {
      return {
        success: false,
        errors: ["Invalid request signature: only ES256 is supported"],
      };
    }

    // Verify signature
    try {
      const publicKey = await importJWK(
        authorisationServerPublicKeyJwk,
        "ES256"
      );
      await jwtVerify(idTokenRequestJwt, publicKey);
    } catch (e) {
      return {
        success: false,
        errors: [
          `Invalid ID Token request: ${
            e instanceof Error ? e.message : "Unknown error"
          }`,
        ],
      };
    }

    // Create ID Token, send to direct_post endpoint
    const idToken = await new SignJWT({
      nonce: idTokenRequest.nonce,
      sub: subjectDid,
    })
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: subjectKid,
      })
      .setIssuer(subjectDid)
      .setAudience(authorizationServer)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(signingKey);

    const idTokenResponseQueryParams = new URLSearchParams({
      id_token: idToken,
      ...(idTokenRequest.state && { state: idTokenRequest.state }),
    }).toString();

    try {
      response = await axios.post(
        idTokenRequest.redirect_uri,
        idTokenResponseQueryParams,
        {
          timeout: this.timeout,
          headers: { "content-type": "application/x-www-form-urlencoded" },
          maxRedirects: 0, // Don't follow redirects
          validateStatus: function validateStatus(status) {
            return status === 302; // Expect a redirect
          },
        }
      );

      // Expect client server to
    } catch (err) {
      logAxiosRequestError(err, this.logger);

      if (
        axios.isAxiosError(err) &&
        err.response &&
        err.response.status >= 200 &&
        err.response.status < 300
      ) {
        return {
          success: false,
          errors: [
            "Authorization Server direct_post must return a redirection.",
          ],
        };
      }

      return {
        success: false,
        errors: [
          `Authorization Server direct_post returned an error: ${getErrorMessage(
            err
          )}`,
        ],
      };
    }

    // Expect response to be:
    // HTTP/1.1 302 Found
    // Location: ${redirectUri}?code=${code}
    const { location } = response.headers;
    if (
      !location ||
      typeof location !== "string" ||
      !location.startsWith(authorisationRequestParams.redirect_uri)
    ) {
      return {
        success: false,
        errors: [
          "Authorization Server direct_post doesn't redirect to the client's redirect_uri",
        ],
      };
    }

    const code = new URL(location).searchParams.get("code");

    if (!code) {
      return {
        success: false,
        errors: [
          `Authorization Server direct_post's response doesn't contain a code. Received response ${location}`,
        ],
      };
    }

    const state = new URL(location).searchParams.get("state");

    if (state !== authorisationRequestParams.state) {
      return {
        success: false,
        errors: [
          `Authorization Server direct_post's response doesn't contain the state ${
            authorisationRequestParams.state ?? ""
          }. Received response ${location}`,
        ],
      };
    }

    return { success: true };
  }

  /**
   * Get credential status list 2021. This function is used in the
   * Verifier Flow in the test of issuing a revoked VC. Then the status
   * list contains revoked VCs. In particular, the position 7 is
   * revoked in all IDs.
   * @see https://api-conformance.ebsi.eu/docs/ct/verifier-functional-flows
   */
  async getCredentialStatusListJwt(id: string): Promise<string> {
    const issuer: EbsiIssuer = {
      did: this.issuerMockDid,
      kid: this.issuerMockKid,
      alg: "ES256",
      ...(await this.getIssuerMockKeyPair("ES256")),
    };

    const options = {
      ebsiAuthority: this.ebsiAuthority,
      skipValidation: true,
    };

    // position 7 is marked as revoked
    const list = new Uint8Array(16384);
    list[0] = 0x01; // 0000 0001 -> position 7 is revoked
    const encodedList = base64.baseEncode(gzip(list));

    // valid from 1 minute ago
    const iat = Math.round(Date.now() / 1000 - 60);
    const exp = iat + 365 * 24 * 3600;
    const issuanceDate = `${new Date(iat * 1000).toISOString().slice(0, -5)}Z`;
    const expirationDate = `${new Date(exp * 1000)
      .toISOString()
      .slice(0, -5)}Z`;
    const credentialStatusListPayload = {
      "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://w3id.org/vc/status-list/2021/v1",
      ],
      id: `${this.issuerMockUri}/credentials/status/${id}`,
      type: [
        "VerifiableCredential",
        "VerifiableAttestation",
        "StatusList2021Credential",
      ],
      issuer: issuer.did,
      issuanceDate,
      issued: issuanceDate,
      validFrom: issuanceDate,
      expirationDate,
      validUntil: expirationDate,
      credentialSubject: {
        id: `${this.issuerMockUri}/credentials/status/${id}#list`,
        type: "StatusList2021",
        statusPurpose: "revocation",
        encodedList,
      },
      credentialSchema: [
        {
          id: this.statusList2021CredentialSchemaUrl,
          type: "FullJsonSchemaValidator2021",
        },
      ],
      termsOfUse: {
        id: this.issuerMockAccreditationUrl,
        type: "IssuanceCertificate",
      },
    };

    return createVerifiableCredentialJwt(
      credentialStatusListPayload,
      issuer,
      options
    );
  }

  /**
   * Verifier
   * - VERIFIER_VP_VALID_VC
   * - VERIFIER_VP_EXPIRED_VC,
   * - VERIFIER_VP_REVOKED_VC,
   * - VERIFIER_VP_NOT_YET_VALID_VC
   * A VP Token test is requested with the scope `openid ver_test:vp_token`
   */
  async processVerifierVp(
    params: CheckParams,
    test: string
  ): Promise<CheckResult> {
    const parsedParams = verifierParamsSchema.safeParse(params);

    if (!parsedParams.success) {
      return {
        success: false,
        errors: formatZodError(parsedParams.error, "data"),
      };
    }

    const { clientId } = parsedParams.data;

    const authorizationServer = clientId;

    // In this test, the credential requester must use the `did:key` method.
    // We use the Issuer Mock public key JWK for the DID.
    const keyPairES256 = await this.getIssuerMockKeyPair("ES256");
    const subjectDid = util.createDid(keyPairES256.publicKeyJwk);
    const methodSpecificIdentifier = subjectDid.replace("did:key:", "");
    const subjectKid = `${subjectDid}#${methodSpecificIdentifier}`;

    let response: AxiosResponse<unknown>;

    // Get AS config
    try {
      response = await axios.get(
        `${authorizationServer}/.well-known/openid-configuration`,
        { timeout: this.timeout }
      );
    } catch (err) {
      logAxiosRequestError(err, this.logger);

      return {
        success: false,
        errors: [
          `Couldn't load Authorization Server's /.well-known/openid-configuration: ${getErrorMessage(
            err
          )}`,
        ],
      };
    }

    // Validate config
    const parsedOpenidConfiguration = openidConfigurationSchema.safeParse(
      response.data
    );

    if (!parsedOpenidConfiguration.success) {
      return {
        success: false,
        errors: [
          "Authorization Server openid-configuration is invalid.",
          ...formatZodError(parsedOpenidConfiguration.error),
        ],
      };
    }

    const openidConfiguration = parsedOpenidConfiguration.data;

    // If "authorization_endpoint" is just the scheme, add "//" to make it a valid URL
    const sanitizedAuthorizationEndpoint =
      openidConfiguration.authorization_endpoint.endsWith(":")
        ? `${openidConfiguration.authorization_endpoint}//`
        : openidConfiguration.authorization_endpoint;

    // Check if "code" flow is supported
    if (
      openidConfiguration.grant_types_supported &&
      !openidConfiguration.grant_types_supported.includes("authorization_code")
    ) {
      return {
        success: false,
        errors: ["Authorization Server must support Code flow."],
      };
    }

    // Create Authorisation Request
    const clientMetadata = {
      vp_formats_supported: {
        jwt_vp: { alg: ["ES256"] },
        jwt_vc: { alg: ["ES256"] },
      },
      response_types_supported: ["vp_token", "id_token"],
      authorization_endpoint: "openid://",
    };

    const authorisationRequestParams = {
      scope: "openid ver_test:vp_token",
      client_id: subjectDid,
      client_metadata: JSON.stringify(clientMetadata),
      redirect_uri: "openid://",
      response_type: "code",
      nonce: randomUUID(),
      state: randomUUID(),
    } satisfies VerifierFlowAuthorisationRequest;

    try {
      response = await axios.get(
        `${sanitizedAuthorizationEndpoint}?${new URLSearchParams(
          authorisationRequestParams
        ).toString()}`,
        {
          timeout: this.timeout,
          maxRedirects: 0, // Don't follow redirects
          validateStatus: function validateStatus(status) {
            return status === 302; // Expect a redirect
          },
        }
      );
    } catch (err) {
      logAxiosRequestError(err, this.logger);

      if (
        axios.isAxiosError(err) &&
        err.response &&
        err.response.status >= 200 &&
        err.response.status < 300
      ) {
        return {
          success: false,
          errors: ["Authorization endpoint must redirect."],
        };
      }

      return {
        success: false,
        errors: [
          `Authorization endpoint returned an error: ${getErrorMessage(err)}`,
        ],
      };
    }

    // Expect response to be VP Token request:
    // HTTP/1.1 302 Found
    // Location: openid://?<VP Token Request>

    // Parse response
    const { location: authenticationRequestUri } = response.headers;
    if (
      !authenticationRequestUri ||
      typeof authenticationRequestUri !== "string" ||
      !authenticationRequestUri.startsWith("openid://")
    ) {
      return {
        success: false,
        errors: [
          `Authorization endpoint's response doesn't redirect to the client authorization endpoint openid://. Received ${
            authenticationRequestUri as string
          }`,
        ],
      };
    }

    const parsedVpTokenRequest = vpTokenRequestSchema.safeParse(
      qs.parse(new URL(authenticationRequestUri).search.slice(1))
    );

    if (!parsedVpTokenRequest.success) {
      return {
        success: false,
        errors: [
          "Authorization endpoint's response doesn't contain a valid VP Token request",
          ...formatZodError(parsedVpTokenRequest.error),
        ],
      };
    }

    const vpTokenRequest = parsedVpTokenRequest.data;

    if (!vpTokenRequest.request && !vpTokenRequest.request_uri) {
      return {
        success: false,
        errors: [
          "Authorization endpoint's response doesn't contain request nor request_uri",
        ],
      };
    }

    let vpTokenRequestJwt = vpTokenRequest.request;

    if (!vpTokenRequestJwt) {
      // Fetch request from request_uri
      const vpTokenRequestUri = vpTokenRequest.request_uri as string;
      try {
        response = await axios.get(vpTokenRequestUri, {
          timeout: this.timeout,
        });
      } catch (err) {
        logAxiosRequestError(err, this.logger);

        return {
          success: false,
          errors: [`Couldn't fetch VP Token request: ${getErrorMessage(err)}`],
        };
      }

      if (!response.data) {
        return {
          success: false,
          errors: ["Invalid empty VP Token request URI response."],
        };
      }

      if (typeof response.data !== "string") {
        return {
          success: false,
          errors: [
            `Invalid VP Token request URI response. Expected: JWT. Received: ${typeof response.data}`,
          ],
        };
      }

      vpTokenRequestJwt = response.data;
    }

    let unsafeVpTokenRequestJwtHeader: ProtectedHeaderParameters;
    try {
      unsafeVpTokenRequestJwtHeader = decodeProtectedHeader(vpTokenRequestJwt);
    } catch (err) {
      return {
        success: false,
        errors: [
          `Couldn't decode VP Token request header: ${
            err instanceof Error ? err.message : "Unknown error"
          }`,
        ],
      };
    }

    const parsedVpTokenRequestJwtHeader =
      vpTokenRequestJwtHeaderSchema.safeParse(unsafeVpTokenRequestJwtHeader);

    if (!parsedVpTokenRequestJwtHeader.success) {
      return {
        success: false,
        errors: [
          "VP Token request doesn't contain a valid header",
          ...formatZodError(parsedVpTokenRequestJwtHeader.error),
        ],
      };
    }

    const vpTokenRequestJwtHeader = parsedVpTokenRequestJwtHeader.data;

    let unsafeVpTokenRequestJwtPayload: JWTPayload;
    try {
      unsafeVpTokenRequestJwtPayload = decodeJwt(vpTokenRequestJwt);
    } catch (err) {
      return {
        success: false,
        errors: [
          `Couldn't decode VP Token request payload: ${
            err instanceof Error ? err.message : "Unknown error"
          }`,
        ],
      };
    }

    const parsedVpTokenRequestJwtPayload =
      vpTokenRequestPayloadSchema.safeParse(unsafeVpTokenRequestJwtPayload);

    if (!parsedVpTokenRequestJwtPayload.success) {
      return {
        success: false,
        errors: [
          "Authorization endpoint's response doesn't contain a valid JWT payload in the VP Token request",
          ...formatZodError(parsedVpTokenRequestJwtPayload.error),
        ],
      };
    }

    // Get Authorization Server's public key
    try {
      response = await axios.get(openidConfiguration.jwks_uri, {
        timeout: this.timeout,
      });
    } catch (err) {
      logAxiosRequestError(err, this.logger);

      return {
        success: false,
        errors: [
          `Couldn't fetch Authorization Server's jwks_uri: ${getErrorMessage(
            err
          )}`,
        ],
      };
    }

    const parsedAuthorisationServerJwks = jwksSchema.safeParse(response.data);

    if (!parsedAuthorisationServerJwks.success) {
      return {
        success: false,
        errors: [
          "Authorization Server's jwks_uri doesn't contain a valid JWKS",
          ...formatZodError(parsedAuthorisationServerJwks.error),
        ],
      };
    }

    const authorisationServerJwks = parsedAuthorisationServerJwks.data;

    // Get AS' JWK corresponding to the request.header.kid
    const authorisationServerPublicKeyJwk = authorisationServerJwks.keys.find(
      (jwk) => jwk.kid === vpTokenRequestJwtHeader.kid
    );

    if (!authorisationServerPublicKeyJwk) {
      return {
        success: false,
        errors: [
          "Authorization Server's jwks_uri doesn't contain a public key matching ID Token request header's kid",
        ],
      };
    }

    // Support only ["ES256"]
    if (
      authorisationServerPublicKeyJwk.kty !== "EC" ||
      authorisationServerPublicKeyJwk.crv !== "P-256"
    ) {
      return {
        success: false,
        errors: ["Invalid request signature: only ES256 is supported"],
      };
    }

    // Verify signature
    try {
      const publicKey = await importJWK(
        authorisationServerPublicKeyJwk,
        "ES256"
      );
      await jwtVerify(vpTokenRequestJwt, publicKey);
    } catch (e) {
      return {
        success: false,
        errors: [
          `Invalid ID Token request: ${
            e instanceof Error ? e.message : "Unknown error"
          }`,
        ],
      };
    }

    // Issue Valid VC
    let otherPayload: Partial<EbsiVerifiableAttestation> = {};
    if (test === VERIFIER_VP_EXPIRED_VC) {
      // expired one day ago (86400 seconds)
      otherPayload = {
        expirationDate: `${new Date(Date.now() - 86400_000)
          .toISOString()
          .slice(0, -5)}Z`,
      };
    } else if (test === VERIFIER_VP_REVOKED_VC) {
      const statusListCredential = `${this.issuerMockProxyUrl}/credentials/status/1`;
      otherPayload = {
        credentialStatus: {
          id: statusListCredential,
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "7",
          statusListCredential,
        },
      };
    } else if (test === VERIFIER_VP_NOT_YET_VALID_VC) {
      // valid 1 hour in the future
      const futureDate = `${new Date(Date.now() + 3600_000)
        .toISOString()
        .slice(0, -5)}Z`;
      otherPayload = {
        validFrom: futureDate,
        issuanceDate: futureDate,
        issued: futureDate,
      };
    }

    const { vcJwt } = await issueCredential(
      this.issuerMockDid,
      this.issuerMockKid,
      keyPairES256.privateKeyJwk,
      this.issuerMockAccreditationUrl,
      this.authorisationCredentialSchema,
      {
        ebsiAuthority: this.ebsiAuthority,
        timeout: this.timeout,
        ebsiEnvConfig: {
          didRegistry: this.didRegistryApiUrl,
          trustedIssuersRegistry: this.trustedIssuersRegistryApiUrl,
          trustedPoliciesRegistry: this.trustedPoliciesRegistryApiUrl,
        },
        skipValidation: true,
      },
      otherPayload,
      {
        format: "jwt_vc",
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAuthorisationToOnboard",
        ],
        proof: {
          proof_type: "jwt",
          jwt: "",
        },
      },
      { sub: subjectDid } as AccessTokenPayload
    );

    // Create VP Token, send to direct_post endpoint
    const issuer: EbsiIssuer = {
      did: subjectDid,
      kid: subjectKid,
      privateKeyJwk: keyPairES256.privateKeyJwk,
      publicKeyJwk: keyPairES256.publicKeyJwk,
      alg: "ES256",
    };

    const payload = {
      id: `urn:did:${randomUUID()}`,
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      type: ["VerifiablePresentation"],
      holder: subjectDid,
      verifiableCredential: [vcJwt],
    } as EbsiVerifiablePresentation;

    const vpJwt = await createVerifiablePresentationJwt(
      payload,
      issuer,
      authorizationServer,
      {
        skipValidation: true,
        ebsiAuthority: this.ebsiAuthority,
        nonce: randomUUID(),
        exp: Math.floor(Date.now() / 1000) + 900,
        nbf: Math.floor(Date.now() / 1000) - 100,
      }
    );

    const presentationDefinition =
      parsedVpTokenRequestJwtPayload.data.presentation_definition;
    const inputDescriptorIds = presentationDefinition.input_descriptors.map(
      (i) => i.id
    );
    const presentationSubmission = {
      id: randomUUID(),
      definition_id: presentationDefinition.id,
      descriptor_map: [
        {
          id: inputDescriptorIds[0],
          format: "jwt_vp",
          path: "$",
          path_nested: {
            id: inputDescriptorIds[0],
            format: "jwt_vc",
            path: "$.verifiableCredential[0]",
          },
        },
        {
          id: inputDescriptorIds[1],
          format: "jwt_vp",
          path: "$",
          path_nested: {
            id: inputDescriptorIds[1],
            format: "jwt_vc",
            path: "$.verifiableCredential[1]",
          },
        },
        {
          id: inputDescriptorIds[2],
          format: "jwt_vp",
          path: "$",
          path_nested: {
            id: inputDescriptorIds[2],
            format: "jwt_vc",
            path: "$.verifiableCredential[2]",
          },
        },
      ],
    };

    const vpTokenResponseQueryParams = new URLSearchParams({
      vp_token: vpJwt,
      ...(vpTokenRequest.state && { state: vpTokenRequest.state }),
      presentation_submission: JSON.stringify(presentationSubmission),
    }).toString();

    try {
      response = await axios.post(
        vpTokenRequest.redirect_uri,
        vpTokenResponseQueryParams,
        {
          timeout: this.timeout,
          headers: { "content-type": "application/x-www-form-urlencoded" },
          maxRedirects: 0, // Don't follow redirects
          validateStatus: function validateStatus(status) {
            return status === 302; // Expect a redirect
          },
        }
      );

      // Expect client server to
    } catch (err) {
      logAxiosRequestError(err, this.logger);

      if (
        axios.isAxiosError(err) &&
        err.response &&
        err.response.status >= 200 &&
        err.response.status < 300
      ) {
        return {
          success: false,
          errors: [
            "Authorization Server direct_post must return a redirection.",
          ],
        };
      }

      return {
        success: false,
        errors: [
          `Authorization Server direct_post returned an error: ${getErrorMessage(
            err
          )}`,
        ],
      };
    }

    // Expect response to be:
    // HTTP/1.1 302 Found
    // Location: ${redirectUri}?code=${code}
    const { location } = response.headers;
    if (
      !location ||
      typeof location !== "string" ||
      !location.startsWith(authorisationRequestParams.redirect_uri)
    ) {
      return {
        success: false,
        errors: [
          "Authorization Server direct_post doesn't redirect to the client's redirect_uri",
        ],
      };
    }

    const { searchParams } = new URL(location);

    const state = searchParams.get("state");
    if (state !== authorisationRequestParams.state) {
      return {
        success: false,
        errors: [
          `Authorization Server direct_post's response doesn't contain the state ${
            authorisationRequestParams.state ?? ""
          }. Received response ${location}`,
        ],
      };
    }

    const error = searchParams.get("error");
    if (
      [
        VERIFIER_VP_EXPIRED_VC,
        VERIFIER_VP_REVOKED_VC,
        VERIFIER_VP_NOT_YET_VALID_VC,
      ].includes(test)
    ) {
      if (error !== "invalid_request") {
        return {
          success: false,
          errors: [
            `Authorization Server direct_post's response doesn't contain an 'invalid_request' error. Received response ${location}`,
          ],
        };
      }

      const errorDescription = searchParams.get("error_description");
      if (!errorDescription) {
        return {
          success: false,
          errors: [
            `Authorization Server direct_post's response doesn't contain an error_description. Received response ${location}`,
          ],
        };
      }

      if (test === VERIFIER_VP_EXPIRED_VC) {
        const validErrorDescriptions = [
          "expirationDate MUST be more recent than validFrom",
          "is expired",
        ];
        if (!validErrorDescriptions.some((v) => errorDescription.includes(v))) {
          return {
            success: false,
            errors: [
              `Authorization Server direct_post's response doesn't mention in the error_description that the VC is expired. Received response ${location}`,
            ],
          };
        }
        return { success: true };
      }

      if (test === VERIFIER_VP_REVOKED_VC) {
        const validErrorDescription = "is revoked";
        if (!errorDescription.includes(validErrorDescription)) {
          return {
            success: false,
            errors: [
              `Authorization Server direct_post's response doesn't mention in the error_description that the VC is revoked. Received response ${location}`,
            ],
          };
        }
        return { success: true };
      }

      if (test === VERIFIER_VP_NOT_YET_VALID_VC) {
        const validErrorDescriptions = [
          "is not valid before",
          "is not yet valid",
        ];
        if (!validErrorDescriptions.some((v) => errorDescription.includes(v))) {
          return {
            success: false,
            errors: [
              `Authorization Server direct_post's response doesn't mention in the error_description that the VC is not yet valid. Received response ${location}`,
            ],
          };
        }
        return { success: true };
      }
    }

    // test = VERIFIER_VP_VALID_VC
    const code = searchParams.get("code");

    if (!code) {
      return {
        success: false,
        errors: [
          `Authorization Server direct_post's response doesn't contain a code. Received response ${location}`,
        ],
      };
    }

    return { success: true };
  }

  async processVerifierVpValidVc(params: CheckParams) {
    return this.processVerifierVp(params, VERIFIER_VP_VALID_VC);
  }

  async processVerifierVpExpiredVc(params: CheckParams) {
    return this.processVerifierVp(params, VERIFIER_VP_EXPIRED_VC);
  }

  async processVerifierVpRevokedVc(params: CheckParams) {
    return this.processVerifierVp(params, VERIFIER_VP_REVOKED_VC);
  }

  async processVerifierVpNotYetValidVc(params: CheckParams) {
    return this.processVerifierVp(params, VERIFIER_VP_NOT_YET_VALID_VC);
  }
}

export default IssuerMockService;
