import { randomBytes, randomUUID } from "node:crypto";
import { URLSearchParams } from "node:url";
import {
  jest,
  describe,
  beforeAll,
  afterEach,
  afterAll,
  it,
  expect,
  beforeEach,
} from "@jest/globals";
import { Test, TestingModule } from "@nestjs/testing";
import { APP_INTERCEPTOR } from "@nestjs/core";
import { ConfigService } from "@nestjs/config";
import {
  INestApplication,
  HttpServer,
  Logger,
  CACHE_MANAGER,
} from "@nestjs/common";
import type { FastifyInstance } from "fastify";
import nock from "nock";
import request from "supertest";
import qs from "qs";
import {
  calculateJwkThumbprint,
  exportJWK,
  generateKeyPair,
  importJWK,
  SignJWT,
} from "jose";
import { util as ebsiDidHelpers } from "@cef-ebsi/ebsi-did-resolver";
import { util as keyDidHelpers } from "@cef-ebsi/key-did-resolver";
import { verifyCredentialJwt } from "@cef-ebsi/verifiable-credential";
import type { VerifyCredentialOptions } from "@cef-ebsi/verifiable-credential";
import type { Cache } from "cache-manager";
import { IssuerMockModule } from "./issuer-mock.module";
import { ApiConfig, DIDR_API_PATH } from "../../config/configuration";
import { LoggingInterceptor } from "../../interceptors/logging.interceptor";
import { EBSIAuthorisationService } from "../ebsi/authorisation.service";
import { configureApp } from "../../../tests/utils/app";
import { getKeyPair } from "../../shared/utils";
import type { KeyPair } from "../../shared/utils";
import {
  createClient,
  createDidDocument,
  createLokiLogsResponse,
  mockConformanceIssuerRecords,
  mockSchemas,
} from "../../../tests/utils/data";
import { getDeferredCredentialId } from "./issuer-mock.utils";
import {
  HOLDER_WALLET_CREDENTIAL_TYPES,
  ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_DEFERRED,
  ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_IN_TIME,
  ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_PRE_AUTHORISED,
  ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_DEFERRED,
  ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_IN_TIME,
  ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_PRE_AUTHORISED,
  PDA1_CREDENTIAL_TYPES,
  RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
  RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
  TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
  TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
  TAO_REVOKE_RIGHTS_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
  TI_REGISTER_DID,
  TI_REGISTER_VERIFIABLE_ACCREDITATION_TO_ATTEST,
  TI_REQUEST_CT_REVOCABLE,
  TI_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST,
  TI_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD,
  TI_REVOKE_CT_REVOCABLE,
  TI_VALIDATE_CT_REVOCABLE,
  VALID_CREDENTIAL_TYPES,
} from "../../shared/constants";
import type { CredentialResponse } from "../../shared/credential-issuer";

describe("Issuer Mock Module", () => {
  let app: INestApplication;
  let server: HttpServer;
  let configService: ConfigService<ApiConfig, true>;
  let apiUrlPrefix: string;
  let ebsiAuthorisationService: EBSIAuthorisationService;
  let authMockUri: string;
  let issuerMockUri: string;
  let authMockPublicKeyPair: KeyPair;
  let ebsiAuthority: string;
  let didRegistry: string;
  let trustedIssuersRegistry: string;
  let trustedPoliciesRegistry: string;
  let cacheManager: Cache;

  function getPathnameWithoutPrefix(uri: string) {
    return new URL(uri).pathname.replace(apiUrlPrefix, "");
  }

  beforeAll(async () => {
    // Disable external requests
    nock.disableNetConnect();
    // Allow localhost connections so we can test local routes and mock servers.
    nock.enableNetConnect("127.0.0.1");

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [IssuerMockModule],
      providers: [
        {
          provide: APP_INTERCEPTOR,
          useClass: LoggingInterceptor,
        },
      ],
    }).compile();

    app = await configureApp(moduleFixture);

    Logger.overrideLogger(false);

    await app.init();
    await (app.getHttpAdapter().getInstance() as FastifyInstance).ready();
    server = app.getHttpServer() as HttpServer;

    ebsiAuthorisationService = moduleFixture.get<EBSIAuthorisationService>(
      EBSIAuthorisationService
    );
    jest
      .spyOn(ebsiAuthorisationService, "getAccessToken")
      .mockImplementation(() => Promise.resolve("token"));

    configService =
      moduleFixture.get<ConfigService<ApiConfig, true>>(ConfigService);

    cacheManager = moduleFixture.get<Cache>(CACHE_MANAGER);

    const conformanceDomain = configService.get<string>("conformanceDomain");
    apiUrlPrefix = configService.get<string>("apiUrlPrefix");
    authMockUri = `${conformanceDomain}${apiUrlPrefix}/auth-mock`;
    issuerMockUri = `${conformanceDomain}${apiUrlPrefix}/issuer-mock`;
    authMockPublicKeyPair = await getKeyPair(
      configService.get<string>("authMockPrivateKey")
    );
    didRegistry = configService.get<string>("didRegistryApiUrl");
    trustedIssuersRegistry = configService.get<string>(
      "trustedIssuersRegistryApiUrl"
    );
    trustedPoliciesRegistry = configService.get<string>(
      "trustedPoliciesRegistryApiUrl"
    );
    ebsiAuthority = configService
      .get<string>("domain")
      .replace(/^https?:\/\//, ""); // remove http protocol scheme
  });

  afterEach(() => {
    nock.cleanAll();
    jest.resetAllMocks();
    jest
      .spyOn(ebsiAuthorisationService, "getAccessToken")
      .mockImplementation(() => Promise.resolve("token"));
  });

  afterAll(async () => {
    nock.restore();

    // Avoid jest open handle error
    await new Promise((r) => {
      setTimeout(r, 500);
    });
    await app.close();
  });

  describe("GET /issuer-mock/.well-known/openid-credential-issuer", () => {
    it("should return the well-known OpenID Credential Issuer config", async () => {
      expect.assertions(2);

      const response = await request(server).get(
        "/issuer-mock/.well-known/openid-credential-issuer"
      );

      expect(response.body).toStrictEqual({
        credential_issuer: issuerMockUri,
        authorization_server: authMockUri,
        credential_endpoint: `${issuerMockUri}/credential`,
        deferred_credential_endpoint: `${issuerMockUri}/credential_deferred`,
        credentials_supported: expect.arrayContaining([
          {
            format: "jwt_vc",
            types: expect.arrayContaining([
              "VerifiableCredential",
              "VerifiableAttestation",
            ]),
            trust_framework: {
              name: "ebsi",
              type: "Accreditation",
              uri: expect.any(String),
            },
            display: expect.arrayContaining([
              {
                name: expect.any(String),
                locale: expect.any(String),
              },
            ]),
          },
        ]),
      });

      expect(response.status).toBe(200);
    });
  });

  describe("POST /issuer-mock/credential", () => {
    it("should return an error if the content type is not application/json", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            types: JSON.stringify([
              "VerifiableCredential",
              "VerifiableAttestation",
              "VerifiableAuthorisationToOnboard",
            ]),
            format: "jwt_vc",
            proof: JSON.stringify({
              proof_type: "jwt",
              jwt: "",
            }),
          }).toString()
        );

      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description: "Content-type must be application/json",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the access token is missing", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post("/issuer-mock/credential")
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: "Authorization header is missing",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the authorization header is not valid", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", "invalid")
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: "Authorization header must contain a Bearer token",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the Bearer token is not a JWT", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", "Bearer invalid")
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: "Invalid Access Token header. Parsing failed.",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the Bearer token header is missing some properties", async () => {
      expect.assertions(2);

      // Create random key pair and invalid access token
      const keyPair = await generateKeyPair("ES256");
      const invalidBearerToken = await new SignJWT({})
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          // "kid" is missing
        })
        .sign(keyPair.privateKey);

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${invalidBearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: "Invalid Access Token header: 'kid': Required",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the Bearer token payload is missing some properties", async () => {
      expect.assertions(2);

      // Create random key pair and invalid access token
      const keyPair = await generateKeyPair("ES256");
      const invalidBearerToken = await new SignJWT({
        // Missing props
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: await calculateJwkThumbprint(await exportJWK(keyPair.publicKey)),
        })
        .sign(keyPair.privateKey);

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${invalidBearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: `Invalid Access Token payload: 'iss': Required
'aud': Invalid input
'sub': Required
'iat': Required
'exp': Required
'nonce': Required
'claims': Required`,
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the Bearer token payload `claims` property is empty", async () => {
      expect.assertions(2);

      // Create random key pair and invalid access token
      const keyPair = await generateKeyPair("ES256");
      const invalidBearerToken = await new SignJWT({
        nonce: randomUUID(),
        claims: {},
      })
        .setIssuer(authMockUri)
        .setAudience([issuerMockUri])
        .setSubject(ebsiDidHelpers.createDid(randomBytes(16)))
        .setIssuedAt()
        .setExpirationTime("24h")
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: await calculateJwkThumbprint(await exportJWK(keyPair.publicKey)),
        })
        .sign(keyPair.privateKey);

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${invalidBearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: `Invalid Access Token payload: 'claims.authorization_details': Required
'claims.c_nonce': Required
'claims.c_nonce_expires_in': Required
'claims.client_id': Required`,
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the Bearer token payload `claims.authorization_details` property is invalid", async () => {
      expect.assertions(2);

      // Create random key pair and invalid access token
      const keyPair = await generateKeyPair("ES256");
      const invalidBearerToken = await new SignJWT({
        nonce: randomUUID(),
        claims: {
          authorization_details: [
            {
              type: "not_openid_credential",
              format: "not_jwt_vc",
              locations: [], // empty
              types: [
                "VerifiableCredential", // ok
                // "VerifiableAttestation" // missing
                "UnknownVerifiableAuthorisationToOnboard", // unknown
              ],
            },
          ],
          c_nonce: randomUUID(),
          c_nonce_expires_in: 86400,
          client_id: "", // not a url
        },
      })
        .setIssuer(authMockUri)
        .setAudience([issuerMockUri])
        .setSubject(ebsiDidHelpers.createDid(randomBytes(16)))
        .setIssuedAt()
        .setExpirationTime("24h")
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: await calculateJwkThumbprint(await exportJWK(keyPair.publicKey)),
        })
        .sign(keyPair.privateKey);

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${invalidBearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: `Invalid Access Token payload: 'claims.authorization_details.0.type': Invalid literal value, expected "openid_credential"
'claims.authorization_details.0.format': Invalid literal value, expected "jwt_vc"
'claims.authorization_details.0.locations': Array must contain at least 1 element(s)
'claims.authorization_details.0.types.1': Invalid enum value. Expected '${VALID_CREDENTIAL_TYPES.join(
          "' | '"
        )}', received 'UnknownVerifiableAuthorisationToOnboard'
'claims.client_id': Invalid url`,
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the Bearer token aud doesn't include the Issuer Mock URI", async () => {
      expect.assertions(2);

      const client = await createClient();

      // Create random key pair and invalid access token
      const keyPair = await generateKeyPair("ES256");
      const invalidBearerToken = await new SignJWT({
        nonce: randomUUID(),
        claims: {
          authorization_details: [
            {
              type: "openid_credential",
              format: "jwt_vc",
              locations: [issuerMockUri],
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "VerifiableAuthorisationToOnboard",
              ],
            },
          ],
          c_nonce: randomUUID(),
          c_nonce_expires_in: 86400,
          client_id: client.clientId,
        },
      })
        .setIssuer(authMockUri)
        .setAudience([authMockUri]) // should be issuerMockUri
        .setSubject(ebsiDidHelpers.createDid(randomBytes(16)))
        .setIssuedAt()
        .setExpirationTime("24h")
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: await calculateJwkThumbprint(await exportJWK(keyPair.publicKey)),
        })
        .sign(keyPair.privateKey);

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${invalidBearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: `Invalid Access Token payload: the audience must include ${issuerMockUri}`,
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the Bearer token kid is different from Auth Mock's kid", async () => {
      expect.assertions(2);

      const client = await createClient();

      // Create random key pair and invalid access token
      const keyPair = await generateKeyPair("ES256");
      const invalidBearerToken = await new SignJWT({
        nonce: randomUUID(),
        claims: {
          authorization_details: [
            {
              type: "openid_credential",
              format: "jwt_vc",
              locations: [issuerMockUri],
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "VerifiableAuthorisationToOnboard",
              ],
            },
          ],
          c_nonce: randomUUID(),
          c_nonce_expires_in: 86400,
          client_id: client.clientId,
        },
      })
        .setIssuer(authMockUri)
        .setAudience([issuerMockUri])
        .setSubject(ebsiDidHelpers.createDid(randomBytes(16)))
        .setIssuedAt()
        .setExpirationTime("24h")
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: await calculateJwkThumbprint(await exportJWK(keyPair.publicKey)), // doesn't use AuthMock's public key kid
        })
        .sign(keyPair.privateKey);

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${invalidBearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description:
          "Invalid Access Token header: kid doesn't match Auth Mock's public key",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the Bearer token is not signed by Auth Mock", async () => {
      expect.assertions(2);

      const client = await createClient();

      // Create random key pair and invalid access token
      const keyPair = await generateKeyPair("ES256");
      const invalidBearerToken = await new SignJWT({
        nonce: randomUUID(),
        claims: {
          authorization_details: [
            {
              type: "openid_credential",
              format: "jwt_vc",
              locations: [issuerMockUri],
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "VerifiableAuthorisationToOnboard",
              ],
            },
          ],
          c_nonce: randomUUID(),
          c_nonce_expires_in: 86400,
          client_id: client.clientId,
        },
      })
        .setIssuer(authMockUri)
        .setAudience([issuerMockUri])
        .setSubject(ebsiDidHelpers.createDid(randomBytes(16)))
        .setIssuedAt()
        .setExpirationTime("24h")
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: authMockPublicKeyPair.publicKeyJwk.kid,
        })
        .sign(keyPair.privateKey); // should use AuthMock private key instead

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${invalidBearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description:
          "Invalid Access Token: signature verification failed",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the Bearer token is expired", async () => {
      expect.assertions(2);

      const client = await createClient();

      // Create random key pair and invalid access token
      const now = Math.floor(Date.now() / 1000);
      const bearerToken = await new SignJWT({
        nonce: randomUUID(),
        claims: {
          authorization_details: [
            {
              type: "openid_credential",
              format: "jwt_vc",
              locations: [issuerMockUri],
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "VerifiableAuthorisationToOnboard",
              ],
            },
          ],
          c_nonce: randomUUID(),
          c_nonce_expires_in: 86400,
          client_id: client.clientId,
        },
      })
        .setIssuer(authMockUri)
        .setAudience([issuerMockUri])
        .setSubject(ebsiDidHelpers.createDid(randomBytes(16)))
        .setIssuedAt()
        .setExpirationTime(now - 1000) // In the past
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: authMockPublicKeyPair.publicKeyJwk.kid,
        })
        .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${bearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: "The access token is expired",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the Bearer token is not yet valid", async () => {
      expect.assertions(2);

      const client = await createClient();

      // Create random key pair and invalid access token
      const now = Math.floor(Date.now() / 1000);
      const bearerToken = await new SignJWT({
        nonce: randomUUID(),
        claims: {
          authorization_details: [
            {
              type: "openid_credential",
              format: "jwt_vc",
              locations: [issuerMockUri],
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "VerifiableAuthorisationToOnboard",
              ],
            },
          ],
          c_nonce: randomUUID(),
          c_nonce_expires_in: 86400,
          client_id: client.clientId,
        },
      })
        .setIssuer(authMockUri)
        .setAudience([issuerMockUri])
        .setSubject(ebsiDidHelpers.createDid(randomBytes(16)))
        .setIssuedAt(now + 1000) // In the future
        .setExpirationTime("24h")
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: authMockPublicKeyPair.publicKeyJwk.kid,
        })
        .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${bearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: "The access token is not yet valid",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the request body is not valid", async () => {
      expect.assertions(2);

      const client = await createClient();

      // Create random key pair and invalid access token
      const bearerToken = await new SignJWT({
        nonce: randomUUID(),
        claims: {
          authorization_details: [
            {
              type: "openid_credential",
              format: "jwt_vc",
              locations: [issuerMockUri],
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "VerifiableAuthorisationToOnboard",
              ],
            },
          ],
          c_nonce: randomUUID(),
          c_nonce_expires_in: 86400,
          client_id: client.clientId,
        },
      })
        .setIssuer(authMockUri)
        .setAudience([issuerMockUri])
        .setSubject(ebsiDidHelpers.createDid(randomBytes(16)))
        .setIssuedAt()
        .setExpirationTime("24h")
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: authMockPublicKeyPair.publicKeyJwk.kid,
        })
        .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${bearerToken}`)
        .send({
          types: [
            // "VerifiableCredential", // missing required type
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt", // should be "jwt_vc"
          proof: {
            // proof_type: "jwt", // missing prop
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description: `Invalid request payload: 'format': Invalid literal value, expected "jwt_vc"
'types': Array must include VerifiableCredential, VerifiableAttestation
'proof.proof_type': Invalid literal value, expected "jwt"`,
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the requested types don't match the authorized types", async () => {
      expect.assertions(2);

      const client = await createClient();

      // Create random key pair and invalid access token
      const bearerToken = await new SignJWT({
        nonce: randomUUID(),
        claims: {
          authorization_details: [
            {
              type: "openid_credential",
              format: "jwt_vc",
              locations: [issuerMockUri],
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "VerifiableAuthorisationToOnboard",
              ],
            },
          ],
          c_nonce: randomUUID(),
          c_nonce_expires_in: 86400,
          client_id: client.clientId,
        },
      })
        .setIssuer(authMockUri)
        .setAudience([issuerMockUri])
        .setSubject(ebsiDidHelpers.createDid(randomBytes(16)))
        .setIssuedAt()
        .setExpirationTime("24h")
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: authMockPublicKeyPair.publicKeyJwk.kid,
        })
        .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${bearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationForTrustChain", // Requesting VerifiableAuthorisationForTrustChain instead of VerifiableAuthorisationToOnboard
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description:
          "Invalid request: the requested types don't correspond to the authorized types",
      });
      expect(response.status).toBe(401);
    });

    it("should return an error if the proof JWT is not a valid JWT", async () => {
      expect.assertions(2);

      const client = await createClient();

      // Create random key pair and invalid access token
      const bearerToken = await new SignJWT({
        nonce: randomUUID(),
        claims: {
          authorization_details: [
            {
              type: "openid_credential",
              format: "jwt_vc",
              locations: [issuerMockUri],
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "VerifiableAuthorisationToOnboard",
              ],
            },
          ],
          c_nonce: randomUUID(),
          c_nonce_expires_in: 86400,
          client_id: client.clientId,
        },
      })
        .setIssuer(authMockUri)
        .setAudience([issuerMockUri])
        .setSubject(ebsiDidHelpers.createDid(randomBytes(16)))
        .setIssuedAt()
        .setExpirationTime("24h")
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: authMockPublicKeyPair.publicKeyJwk.kid,
        })
        .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${bearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: "not a jwt",
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_or_missing_proof",
        error_description: "Invalid Proof JWT. Parsing failed.",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the proof JWT doesn't contain a nonce", async () => {
      expect.assertions(2);

      const client = await createClient();

      // Create random key pair and invalid access token
      const bearerToken = await new SignJWT({
        nonce: randomUUID(),
        claims: {
          authorization_details: [
            {
              type: "openid_credential",
              format: "jwt_vc",
              locations: [issuerMockUri],
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "VerifiableAuthorisationToOnboard",
              ],
            },
          ],
          c_nonce: randomUUID(),
          c_nonce_expires_in: 86400,
          client_id: client.clientId,
        },
      })
        .setIssuer(authMockUri)
        .setAudience([issuerMockUri])
        .setSubject(ebsiDidHelpers.createDid(randomBytes(16)))
        .setIssuedAt()
        .setExpirationTime("24h")
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: authMockPublicKeyPair.publicKeyJwk.kid,
        })
        .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

      const proofJwt = await new SignJWT({
        // nonce: randomUUID(), // no nonce
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
        })
        .setIssuer(client.clientId)
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(await importJWK(client.privateKeyJwk));

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${bearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: proofJwt,
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_or_missing_proof",
        error_description: "Invalid Proof JWT: 'nonce': Required",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the proof JWT nonce doesn't match the c_nonce", async () => {
      expect.assertions(2);

      const client = await createClient();

      // Create random key pair and invalid access token
      const bearerToken = await new SignJWT({
        nonce: randomUUID(),
        claims: {
          authorization_details: [
            {
              type: "openid_credential",
              format: "jwt_vc",
              locations: [issuerMockUri],
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "VerifiableAuthorisationToOnboard",
              ],
            },
          ],
          c_nonce: randomUUID(),
          c_nonce_expires_in: 86400,
          client_id: client.clientId,
        },
      })
        .setIssuer(authMockUri)
        .setAudience([issuerMockUri])
        .setSubject(ebsiDidHelpers.createDid(randomBytes(16)))
        .setIssuedAt()
        .setExpirationTime("24h")
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: authMockPublicKeyPair.publicKeyJwk.kid,
        })
        .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

      const proofJwt = await new SignJWT({
        nonce: randomUUID(), // doesn't match c_nonce
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
        })
        .setIssuer(client.clientId)
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(await importJWK(client.privateKeyJwk));

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${bearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: proofJwt,
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_or_missing_proof",
        error_description:
          "Invalid Proof JWT: nonce doesn't match the expected c_nonce",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the proof JWT aud doesn't match the Issuer Mock URI", async () => {
      expect.assertions(2);

      const client = await createClient();

      // Create random key pair and invalid access token
      const cNonce = randomUUID();
      const bearerToken = await new SignJWT({
        nonce: randomUUID(),
        claims: {
          authorization_details: [
            {
              type: "openid_credential",
              format: "jwt_vc",
              locations: [issuerMockUri],
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "VerifiableAuthorisationToOnboard",
              ],
            },
          ],
          c_nonce: cNonce,
          c_nonce_expires_in: 86400,
          client_id: client.clientId,
        },
      })
        .setIssuer(authMockUri)
        .setAudience([issuerMockUri])
        .setSubject(ebsiDidHelpers.createDid(randomBytes(16)))
        .setIssuedAt()
        .setExpirationTime("24h")
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: authMockPublicKeyPair.publicKeyJwk.kid,
        })
        .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

      const proofJwt = await new SignJWT({
        nonce: cNonce,
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
        })
        .setIssuer(client.clientId)
        .setAudience(authMockUri) // should be issuerMockUri
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(await importJWK(client.privateKeyJwk));

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${bearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: proofJwt,
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_or_missing_proof",
        error_description: `Invalid Proof JWT: aud doesn't match the expected audience ${issuerMockUri}`,
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the proof JWT iss doesn't match the access token client_id claim", async () => {
      expect.assertions(2);

      const client = await createClient();

      // Create random key pair and invalid access token
      const cNonce = randomUUID();
      const bearerToken = await new SignJWT({
        nonce: randomUUID(),
        claims: {
          authorization_details: [
            {
              type: "openid_credential",
              format: "jwt_vc",
              locations: [issuerMockUri],
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "VerifiableAuthorisationToOnboard",
              ],
            },
          ],
          c_nonce: cNonce,
          c_nonce_expires_in: 86400,
          client_id: client.clientId,
        },
      })
        .setIssuer(authMockUri)
        .setAudience([issuerMockUri])
        .setSubject(ebsiDidHelpers.createDid(randomBytes(16)))
        .setIssuedAt()
        .setExpirationTime("24h")
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: authMockPublicKeyPair.publicKeyJwk.kid,
        })
        .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

      const proofJwt = await new SignJWT({
        nonce: cNonce,
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
        })
        .setIssuer("https://example.net") // should be client.clientId
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(await importJWK(client.privateKeyJwk));

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${bearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: proofJwt,
          },
        });

      expect(response.body).toStrictEqual({
        error: "invalid_or_missing_proof",
        error_description:
          "Invalid Proof JWT: iss doesn't match the expected client_id",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error when a CTAAQualificationCredential is requested but not all the tests were successfully passed", async () => {
      expect.assertions(2);

      // Mock TIR and DIDR for trusted issuer
      await mockConformanceIssuerRecords(configService);

      const client = await createClient();
      const clientDid = ebsiDidHelpers.createDid(randomBytes(16));
      const clientKid = `${clientDid}#key-1`;
      const clientDidDocument = createDidDocument(
        clientDid,
        clientKid,
        client.publicKeyJwk
      );

      const lokiLogsResponse = createLokiLogsResponse(
        clientDid,
        client.clientId,
        [
          {
            intent: TI_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD,
            result: { success: true },
          },
          {
            intent: TI_REGISTER_DID,
            result: { success: true },
          },
          {
            intent: TI_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST,
            result: { success: true },
          },
          {
            intent: TI_REGISTER_VERIFIABLE_ACCREDITATION_TO_ATTEST,
            result: { success: true },
          },
          {
            intent: TI_REQUEST_CT_REVOCABLE,
            result: { success: true },
          },
          {
            intent: TI_VALIDATE_CT_REVOCABLE,
            result: { success: false, errors: ["error"] },
          },
          {
            intent: TI_REVOKE_CT_REVOCABLE,
            result: { success: true },
          },
          {
            intent: TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
            result: { success: true },
          },
          {
            intent: TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
            result: { success: true },
          },
          {
            intent: TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
            result: { success: false, errors: ["error"] },
          },
          {
            intent: TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
            result: { success: true },
          },
          {
            intent: TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
            result: { success: true },
          },
          {
            intent: TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
            result: { success: true },
          },
          {
            intent: TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
            result: { success: false, errors: ["error"] },
          },
          {
            intent:
              TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
            result: { success: true },
          },
          {
            intent: TAO_REVOKE_RIGHTS_SUBACCOUNT,
            result: { success: true },
          },
          {
            intent: RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
            result: { success: false, errors: ["error"] },
          },
          {
            intent: RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
            result: { success: false, errors: ["error"] },
          },
        ]
      );

      nock("https://loki.ebsi.xyz")
        .get("/loki/api/v1/query_range")
        .query(true)
        .reply(200, lokiLogsResponse);

      // Mock DID Registry
      nock(configService.get<string>("domain"))
        .get(`${DIDR_API_PATH}/${clientDid}`)
        .reply(200, clientDidDocument)
        .persist();

      mockSchemas();

      // Create random key pair and invalid access token
      const cNonce = randomUUID();
      const bearerToken = await new SignJWT({
        nonce: randomUUID(),
        claims: {
          authorization_details: [
            {
              type: "openid_credential",
              format: "jwt_vc",
              locations: [issuerMockUri],
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "CTAAQualificationCredential",
              ],
            },
          ],
          c_nonce: cNonce,
          c_nonce_expires_in: 86400,
          client_id: client.clientId,
        },
      })
        .setIssuer(authMockUri)
        .setAudience([issuerMockUri])
        .setSubject(clientDid)
        .setIssuedAt()
        .setExpirationTime("24h")
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: authMockPublicKeyPair.publicKeyJwk.kid,
        })
        .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

      const proofJwt = await new SignJWT({
        nonce: cNonce,
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(client.clientId)
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(await importJWK(client.privateKeyJwk));

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${bearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "CTAAQualificationCredential",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: proofJwt,
          },
        });

      const expectedFailedTests = [
        TI_VALIDATE_CT_REVOCABLE,
        TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
        TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
        RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
        RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
      ];

      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description: `The client didn't pass all the tests required to get the CTAAQualificationCredential. Failed tests: ${expectedFailedTests.join(
          ", "
        )}`,
      });
      expect(response.status).toBe(401);
    });

    it("should return an error when a CTIssueQualificationCredential is requested but not all the tests were successfully passed", async () => {
      expect.assertions(2);

      // Mock TIR and DIDR for trusted issuer
      await mockConformanceIssuerRecords(configService);

      const client = await createClient();
      const clientDid = keyDidHelpers.createDid(client.publicKeyJwk);
      const fragmentIdentifier = clientDid.replace("did:key:", "");
      const clientKid = `${clientDid}#${fragmentIdentifier}`;

      mockSchemas();

      const lokiLogsResponse = createLokiLogsResponse(
        clientDid,
        client.clientId,
        [
          {
            intent: ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_IN_TIME,
            result: { success: true },
          },
          {
            intent: ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_IN_TIME,
            result: { success: true },
          },
          {
            intent: ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_DEFERRED,
            result: { success: true },
          },
          {
            intent: ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_DEFERRED,
            result: { success: true },
          },
          {
            intent: ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_PRE_AUTHORISED,
            result: { success: true },
          },
          {
            intent: ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_PRE_AUTHORISED,
            result: { success: false, errors: ["error"] },
          },
        ]
      );

      nock("https://loki.ebsi.xyz")
        .get("/loki/api/v1/query_range")
        .query(true)
        .reply(200, lokiLogsResponse);

      // Create random key pair and invalid access token
      const cNonce = randomUUID();
      const bearerToken = await new SignJWT({
        nonce: randomUUID(),
        claims: {
          authorization_details: [
            {
              type: "openid_credential",
              format: "jwt_vc",
              locations: [issuerMockUri],
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "CTIssueQualificationCredential",
              ],
            },
          ],
          c_nonce: cNonce,
          c_nonce_expires_in: 86400,
          client_id: client.clientId,
        },
      })
        .setIssuer(authMockUri)
        .setAudience([issuerMockUri])
        .setSubject(clientDid)
        .setIssuedAt()
        .setExpirationTime("24h")
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: authMockPublicKeyPair.publicKeyJwk.kid,
        })
        .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

      const proofJwt = await new SignJWT({
        nonce: cNonce,
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(client.clientId)
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(await importJWK(client.privateKeyJwk));

      const response = await request(server)
        .post("/issuer-mock/credential")
        .set("Authorization", `Bearer ${bearerToken}`)
        .send({
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "CTIssueQualificationCredential",
          ],
          format: "jwt_vc",
          proof: {
            proof_type: "jwt",
            jwt: proofJwt,
          },
        });

      const expectedFailedTests = [
        ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_PRE_AUTHORISED,
      ];

      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description: `The client didn't pass all the tests required to get the CTIssueQualificationCredential. Failed tests: ${expectedFailedTests.join(
          ", "
        )}`,
      });
      expect(response.status).toBe(401);
    });

    describe("requesting a VerifiableAuthorisationToOnboard", () => {
      it("should get a VerifiableAuthorisationToOnboard if the request is valid", async () => {
        expect.assertions(5);

        // Mock TIR and DIDR for trusted issuer
        await mockConformanceIssuerRecords(configService);

        mockSchemas();

        const client = await createClient();
        const didToOnboard = ebsiDidHelpers.createDid(randomBytes(16));

        // Create random key pair and invalid access token
        const cNonce = randomUUID();
        const bearerToken = await new SignJWT({
          nonce: randomUUID(),
          claims: {
            authorization_details: [
              {
                type: "openid_credential",
                format: "jwt_vc",
                locations: [issuerMockUri],
                types: [
                  "VerifiableCredential",
                  "VerifiableAttestation",
                  "VerifiableAuthorisationToOnboard",
                ],
              },
            ],
            c_nonce: cNonce,
            c_nonce_expires_in: 86400,
            client_id: client.clientId,
          },
        })
          .setIssuer(authMockUri)
          .setAudience([issuerMockUri])
          .setSubject(didToOnboard)
          .setIssuedAt()
          .setExpirationTime("24h")
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: authMockPublicKeyPair.publicKeyJwk.kid,
          })
          .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

        const proofJwt = await new SignJWT({
          nonce: cNonce,
        })
          .setProtectedHeader({
            typ: "openid4vci-proof+jwt",
            alg: "ES256",
          })
          .setIssuer(client.clientId)
          .setAudience(issuerMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(await importJWK(client.privateKeyJwk));

        let response = await request(server)
          .post("/issuer-mock/credential")
          .set("Authorization", `Bearer ${bearerToken}`)
          .send({
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              "VerifiableAuthorisationToOnboard",
            ],
            format: "jwt_vc",
            proof: {
              proof_type: "jwt",
              jwt: proofJwt,
            },
          });

        expect(response.body).toStrictEqual({
          format: "jwt_vc",
          credential: expect.any(String),
        });
        expect(response.status).toBe(200);

        // Check VC
        const { credential } = response.body as CredentialResponse;
        const options: VerifyCredentialOptions = {
          ebsiAuthority,
          ebsiEnvConfig: {
            didRegistry,
            trustedIssuersRegistry,
            trustedPoliciesRegistry,
          },
        };
        const vcPayload = await verifyCredentialJwt(credential, options);

        expect(vcPayload).toStrictEqual({
          "@context": ["https://www.w3.org/2018/credentials/v1"],
          id: expect.stringContaining("vc:ebsi:conformance#"),
          type: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          issuer: configService.get<string>("issuerMockKid").split("#")[0],
          issuanceDate: expect.any(String),
          issued: expect.any(String),
          validFrom: expect.any(String),
          expirationDate: expect.any(String),
          credentialSubject: { id: didToOnboard },
          credentialSchema: {
            id: configService.get<string>("authorisationCredentialSchema"),
            type: "FullJsonSchemaValidator2021",
          },
          termsOfUse: {
            id: configService.get<string>("issuerMockAccreditationUrl"),
            type: "IssuanceCertificate",
          },
        });

        // Check replay protection
        response = await request(server)
          .post("/issuer-mock/credential")
          .set("Authorization", `Bearer ${bearerToken}`)
          .send({
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              "VerifiableAuthorisationToOnboard",
            ],
            format: "jwt_vc",
            proof: {
              proof_type: "jwt",
              jwt: proofJwt,
            },
          });

        expect(response.body).toStrictEqual({
          error: "invalid_token",
          error_description: "The access token has already been used",
        });
        expect(response.status).toBe(400);
      });
    });

    describe.each([
      "VerifiableAccreditationToAccredit",
      "VerifiableAccreditationToAttest",
      "VerifiableAuthorisationForTrustChain",
      "CTAAQualificationCredential",
      "CTIssueQualificationCredential",
    ] as const)("requesting a %s", (requestedType) => {
      let requestedTypes: (typeof VALID_CREDENTIAL_TYPES)[number][];

      beforeAll(() => {
        requestedTypes = [
          "VerifiableAuthorisationForTrustChain",
          "CTAAQualificationCredential",
          "CTIssueQualificationCredential",
        ].includes(requestedType)
          ? [requestedType]
          : ["VerifiableAccreditation", requestedType];
      });

      it("should return an error if the proof JWT header is invalid", async () => {
        expect.assertions(2);

        const client = await createClient();

        // Create random key pair and invalid access token
        const cNonce = randomUUID();
        const bearerToken = await new SignJWT({
          nonce: randomUUID(),
          claims: {
            authorization_details: [
              {
                type: "openid_credential",
                format: "jwt_vc",
                locations: [issuerMockUri],
                types: [
                  "VerifiableCredential",
                  "VerifiableAttestation",
                  ...requestedTypes,
                ],
              },
            ],
            c_nonce: cNonce,
            c_nonce_expires_in: 86400,
            client_id: client.clientId,
          },
        })
          .setIssuer(authMockUri)
          .setAudience([issuerMockUri])
          .setSubject(ebsiDidHelpers.createDid(randomBytes(16)))
          .setIssuedAt()
          .setExpirationTime("24h")
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: authMockPublicKeyPair.publicKeyJwk.kid,
          })
          .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

        const proofJwt = await new SignJWT({
          nonce: cNonce,
        })
          .setProtectedHeader({
            typ: "JWT", // should be "openid4vci-proof+jwt"
            alg: "ES256",
            kid: undefined, // should be an EBSI DID v1
          })
          .setIssuer(client.clientId)
          .setAudience(issuerMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(await importJWK(client.privateKeyJwk));

        const response = await request(server)
          .post("/issuer-mock/credential")
          .set("Authorization", `Bearer ${bearerToken}`)
          .send({
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              ...requestedTypes,
            ],
            format: "jwt_vc",
            proof: {
              proof_type: "jwt",
              jwt: proofJwt,
            },
          });

        expect(response.body).toStrictEqual({
          error: "invalid_or_missing_proof",
          error_description: `Invalid Proof JWT header: 'typ': Invalid literal value, expected "openid4vci-proof+jwt"
'kid': Required`,
        });
        expect(response.status).toBe(400);
      });

      it("should return an error if the proof JWT kid doesn't match the access token DID", async () => {
        expect.assertions(2);

        const client = await createClient();

        // Create random key pair and invalid access token
        const cNonce = randomUUID();
        const bearerToken = await new SignJWT({
          nonce: randomUUID(),
          claims: {
            authorization_details: [
              {
                type: "openid_credential",
                format: "jwt_vc",
                locations: [issuerMockUri],
                types: [
                  "VerifiableCredential",
                  "VerifiableAttestation",
                  ...requestedTypes,
                ],
              },
            ],
            c_nonce: cNonce,
            c_nonce_expires_in: 86400,
            client_id: client.clientId,
          },
        })
          .setIssuer(authMockUri)
          .setAudience([issuerMockUri])
          .setSubject(ebsiDidHelpers.createDid(randomBytes(16)))
          .setIssuedAt()
          .setExpirationTime("24h")
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: authMockPublicKeyPair.publicKeyJwk.kid,
          })
          .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

        const proofJwt = await new SignJWT({
          nonce: cNonce,
        })
          .setProtectedHeader({
            typ: "openid4vci-proof+jwt",
            alg: "ES256",
            kid: client.publicKeyJwk.kid, // doesn't correspond to the client DID
          })
          .setIssuer(client.clientId)
          .setAudience(issuerMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(await importJWK(client.privateKeyJwk));

        const response = await request(server)
          .post("/issuer-mock/credential")
          .set("Authorization", `Bearer ${bearerToken}`)
          .send({
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              ...requestedTypes,
            ],
            format: "jwt_vc",
            proof: {
              proof_type: "jwt",
              jwt: proofJwt,
            },
          });

        expect(response.body).toStrictEqual({
          error: "invalid_or_missing_proof",
          error_description:
            "Invalid Proof JWT header: kid must correspond to the DID used during the authentication",
        });
        expect(response.status).toBe(400);
      });

      it("should return an error if the proof JWT kid is not a valid EBSI DID v1", async () => {
        expect.assertions(2);

        const client = await createClient();
        const clientDid = `did:ebsi:${randomBytes(16).toString("hex")}`; // Not a valid EBSI DID v1
        const clientKid = `${clientDid}#key-1`;

        // Create random key pair and invalid access token
        const cNonce = randomUUID();
        const bearerToken = await new SignJWT({
          nonce: randomUUID(),
          claims: {
            authorization_details: [
              {
                type: "openid_credential",
                format: "jwt_vc",
                locations: [issuerMockUri],
                types: [
                  "VerifiableCredential",
                  "VerifiableAttestation",
                  ...requestedTypes,
                ],
              },
            ],
            c_nonce: cNonce,
            c_nonce_expires_in: 86400,
            client_id: client.clientId,
          },
        })
          .setIssuer(authMockUri)
          .setAudience([issuerMockUri])
          .setSubject(clientDid)
          .setIssuedAt()
          .setExpirationTime("24h")
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: authMockPublicKeyPair.publicKeyJwk.kid,
          })
          .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

        const proofJwt = await new SignJWT({
          nonce: cNonce,
        })
          .setProtectedHeader({
            typ: "openid4vci-proof+jwt",
            alg: "ES256",
            kid: clientKid,
          })
          .setIssuer(client.clientId)
          .setAudience(issuerMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(await importJWK(client.privateKeyJwk));

        const response = await request(server)
          .post("/issuer-mock/credential")
          .set("Authorization", `Bearer ${bearerToken}`)
          .send({
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              ...requestedTypes,
            ],
            format: "jwt_vc",
            proof: {
              proof_type: "jwt",
              jwt: proofJwt,
            },
          });

        expect(response.body).toStrictEqual({
          error: "invalid_or_missing_proof",
          error_description:
            "Invalid Proof JWT: kid doesn't refer to a valid DID",
        });
        expect(response.status).toBe(400);
      });

      it("should return an error if the DID document related to the Proof JWT kid can't be found", async () => {
        expect.assertions(2);

        const client = await createClient();
        const clientDid = ebsiDidHelpers.createDid(randomBytes(16));
        const clientKid = `${clientDid}#key-1`;

        // Mock DID Registry
        nock(configService.get<string>("domain"))
          .get(`${DIDR_API_PATH}/${clientDid}`)
          .reply(404, "Not found")
          .persist();

        // Create random key pair and invalid access token
        const cNonce = randomUUID();
        const bearerToken = await new SignJWT({
          nonce: randomUUID(),
          claims: {
            authorization_details: [
              {
                type: "openid_credential",
                format: "jwt_vc",
                locations: [issuerMockUri],
                types: [
                  "VerifiableCredential",
                  "VerifiableAttestation",
                  ...requestedTypes,
                ],
              },
            ],
            c_nonce: cNonce,
            c_nonce_expires_in: 86400,
            client_id: client.clientId,
          },
        })
          .setIssuer(authMockUri)
          .setAudience([issuerMockUri])
          .setSubject(clientDid)
          .setIssuedAt()
          .setExpirationTime("24h")
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: authMockPublicKeyPair.publicKeyJwk.kid,
          })
          .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

        const proofJwt = await new SignJWT({
          nonce: cNonce,
        })
          .setProtectedHeader({
            typ: "openid4vci-proof+jwt",
            alg: "ES256",
            kid: clientKid,
          })
          .setIssuer(client.clientId)
          .setAudience(issuerMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(await importJWK(client.privateKeyJwk));

        const response = await request(server)
          .post("/issuer-mock/credential")
          .set("Authorization", `Bearer ${bearerToken}`)
          .send({
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              ...requestedTypes,
            ],
            format: "jwt_vc",
            proof: {
              proof_type: "jwt",
              jwt: proofJwt,
            },
          });

        expect(response.body).toStrictEqual({
          error: "invalid_or_missing_proof",
          error_description: `Invalid Proof JWT: DID ${clientDid} not found in the DID Registry`,
        });
        expect(response.status).toBe(400);
      });

      it("should return an error if the DID document doesn't contain a verification method matching the Proof JWT kid", async () => {
        expect.assertions(2);

        const client = await createClient();
        const clientDid = ebsiDidHelpers.createDid(randomBytes(16));
        const clientKid = `${clientDid}#key-1`;
        const clientDidDocument = createDidDocument(
          clientDid,
          `${clientDid}#key-2`, // should be clientKid
          client.publicKeyJwk
        );

        // Mock DID Registry
        nock(configService.get<string>("domain"))
          .get(`${DIDR_API_PATH}/${clientDid}`)
          .reply(200, clientDidDocument)
          .persist();

        // Create random key pair and invalid access token
        const cNonce = randomUUID();
        const bearerToken = await new SignJWT({
          nonce: randomUUID(),
          claims: {
            authorization_details: [
              {
                type: "openid_credential",
                format: "jwt_vc",
                locations: [issuerMockUri],
                types: [
                  "VerifiableCredential",
                  "VerifiableAttestation",
                  ...requestedTypes,
                ],
              },
            ],
            c_nonce: cNonce,
            c_nonce_expires_in: 86400,
            client_id: client.clientId,
          },
        })
          .setIssuer(authMockUri)
          .setAudience([issuerMockUri])
          .setSubject(clientDid)
          .setIssuedAt()
          .setExpirationTime("24h")
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: authMockPublicKeyPair.publicKeyJwk.kid,
          })
          .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

        const proofJwt = await new SignJWT({
          nonce: cNonce,
        })
          .setProtectedHeader({
            typ: "openid4vci-proof+jwt",
            alg: "ES256",
            kid: clientKid,
          })
          .setIssuer(client.clientId)
          .setAudience(issuerMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(await importJWK(client.privateKeyJwk));

        const response = await request(server)
          .post("/issuer-mock/credential")
          .set("Authorization", `Bearer ${bearerToken}`)
          .send({
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              ...requestedTypes,
            ],
            format: "jwt_vc",
            proof: {
              proof_type: "jwt",
              jwt: proofJwt,
            },
          });

        expect(response.body).toStrictEqual({
          error: "invalid_or_missing_proof",
          error_description: `Invalid Proof JWT: no verification method matching ${clientKid} found in the DID document`,
        });
        expect(response.status).toBe(400);
      });

      it("should return an error if the matching verification method doesn't expose a publicKeyJwk", async () => {
        expect.assertions(2);

        const client = await createClient();
        const clientDid = ebsiDidHelpers.createDid(randomBytes(16));
        const clientKid = `${clientDid}#key-1`;
        const clientDidDocument = createDidDocument(
          clientDid,
          clientKid,
          client.publicKeyJwk
        );

        // Remove publicKeyJwk from DID document
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        delete clientDidDocument.verificationMethod[0].publicKeyJwk;

        // Mock DID Registry
        nock(configService.get<string>("domain"))
          .get(`${DIDR_API_PATH}/${clientDid}`)
          .reply(200, clientDidDocument)
          .persist();

        // Create random key pair and invalid access token
        const cNonce = randomUUID();
        const bearerToken = await new SignJWT({
          nonce: randomUUID(),
          claims: {
            authorization_details: [
              {
                type: "openid_credential",
                format: "jwt_vc",
                locations: [issuerMockUri],
                types: [
                  "VerifiableCredential",
                  "VerifiableAttestation",
                  ...requestedTypes,
                ],
              },
            ],
            c_nonce: cNonce,
            c_nonce_expires_in: 86400,
            client_id: client.clientId,
          },
        })
          .setIssuer(authMockUri)
          .setAudience([issuerMockUri])
          .setSubject(clientDid)
          .setIssuedAt()
          .setExpirationTime("24h")
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: authMockPublicKeyPair.publicKeyJwk.kid,
          })
          .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

        const proofJwt = await new SignJWT({
          nonce: cNonce,
        })
          .setProtectedHeader({
            typ: "openid4vci-proof+jwt",
            alg: "ES256",
            kid: clientKid,
          })
          .setIssuer(client.clientId)
          .setAudience(issuerMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(await importJWK(client.privateKeyJwk));

        const response = await request(server)
          .post("/issuer-mock/credential")
          .set("Authorization", `Bearer ${bearerToken}`)
          .send({
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              ...requestedTypes,
            ],
            format: "jwt_vc",
            proof: {
              proof_type: "jwt",
              jwt: proofJwt,
            },
          });

        expect(response.body).toStrictEqual({
          error: "invalid_or_missing_proof",
          error_description: `Invalid Proof JWT: the verification method matching ${clientKid} doesn't have a publicKeyJwk`,
        });
        expect(response.status).toBe(400);
      });

      it("should return an error if the proof JWT signature is invalid", async () => {
        expect.assertions(2);

        const client = await createClient();
        const clientDid = ebsiDidHelpers.createDid(randomBytes(16));
        const clientKid = `${clientDid}#key-1`;
        const clientDidDocument = createDidDocument(
          clientDid,
          clientKid,
          client.publicKeyJwk
        );

        // Mock DID Registry
        nock(configService.get<string>("domain"))
          .get(`${DIDR_API_PATH}/${clientDid}`)
          .reply(200, clientDidDocument)
          .persist();

        // Create random key pair and invalid access token
        const cNonce = randomUUID();
        const bearerToken = await new SignJWT({
          nonce: randomUUID(),
          claims: {
            authorization_details: [
              {
                type: "openid_credential",
                format: "jwt_vc",
                locations: [issuerMockUri],
                types: [
                  "VerifiableCredential",
                  "VerifiableAttestation",
                  ...requestedTypes,
                ],
              },
            ],
            c_nonce: cNonce,
            c_nonce_expires_in: 86400,
            client_id: client.clientId,
          },
        })
          .setIssuer(authMockUri)
          .setAudience([issuerMockUri])
          .setSubject(clientDid)
          .setIssuedAt()
          .setExpirationTime("24h")
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: authMockPublicKeyPair.publicKeyJwk.kid,
          })
          .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

        const signingKey = (await generateKeyPair("ES256")).privateKey; // Instead of client.privateKeyJwk
        const proofJwt = await new SignJWT({
          nonce: cNonce,
        })
          .setProtectedHeader({
            typ: "openid4vci-proof+jwt",
            alg: "ES256",
            kid: clientKid,
          })
          .setIssuer(client.clientId)
          .setAudience(issuerMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(signingKey);

        const response = await request(server)
          .post("/issuer-mock/credential")
          .set("Authorization", `Bearer ${bearerToken}`)
          .send({
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              ...requestedTypes,
            ],
            format: "jwt_vc",
            proof: {
              proof_type: "jwt",
              jwt: proofJwt,
            },
          });

        expect(response.body).toStrictEqual({
          error: "invalid_or_missing_proof",
          error_description:
            "Invalid Proof JWT signature: signature verification failed",
        });
        expect(response.status).toBe(400);
      });

      it(`should get a ${requestedType} if the request is valid`, async () => {
        expect.assertions(
          requestedType === "VerifiableAuthorisationForTrustChain" ? 4 : 5
        );

        // Mock TIR and DIDR for trusted issuer
        await mockConformanceIssuerRecords(configService);

        const client = await createClient();
        const clientDid = ebsiDidHelpers.createDid(randomBytes(16));
        const clientKid = `${clientDid}#key-1`;
        const clientDidDocument = createDidDocument(
          clientDid,
          clientKid,
          client.publicKeyJwk
        );

        if (requestedType === "CTAAQualificationCredential") {
          const lokiLogsResponse = createLokiLogsResponse(
            clientDid,
            client.clientId,
            [
              {
                intent: TI_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD,
                result: { success: true },
              },
              {
                intent: TI_REGISTER_DID,
                result: { success: true },
              },
              {
                intent: TI_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST,
                result: { success: true },
              },
              {
                intent: TI_REGISTER_VERIFIABLE_ACCREDITATION_TO_ATTEST,
                result: { success: true },
              },
              {
                intent: TI_REQUEST_CT_REVOCABLE,
                result: { success: true },
              },
              {
                intent: TI_VALIDATE_CT_REVOCABLE,
                result: { success: true },
              },
              {
                intent: TI_REVOKE_CT_REVOCABLE,
                result: { success: true },
              },
              {
                intent: TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
                result: { success: true },
              },
              {
                intent: TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
                result: { success: true },
              },
              {
                intent:
                  TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
                result: { success: true },
              },
              {
                intent:
                  TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
                result: { success: true },
              },
              {
                intent:
                  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
                result: { success: true },
              },
              {
                intent:
                  TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
                result: { success: true },
              },
              {
                intent:
                  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
                result: { success: true },
              },
              {
                intent:
                  TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
                result: { success: true },
              },
              {
                intent: TAO_REVOKE_RIGHTS_SUBACCOUNT,
                result: { success: true },
              },
              {
                intent: RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
                result: { success: true },
              },
              {
                intent: RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
                result: { success: true },
              },
            ]
          );

          nock("https://loki.ebsi.xyz")
            .get("/loki/api/v1/query_range")
            .query(true)
            .reply(200, lokiLogsResponse);
        } else if (requestedType === "CTIssueQualificationCredential") {
          const lokiLogsResponse = createLokiLogsResponse(
            clientDid,
            client.clientId,
            [
              {
                intent: ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_IN_TIME,
                result: { success: true },
              },
              {
                intent: ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_IN_TIME,
                result: { success: true },
              },
              {
                intent: ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_DEFERRED,
                result: { success: true },
              },
              {
                intent: ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_DEFERRED,
                result: { success: true },
              },
              {
                intent: ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_PRE_AUTHORISED,
                result: { success: true },
              },
              {
                intent: ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_PRE_AUTHORISED,
                result: { success: true },
              },
            ]
          );

          nock("https://loki.ebsi.xyz")
            .get("/loki/api/v1/query_range")
            .query(true)
            .reply(200, lokiLogsResponse);
        }

        // Mock DID Registry
        nock(configService.get<string>("domain"))
          .get(`${DIDR_API_PATH}/${clientDid}`)
          .reply(200, clientDidDocument)
          .persist();

        mockSchemas();

        // Create random key pair and invalid access token
        const cNonce = randomUUID();
        const bearerToken = await new SignJWT({
          nonce: randomUUID(),
          claims: {
            authorization_details: [
              {
                type: "openid_credential",
                format: "jwt_vc",
                locations: [issuerMockUri],
                types: [
                  "VerifiableCredential",
                  "VerifiableAttestation",
                  ...requestedTypes,
                ],
              },
            ],
            c_nonce: cNonce,
            c_nonce_expires_in: 86400,
            client_id: client.clientId,
          },
        })
          .setIssuer(authMockUri)
          .setAudience([issuerMockUri])
          .setSubject(clientDid)
          .setIssuedAt()
          .setExpirationTime("24h")
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: authMockPublicKeyPair.publicKeyJwk.kid,
          })
          .sign(await importJWK(authMockPublicKeyPair.privateKeyJwk));

        const proofJwt = await new SignJWT({
          nonce: cNonce,
        })
          .setProtectedHeader({
            typ: "openid4vci-proof+jwt",
            alg: "ES256",
            kid: clientKid,
          })
          .setIssuer(client.clientId)
          .setAudience(issuerMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(await importJWK(client.privateKeyJwk));

        let response = await request(server)
          .post("/issuer-mock/credential")
          .set("Authorization", `Bearer ${bearerToken}`)
          .send({
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              ...requestedTypes,
            ],
            format: "jwt_vc",
            proof: {
              proof_type: "jwt",
              jwt: proofJwt,
            },
          });

        expect(response.status).toBe(200);

        /* eslint-disable jest/no-conditional-expect */
        if (requestedType === "VerifiableAuthorisationForTrustChain") {
          expect(response.body).toStrictEqual({
            acceptance_token: expect.any(String),
          });
        } else {
          expect(response.body).toStrictEqual({
            format: "jwt_vc",
            credential: expect.any(String),
          });

          // Check VC
          const { credential } = response.body as CredentialResponse;
          const options: VerifyCredentialOptions = {
            ebsiAuthority,
            ebsiEnvConfig: {
              didRegistry,
              trustedIssuersRegistry,
              trustedPoliciesRegistry,
            },
          };
          const vcPayload = await verifyCredentialJwt(credential, options);

          expect(vcPayload).toStrictEqual({
            "@context": ["https://www.w3.org/2018/credentials/v1"],
            id: expect.stringContaining("vc:ebsi:conformance#"),
            type: [
              "VerifiableCredential",
              "VerifiableAttestation",
              ...requestedTypes,
            ],
            issuer: configService.get<string>("issuerMockKid").split("#")[0],
            issuanceDate: expect.any(String),
            issued: expect.any(String),
            validFrom: expect.any(String),
            expirationDate: expect.any(String),
            credentialSubject: {
              id: clientDid,
              ...(![
                "CTAAQualificationCredential",
                "CTIssueQualificationCredential",
              ].includes(requestedType) && {
                reservedAttributeId: expect.any(String),
              }),
              ...([
                "VerifiableAccreditationToAccredit",
                "VerifiableAccreditationToAttest",
              ].includes(requestedType) && {
                accreditedFor: [
                  {
                    schemaId: configService.get<string>(
                      "authorisationCredentialSchema"
                    ),
                    types: [
                      "VerifiableCredential",
                      "VerifiableAttestation",
                      "CTRevocable",
                    ],
                    limitJurisdiction:
                      "https://publications.europa.eu/resource/authority/atu/FIN",
                  },
                ],
              }),
            },
            credentialSchema: {
              id: configService.get<string>("authorisationCredentialSchema"),
              type: "FullJsonSchemaValidator2021",
            },
            termsOfUse: {
              id: configService.get<string>("issuerMockAccreditationUrl"),
              type: "IssuanceCertificate",
            },
          });
        }
        /* eslint-enable jest/no-conditional-expect */

        // Check replay protection
        response = await request(server)
          .post("/issuer-mock/credential")
          .set("Authorization", `Bearer ${bearerToken}`)
          .send({
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              ...requestedTypes,
            ],
            format: "jwt_vc",
            proof: {
              proof_type: "jwt",
              jwt: proofJwt,
            },
          });

        expect(response.body).toStrictEqual({
          error: "invalid_token",
          error_description: "The access token has already been used",
        });
        expect(response.status).toBe(400);
      });
    });
  });

  describe("POST /issuer-mock/credential_deferred", () => {
    const acceptanceToken = Buffer.from(randomBytes(32)).toString("base64url");

    it("should return an error if the access token is missing", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post("/issuer-mock/credential_deferred")
        .send();

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: "Authorization header is missing",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the authorization header is not valid", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post("/issuer-mock/credential_deferred")
        .set("Authorization", "invalid")
        .send();

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: "Authorization header must contain a Bearer token",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if no deferred credential is found for the given acceptance token", async () => {
      expect.assertions(2);

      jest.spyOn(cacheManager, "get").mockImplementation(async () => {
        return Promise.resolve();
      });

      const response = await request(server)
        .post("/issuer-mock/credential_deferred")
        .set("Authorization", `Bearer ${acceptanceToken}`)
        .send();

      expect(response.body).toStrictEqual({
        error: "invalid_token",
        error_description: "Deferred credential not found",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the deferred credential is not available yet", async () => {
      expect.assertions(2);

      const deferredCredentialId = getDeferredCredentialId(acceptanceToken);
      const keyPair = await generateKeyPair("ES256");
      const cachedJwt = await new SignJWT({ vc: { type: [] } })
        .setProtectedHeader({ typ: "JWT", alg: "ES256" })
        .sign(keyPair.privateKey);
      jest.spyOn(cacheManager, "get").mockImplementation(async (id: string) => {
        if (id === deferredCredentialId) {
          return Promise.resolve({
            vcJwt: cachedJwt,
            notBefore: Date.now() + 10000,
          });
        }
        return Promise.resolve();
      });

      const response = await request(server)
        .post("/issuer-mock/credential_deferred")
        .set("Authorization", `Bearer ${acceptanceToken}`)
        .send();

      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description: "Deferred credential not available yet",
      });
      expect(response.status).toBe(400);
    });

    it("should return the deferred credential corresponding to the given acceptance token", async () => {
      expect.assertions(2);

      const deferredCredentialId = getDeferredCredentialId(acceptanceToken);
      const keyPair = await generateKeyPair("ES256");
      const cachedJwt = await new SignJWT({ vc: { type: [] } })
        .setProtectedHeader({ typ: "JWT", alg: "ES256" })
        .sign(keyPair.privateKey);
      jest.spyOn(cacheManager, "get").mockImplementation(async (id: string) => {
        if (id === deferredCredentialId) {
          return Promise.resolve({
            vcJwt: cachedJwt,
            notBefore: Date.now() - 100,
          });
        }
        return Promise.resolve();
      });

      const response = await request(server)
        .post("/issuer-mock/credential_deferred")
        .set("Authorization", `Bearer ${acceptanceToken}`)
        .send();

      expect(response.body).toStrictEqual({
        credential: cachedJwt,
        format: "jwt_vc",
      });
      expect(response.status).toBe(200);
    });
  });

  describe("GET /issuer-mock/initiate-credential-offer and GET /issuer-mock/offers/:credentialOfferId", () => {
    let clientDid: string;

    beforeEach(async () => {
      const clientKeyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
      clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    describe("Generic tests for /issuer-mock/initiate-credential-offer", () => {
      it("should return an error if there are missing params", async () => {
        expect.assertions(2);

        const response = await request(server)
          .get("/issuer-mock/initiate-credential-offer")
          .send();

        expect(response.status).toBe(400);
        expect(response.body).toStrictEqual({
          detail: `["credential_type must be one of the following values: ${[
            ...HOLDER_WALLET_CREDENTIAL_TYPES,
            ...PDA1_CREDENTIAL_TYPES,
          ].join(
            ", "
          )}","client_id must be a valid DID using the key DID method"]`,
          status: 400,
          title: "Bad Request",
          type: "about:blank",
        });
      });

      it("should return an error if credential_offer_endpoint is not valid", async () => {
        expect.assertions(2);

        const params = {
          credential_type: HOLDER_WALLET_CREDENTIAL_TYPES[0],
          credential_offer_endpoint: "invalid",
          client_id: clientDid,
        };

        const response = await request(server)
          .get(
            `/issuer-mock/initiate-credential-offer?${new URLSearchParams(
              params
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(400);
        expect(response.body).toStrictEqual({
          detail: '["credential_offer_endpoint must be a valid endpoint"]',
          status: 400,
          title: "Bad Request",
          type: "about:blank",
        });
      });
    });

    describe("Generic tests for /issuer-mock/offers/:credentialOfferId", () => {
      it("should throw an error if credentialOfferId is not a valid UUID v4", async () => {
        expect.assertions(2);

        const response = await request(server)
          .get("/issuer-mock/offers/nonce")
          .send();

        expect(response.status).toBe(400);
        expect(response.body).toStrictEqual({
          detail: '["credentialOfferId must be a UUID"]',
          status: 400,
          title: "Bad Request",
          type: "about:blank",
        });
      });

      it("should return an error if the credentialOfferId can't be found", async () => {
        expect.assertions(2);

        const credentialOfferId = randomUUID();

        jest.spyOn(cacheManager, "get").mockImplementation(async () => {
          return Promise.resolve();
        });

        const response = await request(server)
          .get(`/issuer-mock/offers/${credentialOfferId}`)
          .send();

        expect(response.status).toBe(404);
        expect(response.body).toStrictEqual({
          detail: `No Credential Offer found with the ID ${credentialOfferId}`,
          status: 404,
          title: "Not Found",
          type: "about:blank",
        });
      });
    });

    describe.each([
      "CTWalletCrossInTime",
      "CTWalletCrossDeferred",
      "CTWalletQualificationCredential",
    ] as const)("requesting type %s", (requestedType) => {
      it("should return the location of the credential offer", async () => {
        expect.assertions(4);

        const params = {
          credential_type: requestedType,
          credential_offer_endpoint: "openid-credential-offer://",
          client_id: clientDid,
        };

        let response = await request(server)
          .get(
            `/issuer-mock/initiate-credential-offer?${new URLSearchParams(
              params
            ).toString()}`
          )
          .send();

        expect(response.text).toStrictEqual(
          expect.stringContaining(params.credential_offer_endpoint)
        );
        expect(response.status).toBe(200);

        const { search } = new URL(response.text);

        const parsedResponse = qs.parse(search.slice(1));
        expect(parsedResponse).toStrictEqual({
          credential_offer_uri: expect.any(String),
        });

        // Get credential_offer_uri
        response = await request(server)
          .get(
            getPathnameWithoutPrefix(
              parsedResponse["credential_offer_uri"] as string
            )
          )
          .send();

        expect(response.body).toStrictEqual({
          credential_issuer: issuerMockUri,
          credentials: [
            {
              format: "jwt_vc",
              trust_framework: {
                name: "ebsi",
                type: "Accreditation",
                uri: "TIR link towards accreditation",
              },
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                params.credential_type,
              ],
            },
          ],
          grants: {
            authorization_code: {
              issuer_state: expect.any(String),
            },
          },
        });
      });
    });

    describe("requesting type CTWalletCrossPreAuthorised", () => {
      it("should return the location of the credential offer", async () => {
        expect.assertions(4);

        const params = {
          credential_type: "CTWalletCrossPreAuthorised",
          credential_offer_endpoint: "openid-credential-offer://",
          client_id: clientDid,
        };

        let response = await request(server)
          .get(
            `/issuer-mock/initiate-credential-offer?${new URLSearchParams(
              params
            ).toString()}`
          )
          .send();

        expect(response.text).toStrictEqual(
          expect.stringContaining(params.credential_offer_endpoint)
        );
        expect(response.status).toBe(200);

        const { search } = new URL(response.text);

        const parsedResponse = qs.parse(search.slice(1));
        expect(parsedResponse).toStrictEqual({
          credential_offer_uri: expect.any(String),
        });

        // Get credential_offer_uri
        response = await request(server)
          .get(
            getPathnameWithoutPrefix(
              parsedResponse["credential_offer_uri"] as string
            )
          )
          .send();

        expect(response.body).toStrictEqual({
          credential_issuer: issuerMockUri,
          credentials: [
            {
              format: "jwt_vc",
              trust_framework: {
                name: "ebsi",
                type: "Accreditation",
                uri: "TIR link towards accreditation",
              },
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                params.credential_type,
              ],
            },
          ],
          grants: {
            "urn:ietf:params:oauth:grant-type:pre-authorized_code": {
              "pre-authorized_code": expect.any(String),
              user_pin_required: true,
            },
          },
        });
      });
    });

    describe.each(["CTWalletSameInTime", "CTWalletSameDeferred"] as const)(
      "requesting type %s",
      (requestedType) => {
        it("should redirect to the user's credential offer endpoint", async () => {
          expect.assertions(4);

          const params = {
            credential_type: requestedType,
            credential_offer_endpoint: "openid-credential-offer://",
            client_id: clientDid,
          };

          let response = await request(server)
            .get(
              `/issuer-mock/initiate-credential-offer?${new URLSearchParams(
                params
              ).toString()}`
            )
            .send();

          expect(response.status).toBe(302);
          const { location } = response.headers as { location: string };
          expect(location).toStrictEqual(
            expect.stringContaining(params.credential_offer_endpoint)
          );

          const { search } = new URL(location);

          const parsedResponse = qs.parse(search.slice(1));
          expect(parsedResponse).toStrictEqual({
            credential_offer_uri: expect.any(String),
          });

          // Get credential_offer_uri
          response = await request(server)
            .get(
              getPathnameWithoutPrefix(
                parsedResponse["credential_offer_uri"] as string
              )
            )
            .send();

          expect(response.body).toStrictEqual({
            credential_issuer: issuerMockUri,
            credentials: [
              {
                format: "jwt_vc",
                trust_framework: {
                  name: "ebsi",
                  type: "Accreditation",
                  uri: "TIR link towards accreditation",
                },
                types: [
                  "VerifiableCredential",
                  "VerifiableAttestation",
                  params.credential_type,
                ],
              },
            ],
            grants: {
              authorization_code: {
                issuer_state: expect.any(String),
              },
            },
          });
        });
      }
    );

    describe("requesting type CTWalletQualificationCredential with redirect=true", () => {
      it("should redirect to the user's credential offer endpoint", async () => {
        expect.assertions(4);

        const params = {
          credential_type: "CTWalletQualificationCredential",
          credential_offer_endpoint: "openid-credential-offer://",
          client_id: clientDid,
          redirect: "true",
        };

        let response = await request(server)
          .get(
            `/issuer-mock/initiate-credential-offer?${new URLSearchParams(
              params
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        const { location } = response.headers as { location: string };
        expect(location).toStrictEqual(
          expect.stringContaining(params.credential_offer_endpoint)
        );

        const { search } = new URL(location);

        const parsedResponse = qs.parse(search.slice(1));
        expect(parsedResponse).toStrictEqual({
          credential_offer_uri: expect.any(String),
        });

        // Get credential_offer_uri
        response = await request(server)
          .get(
            getPathnameWithoutPrefix(
              parsedResponse["credential_offer_uri"] as string
            )
          )
          .send();

        expect(response.body).toStrictEqual({
          credential_issuer: issuerMockUri,
          credentials: [
            {
              format: "jwt_vc",
              trust_framework: {
                name: "ebsi",
                type: "Accreditation",
                uri: "TIR link towards accreditation",
              },
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                params.credential_type,
              ],
            },
          ],
          grants: {
            authorization_code: {
              issuer_state: expect.any(String),
            },
          },
        });
      });
    });

    describe("requesting type CTWalletSamePreAuthorised", () => {
      it("should redirect to the user's credential offer endpoint", async () => {
        expect.assertions(4);

        const params = {
          credential_type: "CTWalletSamePreAuthorised",
          credential_offer_endpoint: "openid-credential-offer://",
          client_id: clientDid,
        };

        let response = await request(server)
          .get(
            `/issuer-mock/initiate-credential-offer?${new URLSearchParams(
              params
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        const { location } = response.headers as { location: string };
        expect(location).toStrictEqual(
          expect.stringContaining(params.credential_offer_endpoint)
        );

        const { search } = new URL(location);

        const parsedResponse = qs.parse(search.slice(1));
        expect(parsedResponse).toStrictEqual({
          credential_offer_uri: expect.any(String),
        });

        // Get credential_offer_uri
        response = await request(server)
          .get(
            getPathnameWithoutPrefix(
              parsedResponse["credential_offer_uri"] as string
            )
          )
          .send();

        expect(response.body).toStrictEqual({
          credential_issuer: issuerMockUri,
          credentials: [
            {
              format: "jwt_vc",
              trust_framework: {
                name: "ebsi",
                type: "Accreditation",
                uri: "TIR link towards accreditation",
              },
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                params.credential_type,
              ],
            },
          ],
          grants: {
            "urn:ietf:params:oauth:grant-type:pre-authorized_code": {
              "pre-authorized_code": expect.any(String),
              user_pin_required: true,
            },
          },
        });
      });
    });
  });
});
