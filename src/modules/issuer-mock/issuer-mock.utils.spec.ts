import { randomUUID } from "node:crypto";
import { URLSearchParams } from "node:url";
import {
  afterEach,
  beforeAll,
  describe,
  expect,
  it,
  jest,
} from "@jest/globals";
import nock from "nock";
import {
  SignJWT,
  calculateJwkThumbprint,
  exportJWK,
  generateKeyPair,
} from "jose";
import type { Logger } from "@nestjs/common";
import { getSubAccountDid, requestCredential } from "./issuer-mock.utils";
import { KeyPair, getKeyPair } from "../../shared/utils";

describe("getSubAccountDid", () => {
  it("should return a sub-account DID from the given DID", () => {
    expect(getSubAccountDid("did:ebsi:zx8c7wvn28B2gcktaLea4uV")).toBe(
      "did:ebsi:ztCiNm4648JmzBHBSMwEiiE"
    );
  });
});

describe("requestCredential", () => {
  const clientId = "https://client-mock.xyz";
  const clientAuthorizationEndpoint = "openid:";
  const redirectUri = `${clientId}/redirect`;
  const requestedTypes = [
    "VerifiableCredential",
    "VerifiableAttestation",
    "CTRevocable",
  ];
  const subjectDid = "did:example:123";
  const subjectKid = "did:example:123#key-1";
  let keyPair: KeyPair;
  const timeout = 1000;
  const logger = {
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
  } as unknown as Logger;

  beforeAll(async () => {
    keyPair = await getKeyPair(
      await exportJWK((await generateKeyPair("ES256")).privateKey)
    );
  });

  afterEach(() => {
    nock.cleanAll();
    jest.resetAllMocks();
  });

  it("should return an error if the client's /.well-known/openid-credential-issuer config is not available", async () => {
    expect.assertions(1);

    // Mock Client server: Issuer Metadata not found
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(404, "Not Found")
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        "Couldn't load client's /.well-known/openid-credential-issuer configuration: Not Found",
      ],
    });
  });

  it("should return an error if the client's /.well-known/openid-credential-issuer config is invalid", async () => {
    expect.assertions(1);

    // Mock Client server: invalid Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {})
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        "Client's openid-credential-issuer configuration is invalid.",
        "Validation error. Path: 'credential_issuer'. Reason: Required",
        "Validation error. Path: 'credential_endpoint'. Reason: Required",
        "Validation error. Path: 'credentials_supported'. Reason: Required",
      ],
    });
  });

  it("should return an error if the client's /.well-known/openid-credential-issuer config doesn't support the requested types", async () => {
    expect.assertions(1);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [],
      })
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        `Client's openid-credential-issuer configuration doesn't support ${JSON.stringify(
          requestedTypes
        )}.`,
      ],
    });
  });

  it("should return an error if the client's /.well-known/openid-configuration is not available", async () => {
    expect.assertions(1);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata not found
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(404, "Not Found")
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        "Couldn't load Authorization Server's /.well-known/openid-configuration: Not Found",
      ],
    });
  });

  it("should return an error if the client's /.well-known/openid-configuration is not valid", async () => {
    expect.assertions(1);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: invalid OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {})
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        "Authorization Server openid-configuration is invalid.",
        "Validation error. Path: 'issuer'. Reason: Required",
        "Validation error. Path: 'authorization_endpoint'. Reason: Required",
        "Validation error. Path: 'token_endpoint'. Reason: Required",
        "Validation error. Path: 'jwks_uri'. Reason: Required",
        "Validation error. Path: 'response_types_supported'. Reason: Required",
        "Validation error. Path: 'subject_types_supported'. Reason: Required",
        "Validation error. Path: 'id_token_signing_alg_values_supported'. Reason: Required",
      ],
    });
  });

  it("should return an error if the client's authorization server doesn't support the Code flow", async () => {
    expect.assertions(1);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["implicit"], // Missing authorization_code
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: ["Authorization Server must support Code flow."],
    });
  });

  it("should return an error if the authorization endpoint is not available", async () => {
    expect.assertions(1);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS: authorization_endpoint not found
    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(404, "Not Found")
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: ["Authorization endpoint returned an error: Not Found"],
    });
  });

  it("should return an error if the authorization endpoint's response is invalid (not a redirect)", async () => {
    expect.assertions(1);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' authorization_endpoint: invalid response (not a redirect)
    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(200, { code: "abcdef" })
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: ["Authorization endpoint must redirect."],
    });
  });

  it("should return an error if the authorization endpoint's response is invalid (not an ID Token request starting with openid://)", async () => {
    expect.assertions(1);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' authorization_endpoint: invalid response (not an ID Token request starting with openid://)
    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: `https://example.com?${new URLSearchParams({
          code: "abcdef",
        }).toString()}`,
      })
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        "Authorization endpoint's response doesn't redirect to the client authorization endpoint openid:. Received https://example.com?code=abcdef",
      ],
    });
  });

  it("should return an error if the authorization endpoint's response is invalid (not a valid ID Token request)", async () => {
    expect.assertions(1);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request: invalid query parameters
    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: `openid://?${new URLSearchParams({
          test: "true",
        }).toString()}`,
      })
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        "Authorization endpoint's response doesn't contain a valid ID Token request",
        "Validation error. Path: 'client_id'. Reason: Required",
        "Validation error. Path: 'redirect_uri'. Reason: Required",
        "Validation error. Path: 'response_type'. Reason: Invalid literal value, expected \"id_token\"",
        "Validation error. Path: 'response_mode'. Reason: Invalid literal value, expected \"direct_post\"",
        "Validation error. Path: 'scope'. Reason: Invalid literal value, expected \"openid\"",
      ],
    });
  });

  it("should return an error if the ID Token request doesn't contain 'request' or 'request_uri'", async () => {
    expect.assertions(1);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      // The ID Token request doesn't contain "request" or "request_uri"
    }).toString()}`;

    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        "Authorization endpoint's response doesn't contain request nor request_uri",
      ],
    });
  });

  it("should return an error if the ID Token request_uri is not available", async () => {
    expect.assertions(1);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request_uri: `${clientId}/request_uri`,
    }).toString()}`;

    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' request_uri: not available
    nock(clientId).get("/request_uri").reply(404, "Not Found").persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: ["Couldn't fetch ID Token request: Not Found"],
    });
  });

  it("should return an error if the ID Token request_uri returns nothing", async () => {
    expect.assertions(1);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request_uri: `${clientId}/request_uri`,
    }).toString()}`;

    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' request_uri: returns nothing
    nock(clientId).get("/request_uri").reply(200).persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: ["Invalid empty ID Token request URI response."],
    });
  });

  it("should return an error if the ID Token request_uri doesn't return a string", async () => {
    expect.assertions(1);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request_uri: `${clientId}/request_uri`,
    }).toString()}`;

    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' request_uri: returns an object instead of a JWT
    nock(clientId).get("/request_uri").reply(200, {}).persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        "Invalid ID Token request URI response. Expected: JWT. Received: object",
      ],
    });
  });

  it("should return an error if the ID Token request_uri doesn't return a string that is not a JWT", async () => {
    expect.assertions(1);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request_uri: `${clientId}/request_uri`,
    }).toString()}`;

    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' request_uri: returns a string that is not parsable as a JWT
    nock(clientId).get("/request_uri").reply(200, "not a jwt").persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        "Couldn't decode ID Token request header: Invalid Token or Protected Header formatting",
      ],
    });
  });

  it("should return an error if the ID Token request JWT header is invalid", async () => {
    expect.assertions(1);

    const { privateKey } = await generateKeyPair("ES256");

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        // kid is missing
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(privateKey);

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        "ID Token request doesn't contain a valid header",
        "Validation error. Path: 'kid'. Reason: Required",
      ],
    });
  });

  it("should return an error if the authorization server's jwks_uri is not available", async () => {
    expect.assertions(1);

    const { publicKey, privateKey } = await generateKeyPair("ES256");
    const publicKeyJwk = await exportJWK(publicKey);
    const publicKeyThumbprint = await calculateJwkThumbprint(publicKeyJwk);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKeyThumbprint,
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(privateKey);

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' jwks_uri: not available
    nock(clientId).get("/jwks").reply(404, "Not Found").persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: ["Couldn't fetch Authorization Server's jwks_uri: Not Found"],
    });
  });

  it("should return an error if the authorization server's jwks_uri doesn't provide a valid JWKS", async () => {
    expect.assertions(1);

    const { publicKey, privateKey } = await generateKeyPair("ES256");
    const publicKeyJwk = await exportJWK(publicKey);
    const publicKeyThumbprint = await calculateJwkThumbprint(publicKeyJwk);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKeyThumbprint,
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(privateKey);

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' jwks_uri: empty JWKS
    nock(clientId)
      .get("/jwks")
      .reply(200, {
        keys: [],
      })
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        "Authorization Server's jwks_uri doesn't contain a valid JWKS",
        "Validation error. Path: 'keys'. Reason: Array must contain at least 1 element(s)",
      ],
    });
  });

  it("should return an error if the server can't find the public key to verify the ID Token request signature", async () => {
    expect.assertions(1);

    const { publicKey, privateKey } = await generateKeyPair("ES256");
    const publicKeyJwk = await exportJWK(publicKey);
    const publicKeyThumbprint = await calculateJwkThumbprint(publicKeyJwk);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKeyThumbprint,
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(privateKey);

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' jwks_uri: incorrect kid
    nock(clientId)
      .get("/jwks")
      .reply(200, {
        keys: [
          {
            ...publicKeyJwk,
            kid: "notPublicKeyThumbprint",
          },
        ],
      })
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        "Authorization Server's jwks_uri doesn't contain a public key matching ID Token request header's kid",
      ],
    });
  });

  it("should return an error if the signing key is not an ES256 key", async () => {
    expect.assertions(1);

    const { privateKey: signingKey } = await generateKeyPair("ES256");
    const { publicKey } = await generateKeyPair("RS256");
    const publicKeyJwk = await exportJWK(publicKey);
    const publicKeyThumbprint = await calculateJwkThumbprint(publicKeyJwk);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKeyThumbprint,
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(signingKey);

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' jwks_uri
    nock(clientId)
      .get("/jwks")
      .reply(200, {
        keys: [
          {
            ...publicKeyJwk,
            kid: publicKeyThumbprint,
          },
        ],
      })
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: ["Invalid request signature: only ES256 is supported"],
    });
  });

  it("should return an error if signature can't be verified", async () => {
    expect.assertions(1);

    const { publicKey } = await generateKeyPair("ES256");
    const publicKeyJwk = await exportJWK(publicKey);
    const publicKeyThumbprint = await calculateJwkThumbprint(publicKeyJwk);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const { privateKey: randomPrivateKey } = await generateKeyPair("ES256");
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKeyThumbprint,
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(randomPrivateKey); // Sign with random private key

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' jwks_uri
    nock(clientId)
      .get("/jwks")
      .reply(200, {
        keys: [
          {
            ...publicKeyJwk,
            kid: publicKeyThumbprint,
          },
        ],
      })
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: ["Invalid ID Token request: signature verification failed"],
    });
  });

  it("should return an error if the Authorization Server direct_post endpoint is not available", async () => {
    expect.assertions(1);

    const { publicKey, privateKey } = await generateKeyPair("ES256");
    const publicKeyJwk = await exportJWK(publicKey);
    const publicKeyThumbprint = await calculateJwkThumbprint(publicKeyJwk);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKeyThumbprint,
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(privateKey);

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' jwks_uri
    nock(clientId)
      .get("/jwks")
      .reply(200, {
        keys: [
          {
            ...publicKeyJwk,
            kid: publicKeyThumbprint,
          },
        ],
      })
      .persist();

    // Mock Client AS' direct_post: not found
    nock(clientId).post("/direct_post").reply(404, "Not Found").persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: ["Authorization Server direct_post returned an error: Not Found"],
    });
  });

  it("should return an error if the Authorization Server direct_post endpoint doesn't redirect", async () => {
    expect.assertions(1);

    const { publicKey, privateKey } = await generateKeyPair("ES256");
    const publicKeyJwk = await exportJWK(publicKey);
    const publicKeyThumbprint = await calculateJwkThumbprint(publicKeyJwk);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKeyThumbprint,
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(privateKey);

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' jwks_uri
    nock(clientId)
      .get("/jwks")
      .reply(200, {
        keys: [
          {
            ...publicKeyJwk,
            kid: publicKeyThumbprint,
          },
        ],
      })
      .persist();

    // Mock Client AS' direct_post: does not redirect
    nock(clientId).post("/direct_post").reply(200, {}).persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: ["Authorization Server direct_post must return a redirection."],
    });
  });

  it("should return an error if the Authorization Server direct_post response is invalid (not using the issuer uri)", async () => {
    expect.assertions(1);

    const { publicKey, privateKey } = await generateKeyPair("ES256");
    const publicKeyJwk = await exportJWK(publicKey);
    const publicKeyThumbprint = await calculateJwkThumbprint(publicKeyJwk);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKeyThumbprint,
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(privateKey);

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' jwks_uri
    nock(clientId)
      .get("/jwks")
      .reply(200, {
        keys: [
          {
            ...publicKeyJwk,
            kid: publicKeyThumbprint,
          },
        ],
      })
      .persist();

    // Mock Client AS' direct_post: invalid response (doesn't start with clientId)
    nock(clientId)
      .post("/direct_post")
      .reply(302, undefined, {
        Location: `https://example.com?${new URLSearchParams({
          code: "abcdef",
        }).toString()}`,
      })
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        "Authorization Server direct_post doesn't redirect to the client's redirect_uri",
      ],
    });
  });

  it("should return an error if the Authorization Server direct_post response is invalid (missing code)", async () => {
    expect.assertions(1);

    const { publicKey, privateKey } = await generateKeyPair("ES256");
    const publicKeyJwk = await exportJWK(publicKey);
    const publicKeyThumbprint = await calculateJwkThumbprint(publicKeyJwk);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKeyThumbprint,
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(privateKey);

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    nock(clientId)
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' jwks_uri
    nock(clientId)
      .get("/jwks")
      .reply(200, {
        keys: [
          {
            ...publicKeyJwk,
            kid: publicKeyThumbprint,
          },
        ],
      })
      .persist();

    // Mock Client AS' direct_post: invalid response (no code)
    nock(clientId)
      .post("/direct_post")
      .reply(302, undefined, {
        Location: `${clientId}/redirect?${new URLSearchParams({
          code: "",
        }).toString()}`,
      })
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        `Authorization Server direct_post's response doesn't contain a code. Received response ${clientId}/redirect?${new URLSearchParams(
          { code: "" }
        ).toString()}`,
      ],
    });
  });

  it("should return an error if the token endpoint is not available", async () => {
    expect.assertions(1);

    const { publicKey, privateKey } = await generateKeyPair("ES256");
    const publicKeyJwk = await exportJWK(publicKey);
    const publicKeyThumbprint = await calculateJwkThumbprint(publicKeyJwk);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKeyThumbprint,
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(privateKey);

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    let clientState: string | null = "";

    nock(clientId)
      .filteringPath((path) => {
        if (!path.startsWith("/authorize")) return path;
        clientState = new URL(`${clientId}${path}`).searchParams.get("state");
        return path;
      })
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' jwks_uri
    nock(clientId)
      .get("/jwks")
      .reply(200, {
        keys: [
          {
            ...publicKeyJwk,
            kid: publicKeyThumbprint,
          },
        ],
      })
      .persist();

    // Mock Client AS' direct_post
    nock(clientId)
      .post("/direct_post")
      .reply(() => {
        return [
          302,
          undefined,
          {
            Location: `${clientId}/redirect?${new URLSearchParams({
              code: "abcdef",
              ...(clientState && { state: clientState }),
            }).toString()}`,
          },
        ];
      })
      .persist();

    // Mock Client AS: missing token_endpoint
    nock(clientId).post("/token").reply(404, "Not Found").persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: ["Token endpoint returned an error: Not Found"],
    });
  });

  it("should return an error if the token endpoint's response is invalid", async () => {
    expect.assertions(1);

    const { publicKey, privateKey } = await generateKeyPair("ES256");
    const publicKeyJwk = await exportJWK(publicKey);
    const publicKeyThumbprint = await calculateJwkThumbprint(publicKeyJwk);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKeyThumbprint,
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(privateKey);

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    let clientState: string | null = "";

    nock(clientId)
      .filteringPath((path) => {
        if (!path.startsWith("/authorize")) return path;
        clientState = new URL(`${clientId}${path}`).searchParams.get("state");
        return path;
      })
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' jwks_uri
    nock(clientId)
      .get("/jwks")
      .reply(200, {
        keys: [
          {
            ...publicKeyJwk,
            kid: publicKeyThumbprint,
          },
        ],
      })
      .persist();

    // Mock Client AS' direct_post
    nock(clientId)
      .post("/direct_post")
      .reply(() => {
        return [
          302,
          undefined,
          {
            Location: `${clientId}/redirect?${new URLSearchParams({
              code: "abcdef",
              ...(clientState && { state: clientState }),
            }).toString()}`,
          },
        ];
      })
      .persist();

    // Mock Client AS' token_endpoint: invalid response
    nock(clientId).post("/token").reply(200, {}).persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        "Authorization Server Token endpoint response is invalid.",
        "Validation error. Path: 'access_token'. Reason: Required",
        "Validation error. Path: 'token_type'. Reason: Required",
        "Validation error. Path: 'expires_in'. Reason: Required",
      ],
    });
  });

  it("should return an error if the Credential endpoint's response is not available", async () => {
    expect.assertions(1);

    const { publicKey, privateKey } = await generateKeyPair("ES256");
    const publicKeyJwk = await exportJWK(publicKey);
    const publicKeyThumbprint = await calculateJwkThumbprint(publicKeyJwk);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKeyThumbprint,
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(privateKey);

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    let clientState: string | null = "";

    nock(clientId)
      .filteringPath((path) => {
        if (!path.startsWith("/authorize")) return path;
        clientState = new URL(`${clientId}${path}`).searchParams.get("state");
        return path;
      })
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' jwks_uri
    nock(clientId)
      .get("/jwks")
      .reply(200, {
        keys: [
          {
            ...publicKeyJwk,
            kid: publicKeyThumbprint,
          },
        ],
      })
      .persist();

    // Mock Client AS' direct_post
    nock(clientId)
      .post("/direct_post")
      .reply(() => {
        return [
          302,
          undefined,
          {
            Location: `${clientId}/redirect?${new URLSearchParams({
              code: "abcdef",
              ...(clientState && { state: clientState }),
            }).toString()}`,
          },
        ];
      })
      .persist();

    // Mock Client AS' token_endpoint
    nock(clientId)
      .post("/token")
      .reply(200, {
        access_token: "token",
        token_type: "bearer",
        expires_in: 86400,
      })
      .persist();

    // Mock Client's Credential endpoint: not found
    nock(clientId).post("/credential").reply(404, "Not Found").persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: ["Credential request returned an error: Not Found"],
    });
  });

  it("should return an error if the Credential endpoint's response is invalid", async () => {
    expect.assertions(1);

    const { publicKey, privateKey } = await generateKeyPair("ES256");
    const publicKeyJwk = await exportJWK(publicKey);
    const publicKeyThumbprint = await calculateJwkThumbprint(publicKeyJwk);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKeyThumbprint,
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(privateKey);

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    let clientState: string | null = "";

    nock(clientId)
      .filteringPath((path) => {
        if (!path.startsWith("/authorize")) return path;
        clientState = new URL(`${clientId}${path}`).searchParams.get("state");
        return path;
      })
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' jwks_uri
    nock(clientId)
      .get("/jwks")
      .reply(200, {
        keys: [
          {
            ...publicKeyJwk,
            kid: publicKeyThumbprint,
          },
        ],
      })
      .persist();

    // Mock Client AS' direct_post
    nock(clientId)
      .post("/direct_post")
      .reply(() => {
        return [
          302,
          undefined,
          {
            Location: `${clientId}/redirect?${new URLSearchParams({
              code: "abcdef",
              ...(clientState && { state: clientState }),
            }).toString()}`,
          },
        ];
      })
      .persist();

    // Mock Client AS' token_endpoint
    nock(clientId)
      .post("/token")
      .reply(200, {
        access_token: "token",
        token_type: "bearer",
        expires_in: 86400,
      })
      .persist();

    // Mock Client's Credential endpoint: invalid response
    nock(clientId).post("/credential").reply(200, {}).persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        "Credential response is invalid.",
        "Validation error. Path: 'format'. Reason: Invalid literal value, expected \"jwt_vc\"",
        "Validation error. Path: 'credential'. Reason: Required",
      ],
    });
  });

  it("should return an error if the credential is not a JWT", async () => {
    expect.assertions(1);

    const { publicKey, privateKey } = await generateKeyPair("ES256");
    const publicKeyJwk = await exportJWK(publicKey);
    const publicKeyThumbprint = await calculateJwkThumbprint(publicKeyJwk);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKeyThumbprint,
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(privateKey);

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    let clientState: string | null = "";

    nock(clientId)
      .filteringPath((path) => {
        if (!path.startsWith("/authorize")) return path;
        clientState = new URL(`${clientId}${path}`).searchParams.get("state");
        return path;
      })
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' jwks_uri
    nock(clientId)
      .get("/jwks")
      .reply(200, {
        keys: [
          {
            ...publicKeyJwk,
            kid: publicKeyThumbprint,
          },
        ],
      })
      .persist();

    // Mock Client AS' direct_post
    nock(clientId)
      .post("/direct_post")
      .reply(() => {
        return [
          302,
          undefined,
          {
            Location: `${clientId}/redirect?${new URLSearchParams({
              code: "abcdef",
              ...(clientState && { state: clientState }),
            }).toString()}`,
          },
        ];
      })
      .persist();

    // Mock Client AS' token_endpoint
    nock(clientId)
      .post("/token")
      .reply(200, {
        access_token: "token",
        token_type: "bearer",
        expires_in: 86400,
      })
      .persist();

    // Mock Client's Credential endpoint
    nock(clientId)
      .post("/credential")
      .reply(200, {
        format: "jwt_vc",
        credential: "jwt",
      })
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        "Credential response is invalid.",
        "Validation error. Path: 'credential'. Reason: Not a valid JWT",
      ],
    });
  });

  it("should return { success: true } and the credential if the request is valid", async () => {
    expect.assertions(1);

    const { publicKey, privateKey } = await generateKeyPair("ES256");
    const publicKeyJwk = await exportJWK(publicKey);
    const publicKeyThumbprint = await calculateJwkThumbprint(publicKeyJwk);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKeyThumbprint,
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(privateKey);

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    let clientState: string | null = "";

    nock(clientId)
      .filteringPath((path) => {
        if (!path.startsWith("/authorize")) return path;
        clientState = new URL(`${clientId}${path}`).searchParams.get("state");
        return path;
      })
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' jwks_uri
    nock(clientId)
      .get("/jwks")
      .reply(200, {
        keys: [
          {
            ...publicKeyJwk,
            kid: publicKeyThumbprint,
          },
        ],
      })
      .persist();

    // Mock Client AS' direct_post
    nock(clientId)
      .post("/direct_post")
      .reply(() => {
        return [
          302,
          undefined,
          {
            Location: `${clientId}/redirect?${new URLSearchParams({
              code: "abcdef",
              ...(clientState && { state: clientState }),
            }).toString()}`,
          },
        ];
      })
      .persist();

    // Mock Client AS' token_endpoint
    nock(clientId)
      .post("/token")
      .reply(200, {
        access_token: "token",
        token_type: "bearer",
        expires_in: 86400,
      })
      .persist();

    // Mock Client's Credential endpoint
    const jwt =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
    nock(clientId)
      .post("/credential")
      .reply(200, {
        format: "jwt_vc",
        credential: jwt,
      })
      .persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({ success: true, credential: jwt });
  });

  it("should return an error if the state is invalid in the direct_post response", async () => {
    expect.assertions(1);

    const { publicKey, privateKey } = await generateKeyPair("ES256");
    const publicKeyJwk = await exportJWK(publicKey);
    const publicKeyThumbprint = await calculateJwkThumbprint(publicKeyJwk);

    // Mock Client server: Issuer Metadata
    nock(clientId)
      .get("/.well-known/openid-credential-issuer")
      .reply(200, {
        credential_issuer: clientId,
        credential_endpoint: `${clientId}/credential`,
        credentials_supported: [
          {
            format: "jwt_vc",
            types: requestedTypes,
          },
        ],
      })
      .persist();

    // Mock Client server: OpenID Metadata
    nock(clientId)
      .get("/.well-known/openid-configuration")
      .reply(200, {
        issuer: clientId,
        authorization_endpoint: `${clientId}/authorize`,
        token_endpoint: `${clientId}/token`,
        jwks_uri: `${clientId}/jwks`,
        response_types_supported: ["vp_token", "id_token"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
      })
      .persist();

    // Mock Client AS' ID Token request
    const idTokenRequest = {
      state: randomUUID(),
      client_id: clientId,
      redirect_uri: `${clientId}/direct_post`,
      response_type: "id_token",
      response_mode: "direct_post",
      scope: "openid",
      nonce: randomUUID(),
    };
    const requestJwt = await new SignJWT(idTokenRequest)
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKeyThumbprint,
      })
      .setIssuer(clientId)
      .setAudience(clientId)
      .sign(privateKey);

    // Redirect: ID Token Request
    const location = `openid://?${new URLSearchParams({
      ...idTokenRequest,
      request: requestJwt,
    }).toString()}`;

    let clientState: string | null = "";

    nock(clientId)
      .filteringPath((path) => {
        if (!path.startsWith("/authorize")) return path;
        clientState = new URL(`${clientId}${path}`).searchParams.get("state");
        return path;
      })
      .get("/authorize")
      .query(true)
      .reply(302, undefined, {
        Location: location,
      })
      .persist();

    // Mock Client AS' jwks_uri
    nock(clientId)
      .get("/jwks")
      .reply(200, {
        keys: [
          {
            ...publicKeyJwk,
            kid: publicKeyThumbprint,
          },
        ],
      })
      .persist();

    // Mock Client AS' direct_post
    nock(clientId)
      .post("/direct_post")
      .reply(302, undefined, {
        Location: `${clientId}/redirect?${new URLSearchParams({
          code: "abcdef",
          state: "invalid_state",
        }).toString()}`,
      })
      .persist();

    // Mock Client AS: missing token_endpoint
    nock(clientId).post("/token").reply(404, "Not Found").persist();

    const response = await requestCredential({
      clientAuthorizationEndpoint,
      clientId,
      clientMetadata: {
        redirect_uris: [`${clientId}/code-cb`],
        jwks_uri: `${clientId}/jwks`,
        authorization_endpoint: clientAuthorizationEndpoint,
      },
      credentialIssuer: clientId,
      keyPair,
      redirectUri,
      requestedTypes,
      subjectDid,
      subjectKid,
      logger,
      timeout,
    });

    expect(response).toStrictEqual({
      success: false,
      errors: [
        `Authorization Server direct_post's response doesn't contain the state ${clientState}. Received response ${clientId}/redirect?${new URLSearchParams(
          {
            code: "abcdef",
            state: "invalid_state",
          }
        ).toString()}`,
      ],
    });
  });
});
