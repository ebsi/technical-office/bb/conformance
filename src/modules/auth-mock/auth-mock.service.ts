import { Injectable, OnModuleDestroy, OnModuleInit } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import {
  BadRequestError,
  NotFoundError,
} from "@cef-ebsi/problem-details-errors";
import { Level } from "level";
import type { ApiConfig } from "../../config/configuration";
import type {
  TokenResponse,
  LevelDbKeyAuth,
  LevelDbObjectAuth,
  OPMetadata,
} from "../../shared/auth-server";
import { AuthServer, GetAuthorizeDto } from "../../shared/auth-server";
import type { JsonWebKeySet } from "../../shared/interfaces";
import { getKeyPair } from "../../shared/utils";

@Injectable()
export class AuthMockService implements OnModuleInit, OnModuleDestroy {
  private readonly authServer: AuthServer;

  private readonly db: Level<LevelDbKeyAuth, LevelDbObjectAuth>;

  private readonly privateKeyHex: string;

  constructor(configService: ConfigService<ApiConfig, true>) {
    this.privateKeyHex = configService.get<string>("authMockPrivateKey");
    const conformanceDomain = configService.get<string>("conformanceDomain");
    const domain = configService.get<string>("domain");
    const apiUrlPrefix = configService.get<string>("apiUrlPrefix");
    const didRegistryApiUrl = configService.get<string>("didRegistryApiUrl");
    const timeout = configService.get<number>("requestTimeout");
    const issuerMockPrivateKey = configService.get<string>(
      "issuerMockPrivateKey"
    );

    this.db = new Level<LevelDbKeyAuth, LevelDbObjectAuth>("db/authMock", {
      keyEncoding: "json",
      valueEncoding: "json",
    });

    this.authServer = new AuthServer({
      db: this.db,
      did: "",
      url: `${conformanceDomain}${apiUrlPrefix}/auth-mock`,
      didRegistryApiUrl,
      ebsiAuthority: domain.replace(/^https?:\/\//, ""),
      issuerMockPrivateKey,
      timeout,
    });
  }

  async onModuleInit() {
    await this.db.open();
    const { privateKeyJwk } = await getKeyPair(this.privateKeyHex);
    await this.db.put({ did: "", jwks: true }, [privateKeyJwk]);
  }

  async onModuleDestroy() {
    await this.authServer.close();
    await this.db.close();
  }

  getOPMetadata(): OPMetadata {
    return this.authServer.getOPMetadata();
  }

  /**
   * Expose Auth Mock's public keys.
   *
   * @returns Auth Mock's JWKS
   */
  async getJwks(): Promise<JsonWebKeySet> {
    return this.authServer.getPublicJwks();
  }

  /**
   * Process client's auth request.
   *
   * @returns The redirect location.
   */
  async authorize(query: GetAuthorizeDto): Promise<string> {
    return this.authServer.authorize(query);
  }

  /**
   * Get Authorization Request by ID.
   */
  async getRequestById(requestId: string): Promise<string> {
    try {
      return await this.authServer.getRequestById(requestId);
    } catch (error) {
      throw new NotFoundError(NotFoundError.defaultTitle, {
        detail: `No Authorization Request found with the ID ${requestId}`,
      });
    }
  }

  /**
   * Process /direct_post request.
   *
   * @returns The location URI based on the Authentication Request `redirect_uri`, with `code` and `state` params.
   */
  async directPost(query: unknown): Promise<string> {
    try {
      return await this.authServer.directPost(query);
    } catch (error) {
      if (error instanceof Error) {
        throw new BadRequestError(BadRequestError.defaultTitle, {
          detail: error.message,
        });
      }
      throw error;
    }
  }

  /**
   * Process /token request.
   * Access Token is delivered as a response payload from a successful Token Endpoint initiation.
   * `c_nonce` (Challenge Nonce) must be stored until a new one is given.
   *
   * @see https://www.rfc-editor.org/rfc/rfc6749#section-4.1.4
   *
   * @param body The POST /token request payload
   * @returns A token response.
   */
  async token(query: unknown): Promise<TokenResponse> {
    return this.authServer.token(query);
  }
}

export default AuthMockService;
