import { Module } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { ApiConfigModule } from "../../config/configuration";
import { AuthMockController } from "./auth-mock.controller";
import { AuthMockService } from "./auth-mock.service";

@Module({
  imports: [ApiConfigModule],
  controllers: [AuthMockController],
  providers: [ConfigService, AuthMockService],
  exports: [AuthMockService],
})
export class AuthMockModule {}

export default AuthMockModule;
