import { BadRequestError } from "@cef-ebsi/problem-details-errors";
import {
  Body,
  Controller,
  Get,
  Header,
  Headers,
  HttpCode,
  Param,
  Post,
  Query,
  Response,
} from "@nestjs/common";
import type { FastifyReply } from "fastify";
import { OAuth2TokenError } from "../../shared/errors";
import type { TokenResponse, OPMetadata } from "../../shared/auth-server";
import type { JsonWebKeySet } from "../../shared/interfaces";
import { GetAuthorizeDto, GetRequestUriDto } from "../../shared/auth-server";
import { AuthMockService } from "./auth-mock.service";

@Controller("/auth-mock")
export class AuthMockController {
  constructor(private authMockService: AuthMockService) {}

  @HttpCode(200)
  @Get("/.well-known/openid-configuration")
  getOPMetadata(): OPMetadata {
    return this.authMockService.getOPMetadata();
  }

  @HttpCode(200)
  @Get("/jwks")
  @Header("Content-type", "application/jwk-set+json")
  async getJwks(): Promise<JsonWebKeySet> {
    return this.authMockService.getJwks();
  }

  @Get("/authorize")
  async getAuthorize(
    @Query() query: GetAuthorizeDto,
    @Response({ passthrough: true }) res: FastifyReply
  ): Promise<void> {
    const location = await this.authMockService.authorize(query);

    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    res.code(302).header("Location", location).send();
  }

  @Get("/request_uri/:requestId")
  @Header("Content-type", "application/jwt")
  async getRequestById(@Param() params: GetRequestUriDto): Promise<string> {
    return this.authMockService.getRequestById(params.requestId);
  }

  @Post("/direct_post")
  async directPost(
    @Headers("content-type") contentType: string,
    @Body() body: unknown, // Validate DTO within the service method so we can properly handle the error response
    @Response({ passthrough: true }) res: FastifyReply
  ): Promise<void> {
    // Only accept application/x-www-form-urlencoded
    // https://openid.net/specs/openid-4-verifiable-presentations-1_0-15.html#name-response-mode-direct_post
    if (
      !contentType.toLowerCase().includes("application/x-www-form-urlencoded")
    ) {
      throw new BadRequestError(BadRequestError.defaultTitle, {
        detail: "Content-type must be application/x-www-form-urlencoded",
      });
    }

    const location = await this.authMockService.directPost(body);

    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    res.code(302).header("Location", location).send();
  }

  @HttpCode(200)
  @Post("/token")
  @Header("Cache-Control", "no-store")
  @Header("Pragma", "no-cache")
  async postToken(
    @Headers("content-type") contentType: string,
    @Body() body: unknown // Validate DTO within the service method so we can properly handle the error response
  ): Promise<TokenResponse> {
    // Only accept application/x-www-form-urlencoded
    // https://www.rfc-editor.org/rfc/rfc6749#section-4.1.3
    if (
      !contentType.toLowerCase().includes("application/x-www-form-urlencoded")
    ) {
      throw new OAuth2TokenError("invalid_request", {
        errorDescription:
          "Content-type must be application/x-www-form-urlencoded",
      });
    }

    return this.authMockService.token(body);
  }
}

export default AuthMockController;
