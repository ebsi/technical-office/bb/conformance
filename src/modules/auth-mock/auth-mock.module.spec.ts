import { createHash, randomBytes, randomUUID } from "node:crypto";
import { URLSearchParams } from "node:url";
import {
  jest,
  describe,
  beforeAll,
  afterEach,
  afterAll,
  it,
  expect,
  beforeEach,
} from "@jest/globals";
import { Test, TestingModule } from "@nestjs/testing";
import { APP_INTERCEPTOR } from "@nestjs/core";
import { ConfigService } from "@nestjs/config";
import { Logger } from "@nestjs/common";
import type { INestApplication, HttpServer } from "@nestjs/common";
import type { FastifyInstance } from "fastify";
import nock from "nock";
import request from "supertest";
import {
  calculateJwkThumbprint,
  decodeJwt,
  decodeProtectedHeader,
  exportJWK,
  generateKeyPair,
  importJWK,
  jwtVerify,
  SignJWT,
} from "jose";
import type { GenerateKeyPairResult, JWK, KeyLike } from "jose";
import qs from "qs";
import { util } from "@cef-ebsi/ebsi-did-resolver";
import { util as keyDidHelpers } from "@cef-ebsi/key-did-resolver";
import { createVerifiablePresentationJwt } from "@cef-ebsi/verifiable-presentation";
import type {
  EbsiIssuer,
  EbsiVerifiablePresentation,
} from "@cef-ebsi/verifiable-presentation";
import * as verifiablePresentationLib from "@cef-ebsi/verifiable-presentation";
import { createVerifiableCredentialJwt } from "@cef-ebsi/verifiable-credential";
import { base64url } from "multiformats/bases/base64";
import { Level } from "level";
import type { PresentationSubmission } from "@sphereon/pex-models";
import { AuthMockModule } from "./auth-mock.module";
import { DIDR_API_PATH, TIR_API_PATH } from "../../config/configuration";
import type { ApiConfig } from "../../config/configuration";
import { LoggingInterceptor } from "../../interceptors/logging.interceptor";
import { configureApp } from "../../../tests/utils/app";
import {
  createClient,
  createDidDocument,
  createLegalEntity,
  createQueryParams,
  createTokenRequestParams,
  mockClientServer,
} from "../../../tests/utils/data";
import {
  GetAuthorizeHolderWallerDto,
  HOLDER_WALLET_QUALIFICATION_PRESENTATION_DEFINITION,
  VA_TO_ONBOARD_PRESENTATION_DEFINITION,
} from "../../shared/auth-server";
import type {
  TokenResponse,
  AuthenticationRequest,
  CachedCodeResponse,
  CachedAuthRequest,
  LevelDbKeyAuth,
  PostTokenPkceDto,
  PostTokenPreAuthorizedCodeDto,
} from "../../shared/auth-server";
import type { AuthorizationDetails } from "../../shared/validators/authorization-details.validator";
import { getKeyPair, getUserPin } from "../../shared/utils";
import type { KeyPair } from "../../shared/utils";
import {
  HOLDER_WALLET_CREDENTIAL_TYPES,
  VALID_CREDENTIAL_TYPES,
} from "../../shared/constants";

describe("Auth Mock Module", () => {
  let app: INestApplication;
  let server: HttpServer;
  let configService: ConfigService<ApiConfig, true>;
  let authMockUri: string;
  let issuerMockUri: string;
  let issuerMockPrivateKeyHex: string;

  beforeAll(async () => {
    // Disable external requests
    nock.disableNetConnect();
    // Allow localhost connections so we can test local routes and mock servers.
    nock.enableNetConnect("127.0.0.1");

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AuthMockModule],
      providers: [
        {
          provide: APP_INTERCEPTOR,
          useClass: LoggingInterceptor,
        },
      ],
    }).compile();

    app = await configureApp(moduleFixture);

    Logger.overrideLogger(false);

    await app.init();
    await (app.getHttpAdapter().getInstance() as FastifyInstance).ready();
    server = app.getHttpServer() as HttpServer;

    configService =
      moduleFixture.get<ConfigService<ApiConfig, true>>(ConfigService);

    const conformanceDomain = configService.get<string>("conformanceDomain");
    const apiUrlPrefix = configService.get<string>("apiUrlPrefix");
    authMockUri = `${conformanceDomain}${apiUrlPrefix}/auth-mock`;
    issuerMockUri = `${conformanceDomain}${apiUrlPrefix}/issuer-mock`;
    issuerMockPrivateKeyHex = configService.get<string>("issuerMockPrivateKey");
  });

  afterEach(() => {
    nock.cleanAll();
    jest.restoreAllMocks();
  });

  afterAll(async () => {
    nock.restore();

    // Avoid jest open handle error
    await new Promise((r) => {
      setTimeout(r, 500);
    });
    await app.close();
  });

  describe("GET /auth-mock/.well-known/openid-configuration", () => {
    it("should return the well-known OpenID configuration", async () => {
      expect.assertions(2);

      const response = await request(server).get(
        "/auth-mock/.well-known/openid-configuration"
      );

      expect(response.body).toStrictEqual({
        redirect_uris: [`${authMockUri}/direct_post`],
        issuer: authMockUri,
        authorization_endpoint: `${authMockUri}/authorize`,
        token_endpoint: `${authMockUri}/token`,
        jwks_uri: `${authMockUri}/jwks`,
        scopes_supported: ["openid"],
        response_types_supported: ["vp_token", "id_token"],
        response_modes_supported: ["query"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
        request_object_signing_alg_values_supported: ["ES256"],
        request_parameter_supported: true,
        request_uri_parameter_supported: true,
        token_endpoint_auth_methods_supported: ["private_key_jwt"],
        request_authentication_methods_supported: {
          authorization_endpoint: ["request_object"],
        },
        vp_formats_supported: {
          jwt_vp: {
            alg_values_supported: ["ES256"],
          },
          jwt_vc: {
            alg_values_supported: ["ES256"],
          },
        },
        subject_syntax_types_supported: ["did:key", "did:ebsi"],
        subject_syntax_types_discriminations: [
          "did:key:jwk_jcs-pub",
          "did:ebsi:v1",
        ],
        subject_trust_frameworks_supported: ["ebsi"],
        id_token_types_supported: [
          "subject_signed_id_token",
          "attester_signed_id_token",
        ],
      });

      expect(response.status).toBe(200);
    });
  });

  describe("GET /auth-mock/jwks", () => {
    it("should return the OP's JWKS", async () => {
      expect.assertions(4);

      const response = await request(server).get("/auth-mock/jwks");

      expect(response.body).toStrictEqual({
        keys: [
          {
            kty: "EC",
            crv: "P-256",
            alg: "ES256",
            x: expect.any(String),
            y: expect.any(String),
            kid: expect.any(String),
          },
        ],
      });

      expect(response.status).toBe(200);
      expect(
        (response.headers as Record<string, unknown>)["content-type"]
      ).toBe("application/jwk-set+json; charset=utf-8");

      const jwk = (response.body as { keys: [JWK] }).keys[0];
      const thumbprint = await calculateJwkThumbprint(jwk);

      expect(jwk.kid).toBe(thumbprint);
    });
  });

  describe("GET /auth-mock/authorize", () => {
    describe("Service Wallet", () => {
      afterEach(() => {
        nock.cleanAll();
        jest.restoreAllMocks();
      });

      it("should reject a request with an invalid redirect_uri", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customQueryParams: { redirect_uri: "" },
          }
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        // If redirect_uri is incorrect, the server can't redirect the user -> returns 400
        expect(response.status).toBe(400);
        expect(response.body).toStrictEqual({
          detail: '["redirect_uri should not be empty"]',
          status: 400,
          title: "Bad Request",
          type: "about:blank",
        });
      });

      it("should reject a request without client_metadata", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          { ignoreClientMetadata: true }
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description: `[invalid_type] in 'request.client_metadata': Required`,
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should reject a request with both request and request_uri", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams({
              ...queryParams,
              request_uri: `${client.domain}/requests/abc`,
            }).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description:
              "only one of `request` or `request_uri` must be present",
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should reject a request without request nor request_uri", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const { request: queryRequest, ...queryParams } =
          await createQueryParams(client, authMockUri, issuerMockUri);

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description:
              "one of `request` or `request_uri` must be present",
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should reject a request when request_uri is not available", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const { request: requestJwt, ...queryParams } = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri
        );

        // Mock client's /request_uri/abc endpoint: 404
        nock(client.domain)
          .get(`${client.pathname}/request_uri/abc`)
          .reply(404, "Not Found")
          .persist();

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams({
              ...queryParams,
              request_uri: `${client.domain}${client.pathname}/request_uri/abc`,
            }).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description:
              "unable to load request_uri: Request failed with status code 404",
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should reject a request when request_uri doesn't respond with a string (JWT)", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const { request: requestJwt, ...queryParams } = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri
        );

        // Mock client's /request_uri/abc endpoint: 200, but the body is an object, not a string
        nock(client.domain)
          .get(`${client.pathname}/request_uri/abc`)
          .reply(200, { jwt: "token" })
          .persist();

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams({
              ...queryParams,
              request_uri: `${client.domain}${client.pathname}/request_uri/abc`,
            }).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description:
              "invalid response from request_uri, expected a string",
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should reject a request when request_uri is not a parsable JWT", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const { request: requestJwt, ...queryParams } = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri
        );

        // Mock client's /request_uri/abc endpoint: 200, but the body is not parsable
        nock(client.domain)
          .get(`${client.pathname}/request_uri/abc`)
          .reply(200, "not a valid jwt")
          .persist();

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams({
              ...queryParams,
              request_uri: `${client.domain}${client.pathname}/request_uri/abc`,
            }).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description: "can't decode request JWT: Invalid JWT",
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should reject a request with an invalid scope", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customQueryParams: {
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-expect-error
              scope: "bad scope",
            },
          }
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_scope",
            error_description:
              "scope must be one of the following values: openid, openid ver_test:id_token, openid ver_test:vp_token",
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should reject a request with an invalid client_id", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customQueryParams: { client_id: "not_an_url" },
          }
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description:
              "client_id must either be a URL or a DID using the key DID method",
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should reject a request with an invalid response_type", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customQueryParams: {
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-expect-error
              response_type: "not code",
            },
          }
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description: "response_type must be equal to code",
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should reject a request with an invalid request parameter", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri
        );
        queryParams.request = "not a jwt";

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description: "request must be a jwt string",
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should reject a request with an invalid request audience", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const queryParams = await createQueryParams(
          client,
          "not the expected aud",
          issuerMockUri
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description: `request.aud must be equal to ${authMockUri}`,
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should reject a request where the request JWT parameters don't match the parent request", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customJwtPayload: {
              redirect_uri: "http://example.net/invalid",
            },
          }
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description:
              "request.redirect_uri must be equal to redirect_uri",
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should reject a request where the request JWT authorization_details are not valid (missing required type)", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customJwtPayload: {
              authorization_details: [
                {
                  // @ts-expect-error Invalid type
                  type: "invalid",
                  // @ts-expect-error Invalid format
                  format: "jwt_vp",
                  // locations: [issuerMockUri], // missing locations
                  types: [
                    // "VerifiableCredential", // <- missing
                    "VerifiableAttestation",
                    "VerifiableAuthorisationToOnboard",
                  ],
                },
              ],
            },
          }
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description: `[invalid_literal] in 'request.authorization_details.0.type': Invalid literal value, expected "openid_credential"
[invalid_literal] in 'request.authorization_details.0.format': Invalid literal value, expected "jwt_vc"
[custom] in 'request.authorization_details.0.types': Array must include VerifiableCredential, VerifiableAttestation`,
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should reject a request where the request JWT authorization_details are not valid (unsupported type)", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customJwtPayload: {
              authorization_details: [
                {
                  // @ts-expect-error Invalid type
                  type: "invalid",
                  // @ts-expect-error Invalid format
                  format: "jwt_vp",
                  types: [
                    "VerifiableCredential",
                    // @ts-expect-error Invalid type
                    "VerifiableAttestation2", // <- unsupported
                    "VerifiableAuthorisationToOnboard",
                  ],
                },
              ],
            },
          }
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description: `[invalid_literal] in 'request.authorization_details.0.type': Invalid literal value, expected "openid_credential"
[invalid_literal] in 'request.authorization_details.0.format': Invalid literal value, expected "jwt_vc"
[invalid_enum_value] in 'request.authorization_details.0.types.1': Invalid enum value. Expected '${VALID_CREDENTIAL_TYPES.join(
              "' | '"
            )}', received 'VerifiableAttestation2'`,
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should fail if the request JWT header kid param is missing", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();

        mockClientServer(client);

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customJwtHeaderParameters: {
              kid: undefined,
            },
          }
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description: "request.header must contain kid",
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should fail if the client's JWKS is not accessible", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();

        nock(client.domain)
          .get(`${client.pathname}/jwks`)
          .reply(404, "Page Not Found")
          .persist();

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description:
              "Can't get client's JWKS: Request failed with status code 404",
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should fail if the client's JWKS is empty", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();

        nock(client.domain)
          .get(`${client.pathname}/jwks`)
          .reply(200, {
            keys: [], // Return empty JWKS
          })
          .persist();

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description:
              "invalid client jwks: [too_small] in 'keys': Array must contain at least 1 element(s)",
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should fail if the client's public key can't be found", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();

        const randomPublicKeyJwk = await exportJWK(
          (
            await generateKeyPair("ES256")
          ).publicKey
        );
        const kid = await calculateJwkThumbprint(randomPublicKeyJwk);

        nock(client.domain)
          .get(`${client.pathname}/jwks`)
          .reply(200, {
            keys: [
              {
                ...randomPublicKeyJwk,
                kid,
              },
            ],
          })
          .persist();

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description:
              "no public key found matching request.header.kid",
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should fail if the client's public key is not an ES256 key", async () => {
        expect.assertions(2);

        // Create mock client
        const keyPair = await generateKeyPair("ES256K");
        const client = await createClient({
          privateKey: await exportJWK(keyPair.privateKey),
        });

        nock(client.domain)
          .get(`${client.pathname}/jwks`)
          .reply(200, {
            keys: [client.publicKeyJwk],
          })
          .persist();

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            alg: "ES256K",
          }
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description:
              "invalid request signature: only ES256 is supported",
            state: queryParams.state,
          }).toString()}`
        );
      });

      it("should fail if the signature doesn't match the client's public key", async () => {
        expect.assertions(2);

        // Create mock client
        const client = await createClient();

        const { publicKey } = await generateKeyPair("ES256");
        const randomPublicKeyJWK = await exportJWK(publicKey);
        nock(client.domain)
          .get(`${client.pathname}/jwks`)
          .reply(200, {
            keys: [
              {
                ...randomPublicKeyJWK,
                kid: client.publicKeyJwk.kid, // So the key can be found
              },
            ],
          })
          .persist();

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${queryParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description:
              "invalid request signature: signature verification failed",
            state: queryParams.state,
          }).toString()}`
        );
      });

      it.each([
        "mywallet://localhost:3000/suffix/xyz/code-cb",
        "https://my-issuer.eu/suffix/xyz/code-cb",
      ])("should work (redirect_uri: %s)", async (redirect_uri: string) => {
        expect.assertions(5);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customQueryParams: { redirect_uri },
          }
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              queryParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };
        expect(location).toStrictEqual(
          expect.stringContaining(client.authorizationEndpoint)
        );

        // Verify query params
        const responseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(responseQueryParams).toStrictEqual({
          state: expect.any(String),
          client_id: authMockUri,
          redirect_uri: `${authMockUri}/direct_post`,
          response_type: "id_token",
          response_mode: "direct_post",
          scope: "openid",
          nonce: expect.any(String),
          request_uri: expect.any(String),
        });

        const requestUri = responseQueryParams["request_uri"] as string;
        const requestUriId = requestUri.split("/").pop() as string;
        const result = await request(server)
          .get(`/auth-mock/request_uri/${requestUriId}`)
          .send();

        // Verify responseQueryParams["request_uri"] JWT
        const { publicKeyJwk } = await getKeyPair(
          configService.get<string>("authMockPrivateKey")
        );
        const publicKey = await importJWK(publicKeyJwk);
        const verifyResult = await jwtVerify(result.text, publicKey);

        expect(verifyResult.protectedHeader).toStrictEqual({
          alg: "ES256",
          typ: "JWT",
          kid: publicKeyJwk.kid,
        });
        expect(verifyResult.payload).toStrictEqual({
          state: expect.any(String),
          client_id: authMockUri,
          redirect_uri: `${authMockUri}/direct_post`,
          response_type: "id_token",
          response_mode: "direct_post",
          scope: "openid",
          nonce: expect.any(String),
          iss: authMockUri,
          aud: queryParams.client_id,
        });
      });

      it("should work (with request_uri in the response)", async () => {
        expect.assertions(3);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const queryParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri
        );

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams({
              ...queryParams,
              request_object: "reference",
            }).toString()}`
          )
          .send();

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };
        expect(location).toStrictEqual(
          expect.stringContaining(client.authorizationEndpoint)
        );

        // Verify query params
        const responseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(responseQueryParams).toStrictEqual({
          state: expect.any(String),
          client_id: authMockUri,
          redirect_uri: `${authMockUri}/direct_post`,
          response_type: "id_token",
          response_mode: "direct_post",
          scope: "openid",
          nonce: expect.any(String),
          request_uri: expect.stringContaining(`${authMockUri}/request_uri/`),
        });
      });

      it("should work (with request_uri instead of request in the query params)", async () => {
        expect.assertions(5);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const { request: requestJwt, ...queryParams } = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri
        );

        // Mock client's /request_uri/abc endpoint
        nock(client.domain)
          .get(`${client.pathname}/request_uri/abc`)
          .reply(200, requestJwt)
          .persist();

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams({
              ...queryParams,
              request_uri: `${client.domain}${client.pathname}/request_uri/abc`,
            }).toString()}`
          )
          .send();

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };
        expect(location).toStrictEqual(
          expect.stringContaining(client.authorizationEndpoint)
        );

        // Verify query params
        const responseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(responseQueryParams).toStrictEqual({
          state: expect.any(String),
          client_id: authMockUri,
          redirect_uri: `${authMockUri}/direct_post`,
          response_type: "id_token",
          response_mode: "direct_post",
          scope: "openid",
          nonce: expect.any(String),
          request_uri: expect.any(String),
        });

        const requestUri = responseQueryParams["request_uri"] as string;
        const requestUriId = requestUri.split("/").pop() as string;
        const result = await request(server)
          .get(`/auth-mock/request_uri/${requestUriId}`)
          .send();

        // Verify responseQueryParams["request_uri"] JWT
        const { publicKeyJwk } = await getKeyPair(
          configService.get<string>("authMockPrivateKey")
        );
        const publicKey = await importJWK(publicKeyJwk);
        const verifyResult = await jwtVerify(result.text, publicKey);

        expect(verifyResult.protectedHeader).toStrictEqual({
          alg: "ES256",
          typ: "JWT",
          kid: publicKeyJwk.kid,
        });
        expect(verifyResult.payload).toStrictEqual({
          state: expect.any(String),
          client_id: authMockUri,
          redirect_uri: `${authMockUri}/direct_post`,
          response_type: "id_token",
          response_mode: "direct_post",
          scope: "openid",
          nonce: expect.any(String),
          iss: authMockUri,
          aud: queryParams.client_id,
        });
      });

      it("should work (with authorization_details for Root TAO)", async () => {
        expect.assertions(6);

        // Create mock client
        const client = await createClient();
        mockClientServer(client);

        const { request: requestJwt, ...queryParams } = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customJwtPayload: {
              authorization_details: [
                {
                  type: "openid_credential",
                  format: "jwt_vc",
                  locations: [issuerMockUri],
                  types: [
                    "VerifiableCredential",
                    "VerifiableAttestation",
                    "VerifiableAuthorisationForTrustChain",
                  ],
                },
              ],
            },
          }
        );

        // Mock client's /request_uri/abc endpoint
        nock(client.domain)
          .get(`${client.pathname}/request_uri/abc`)
          .reply(200, requestJwt)
          .persist();

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams({
              ...queryParams,
              request_uri: `${client.domain}${client.pathname}/request_uri/abc`,
            }).toString()}`
          )
          .send();

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };
        expect(location).toStrictEqual(
          expect.stringContaining(client.authorizationEndpoint)
        );

        // Verify query params
        const responseQueryParams = qs.parse(
          new URL(location).search.substring(1),
          {
            depth: 50,
            parameterLimit: 1000,
          }
        );

        expect(responseQueryParams).toStrictEqual({
          state: expect.any(String),
          client_id: authMockUri,
          redirect_uri: `${authMockUri}/direct_post`,
          response_type: "vp_token",
          response_mode: "direct_post",
          scope: "openid",
          nonce: expect.any(String),
          presentation_definition: expect.any(String),
          request: expect.any(String),
        });
        expect(
          JSON.parse(responseQueryParams["presentation_definition"] as string)
        ).toStrictEqual(VA_TO_ONBOARD_PRESENTATION_DEFINITION);

        // Verify responseQueryParams["request"] JWT
        const { publicKeyJwk } = await getKeyPair(
          configService.get<string>("authMockPrivateKey")
        );
        const publicKey = await importJWK(publicKeyJwk);
        const verifyResult = await jwtVerify(
          responseQueryParams["request"] as string,
          publicKey
        );

        expect(verifyResult.protectedHeader).toStrictEqual({
          alg: "ES256",
          typ: "JWT",
          kid: publicKeyJwk.kid,
        });
        expect(verifyResult.payload).toStrictEqual({
          state: expect.any(String),
          client_id: authMockUri,
          redirect_uri: `${authMockUri}/direct_post`,
          response_type: "vp_token",
          response_mode: "direct_post",
          scope: "openid",
          nonce: expect.any(String),
          iss: authMockUri,
          aud: queryParams.client_id,
          exp: expect.any(Number),
          presentation_definition: VA_TO_ONBOARD_PRESENTATION_DEFINITION,
        });
      });
    });

    describe("Holder Wallet", () => {
      afterEach(() => {
        nock.cleanAll();
        jest.restoreAllMocks();
      });

      it("should fail if the requested type requires an issuer_state and none is provided", async () => {
        expect.assertions(2);

        const types = [
          "VerifiableCredential",
          "VerifiableAttestation",
          HOLDER_WALLET_CREDENTIAL_TYPES[0],
        ] satisfies AuthorizationDetails[number]["types"];

        const clientKeyPair = await generateKeyPair("ES256");
        const clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
        const clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);

        const codeVerifier = randomBytes(50).toString("base64url");
        const codeChallenge = base64url.baseEncode(
          createHash("sha256").update(codeVerifier).digest()
        );

        const clientMetadata = {
          authorization_endpoint: "openid:",
        };
        const authenticationRequestParams = {
          scope: "openid",
          client_id: clientDid,
          response_type: "code",
          redirect_uri: "openid://callback",
          code_challenge: codeChallenge,
          code_challenge_method: "S256",
          state: randomUUID(),
          // issuer_state: issuerState,
          client_metadata: JSON.stringify(clientMetadata),
          authorization_details: JSON.stringify([
            {
              type: "openid_credential",
              format: "jwt_vc",
              locations: [issuerMockUri],
              types,
            },
          ]),
        } satisfies GetAuthorizeHolderWallerDto;

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              authenticationRequestParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);
        expect((response.headers as { location: string }).location).toBe(
          `${authenticationRequestParams.redirect_uri}?${new URLSearchParams({
            error: "invalid_request",
            error_description: "issuer_state is required for this request",
            state: authenticationRequestParams.state,
          }).toString()}`
        );
      });

      it("should fail if the issuer_state is not signed by Issuer Mock", async () => {
        expect.assertions(3);

        const types = [
          "VerifiableCredential",
          "VerifiableAttestation",
          HOLDER_WALLET_CREDENTIAL_TYPES[0],
        ] satisfies AuthorizationDetails[number]["types"];

        const clientKeyPair = await generateKeyPair("ES256");
        const clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
        const clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
        const clientPrivateKey = clientKeyPair.privateKey;

        const codeVerifier = randomBytes(50).toString("base64url");
        const codeChallenge = base64url.baseEncode(
          createHash("sha256").update(codeVerifier).digest()
        );

        // Create issuer_state JWT
        const issuerMockKeyPair = await getKeyPair(
          issuerMockPrivateKeyHex,
          "ES256"
        );
        const issuerState = await new SignJWT({
          client_id: clientDid,
          credential_types: types,
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: issuerMockKeyPair.publicKeyJwk.kid,
          })
          .setIssuedAt()
          .setExpirationTime("5m")
          .setIssuer(issuerMockUri)
          .setAudience(authMockUri)
          .setSubject(clientDid)
          .sign(clientPrivateKey); // Sign with client private key instead of issuer private key

        const clientMetadata = {
          authorization_endpoint: "openid:",
        };
        const authenticationRequestParams = {
          scope: "openid",
          client_id: clientDid,
          response_type: "code",
          redirect_uri: "openid://callback",
          code_challenge: codeChallenge,
          code_challenge_method: "S256",
          state: randomUUID(),
          issuer_state: issuerState,
          client_metadata: JSON.stringify(clientMetadata),
          authorization_details: JSON.stringify([
            {
              type: "openid_credential",
              format: "jwt_vc",
              locations: [issuerMockUri],
              types,
            },
          ]),
        } satisfies GetAuthorizeHolderWallerDto;

        const response = await request(server)
          .get(
            `/auth-mock/authorize?${new URLSearchParams(
              authenticationRequestParams
            ).toString()}`
          )
          .send();

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };
        expect(location).toStrictEqual(
          expect.stringContaining(clientMetadata.authorization_endpoint)
        );

        // Verify query params
        const responseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(responseQueryParams).toStrictEqual({
          state: expect.any(String),
          error: "invalid_request",
          error_description:
            "error while verifying the signature of issuer_state: signature verification failed",
        });
      });

      describe("PKCE", () => {
        let clientKeyPair: GenerateKeyPairResult<KeyLike>;
        let clientPublicKeyJwk: JWK;
        let clientDid: string;

        beforeEach(async () => {
          clientKeyPair = await generateKeyPair("ES256");
          clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
          clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
        });

        it("should throw an error if code_challenge and code_challenge_method are missing", async () => {
          expect.assertions(3);

          const types = [
            "VerifiableCredential",
            "VerifiableAttestation",
            HOLDER_WALLET_CREDENTIAL_TYPES[0],
          ] satisfies AuthorizationDetails[number]["types"];

          // Create issuer_state JWT
          const issuerMockKeyPair = await getKeyPair(
            issuerMockPrivateKeyHex,
            "ES256"
          );
          const issuerMockSigningKey = await importJWK(
            issuerMockKeyPair.privateKeyJwk
          );
          const issuerState = await new SignJWT({
            client_id: clientDid,
            credential_types: types,
          })
            .setProtectedHeader({
              typ: "JWT",
              alg: "ES256",
              kid: issuerMockKeyPair.publicKeyJwk.kid,
            })
            .setIssuedAt()
            .setExpirationTime("5m")
            .setIssuer(issuerMockUri)
            .setAudience(authMockUri)
            .setSubject(clientDid)
            .sign(issuerMockSigningKey);

          const clientMetadata = {
            authorization_endpoint: "openid:",
          };
          const authenticationRequestParams = {
            scope: "openid",
            client_id: clientDid,
            response_type: "code",
            redirect_uri: "openid://callback",
            // Missing params
            // code_challenge: codeChallenge,
            // code_challenge_method: "S256",
            state: randomUUID(),
            issuer_state: issuerState,
            client_metadata: JSON.stringify(clientMetadata),
            authorization_details: JSON.stringify([
              {
                type: "openid_credential",
                format: "jwt_vc",
                locations: [issuerMockUri],
                types,
              },
            ]),
          } satisfies GetAuthorizeHolderWallerDto;

          const response = await request(server)
            .get(
              `/auth-mock/authorize?${new URLSearchParams(
                authenticationRequestParams
              ).toString()}`
            )
            .send();

          expect(response.status).toBe(302);

          const { location } = response.headers as { location: string };
          expect(location).toStrictEqual(
            expect.stringContaining(clientMetadata.authorization_endpoint)
          );

          // Verify query params
          const responseQueryParams = qs.parse(
            new URL(location).search.substring(1)
          );

          expect(responseQueryParams).toStrictEqual({
            state: expect.any(String),
            error: "invalid_request",
            error_description:
              "code_challenge_method and code_challenge are required for this request",
          });
        });

        it("should work with PKCE (natural person)", async () => {
          expect.assertions(5);

          const types = [
            "VerifiableCredential",
            "VerifiableAttestation",
            HOLDER_WALLET_CREDENTIAL_TYPES[0],
          ] satisfies AuthorizationDetails[number]["types"];

          const codeVerifier = randomBytes(50).toString("base64url");
          const codeChallenge = base64url.baseEncode(
            createHash("sha256").update(codeVerifier).digest()
          );

          // Create issuer_state JWT
          const issuerMockKeyPair = await getKeyPair(
            issuerMockPrivateKeyHex,
            "ES256"
          );
          const issuerMockSigningKey = await importJWK(
            issuerMockKeyPair.privateKeyJwk
          );
          const issuerState = await new SignJWT({
            client_id: clientDid,
            credential_types: types,
          })
            .setProtectedHeader({
              typ: "JWT",
              alg: "ES256",
              kid: issuerMockKeyPair.publicKeyJwk.kid,
            })
            .setIssuedAt()
            .setExpirationTime("5m")
            .setIssuer(issuerMockUri)
            .setAudience(authMockUri)
            .setSubject(clientDid)
            .sign(issuerMockSigningKey);

          const clientMetadata = {
            authorization_endpoint: "openid:",
          };
          const authenticationRequestParams = {
            scope: "openid",
            client_id: clientDid,
            response_type: "code",
            redirect_uri: "openid://callback",
            code_challenge: codeChallenge,
            code_challenge_method: "S256",
            state: randomUUID(),
            issuer_state: issuerState,
            client_metadata: JSON.stringify(clientMetadata),
            authorization_details: JSON.stringify([
              {
                type: "openid_credential",
                format: "jwt_vc",
                locations: [issuerMockUri],
                types,
              },
            ]),
          } satisfies GetAuthorizeHolderWallerDto;

          const response = await request(server)
            .get(
              `/auth-mock/authorize?${new URLSearchParams(
                authenticationRequestParams
              ).toString()}`
            )
            .send();

          expect(response.status).toBe(302);

          const { location } = response.headers as { location: string };
          expect(location).toStrictEqual(
            expect.stringContaining(clientMetadata.authorization_endpoint)
          );

          // Verify query params
          const responseQueryParams = qs.parse(
            new URL(location).search.substring(1)
          );

          expect(responseQueryParams).toStrictEqual({
            state: expect.any(String),
            client_id: authMockUri,
            redirect_uri: `${authMockUri}/direct_post`,
            response_type: "id_token",
            response_mode: "direct_post",
            scope: "openid",
            nonce: expect.any(String),
            request_uri: expect.any(String),
          });

          const requestUri = responseQueryParams["request_uri"] as string;
          const requestUriId = requestUri.split("/").pop() as string;
          const result = await request(server)
            .get(`/auth-mock/request_uri/${requestUriId}`)
            .send();

          // Verify responseQueryParams["request_uri"] JWT
          const { publicKeyJwk } = await getKeyPair(
            configService.get<string>("authMockPrivateKey")
          );
          const publicKey = await importJWK(publicKeyJwk);
          const verifyResult = await jwtVerify(result.text, publicKey);

          expect(verifyResult.protectedHeader).toStrictEqual({
            alg: "ES256",
            typ: "JWT",
            kid: publicKeyJwk.kid,
          });
          expect(verifyResult.payload).toStrictEqual({
            state: expect.any(String),
            client_id: authMockUri,
            redirect_uri: `${authMockUri}/direct_post`,
            response_type: "id_token",
            response_mode: "direct_post",
            scope: "openid",
            nonce: expect.any(String),
            iss: authMockUri,
            aud: authenticationRequestParams.client_id,
          });
        });

        it("should return a VP Token request when a CTWalletQualificationCredential is requested", async () => {
          expect.assertions(6);

          const types = [
            "VerifiableCredential",
            "VerifiableAttestation",
            "CTWalletQualificationCredential",
          ] satisfies AuthorizationDetails[number]["types"];

          const codeVerifier = randomBytes(50).toString("base64url");
          const codeChallenge = base64url.baseEncode(
            createHash("sha256").update(codeVerifier).digest()
          );

          // Create issuer_state JWT
          const issuerMockKeyPair = await getKeyPair(
            issuerMockPrivateKeyHex,
            "ES256"
          );
          const issuerMockSigningKey = await importJWK(
            issuerMockKeyPair.privateKeyJwk
          );
          const issuerState = await new SignJWT({
            client_id: clientDid,
            credential_types: types,
          })
            .setProtectedHeader({
              typ: "JWT",
              alg: "ES256",
              kid: issuerMockKeyPair.publicKeyJwk.kid,
            })
            .setIssuedAt()
            .setExpirationTime("5m")
            .setIssuer(issuerMockUri)
            .setAudience(authMockUri)
            .setSubject(clientDid)
            .sign(issuerMockSigningKey);

          const clientMetadata = {
            authorization_endpoint: "openid:",
          };
          const authenticationRequestParams = {
            scope: "openid",
            client_id: clientDid,
            response_type: "code",
            redirect_uri: "openid://callback",
            code_challenge: codeChallenge,
            code_challenge_method: "S256",
            state: randomUUID(),
            issuer_state: issuerState,
            client_metadata: JSON.stringify(clientMetadata),
            authorization_details: JSON.stringify([
              {
                type: "openid_credential",
                format: "jwt_vc",
                locations: [issuerMockUri],
                types,
              },
            ]),
          } satisfies GetAuthorizeHolderWallerDto;

          const response = await request(server)
            .get(
              `/auth-mock/authorize?${new URLSearchParams(
                authenticationRequestParams
              ).toString()}`
            )
            .send();

          expect(response.status).toBe(302);

          const { location } = response.headers as { location: string };
          expect(location).toStrictEqual(
            expect.stringContaining(clientMetadata.authorization_endpoint)
          );

          // Verify query params
          const responseQueryParams = qs.parse(
            new URL(location).search.substring(1),
            {
              depth: 50,
              parameterLimit: 1000,
            }
          );

          expect(responseQueryParams).toStrictEqual({
            state: expect.any(String),
            client_id: authMockUri,
            redirect_uri: `${authMockUri}/direct_post`,
            response_type: "vp_token",
            response_mode: "direct_post",
            scope: "openid",
            nonce: expect.any(String),
            request: expect.any(String),
            presentation_definition: expect.any(String),
          });
          expect(
            JSON.parse(responseQueryParams["presentation_definition"] as string)
          ).toStrictEqual(HOLDER_WALLET_QUALIFICATION_PRESENTATION_DEFINITION);

          // Verify responseQueryParams["request"] JWT
          const { publicKeyJwk } = await getKeyPair(
            configService.get<string>("authMockPrivateKey")
          );
          const publicKey = await importJWK(publicKeyJwk);
          const verifyResult = await jwtVerify(
            responseQueryParams["request"] as string,
            publicKey
          );

          expect(verifyResult.protectedHeader).toStrictEqual({
            alg: "ES256",
            typ: "JWT",
            kid: publicKeyJwk.kid,
          });
          expect(verifyResult.payload).toStrictEqual({
            state: expect.any(String),
            client_id: authMockUri,
            redirect_uri: `${authMockUri}/direct_post`,
            response_type: "vp_token",
            response_mode: "direct_post",
            scope: "openid",
            nonce: expect.any(String),
            iss: authMockUri,
            aud: authenticationRequestParams.client_id,
            exp: expect.any(Number),
            presentation_definition:
              HOLDER_WALLET_QUALIFICATION_PRESENTATION_DEFINITION,
          });
        });
      });
    });
  });

  describe("GET /auth-mock/request_uri/{requestId}", () => {
    afterEach(() => {
      nock.cleanAll();
      jest.restoreAllMocks();
    });

    it("should reject a request with an invalid requestId (not a nonce)", async () => {
      expect.assertions(3);

      const requestId = "invalid-nonce";

      const response = await request(server)
        .get(`/auth-mock/request_uri/${requestId}`)
        .send();

      expect(response.status).toBe(400);
      expect(response.body).toStrictEqual({
        detail: '["requestId must be a UUID"]',
        status: 400,
        title: "Bad Request",
        type: "about:blank",
      });
      expect(
        (response.headers as Record<string, unknown>)["content-type"]
      ).toBe("application/problem+json; charset=utf-8");
    });

    it("should reject a request with an unknown nonce", async () => {
      expect.assertions(3);

      const requestId = randomUUID();

      const response = await request(server)
        .get(`/auth-mock/request_uri/${requestId}`)
        .send();

      expect(response.status).toBe(404);
      expect(response.body).toStrictEqual({
        detail: `No Authorization Request found with the ID ${requestId}`,
        status: 404,
        title: "Not Found",
        type: "about:blank",
      });
      expect(
        (response.headers as Record<string, unknown>)["content-type"]
      ).toBe("application/problem+json; charset=utf-8");
    });

    it("should return a request JWT if the nonce is valid", async () => {
      expect.assertions(3);

      const requestId = randomUUID();

      // Mock cache
      jest
        .spyOn(Level.prototype, "get")
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        .mockImplementation((k: unknown): Promise<unknown> => {
          const key = k as LevelDbKeyAuth;
          if (key.did !== "" || key.requestNonce !== requestId) {
            return Promise.reject(new Error("not found"));
          }
          return Promise.resolve({ requestJwt: "jwt" });
        });

      const response = await request(server)
        .get(`/auth-mock/request_uri/${requestId}`)
        .send();

      expect(response.status).toBe(200);
      expect(response.text).toBe("jwt");
      expect(
        (response.headers as Record<string, unknown>)["content-type"]
      ).toBe("application/jwt");
    });
  });

  describe("POST /auth-mock/direct_post", () => {
    afterEach(() => {
      nock.cleanAll();
      jest.restoreAllMocks();
    });

    it("should return an error if the content type is not application/x-www-form-urlencoded", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post("/auth-mock/direct_post")
        // .set("Content-Type", "application/x-www-form-urlencoded")
        .send({
          id_token:
            "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6ImRpZDplYnNpOnpkUGoxR1BYamZFUlh4WFBFMVlUWWRKIzdqM1RwYU5kUE5UT3pPdG91T09rbmxPTFFrM0pQLXlrVGZyYVd0WTNHTUUifQ.eyJpc3MiOiJodHRwczovL215LWlzc3Vlci5ldS9zdWZmaXgveHl6Iiwic3ViIjoiaHR0cHM6Ly9teS1pc3N1ZXIuZXUvc3VmZml4L3h5eiIsImF1ZCI6Imh0dHBzOi8vYXBpLmNvbmZvcm1hbmNlLmVic2kuZXUvY29uZm9ybWFuY2UvdjMvYXV0aC1tb2NrIiwiZXhwIjoxNTg5Njk5MzYwLCJpYXQiOjE1ODk2OTkyNjAsIm5vbmNlIjoibi0wUzZfV3pBMk1qIn0.Ne2nUwjR_32wImigcTnHCqtB7BlOQ_WBiHjPoQ4UVrAEeZOMjdcGjuVJM1uY3mkLqn99kJXRFCVVDo13bEY4DQ",
          state: randomUUID(),
        });

      expect(response.body).toStrictEqual({
        detail: expect.stringMatching(
          "Content-type must be application/x-www-form-urlencoded"
        ),
        status: 400,
        title: "Bad Request",
        type: "about:blank",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the ID Token is not a JWT", async () => {
      expect.assertions(4);

      const client = await createClient();
      mockClientServer(client);

      // Mock Auth Req in cache
      const authenticationRequestParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri
      );
      const idTokenRequestState = randomUUID();
      const idTokenRequestNonce = randomUUID();

      jest
        .spyOn(Level.prototype, "get")
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        .mockImplementation((k: unknown): Promise<unknown> => {
          const key = k as LevelDbKeyAuth;
          if (key.did !== "" || key.state !== idTokenRequestState) {
            return Promise.reject(new Error("not found"));
          }
          return Promise.resolve({
            nonce: idTokenRequestNonce,
            requestPayload: decodeJwt(
              authenticationRequestParams.request
            ) as AuthenticationRequest,
          } satisfies Partial<CachedAuthRequest>);
        });

      const response = await request(server)
        .post("/auth-mock/direct_post")
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token: "test",
            state: idTokenRequestState,
          }).toString()
        );

      expect(response.status).toBe(302);

      const { location } = response.headers as { location: string };

      const authenticationResponseQueryParams = qs.parse(
        new URL(location).search.substring(1)
      );

      expect(authenticationResponseQueryParams["error"]).toBe(
        "invalid_request"
      );
      expect(authenticationResponseQueryParams["error_description"]).toBe(
        "invalid_request: 'id_token': Must be a valid JWT"
      );
      expect(authenticationResponseQueryParams["state"]).toBe(
        authenticationRequestParams.state
      );
    });

    it("should return an error if the state param is missing", async () => {
      expect.assertions(2);

      const response = await request(server)
        .post("/auth-mock/direct_post")
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token:
              "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6ImRpZDplYnNpOnpkUGoxR1BYamZFUlh4WFBFMVlUWWRKIzdqM1RwYU5kUE5UT3pPdG91T09rbmxPTFFrM0pQLXlrVGZyYVd0WTNHTUUifQ.eyJpc3MiOiJodHRwczovL215LWlzc3Vlci5ldS9zdWZmaXgveHl6Iiwic3ViIjoiaHR0cHM6Ly9teS1pc3N1ZXIuZXUvc3VmZml4L3h5eiIsImF1ZCI6Imh0dHBzOi8vYXBpLmNvbmZvcm1hbmNlLmVic2kuZXUvY29uZm9ybWFuY2UvdjMvYXV0aC1tb2NrIiwiZXhwIjoxNTg5Njk5MzYwLCJpYXQiOjE1ODk2OTkyNjAsIm5vbmNlIjoibi0wUzZfV3pBMk1qIn0.Ne2nUwjR_32wImigcTnHCqtB7BlOQ_WBiHjPoQ4UVrAEeZOMjdcGjuVJM1uY3mkLqn99kJXRFCVVDo13bEY4DQ",
          }).toString()
        );

      expect(response.body).toStrictEqual({
        detail: "state must be a string",
        status: 400,
        title: "Bad Request",
        type: "about:blank",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the state doesn't match any existing authentication request", async () => {
      expect.assertions(2);

      const randomState = randomUUID();
      const response = await request(server)
        .post("/auth-mock/direct_post")
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token:
              "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6ImRpZDplYnNpOnpkUGoxR1BYamZFUlh4WFBFMVlUWWRKIzdqM1RwYU5kUE5UT3pPdG91T09rbmxPTFFrM0pQLXlrVGZyYVd0WTNHTUUifQ.eyJpc3MiOiJodHRwczovL215LWlzc3Vlci5ldS9zdWZmaXgveHl6Iiwic3ViIjoiaHR0cHM6Ly9teS1pc3N1ZXIuZXUvc3VmZml4L3h5eiIsImF1ZCI6Imh0dHBzOi8vYXBpLmNvbmZvcm1hbmNlLmVic2kuZXUvY29uZm9ybWFuY2UvdjMvYXV0aC1tb2NrIiwiZXhwIjoxNTg5Njk5MzYwLCJpYXQiOjE1ODk2OTkyNjAsIm5vbmNlIjoibi0wUzZfV3pBMk1qIn0.Ne2nUwjR_32wImigcTnHCqtB7BlOQ_WBiHjPoQ4UVrAEeZOMjdcGjuVJM1uY3mkLqn99kJXRFCVVDo13bEY4DQ",
            state: randomState,
          }).toString()
        );

      expect(response.body).toStrictEqual({
        detail: `No Authentication Request bound to state ${randomState} found`,
        status: 400,
        title: "Bad Request",
        type: "about:blank",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the ID Token kid is missing", async () => {
      expect.assertions(5);

      const client = await createClient();
      mockClientServer(client);

      // Mock Auth Req in cache
      const authenticationRequestParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri
      );
      const idTokenRequestState = randomUUID();
      const idTokenRequestNonce = randomUUID();
      jest
        .spyOn(Level.prototype, "get")
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        .mockImplementation((k: unknown): Promise<unknown> => {
          const key = k as LevelDbKeyAuth;
          if (key.did !== "" || key.state !== idTokenRequestState) {
            return Promise.reject(new Error("not found"));
          }
          return Promise.resolve({
            nonce: idTokenRequestNonce,
            requestPayload: decodeJwt(
              authenticationRequestParams.request
            ) as AuthenticationRequest,
          } satisfies Partial<CachedAuthRequest>);
        });

      const privateKey = await importJWK(client.privateKeyJwk);
      const idToken = await new SignJWT({
        nonce: idTokenRequestNonce,
        sub: authenticationRequestParams.client_id,
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: undefined, // Missing kid
        })
        .setIssuer(authenticationRequestParams.client_id)
        .setAudience(authMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(privateKey);

      const response = await request(server)
        .post("/auth-mock/direct_post")
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token: idToken,
            state: idTokenRequestState,
          }).toString()
        );

      expect(response.status).toBe(302);

      const { location } = response.headers as { location: string };

      expect(location).toStrictEqual(
        expect.stringContaining(authenticationRequestParams.redirect_uri)
      );

      const authenticationResponseQueryParams = qs.parse(
        new URL(location).search.substring(1)
      );

      expect(authenticationResponseQueryParams["error"]).toBe(
        "invalid_request"
      );
      expect(authenticationResponseQueryParams["error_description"]).toBe(
        "invalid ID Token header: [invalid_type] in 'kid': Required"
      );
      expect(authenticationResponseQueryParams["state"]).toBe(
        authenticationRequestParams.state
      );
    });

    it("should return an error if the ID Token kid is not a valid EBSI DID v1 or Key DID", async () => {
      expect.assertions(5);

      const client = await createClient();
      mockClientServer(client);

      // Mock Auth Req in cache
      const authenticationRequestParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri
      );
      const idTokenRequestState = randomUUID();
      const idTokenRequestNonce = randomUUID();
      jest
        .spyOn(Level.prototype, "get")
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        .mockImplementation((k: unknown): Promise<unknown> => {
          const key = k as LevelDbKeyAuth;
          if (key.did !== "" || key.state !== idTokenRequestState) {
            return Promise.reject(new Error("not found"));
          }
          return Promise.resolve({
            nonce: idTokenRequestNonce,
            requestPayload: decodeJwt(
              authenticationRequestParams.request
            ) as AuthenticationRequest,
          } satisfies Partial<CachedAuthRequest>);
        });

      const privateKey = await importJWK(client.privateKeyJwk);
      const idToken = await new SignJWT({
        nonce: idTokenRequestNonce,
        sub: authenticationRequestParams.client_id,
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: client.publicKeyJwk.kid, // Not a valid EBSI DID
        })
        .setIssuer(authenticationRequestParams.client_id)
        .setAudience(authMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(privateKey);

      const response = await request(server)
        .post("/auth-mock/direct_post")
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token: idToken,
            state: idTokenRequestState,
          }).toString()
        );

      expect(response.status).toBe(302);

      const { location } = response.headers as { location: string };

      expect(location).toStrictEqual(
        expect.stringContaining(authenticationRequestParams.redirect_uri)
      );

      const authenticationResponseQueryParams = qs.parse(
        new URL(location).search.substring(1)
      );

      expect(authenticationResponseQueryParams["error"]).toBe(
        "invalid_request"
      );
      expect(authenticationResponseQueryParams["error_description"]).toBe(
        "invalid ID Token: the DID is not a valid EBSI DID v1 or Key DID"
      );
      expect(authenticationResponseQueryParams["state"]).toBe(
        authenticationRequestParams.state
      );
    });

    it("should return an error if the ID Token iss doesn't match the DID from the kid", async () => {
      expect.assertions(5);

      const client = await createClient();
      mockClientServer(client);

      const didToOnboard = util.createDid(randomBytes(16));

      // Mock Auth Req in cache
      const authenticationRequestParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri
      );
      const idTokenRequestState = randomUUID();
      const idTokenRequestNonce = randomUUID();
      jest
        .spyOn(Level.prototype, "get")
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        .mockImplementation((k: unknown): Promise<unknown> => {
          const key = k as LevelDbKeyAuth;
          if (key.did !== "" || key.state !== idTokenRequestState) {
            return Promise.reject(new Error("not found"));
          }
          return Promise.resolve({
            nonce: idTokenRequestNonce,
            requestPayload: decodeJwt(
              authenticationRequestParams.request
            ) as AuthenticationRequest,
          } satisfies Partial<CachedAuthRequest>);
        });

      const privateKey = await importJWK(client.privateKeyJwk);
      const idToken = await new SignJWT({
        nonce: idTokenRequestNonce,
        sub: authenticationRequestParams.client_id,
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: didToOnboard,
        })
        .setIssuer(authenticationRequestParams.client_id)
        .setAudience(authMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(privateKey);

      const response = await request(server)
        .post("/auth-mock/direct_post")
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token: idToken,
            state: idTokenRequestState,
          }).toString()
        );

      expect(response.status).toBe(302);

      const { location } = response.headers as { location: string };

      expect(location).toStrictEqual(
        expect.stringContaining(authenticationRequestParams.redirect_uri)
      );

      const authenticationResponseQueryParams = qs.parse(
        new URL(location).search.substring(1)
      );

      expect(authenticationResponseQueryParams["error"]).toBe(
        "invalid_request"
      );
      expect(authenticationResponseQueryParams["error_description"]).toBe(
        "invalid ID Token payload: iss doesn't match the DID from the kid"
      );
      expect(authenticationResponseQueryParams["state"]).toBe(
        authenticationRequestParams.state
      );
    });

    it("should return an error if the ID Token sub doesn't match the DID from the kid", async () => {
      expect.assertions(5);

      const client = await createClient();
      mockClientServer(client);

      const didToOnboard = util.createDid(randomBytes(16));

      // Mock Auth Req in cache
      const authenticationRequestParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri
      );
      const idTokenRequestState = randomUUID();
      const idTokenRequestNonce = randomUUID();
      jest
        .spyOn(Level.prototype, "get")
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        .mockImplementation((k: unknown): Promise<unknown> => {
          const key = k as LevelDbKeyAuth;
          if (key.did !== "" || key.state !== idTokenRequestState) {
            return Promise.reject(new Error("not found"));
          }
          return Promise.resolve({
            nonce: idTokenRequestNonce,
            requestPayload: decodeJwt(
              authenticationRequestParams.request
            ) as AuthenticationRequest,
          } satisfies Partial<CachedAuthRequest>);
        });

      const privateKey = await importJWK(client.privateKeyJwk);
      const idToken = await new SignJWT({
        nonce: idTokenRequestNonce,
        sub: authenticationRequestParams.client_id,
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: didToOnboard,
        })
        .setIssuer(didToOnboard)
        .setAudience(authMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(privateKey);

      const response = await request(server)
        .post("/auth-mock/direct_post")
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token: idToken,
            state: idTokenRequestState,
          }).toString()
        );

      expect(response.status).toBe(302);

      const { location } = response.headers as { location: string };

      expect(location).toStrictEqual(
        expect.stringContaining(authenticationRequestParams.redirect_uri)
      );

      const authenticationResponseQueryParams = qs.parse(
        new URL(location).search.substring(1)
      );

      expect(authenticationResponseQueryParams["error"]).toBe(
        "invalid_request"
      );
      expect(authenticationResponseQueryParams["error_description"]).toBe(
        "invalid ID Token payload: sub doesn't match the DID from the kid"
      );
      expect(authenticationResponseQueryParams["state"]).toBe(
        authenticationRequestParams.state
      );
    });

    it("should return an error if the ID Token payload is not a valid (missing exp)", async () => {
      expect.assertions(5);

      const client = await createClient();
      mockClientServer(client);

      const didToOnboard = util.createDid(randomBytes(16));

      // Mock Auth Req in cache
      const authenticationRequestParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri
      );
      const idTokenRequestState = randomUUID();
      const idTokenRequestNonce = randomUUID();
      jest
        .spyOn(Level.prototype, "get")
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        .mockImplementation((k: unknown): Promise<unknown> => {
          const key = k as LevelDbKeyAuth;
          if (key.did !== "" || key.state !== idTokenRequestState) {
            return Promise.reject(new Error("not found"));
          }
          return Promise.resolve({
            nonce: idTokenRequestNonce,
            requestPayload: decodeJwt(
              authenticationRequestParams.request
            ) as AuthenticationRequest,
          } satisfies Partial<CachedAuthRequest>);
        });

      const privateKey = await importJWK(client.privateKeyJwk);
      const idToken = await new SignJWT({
        nonce: idTokenRequestNonce,
        sub: didToOnboard,
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: didToOnboard,
        })
        .setIssuer(didToOnboard)
        .setAudience(authMockUri)
        .setIssuedAt()
        // .setExpirationTime("5m") // `exp` is required! An ID Token without `exp` is invalid
        .sign(privateKey);

      const response = await request(server)
        .post("/auth-mock/direct_post")
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token: idToken,
            state: idTokenRequestState,
          }).toString()
        );

      expect(response.status).toBe(302);

      const { location } = response.headers as { location: string };

      expect(location).toStrictEqual(
        expect.stringContaining(authenticationRequestParams.redirect_uri)
      );

      const authenticationResponseQueryParams = qs.parse(
        new URL(location).search.substring(1)
      );

      expect(authenticationResponseQueryParams["error"]).toBe(
        "invalid_request"
      );
      expect(authenticationResponseQueryParams["error_description"]).toBe(
        "invalid ID Token payload: [invalid_type] in 'exp': Required"
      );
      expect(authenticationResponseQueryParams["state"]).toBe(
        authenticationRequestParams.state
      );
    });

    it("should return an error if the ID Token payload is not a valid (nonce doesn't match the authentication request)", async () => {
      expect.assertions(5);

      const client = await createClient();
      mockClientServer(client);

      const didToOnboard = util.createDid(randomBytes(16));

      // Mock Auth Req in cache
      const authenticationRequestParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri
      );
      const idTokenRequestState = randomUUID();
      const idTokenRequestNonce = randomUUID();
      jest
        .spyOn(Level.prototype, "get")
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        .mockImplementation((k: unknown): Promise<unknown> => {
          const key = k as LevelDbKeyAuth;
          if (key.did !== "" || key.state !== idTokenRequestState) {
            return Promise.reject(new Error("not found"));
          }
          return Promise.resolve({
            nonce: idTokenRequestNonce,
            requestPayload: decodeJwt(
              authenticationRequestParams.request
            ) as AuthenticationRequest,
          } satisfies Partial<CachedAuthRequest>);
        });

      const privateKey = await importJWK(client.privateKeyJwk);
      const idToken = await new SignJWT({
        nonce: randomUUID(), // Does not match idTokenRequestNonce,
        sub: didToOnboard,
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: didToOnboard,
        })
        .setIssuer(didToOnboard)
        .setAudience(authMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(privateKey);

      const response = await request(server)
        .post("/auth-mock/direct_post")
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token: idToken,
            state: idTokenRequestState,
          }).toString()
        );

      expect(response.status).toBe(302);

      const { location } = response.headers as { location: string };

      expect(location).toStrictEqual(
        expect.stringContaining(authenticationRequestParams.redirect_uri)
      );

      const authenticationResponseQueryParams = qs.parse(
        new URL(location).search.substring(1)
      );

      expect(authenticationResponseQueryParams["error"]).toBe(
        "invalid_request"
      );
      expect(authenticationResponseQueryParams["error_description"]).toBe(
        "invalid ID Token payload: nonce doesn't match the Authentication Request nonce"
      );
      expect(authenticationResponseQueryParams["state"]).toBe(
        authenticationRequestParams.state
      );
    });

    describe("requesting a VerifiableAuthorisationToOnboard", () => {
      it("should return an error if the DID is already registered", async () => {
        expect.assertions(5);

        const client = await createClient();
        mockClientServer(client);

        const didToOnboard = util.createDid(randomBytes(16));

        nock(configService.get<string>("domain"))
          .get(`${DIDR_API_PATH}/${didToOnboard}`)
          .reply(200, {}) // Note: we don't need to return an actual DID Doc
          .persist();

        // Mock Auth Req in cache
        const authenticationRequestParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri
        );
        const idTokenRequestState = randomUUID();
        const idTokenRequestNonce = randomUUID();
        jest
          .spyOn(Level.prototype, "get")
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          .mockImplementation((k: unknown): Promise<unknown> => {
            const key = k as LevelDbKeyAuth;
            if (key.did !== "" || key.state !== idTokenRequestState) {
              return Promise.reject(new Error("not found"));
            }
            return Promise.resolve({
              nonce: idTokenRequestNonce,
              requestPayload: decodeJwt(
                authenticationRequestParams.request
              ) as AuthenticationRequest,
            } satisfies Partial<CachedAuthRequest>);
          });

        const privateKey = await importJWK(client.privateKeyJwk);
        const idToken = await new SignJWT({
          nonce: idTokenRequestNonce,
          sub: didToOnboard,
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: didToOnboard,
          })
          .setIssuer(didToOnboard)
          .setAudience(authMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(privateKey);

        const response = await request(server)
          .post("/auth-mock/direct_post")
          .set("Content-Type", "application/x-www-form-urlencoded")
          .send(
            new URLSearchParams({
              id_token: idToken,
              state: idTokenRequestState,
            }).toString()
          );

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };

        expect(location).toStrictEqual(
          expect.stringContaining(authenticationRequestParams.redirect_uri)
        );

        const authenticationResponseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(authenticationResponseQueryParams["error"]).toBe(
          "invalid_request"
        );
        expect(authenticationResponseQueryParams["error_description"]).toBe(
          `invalid ID Token: DID ${didToOnboard} is already registered in the DID Registry`
        );
        expect(authenticationResponseQueryParams["state"]).toBe(
          authenticationRequestParams.state
        );
      });

      it("should obtain a code", async () => {
        expect.assertions(4);

        const client = await createClient();
        mockClientServer(client);

        const didToOnboard = util.createDid(randomBytes(16));

        // Mock Auth Req in cache
        const authenticationRequestParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri
        );
        const idTokenRequestState = randomUUID();
        const idTokenRequestNonce = randomUUID();
        jest
          .spyOn(Level.prototype, "get")
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          .mockImplementation((k: unknown): Promise<unknown> => {
            const key = k as LevelDbKeyAuth;
            if (key.did !== "" || key.state !== idTokenRequestState) {
              return Promise.reject(new Error("not found"));
            }
            return Promise.resolve({
              nonce: idTokenRequestNonce,
              requestPayload: decodeJwt(
                authenticationRequestParams.request
              ) as AuthenticationRequest,
            } satisfies Partial<CachedAuthRequest>);
          });

        const privateKey = await importJWK(client.privateKeyJwk);
        const idToken = await new SignJWT({
          nonce: idTokenRequestNonce,
          sub: didToOnboard,
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: didToOnboard,
          })
          .setIssuer(didToOnboard)
          .setAudience(authMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(privateKey);

        const response = await request(server)
          .post("/auth-mock/direct_post")
          .set("Content-Type", "application/x-www-form-urlencoded")
          .send(
            new URLSearchParams({
              id_token: idToken,
              state: idTokenRequestState,
            }).toString()
          );

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };

        expect(location).toStrictEqual(
          expect.stringContaining(authenticationRequestParams.redirect_uri)
        );

        const authenticationResponseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(authenticationResponseQueryParams["state"]).toBe(
          authenticationRequestParams.state
        );

        expect(authenticationResponseQueryParams["code"]).toStrictEqual(
          expect.any(String)
        );
      });
    });

    describe.each([
      "VerifiableAccreditationToAccredit",
      "VerifiableAccreditationToAttest",
      "CTAAQualificationCredential",
    ] as const)("requesting a %s", (requestedType) => {
      const requestedTypes: (typeof VALID_CREDENTIAL_TYPES)[number][] =
        requestedType === "CTAAQualificationCredential"
          ? [requestedType]
          : ["VerifiableAccreditation", requestedType];

      it("should return an error if the DID is not registered in the DIDR", async () => {
        expect.assertions(5);

        const client = await createClient();
        mockClientServer(client);

        const clientDid = util.createDid(randomBytes(16));
        const clientKid = `${clientDid}#key-1`;

        // Mock DID Registry
        nock(configService.get<string>("domain"))
          .get(`${DIDR_API_PATH}/${clientDid}`)
          .reply(404, "Not found")
          .persist();

        // Mock Auth Req in cache
        const authenticationRequestParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customJwtPayload: {
              authorization_details: [
                {
                  type: "openid_credential",
                  format: "jwt_vc",
                  locations: [issuerMockUri],
                  types: [
                    "VerifiableCredential",
                    "VerifiableAttestation",
                    ...requestedTypes,
                  ],
                },
              ] satisfies AuthorizationDetails,
            },
          }
        );
        const idTokenRequestState = randomUUID();
        const idTokenRequestNonce = randomUUID();
        jest
          .spyOn(Level.prototype, "get")
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          .mockImplementation((k: unknown): Promise<unknown> => {
            const key = k as LevelDbKeyAuth;
            if (key.did !== "" || key.state !== idTokenRequestState) {
              return Promise.reject(new Error("not found"));
            }
            return Promise.resolve({
              nonce: idTokenRequestNonce,
              requestPayload: decodeJwt(
                authenticationRequestParams.request
              ) as AuthenticationRequest,
            } satisfies Partial<CachedAuthRequest>);
          });

        const privateKey = await importJWK(client.privateKeyJwk);
        const idToken = await new SignJWT({
          nonce: idTokenRequestNonce,
          sub: clientDid,
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: clientKid,
          })
          .setIssuer(clientDid)
          .setAudience(authMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(privateKey);

        const response = await request(server)
          .post("/auth-mock/direct_post")
          .set("Content-Type", "application/x-www-form-urlencoded")
          .send(
            new URLSearchParams({
              id_token: idToken,
              state: idTokenRequestState,
            }).toString()
          );

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };

        expect(location).toStrictEqual(
          expect.stringContaining(authenticationRequestParams.redirect_uri)
        );

        const authenticationResponseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(authenticationResponseQueryParams["error"]).toBe(
          "invalid_request"
        );
        expect(authenticationResponseQueryParams["error_description"]).toBe(
          `invalid ID Token: DID ${clientDid} not found in the DID Registry`
        );
        expect(authenticationResponseQueryParams["state"]).toBe(
          authenticationRequestParams.state
        );
      });

      it("should return an error if no verification method from the DID document matches the kid", async () => {
        expect.assertions(5);

        const client = await createClient();
        mockClientServer(client);

        const clientDid = util.createDid(randomBytes(16));
        const clientKid = `${clientDid}#key-1`;
        const clientDidDocument = createDidDocument(
          clientDid,
          `${clientDid}#no-match`, // Should be clientKid,
          client.publicKeyJwk
        );

        // Mock DID Registry
        nock(configService.get<string>("domain"))
          .get(`${DIDR_API_PATH}/${clientDid}`)
          .reply(200, clientDidDocument)
          .persist();

        // Mock Auth Req in cache
        const authenticationRequestParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customJwtPayload: {
              authorization_details: [
                {
                  type: "openid_credential",
                  format: "jwt_vc",
                  locations: [issuerMockUri],
                  types: [
                    "VerifiableCredential",
                    "VerifiableAttestation",
                    ...requestedTypes,
                  ],
                },
              ] satisfies AuthorizationDetails,
            },
          }
        );
        const idTokenRequestState = randomUUID();
        const idTokenRequestNonce = randomUUID();
        jest
          .spyOn(Level.prototype, "get")
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          .mockImplementation((k: unknown): Promise<unknown> => {
            const key = k as LevelDbKeyAuth;
            if (key.did !== "" || key.state !== idTokenRequestState) {
              return Promise.reject(new Error("not found"));
            }
            return Promise.resolve({
              nonce: idTokenRequestNonce,
              requestPayload: decodeJwt(
                authenticationRequestParams.request
              ) as AuthenticationRequest,
            } satisfies Partial<CachedAuthRequest>);
          });

        const privateKey = await importJWK(client.privateKeyJwk);
        const idToken = await new SignJWT({
          nonce: idTokenRequestNonce,
          sub: clientDid,
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: clientKid,
          })
          .setIssuer(clientDid)
          .setAudience(authMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(privateKey);

        const response = await request(server)
          .post("/auth-mock/direct_post")
          .set("Content-Type", "application/x-www-form-urlencoded")
          .send(
            new URLSearchParams({
              id_token: idToken,
              state: idTokenRequestState,
            }).toString()
          );

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };

        expect(location).toStrictEqual(
          expect.stringContaining(authenticationRequestParams.redirect_uri)
        );

        const authenticationResponseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(authenticationResponseQueryParams["error"]).toBe(
          "invalid_request"
        );
        expect(authenticationResponseQueryParams["error_description"]).toBe(
          `invalid ID Token: no authentication method matching ${clientKid} found in the DID document`
        );
        expect(authenticationResponseQueryParams["state"]).toBe(
          authenticationRequestParams.state
        );
      });

      it("should return an error if the verification method from the DID document doesn't contain a publicKeyJwk", async () => {
        expect.assertions(5);

        const client = await createClient();
        mockClientServer(client);

        const clientDid = util.createDid(randomBytes(16));
        const clientKid = `${clientDid}#key-1`;
        const clientDidDocument = createDidDocument(
          clientDid,
          clientKid,
          client.publicKeyJwk
        );

        // Remove publicKeyJwk from DID document
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        delete clientDidDocument.verificationMethod[0].publicKeyJwk;

        // Mock DID Registry
        nock(configService.get<string>("domain"))
          .get(`${DIDR_API_PATH}/${clientDid}`)
          .reply(200, clientDidDocument)
          .persist();

        // Mock Auth Req in cache
        const authenticationRequestParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customJwtPayload: {
              authorization_details: [
                {
                  type: "openid_credential",
                  format: "jwt_vc",
                  locations: [issuerMockUri],
                  types: [
                    "VerifiableCredential",
                    "VerifiableAttestation",
                    ...requestedTypes,
                  ],
                },
              ] satisfies AuthorizationDetails,
            },
          }
        );
        const idTokenRequestState = randomUUID();
        const idTokenRequestNonce = randomUUID();
        jest
          .spyOn(Level.prototype, "get")
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          .mockImplementation((k: unknown): Promise<unknown> => {
            const key = k as LevelDbKeyAuth;
            if (key.did !== "" || key.state !== idTokenRequestState) {
              return Promise.reject(new Error("not found"));
            }
            return Promise.resolve({
              nonce: idTokenRequestNonce,
              requestPayload: decodeJwt(
                authenticationRequestParams.request
              ) as AuthenticationRequest,
            } satisfies Partial<CachedAuthRequest>);
          });

        const privateKey = await importJWK(client.privateKeyJwk);
        const idToken = await new SignJWT({
          nonce: idTokenRequestNonce,
          sub: clientDid,
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: clientKid,
          })
          .setIssuer(clientDid)
          .setAudience(authMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(privateKey);

        const response = await request(server)
          .post("/auth-mock/direct_post")
          .set("Content-Type", "application/x-www-form-urlencoded")
          .send(
            new URLSearchParams({
              id_token: idToken,
              state: idTokenRequestState,
            }).toString()
          );

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };

        expect(location).toStrictEqual(
          expect.stringContaining(authenticationRequestParams.redirect_uri)
        );

        const authenticationResponseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(authenticationResponseQueryParams["error"]).toBe(
          "invalid_request"
        );
        expect(authenticationResponseQueryParams["error_description"]).toBe(
          `invalid ID Token: the authentication method matching ${clientKid} doesn't have a publicKeyJwk`
        );
        expect(authenticationResponseQueryParams["state"]).toBe(
          authenticationRequestParams.state
        );
      });

      it("should return an error if the signature of the ID Token is signed with a ES256K key", async () => {
        expect.assertions(5);

        const keyPair = await generateKeyPair("ES256K");
        const client = await createClient({
          privateKey: await exportJWK(keyPair.privateKey),
        });
        mockClientServer(client);

        const clientDid = util.createDid(randomBytes(16));
        const clientKid = `${clientDid}#key-1`;
        const clientDidDocument = createDidDocument(
          clientDid,
          clientKid,
          await exportJWK(keyPair.publicKey)
        );

        // Mock DID Registry
        nock(configService.get<string>("domain"))
          .get(`${DIDR_API_PATH}/${clientDid}`)
          .reply(200, clientDidDocument)
          .persist();

        // Mock Auth Req in cache
        const authenticationRequestParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customJwtPayload: {
              authorization_details: [
                {
                  type: "openid_credential",
                  format: "jwt_vc",
                  locations: [issuerMockUri],
                  types: [
                    "VerifiableCredential",
                    "VerifiableAttestation",
                    ...requestedTypes,
                  ],
                },
              ] satisfies AuthorizationDetails,
            },
            alg: "ES256K",
          }
        );
        const idTokenRequestState = randomUUID();
        const idTokenRequestNonce = randomUUID();
        jest
          .spyOn(Level.prototype, "get")
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          .mockImplementation((k: unknown): Promise<unknown> => {
            const key = k as LevelDbKeyAuth;
            if (key.did !== "" || key.state !== idTokenRequestState) {
              return Promise.reject(new Error("not found"));
            }
            return Promise.resolve({
              nonce: idTokenRequestNonce,
              requestPayload: decodeJwt(
                authenticationRequestParams.request
              ) as AuthenticationRequest,
            } satisfies Partial<CachedAuthRequest>);
          });

        const privateKey = await importJWK(client.privateKeyJwk, "ES256K");
        const idToken = await new SignJWT({
          nonce: idTokenRequestNonce,
          sub: clientDid,
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256K",
            kid: clientKid,
          })
          .setIssuer(clientDid)
          .setAudience(authMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(privateKey);

        const response = await request(server)
          .post("/auth-mock/direct_post")
          .set("Content-Type", "application/x-www-form-urlencoded")
          .send(
            new URLSearchParams({
              id_token: idToken,
              state: idTokenRequestState,
            }).toString()
          );

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };

        expect(location).toStrictEqual(
          expect.stringContaining(authenticationRequestParams.redirect_uri)
        );

        const authenticationResponseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(authenticationResponseQueryParams["error"]).toBe(
          "invalid_request"
        );
        expect(authenticationResponseQueryParams["error_description"]).toBe(
          "invalid ID Token signature: only ES256 is supported"
        );
        expect(authenticationResponseQueryParams["state"]).toBe(
          authenticationRequestParams.state
        );
      });

      it("should return an error if the signature of the ID Token doesn't match the public key from the DIDR", async () => {
        expect.assertions(5);

        const client = await createClient();
        mockClientServer(client);

        const clientDid = util.createDid(randomBytes(16));
        const clientKid = `${clientDid}#key-1`;
        const clientDidDocument = createDidDocument(
          clientDid,
          clientKid,
          await exportJWK((await generateKeyPair("ES256")).publicKey)
        );

        // Mock DID Registry
        nock(configService.get<string>("domain"))
          .get(`${DIDR_API_PATH}/${clientDid}`)
          .reply(200, clientDidDocument)
          .persist();

        // Mock Auth Req in cache
        const authenticationRequestParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customJwtPayload: {
              authorization_details: [
                {
                  type: "openid_credential",
                  format: "jwt_vc",
                  locations: [issuerMockUri],
                  types: [
                    "VerifiableCredential",
                    "VerifiableAttestation",
                    ...requestedTypes,
                  ],
                },
              ] satisfies AuthorizationDetails,
            },
          }
        );
        const idTokenRequestState = randomUUID();
        const idTokenRequestNonce = randomUUID();
        jest
          .spyOn(Level.prototype, "get")
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          .mockImplementation((k: unknown): Promise<unknown> => {
            const key = k as LevelDbKeyAuth;
            if (key.did !== "" || key.state !== idTokenRequestState) {
              return Promise.reject(new Error("not found"));
            }
            return Promise.resolve({
              nonce: idTokenRequestNonce,
              requestPayload: decodeJwt(
                authenticationRequestParams.request
              ) as AuthenticationRequest,
            } satisfies Partial<CachedAuthRequest>);
          });

        const privateKey = await importJWK(client.privateKeyJwk);
        const idToken = await new SignJWT({
          nonce: idTokenRequestNonce,
          sub: clientDid,
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: clientKid,
          })
          .setIssuer(clientDid)
          .setAudience(authMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(privateKey);

        const response = await request(server)
          .post("/auth-mock/direct_post")
          .set("Content-Type", "application/x-www-form-urlencoded")
          .send(
            new URLSearchParams({
              id_token: idToken,
              state: idTokenRequestState,
            }).toString()
          );

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };

        expect(location).toStrictEqual(
          expect.stringContaining(authenticationRequestParams.redirect_uri)
        );

        const authenticationResponseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(authenticationResponseQueryParams["error"]).toBe(
          "invalid_request"
        );
        expect(authenticationResponseQueryParams["error_description"]).toBe(
          "invalid ID Token signature: signature verification failed"
        );
        expect(authenticationResponseQueryParams["state"]).toBe(
          authenticationRequestParams.state
        );
      });

      it("should obtain a code", async () => {
        expect.assertions(4);

        const client = await createClient();
        mockClientServer(client);

        const clientDid = util.createDid(randomBytes(16));
        const clientKid = `${clientDid}#key-1`;
        const clientDidDocument = createDidDocument(
          clientDid,
          clientKid,
          client.publicKeyJwk
        );

        // Mock DID Registry
        nock(configService.get<string>("domain"))
          .get(`${DIDR_API_PATH}/${clientDid}`)
          .reply(200, clientDidDocument)
          .persist();

        // Mock TIR
        nock(configService.get<string>("domain"))
          .get(`${TIR_API_PATH}/${clientDid}`)
          .reply(200, {})
          .persist();

        // Mock Auth Req in cache
        const authenticationRequestParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customJwtPayload: {
              authorization_details: [
                {
                  type: "openid_credential",
                  format: "jwt_vc",
                  locations: [issuerMockUri],
                  types: [
                    "VerifiableCredential",
                    "VerifiableAttestation",
                    ...requestedTypes,
                  ],
                },
              ] satisfies AuthorizationDetails,
            },
          }
        );
        const idTokenRequestState = randomUUID();
        const idTokenRequestNonce = randomUUID();
        jest
          .spyOn(Level.prototype, "get")
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          .mockImplementation((k: unknown): Promise<unknown> => {
            const key = k as LevelDbKeyAuth;
            if (key.did !== "" || key.state !== idTokenRequestState) {
              return Promise.reject(new Error("not found"));
            }
            return Promise.resolve({
              nonce: idTokenRequestNonce,
              requestPayload: decodeJwt(
                authenticationRequestParams.request
              ) as AuthenticationRequest,
            } satisfies Partial<CachedAuthRequest>);
          });

        const privateKey = await importJWK(client.privateKeyJwk);
        const idToken = await new SignJWT({
          nonce: idTokenRequestNonce,
          sub: clientDid,
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: clientKid,
          })
          .setIssuer(clientDid)
          .setAudience(authMockUri)
          .setIssuedAt()
          .setExpirationTime("5m")
          .sign(privateKey);

        const response = await request(server)
          .post("/auth-mock/direct_post")
          .set("Content-Type", "application/x-www-form-urlencoded")
          .send(
            new URLSearchParams({
              id_token: idToken,
              state: idTokenRequestState,
            }).toString()
          );

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };

        expect(location).toStrictEqual(
          expect.stringContaining(authenticationRequestParams.redirect_uri)
        );

        const authenticationResponseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(authenticationResponseQueryParams["state"]).toBe(
          authenticationRequestParams.state
        );

        expect(authenticationResponseQueryParams["code"]).toStrictEqual(
          expect.any(String)
        );
      });
    });

    describe("requesting a VerifiableAuthorisationForTrustChain", () => {
      it("should return error if the presentation doesn't have a VCs", async () => {
        expect.assertions(3);

        const client = await createClient();
        mockClientServer(client);

        const clientDid = util.createDid(randomBytes(16));
        const clientKid = `${clientDid}#key-1`;
        const clientDidDocument = createDidDocument(
          clientDid,
          clientKid,
          client.publicKeyJwk
        );

        // Mock DID Registry
        nock(configService.get<string>("domain"))
          .get(`${DIDR_API_PATH}/${clientDid}`)
          .reply(200, clientDidDocument)
          .persist();

        // Mock Auth Req in cache
        const authenticationRequestParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customJwtPayload: {
              authorization_details: [
                {
                  type: "openid_credential",
                  format: "jwt_vc",
                  locations: [issuerMockUri],
                  types: [
                    "VerifiableCredential",
                    "VerifiableAttestation",
                    "VerifiableAuthorisationForTrustChain",
                  ],
                },
              ] satisfies AuthorizationDetails,
            },
          }
        );
        const idTokenRequestState = randomUUID();
        const idTokenRequestNonce = randomUUID();
        jest
          .spyOn(Level.prototype, "get")
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          .mockImplementation((k: unknown): Promise<unknown> => {
            const key = k as LevelDbKeyAuth;
            if (key.did !== "" || key.state !== idTokenRequestState) {
              return Promise.reject(new Error("not found"));
            }
            return Promise.resolve({
              nonce: idTokenRequestNonce,
              requestPayload: decodeJwt(
                authenticationRequestParams.request
              ) as AuthenticationRequest,
            } satisfies Partial<CachedAuthRequest>);
          });

        const vpPayload = {
          id: `urn:did:${randomUUID()}`,
          "@context": ["https://www.w3.org/2018/credentials/v1"],
          type: ["VerifiablePresentation"],
          holder: clientDid,
          verifiableCredential: [],
        };

        const credentialSubject: EbsiIssuer = {
          did: clientDid,
          kid: clientKid,
          privateKeyJwk: client.privateKeyJwk,
          publicKeyJwk: client.publicKeyJwk,
          alg: "ES256",
        };

        const vpToken = await createVerifiablePresentationJwt(
          vpPayload,
          credentialSubject,
          authMockUri,
          {
            ebsiAuthority: "example.net",
            skipValidation: true,
            nonce: randomUUID(),
            exp: Math.floor(Date.now() / 1000) + 100,
            nbf: Math.floor(Date.now() / 1000) - 100,
          }
        );

        const presentationSubmission = {
          id: randomUUID(),
          definition_id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.id,
          descriptor_map: [
            {
              id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.input_descriptors[0].id,
              format: "jwt_vp",
              path: "$",
              path_nested: {
                id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.input_descriptors[0]
                  .id,
                format: "jwt_vc",
                path: "$.verifiableCredential[0]",
              },
            },
          ],
        };

        const response = await request(server)
          .post("/auth-mock/direct_post")
          .set("Content-Type", "application/x-www-form-urlencoded")
          .send(
            new URLSearchParams({
              vp_token: vpToken,
              state: idTokenRequestState,
              presentation_submission: JSON.stringify(presentationSubmission),
            }).toString()
          );

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };

        expect(location).toStrictEqual(
          expect.stringContaining(authenticationRequestParams.redirect_uri)
        );

        const authenticationResponseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(authenticationResponseQueryParams).toStrictEqual({
          error: "invalid_request",
          error_description:
            "Invalid Verifiable Presentation: The presentation must contain at least 1 verifiable credential",
          state: authenticationRequestParams.state,
        });
      });

      it("should return error if the presentation doesn't have a VerifiableAuthorisationToOnboard", async () => {
        expect.assertions(3);

        const client = await createClient();
        mockClientServer(client);

        const clientDid = util.createDid(randomBytes(16));
        const clientKid = `${clientDid}#key-1`;
        const clientDidDocument = createDidDocument(
          clientDid,
          clientKid,
          client.publicKeyJwk
        );

        const credentialIssuer = await createLegalEntity("ES256K");
        const credentialIssuerAccreditationUrl = `${configService.get<string>(
          "domain"
        )}${TIR_API_PATH}/${credentialIssuer.did}/attributes/${randomBytes(
          16
        ).toString("hex")}`;

        // Mock DID Registry
        nock(configService.get<string>("domain"))
          .get(`${DIDR_API_PATH}/${clientDid}`)
          .reply(200, clientDidDocument)
          .persist();

        // Mock Auth Req in cache
        const authenticationRequestParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customJwtPayload: {
              authorization_details: [
                {
                  type: "openid_credential",
                  format: "jwt_vc",
                  locations: [issuerMockUri],
                  types: [
                    "VerifiableCredential",
                    "VerifiableAttestation",
                    "VerifiableAuthorisationForTrustChain",
                  ],
                },
              ] satisfies AuthorizationDetails,
            },
          }
        );
        const idTokenRequestState = randomUUID();
        const idTokenRequestNonce = randomUUID();
        jest
          .spyOn(Level.prototype, "get")
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          .mockImplementation((k: unknown): Promise<unknown> => {
            const key = k as LevelDbKeyAuth;
            if (key.did !== "" || key.state !== idTokenRequestState) {
              return Promise.reject(new Error("not found"));
            }
            return Promise.resolve({
              nonce: idTokenRequestNonce,
              requestPayload: decodeJwt(
                authenticationRequestParams.request
              ) as AuthenticationRequest,
            } satisfies Partial<CachedAuthRequest>);
          });

        const issuanceDate = new Date();
        // JWT access token must have 2 hours expiration time and there are no Refresh Tokens.
        const expirationDate = new Date(
          issuanceDate.getTime() + 2 * 60 * 60 * 1000
        );

        const vcPayload = {
          "@context": ["https://www.w3.org/2018/credentials/v1"],
          id: `urn:uuid:${randomUUID()}`,
          // NO VerifiableAuthorisationToOnboard
          type: ["VerifiableCredential", "VerifiableAttestation"],
          issuer: credentialIssuer.did,
          issuanceDate: `${issuanceDate.toISOString().slice(0, -5)}Z`,
          issued: `${issuanceDate.toISOString().slice(0, -5)}Z`,
          validFrom: `${issuanceDate.toISOString().slice(0, -5)}Z`,
          expirationDate: `${expirationDate.toISOString().slice(0, -5)}Z`,
          credentialSubject: {
            id: clientDid,
            type: "same-device",
          },
          credentialSchema: {
            id: configService.get<string>("authorisationCredentialSchema"),
            type: "FullJsonSchemaValidator2021",
          },
          termsOfUse: {
            id: credentialIssuerAccreditationUrl,
            type: "IssuanceCertificate",
          },
        };

        const vpPayload = {
          id: `urn:did:${randomUUID()}`,
          "@context": ["https://www.w3.org/2018/credentials/v1"],
          type: ["VerifiablePresentation"],
          holder: clientDid,
          verifiableCredential: [] as string[],
        };

        const vcJwt = await createVerifiableCredentialJwt(
          vcPayload,
          credentialIssuer,
          {
            ebsiAuthority: "example.net",
            skipValidation: true,
          }
        );

        vpPayload.verifiableCredential.push(vcJwt);

        const credentialSubject: EbsiIssuer = {
          did: clientDid,
          kid: clientKid,
          privateKeyJwk: client.privateKeyJwk,
          publicKeyJwk: client.publicKeyJwk,
          alg: "ES256",
        };

        const vpToken = await createVerifiablePresentationJwt(
          vpPayload,
          credentialSubject,
          authMockUri,
          {
            ebsiAuthority: "example.net",
            skipValidation: true,
            nonce: randomUUID(),
            exp: Math.floor(Date.now() / 1000) + 100,
            nbf: Math.floor(Date.now() / 1000) - 100,
          }
        );

        const presentationSubmission = {
          id: randomUUID(),
          definition_id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.id,
          descriptor_map: [
            {
              id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.input_descriptors[0].id,
              format: "jwt_vp",
              path: "$",
              path_nested: {
                id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.input_descriptors[0]
                  .id,
                format: "jwt_vc",
                path: "$.verifiableCredential[0]",
              },
            },
          ],
        };

        const response = await request(server)
          .post("/auth-mock/direct_post")
          .set("Content-Type", "application/x-www-form-urlencoded")
          .send(
            new URLSearchParams({
              vp_token: vpToken,
              state: idTokenRequestState,
              presentation_submission: JSON.stringify(presentationSubmission),
            }).toString()
          );

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };

        expect(location).toStrictEqual(
          expect.stringContaining(authenticationRequestParams.redirect_uri)
        );

        const authenticationResponseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(authenticationResponseQueryParams).toStrictEqual({
          error: "invalid_request",
          error_description:
            "Invalid Presentation Exchange: FilterEvaluation tag: Input candidate failed filter evaluation: $.input_descriptors[0]: $.verifiableCredential[0];,MarkForSubmissionEvaluation tag: The input candidate is not eligible for submission: $.input_descriptors[0]: $.verifiableCredential[0];",
          state: authenticationRequestParams.state,
        });
      });

      it("should obtain a code", async () => {
        expect.assertions(3);

        const client = await createClient();
        mockClientServer(client);

        const clientDid = util.createDid(randomBytes(16));
        const clientKid = `${clientDid}#key-1`;
        const clientDidDocument = createDidDocument(
          clientDid,
          clientKid,
          client.publicKeyJwk
        );

        const credentialIssuer = await createLegalEntity("ES256K");
        const credentialIssuerAccreditationUrl = `${configService.get<string>(
          "domain"
        )}${TIR_API_PATH}/${credentialIssuer.did}/attributes/${randomBytes(
          16
        ).toString("hex")}`;

        // Mock DID Registry
        nock(configService.get<string>("domain"))
          .get(`${DIDR_API_PATH}/${clientDid}`)
          .reply(200, clientDidDocument)
          .persist();

        jest
          .spyOn(verifiablePresentationLib, "verifyPresentationJwt")
          .mockImplementation(() =>
            Promise.resolve({} as EbsiVerifiablePresentation)
          );

        // Mock Auth Req in cache
        const authenticationRequestParams = await createQueryParams(
          client,
          authMockUri,
          issuerMockUri,
          {
            customJwtPayload: {
              authorization_details: [
                {
                  type: "openid_credential",
                  format: "jwt_vc",
                  locations: [issuerMockUri],
                  types: [
                    "VerifiableCredential",
                    "VerifiableAttestation",
                    "VerifiableAuthorisationForTrustChain",
                  ],
                },
              ] satisfies AuthorizationDetails,
            },
          }
        );
        const idTokenRequestState = randomUUID();
        const idTokenRequestNonce = randomUUID();
        jest
          .spyOn(Level.prototype, "get")
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          .mockImplementation((k: unknown): Promise<unknown> => {
            const key = k as LevelDbKeyAuth;
            if (key.did !== "" || key.state !== idTokenRequestState) {
              return Promise.reject(new Error("not found"));
            }
            return Promise.resolve({
              nonce: idTokenRequestNonce,
              requestPayload: decodeJwt(
                authenticationRequestParams.request
              ) as AuthenticationRequest,
            } satisfies Partial<CachedAuthRequest>);
          });

        const issuanceDate = new Date();
        // JWT access token must have 2 hours expiration time and there are no Refresh Tokens.
        const expirationDate = new Date(
          issuanceDate.getTime() + 2 * 60 * 60 * 1000
        );

        const vcPayload = {
          "@context": ["https://www.w3.org/2018/credentials/v1"],
          id: `urn:uuid:${randomUUID()}`,
          type: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          issuer: credentialIssuer.did,
          issuanceDate: `${issuanceDate.toISOString().slice(0, -5)}Z`,
          issued: `${issuanceDate.toISOString().slice(0, -5)}Z`,
          validFrom: `${issuanceDate.toISOString().slice(0, -5)}Z`,
          expirationDate: `${expirationDate.toISOString().slice(0, -5)}Z`,
          credentialSubject: {
            id: clientDid,
            type: "same-device",
          },
          credentialSchema: {
            id: configService.get<string>("authorisationCredentialSchema"),
            type: "FullJsonSchemaValidator2021",
          },
          termsOfUse: {
            id: credentialIssuerAccreditationUrl,
            type: "IssuanceCertificate",
          },
        };

        const vpPayload = {
          id: `urn:did:${randomUUID()}`,
          "@context": ["https://www.w3.org/2018/credentials/v1"],
          type: ["VerifiablePresentation"],
          holder: clientDid,
          verifiableCredential: [] as string[],
        };

        const vcJwt = await createVerifiableCredentialJwt(
          vcPayload,
          credentialIssuer,
          {
            ebsiAuthority: "example.net",
            skipValidation: true,
          }
        );

        vpPayload.verifiableCredential.push(vcJwt);

        const credentialSubject: EbsiIssuer = {
          did: clientDid,
          kid: clientKid,
          privateKeyJwk: client.privateKeyJwk,
          publicKeyJwk: client.publicKeyJwk,
          alg: "ES256",
        };

        const vpToken = await createVerifiablePresentationJwt(
          vpPayload,
          credentialSubject,
          authMockUri,
          {
            ebsiAuthority: "example.net",
            skipValidation: true,
            nonce: randomUUID(),
            exp: Math.floor(Date.now() / 1000) + 100,
            nbf: Math.floor(Date.now() / 1000) - 100,
          }
        );

        const presentationSubmission = {
          id: randomUUID(),
          definition_id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.id,
          descriptor_map: [
            {
              id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.input_descriptors[0].id,
              format: "jwt_vp",
              path: "$",
              path_nested: {
                id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.input_descriptors[0]
                  .id,
                format: "jwt_vc",
                path: "$.verifiableCredential[0]",
              },
            },
          ],
        };

        const response = await request(server)
          .post("/auth-mock/direct_post")
          .set("Content-Type", "application/x-www-form-urlencoded")
          .send(
            new URLSearchParams({
              vp_token: vpToken,
              state: idTokenRequestState,
              presentation_submission: JSON.stringify(presentationSubmission),
            }).toString()
          );

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };

        expect(location).toStrictEqual(
          expect.stringContaining(authenticationRequestParams.redirect_uri)
        );

        const authenticationResponseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(authenticationResponseQueryParams).toStrictEqual({
          state: authenticationRequestParams.state,
          code: expect.any(String),
        });
      });
    });

    describe("requesting a CTWalletQualificationCredential", () => {
      let clientDid: string;
      let clientKid: string;
      let clientPublicKeyJwk: JWK;
      let clientPrivateKeyJwk: JWK;
      let credentials: string[];
      let authenticationRequestParams: GetAuthorizeHolderWallerDto;
      let vpTokenRequestState: string;

      beforeEach(async () => {
        const clientKeyPair = await generateKeyPair("ES256");
        clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
        clientPrivateKeyJwk = await exportJWK(clientKeyPair.privateKey);
        clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
        const methodSpecificIdentifier = clientDid.replace("did:key:", "");
        clientKid = `${clientDid}#${methodSpecificIdentifier}`;
        const codeVerifier = randomBytes(50).toString("base64url");
        const codeChallenge = base64url.baseEncode(
          createHash("sha256").update(codeVerifier).digest()
        );

        // Create issuer_state JWT
        const issuerMockKeyPair = await getKeyPair(
          issuerMockPrivateKeyHex,
          "ES256"
        );
        const issuerMockSigningKey = await importJWK(
          issuerMockKeyPair.privateKeyJwk
        );
        const issuerState = await new SignJWT({
          client_id: clientDid,
          credential_types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "CTWalletQualificationCredential",
          ],
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: issuerMockKeyPair.publicKeyJwk.kid,
          })
          .setIssuedAt()
          .setExpirationTime("5m")
          .setIssuer(issuerMockUri)
          .setAudience(authMockUri)
          .setSubject(clientDid)
          .sign(issuerMockSigningKey);

        // Mock Auth Req in cache
        const authorizationDetails = [
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [issuerMockUri],
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              "CTWalletQualificationCredential",
            ],
          },
        ] satisfies AuthorizationDetails;
        authenticationRequestParams = {
          scope: "openid",
          client_id: clientDid,
          response_type: "code",
          redirect_uri: "openid://callback",
          code_challenge: codeChallenge,
          code_challenge_method: "S256",
          state: randomUUID(),
          issuer_state: issuerState,
          client_metadata: JSON.stringify({
            authorization_endpoint: "openid:",
          }),
          authorization_details: JSON.stringify(authorizationDetails),
        };

        vpTokenRequestState = randomUUID();
        const vpTokenRequestNonce = randomUUID();
        jest
          .spyOn(Level.prototype, "get")
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          .mockImplementation((k: unknown): Promise<unknown> => {
            const key = k as LevelDbKeyAuth;
            if (key.did !== "" || key.state !== vpTokenRequestState) {
              return Promise.reject(new Error("not found"));
            }
            return Promise.resolve({
              nonce: vpTokenRequestNonce,
              requestPayload: {
                ...authenticationRequestParams,
                authorization_details: authorizationDetails,
              },
            } satisfies Partial<CachedAuthRequest>);
          });

        const credentialIssuer = await createLegalEntity("ES256K");
        const credentialIssuerAccreditationUrl = `${configService.get<string>(
          "domain"
        )}${TIR_API_PATH}/${credentialIssuer.did}/attributes/${randomBytes(
          16
        ).toString("hex")}`;

        jest
          .spyOn(verifiablePresentationLib, "verifyPresentationJwt")
          .mockImplementation(() =>
            Promise.resolve({} as EbsiVerifiablePresentation)
          );

        const createVcJwt = async (type: string) => {
          const issuanceDate = new Date();
          // JWT access token must have 2 hours expiration time and there are no Refresh Tokens.
          const expirationDate = new Date(
            issuanceDate.getTime() + 2 * 60 * 60 * 1000
          );
          const vcPayload = {
            "@context": ["https://www.w3.org/2018/credentials/v1"],
            id: `urn:uuid:${randomUUID()}`,
            type: ["VerifiableCredential", "VerifiableAttestation", type],
            issuer: credentialIssuer.did,
            issuanceDate: `${issuanceDate.toISOString().slice(0, -5)}Z`,
            issued: `${issuanceDate.toISOString().slice(0, -5)}Z`,
            validFrom: `${issuanceDate.toISOString().slice(0, -5)}Z`,
            expirationDate: `${expirationDate.toISOString().slice(0, -5)}Z`,
            credentialSubject: {
              id: clientDid,
            },
            credentialSchema: {
              id: configService.get<string>("authorisationCredentialSchema"),
              type: "FullJsonSchemaValidator2021",
            },
            termsOfUse: {
              id: credentialIssuerAccreditationUrl,
              type: "IssuanceCertificate",
            },
          };

          return createVerifiableCredentialJwt(vcPayload, credentialIssuer, {
            ebsiAuthority: "example.net",
            skipValidation: true,
          });
        };

        // Create fake credentials
        credentials = [
          await createVcJwt("CTWalletSameInTime"),
          await createVcJwt("CTWalletCrossInTime"),
          await createVcJwt("CTWalletSameDeferred"),
          await createVcJwt("CTWalletCrossDeferred"),
          await createVcJwt("CTWalletSamePreAuthorised"),
          await createVcJwt("CTWalletCrossPreAuthorised"),
        ];
      });

      it("should return an error if the presentation submission ID doesn't match the expected ID", async () => {
        expect.assertions(3);

        const vpPayload = {
          id: `urn:did:${randomUUID()}`,
          "@context": ["https://www.w3.org/2018/credentials/v1"],
          type: ["VerifiablePresentation"],
          holder: clientDid,
          verifiableCredential: credentials,
        };

        const credentialSubject: EbsiIssuer = {
          did: clientDid,
          kid: clientKid,
          privateKeyJwk: clientPrivateKeyJwk,
          publicKeyJwk: clientPublicKeyJwk,
          alg: "ES256",
        };

        const vpToken = await createVerifiablePresentationJwt(
          vpPayload,
          credentialSubject,
          authMockUri,
          {
            ebsiAuthority: "example.net",
            skipValidation: true,
            nonce: randomUUID(),
            exp: Math.floor(Date.now() / 1000) + 100,
            nbf: Math.floor(Date.now() / 1000) - 100,
          }
        );

        // Create a Presentation Submission for VA_TO_ONBOARD_PRESENTATION_DEFINITION instead of HOLDER_WALLET_QUALIFICATION_PRESENTATION_DEFINITION
        const presentationSubmission = {
          id: randomUUID(),
          definition_id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.id,
          descriptor_map: [
            {
              id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.input_descriptors[0].id,
              format: "jwt_vp",
              path: "$",
              path_nested: {
                id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.input_descriptors[0]
                  .id,
                format: "jwt_vc",
                path: "$.verifiableCredential[0]",
              },
            },
          ],
        };

        const response = await request(server)
          .post("/auth-mock/direct_post")
          .set("Content-Type", "application/x-www-form-urlencoded")
          .send(
            new URLSearchParams({
              vp_token: vpToken,
              state: vpTokenRequestState,
              presentation_submission: JSON.stringify(presentationSubmission),
            }).toString()
          );

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };

        expect(location).toStrictEqual(
          expect.stringContaining(authenticationRequestParams.redirect_uri)
        );

        const authenticationResponseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(authenticationResponseQueryParams).toStrictEqual({
          state: authenticationRequestParams.state,
          error: "invalid_request",
          error_description:
            "Invalid Presentation Submission: definition_id doesn't match the expected Presentation Definition ID for the requested scope",
        });
      });

      it("should return an error if 1 input descriptor is not satisfied", async () => {
        expect.assertions(3);

        const vpPayload = {
          id: `urn:did:${randomUUID()}`,
          "@context": ["https://www.w3.org/2018/credentials/v1"],
          type: ["VerifiablePresentation"],
          holder: clientDid,
          verifiableCredential: credentials,
        };

        const credentialSubject: EbsiIssuer = {
          did: clientDid,
          kid: clientKid,
          privateKeyJwk: clientPrivateKeyJwk,
          publicKeyJwk: clientPublicKeyJwk,
          alg: "ES256",
        };

        const vpToken = await createVerifiablePresentationJwt(
          vpPayload,
          credentialSubject,
          authMockUri,
          {
            ebsiAuthority: "example.net",
            skipValidation: true,
            nonce: randomUUID(),
            exp: Math.floor(Date.now() / 1000) + 100,
            nbf: Math.floor(Date.now() / 1000) - 100,
          }
        );

        const presentationSubmission: PresentationSubmission = {
          id: randomUUID(),
          definition_id: HOLDER_WALLET_QUALIFICATION_PRESENTATION_DEFINITION.id,
          descriptor_map: [
            {
              id: "same-device-in-time-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "same-device-in-time-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[0]",
              },
            },
            // The following input descriptor is not satisfied
            // {
            //   id: "cross-device-in-time-credential",
            //   path: "$",
            //   format: "jwt_vp",
            //   path_nested: {
            //     id: "cross-device-in-time-credential",
            //     format: "jwt_vc",
            //     path: "$.verifiableCredential[1]",
            //   },
            // },
            {
              id: "same-device-deferred-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "same-device-deferred-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[2]",
              },
            },
            {
              id: "cross-device-deferred-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "cross-device-deferred-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[3]",
              },
            },
            {
              id: "same-device-pre_authorised-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "same-device-pre_authorised-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[4]",
              },
            },
            {
              id: "cross-device-pre_authorised-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "cross-device-pre_authorised-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[5]",
              },
            },
          ],
        };

        const response = await request(server)
          .post("/auth-mock/direct_post")
          .set("Content-Type", "application/x-www-form-urlencoded")
          .send(
            new URLSearchParams({
              vp_token: vpToken,
              state: vpTokenRequestState,
              presentation_submission: JSON.stringify(presentationSubmission),
            }).toString()
          );

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };

        expect(location).toStrictEqual(
          expect.stringContaining(authenticationRequestParams.redirect_uri)
        );

        const authenticationResponseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(authenticationResponseQueryParams).toStrictEqual({
          state: authenticationRequestParams.state,
          error: "invalid_request",
          error_description:
            "Invalid Presentation Submission: Input descriptor cross-device-in-time-credential is missing",
        });
      });

      it("should return an error if the presentation submission contains an input descriptor that doesn't exist in the presentation definition", async () => {
        expect.assertions(3);

        const vpPayload = {
          id: `urn:did:${randomUUID()}`,
          "@context": ["https://www.w3.org/2018/credentials/v1"],
          type: ["VerifiablePresentation"],
          holder: clientDid,
          verifiableCredential: credentials,
        };

        const credentialSubject: EbsiIssuer = {
          did: clientDid,
          kid: clientKid,
          privateKeyJwk: clientPrivateKeyJwk,
          publicKeyJwk: clientPublicKeyJwk,
          alg: "ES256",
        };

        const vpToken = await createVerifiablePresentationJwt(
          vpPayload,
          credentialSubject,
          authMockUri,
          {
            ebsiAuthority: "example.net",
            skipValidation: true,
            nonce: randomUUID(),
            exp: Math.floor(Date.now() / 1000) + 100,
            nbf: Math.floor(Date.now() / 1000) - 100,
          }
        );

        const presentationSubmission: PresentationSubmission = {
          id: randomUUID(),
          definition_id: HOLDER_WALLET_QUALIFICATION_PRESENTATION_DEFINITION.id,
          descriptor_map: [
            {
              id: "same-device-in-time-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "same-device-in-time-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[0]",
              },
            },
            {
              id: "cross-device-in-time-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "cross-device-in-time-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[1]",
              },
            },
            {
              id: "same-device-deferred-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "same-device-deferred-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[2]",
              },
            },
            {
              id: "cross-device-deferred-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "cross-device-deferred-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[3]",
              },
            },
            {
              id: "same-device-pre_authorised-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "same-device-pre_authorised-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[4]",
              },
            },
            {
              id: "cross-device-pre_authorised-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "cross-device-pre_authorised-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[5]",
              },
            },
            // The following input descriptor doesn't exist in the presentation definition
            {
              id: "invalid-input-descriptor",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "invalid-input-descriptor",
                format: "jwt_vc",
                path: "$.verifiableCredential[6]",
              },
            },
          ],
        };

        const req = new URLSearchParams({
          vp_token: vpToken,
          state: vpTokenRequestState,
          presentation_submission: JSON.stringify(presentationSubmission),
        }).toString();

        const response = await request(server)
          .post("/auth-mock/direct_post")
          .set("Content-Type", "application/x-www-form-urlencoded")
          .send(req);

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };

        expect(location).toStrictEqual(
          expect.stringContaining(authenticationRequestParams.redirect_uri)
        );

        const authenticationResponseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(authenticationResponseQueryParams).toStrictEqual({
          state: authenticationRequestParams.state,
          error: "invalid_request",
          error_description:
            "Invalid Presentation Submission: The presentation definition doesn't contain any input descriptor with the ID invalid-input-descriptor",
        });
      });

      it("should obtain a code", async () => {
        expect.assertions(3);

        const vpPayload = {
          id: `urn:did:${randomUUID()}`,
          "@context": ["https://www.w3.org/2018/credentials/v1"],
          type: ["VerifiablePresentation"],
          holder: clientDid,
          verifiableCredential: credentials,
        };

        const credentialSubject: EbsiIssuer = {
          did: clientDid,
          kid: clientKid,
          privateKeyJwk: clientPrivateKeyJwk,
          publicKeyJwk: clientPublicKeyJwk,
          alg: "ES256",
        };

        const vpToken = await createVerifiablePresentationJwt(
          vpPayload,
          credentialSubject,
          authMockUri,
          {
            ebsiAuthority: "example.net",
            skipValidation: true,
            nonce: randomUUID(),
            exp: Math.floor(Date.now() / 1000) + 100,
            nbf: Math.floor(Date.now() / 1000) - 100,
          }
        );

        const presentationSubmission: PresentationSubmission = {
          id: randomUUID(),
          definition_id: HOLDER_WALLET_QUALIFICATION_PRESENTATION_DEFINITION.id,
          descriptor_map: [
            {
              id: "same-device-in-time-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "same-device-in-time-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[0]",
              },
            },
            {
              id: "cross-device-in-time-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "cross-device-in-time-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[1]",
              },
            },
            {
              id: "same-device-deferred-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "same-device-deferred-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[2]",
              },
            },
            {
              id: "cross-device-deferred-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "cross-device-deferred-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[3]",
              },
            },
            {
              id: "same-device-pre_authorised-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "same-device-pre_authorised-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[4]",
              },
            },
            {
              id: "cross-device-pre_authorised-credential",
              path: "$",
              format: "jwt_vp",
              path_nested: {
                id: "cross-device-pre_authorised-credential",
                format: "jwt_vc",
                path: "$.verifiableCredential[5]",
              },
            },
          ],
        };

        const response = await request(server)
          .post("/auth-mock/direct_post")
          .set("Content-Type", "application/x-www-form-urlencoded")
          .send(
            new URLSearchParams({
              vp_token: vpToken,
              state: vpTokenRequestState,
              presentation_submission: JSON.stringify(presentationSubmission),
            }).toString()
          );

        expect(response.status).toBe(302);

        const { location } = response.headers as { location: string };

        expect(location).toStrictEqual(
          expect.stringContaining(authenticationRequestParams.redirect_uri)
        );

        const authenticationResponseQueryParams = qs.parse(
          new URL(location).search.substring(1)
        );

        expect(authenticationResponseQueryParams).toStrictEqual({
          state: authenticationRequestParams.state,
          code: expect.any(String),
        });
      });
    });
  });

  describe("POST /auth-mock/token", () => {
    afterEach(() => {
      nock.cleanAll();
      jest.restoreAllMocks();
    });

    it("should return an error if the content type is not application/x-www-form-urlencoded", async () => {
      expect.assertions(2);

      const client = await createClient();

      const code = randomUUID();
      const queryParams = await createTokenRequestParams(
        client,
        authMockUri,
        code
      );

      const response = await request(server)
        .post("/auth-mock/token")
        // .set("Content-Type", "application/x-www-form-urlencoded")
        .send(queryParams);

      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description:
          "Content-type must be application/x-www-form-urlencoded",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the grant_type is not supported", async () => {
      expect.assertions(2);

      // Create mock client
      const client = await createClient();

      const code = randomUUID();
      const queryParams = await createTokenRequestParams(
        client,
        authMockUri,
        code,
        {
          customQueryParams: {
            // @ts-expect-error grant_type is not supported
            grant_type: "unsupported",
          },
        }
      );

      const response = await request(server)
        .post("/auth-mock/token")
        .send(new URLSearchParams(queryParams).toString());

      expect(response.status).toBe(400);
      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description:
          "grant_type must be one of the following values: authorization_code, urn:ietf:params:oauth:grant-type:pre-authorized_code",
      });
    });

    it("should fail if the code is invalid", async () => {
      expect.assertions(2);

      // Create mock client
      const client = await createClient();

      const code = randomUUID();
      const queryParams = await createTokenRequestParams(
        client,
        authMockUri,
        code
      );

      const response = await request(server)
        .post("/auth-mock/token")
        .send(new URLSearchParams(queryParams).toString());

      expect(response.status).toBe(400);
      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description: "Invalid code",
      });
    });

    it("should fail if the client ID doesn't match the client ID from the Authentication Request", async () => {
      expect.assertions(2);

      // Create mock client
      const client = await createClient();

      const code = randomUUID();
      const queryParams = await createTokenRequestParams(
        client,
        authMockUri,
        code
      );

      // Mock cache
      jest
        .spyOn(Level.prototype, "get")
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        .mockImplementation((k: unknown): Promise<unknown> => {
          const key = k as LevelDbKeyAuth;
          if (key.did !== "" || key.code !== code) {
            return Promise.reject(new Error("not found"));
          }
          return Promise.resolve({
            clientId: "https://another-issuer.eu/suffix/xyz",
            did: util.createDid(randomBytes(16)),
            authorizationDetails: [
              {
                type: "openid_credential",
                format: "jwt_vc",
                locations: [issuerMockUri],
                types: [
                  "VerifiableCredential",
                  "VerifiableAttestation",
                  "VerifiableAuthorisationToOnboard",
                ],
              },
            ],
            jwks: {
              keys: [client.publicKeyJwk],
            },
          } satisfies CachedCodeResponse);
        });

      const response = await request(server)
        .post("/auth-mock/token")
        .send(new URLSearchParams(queryParams).toString());

      expect(response.status).toBe(400);
      expect(response.body).toStrictEqual({
        error: "invalid_client",
        error_description:
          "Client ID doesn't match the Authentication Request client ID",
      });
    });

    it("should return an error if the client assertion is not a JWT", async () => {
      expect.assertions(2);

      const client = await createClient();

      const code = randomUUID();
      const queryParams = await createTokenRequestParams(
        client,
        authMockUri,
        code
      );
      queryParams.client_assertion = "not a jwt";

      // Mock authentication request in cache
      const authenticationRequestDid = util.createDid(randomBytes(16));
      const authorizationDetails: AuthorizationDetails = [
        {
          type: "openid_credential",
          format: "jwt_vc",
          locations: [issuerMockUri],
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
        },
      ];
      jest
        .spyOn(Level.prototype, "get")
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        .mockImplementation((k: unknown): Promise<unknown> => {
          const key = k as LevelDbKeyAuth;
          if (key.did !== "" || key.code !== code) {
            return Promise.reject(new Error("not found"));
          }
          return Promise.resolve({
            clientId: client.clientId,
            did: authenticationRequestDid,
            authorizationDetails,
            jwks: {
              keys: [client.publicKeyJwk],
            },
          } satisfies CachedCodeResponse);
        });

      const response = await request(server)
        .post("/auth-mock/token")
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(new URLSearchParams(queryParams).toString());

      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description: "client_assertion must be a jwt string",
      });
      expect(response.status).toBe(400);
    });

    it("should return an error if the client assertion JWT header doesn't contain a kid", async () => {
      expect.assertions(2);

      const client = await createClient();

      const code = randomUUID();
      const queryParams = await createTokenRequestParams(
        client,
        authMockUri,
        code,
        {
          customJwtHeaderParameters: {
            kid: undefined,
          },
        }
      );

      // Mock authentication request in cache
      const authenticationRequestDid = util.createDid(randomBytes(16));
      const authorizationDetails: AuthorizationDetails = [
        {
          type: "openid_credential",
          format: "jwt_vc",
          locations: [issuerMockUri],
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
        },
      ];
      jest
        .spyOn(Level.prototype, "get")
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        .mockImplementation((k: unknown): Promise<unknown> => {
          const key = k as LevelDbKeyAuth;
          if (key.did !== "" || key.code !== code) {
            return Promise.reject(new Error("not found"));
          }
          return Promise.resolve({
            clientId: client.clientId,
            did: authenticationRequestDid,
            authorizationDetails,
            jwks: {
              keys: [client.publicKeyJwk],
            },
          } satisfies CachedCodeResponse);
        });

      const response = await request(server)
        .post("/auth-mock/token")
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(new URLSearchParams(queryParams).toString());

      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description: "client_assertion.header must contain kid",
      });
      expect(response.status).toBe(400);
    });

    it("should fail if the client's public key can't be found", async () => {
      expect.assertions(2);

      // Create mock client
      const client = await createClient();

      const randomPublicKeyJwk = await exportJWK(
        (
          await generateKeyPair("ES256")
        ).publicKey
      );
      const kid = await calculateJwkThumbprint(randomPublicKeyJwk);

      const code = randomUUID();
      const queryParams = await createTokenRequestParams(
        client,
        authMockUri,
        code
      );

      // Mock authentication request in cache
      const authenticationRequestDid = util.createDid(randomBytes(16));
      const authorizationDetails: AuthorizationDetails = [
        {
          type: "openid_credential",
          format: "jwt_vc",
          locations: [issuerMockUri],
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
        },
      ];
      jest
        .spyOn(Level.prototype, "get")
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        .mockImplementation((k: unknown): Promise<unknown> => {
          const key = k as LevelDbKeyAuth;
          if (key.did !== "" || key.code !== code) {
            return Promise.reject(new Error("not found"));
          }
          return Promise.resolve({
            clientId: client.clientId,
            did: authenticationRequestDid,
            authorizationDetails,
            jwks: {
              keys: [
                {
                  ...randomPublicKeyJwk,
                  kid,
                },
              ],
            },
          } satisfies CachedCodeResponse);
        });

      const response = await request(server)
        .post("/auth-mock/token")
        .send(new URLSearchParams(queryParams).toString());

      expect(response.status).toBe(400);
      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description: "no public key found matching request.header.kid",
      });
    });

    it("should fail if the client's public key is not an ES256 key", async () => {
      expect.assertions(2);

      // Create mock client
      const keyPair = await generateKeyPair("ES256K");
      const client = await createClient({
        privateKey: await exportJWK(keyPair.privateKey),
      });

      const code = randomUUID();
      const queryParams = await createTokenRequestParams(
        client,
        authMockUri,
        code,
        {
          alg: "ES256K",
        }
      );

      // Mock authentication request in cache
      const authenticationRequestDid = util.createDid(randomBytes(16));
      const authorizationDetails: AuthorizationDetails = [
        {
          type: "openid_credential",
          format: "jwt_vc",
          locations: [issuerMockUri],
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
        },
      ];
      jest
        .spyOn(Level.prototype, "get")
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        .mockImplementation((k: unknown): Promise<unknown> => {
          const key = k as LevelDbKeyAuth;
          if (key.did !== "" || key.code !== code) {
            return Promise.reject(new Error("not found"));
          }
          return Promise.resolve({
            clientId: client.clientId,
            did: authenticationRequestDid,
            authorizationDetails,
            jwks: {
              keys: [client.publicKeyJwk],
            },
          } satisfies CachedCodeResponse);
        });

      const response = await request(server)
        .post("/auth-mock/token")
        .send(new URLSearchParams(queryParams).toString());

      expect(response.status).toBe(400);
      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description: "invalid request signature: only ES256 is supported",
      });
    });

    it("should fail if the signature doesn't match the client's public key", async () => {
      expect.assertions(2);

      // Create mock client
      const client = await createClient();

      const { publicKey } = await generateKeyPair("ES256");
      const randomPublicKeyJWK = await exportJWK(publicKey);

      const code = randomUUID();
      const queryParams = await createTokenRequestParams(
        client,
        authMockUri,
        code
      );

      // Mock authentication request in cache
      const authenticationRequestDid = util.createDid(randomBytes(16));
      const authorizationDetails: AuthorizationDetails = [
        {
          type: "openid_credential",
          format: "jwt_vc",
          locations: [issuerMockUri],
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
        },
      ];
      jest
        .spyOn(Level.prototype, "get")
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        .mockImplementation((k: unknown): Promise<unknown> => {
          const key = k as LevelDbKeyAuth;
          if (key.did !== "" || key.code !== code) {
            return Promise.reject(new Error("not found"));
          }
          return Promise.resolve({
            clientId: client.clientId,
            did: authenticationRequestDid,
            authorizationDetails,
            jwks: {
              keys: [
                {
                  ...randomPublicKeyJWK,
                  kid: client.publicKeyJwk.kid, // So the key can be found
                },
              ],
            },
          } satisfies CachedCodeResponse);
        });

      const response = await request(server)
        .post("/auth-mock/token")
        .send(new URLSearchParams(queryParams).toString());

      expect(response.status).toBe(400);
      expect(response.body).toStrictEqual({
        error: "invalid_client",
        error_description:
          "invalid client_assertion: signature verification failed",
      });
    });

    it("should get an access token", async () => {
      expect.assertions(7);

      // Create mock client
      const client = await createClient();

      const code = randomUUID();
      const queryParams = await createTokenRequestParams(
        client,
        authMockUri,
        code
      );

      // Mock authentication request in cache
      const authenticationRequestDid = util.createDid(randomBytes(16));
      const authorizationDetails: AuthorizationDetails = [
        {
          type: "openid_credential",
          format: "jwt_vc",
          locations: [issuerMockUri],
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
        },
      ];
      jest
        .spyOn(Level.prototype, "get")
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        .mockImplementation((k: unknown): Promise<unknown> => {
          const key = k as LevelDbKeyAuth;
          if (key.did !== "" || key.code !== code) {
            return Promise.reject(new Error("not found"));
          }
          return Promise.resolve({
            clientId: client.clientId,
            did: authenticationRequestDid,
            authorizationDetails,
            jwks: {
              keys: [client.publicKeyJwk],
            },
          } satisfies CachedCodeResponse);
        });

      const response = await request(server)
        .post("/auth-mock/token")
        .send(new URLSearchParams(queryParams).toString());

      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        access_token: expect.any(String),
        c_nonce: expect.any(String),
        c_nonce_expires_in: 86400,
        expires_in: 86400,
        id_token: expect.any(String),
        token_type: "Bearer",
      });
      expect(response.headers).toStrictEqual(
        expect.objectContaining({
          "cache-control": "no-store",
          pragma: "no-cache",
        })
      );

      const {
        c_nonce: cNonce,
        c_nonce_expires_in: cNonceExpiresIn,
        id_token: idToken,
        access_token: accessToken,
      } = response.body as TokenResponse;

      // Verify ID Token
      expect(decodeProtectedHeader(idToken)).toStrictEqual({
        alg: "ES256",
        kid: expect.any(String),
        typ: "JWT",
      });
      expect(decodeJwt(idToken)).toStrictEqual({
        iss: authMockUri,
        aud: client.clientId,
        sub: authenticationRequestDid,
        exp: expect.any(Number),
        iat: expect.any(Number),
        nonce: expect.any(String),
      });

      // Verify Access Token
      expect(decodeProtectedHeader(accessToken)).toStrictEqual({
        alg: "ES256",
        kid: expect.any(String),
        typ: "JWT",
      });
      expect(decodeJwt(accessToken)).toStrictEqual({
        iss: authMockUri,
        aud: [issuerMockUri],
        sub: authenticationRequestDid,
        exp: expect.any(Number),
        iat: expect.any(Number),
        claims: {
          authorization_details: authorizationDetails,
          c_nonce: cNonce,
          c_nonce_expires_in: cNonceExpiresIn,
          client_id: client.clientId,
        },
        nonce: expect.any(String),
      });
    });

    describe("PKCE", () => {
      const types = [
        "VerifiableCredential",
        "VerifiableAttestation",
        HOLDER_WALLET_CREDENTIAL_TYPES[0],
      ] satisfies AuthorizationDetails[number]["types"];

      let clientKeyPair: GenerateKeyPairResult<KeyLike>;
      let clientPublicKeyJwk: JWK;
      let clientDid: string;
      let clientKid: string;

      beforeEach(async () => {
        clientKeyPair = await generateKeyPair("ES256");
        clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
        clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
        const methodSpecificIdentifier = clientDid.replace("did:key:", "");
        clientKid = `${clientDid}#${methodSpecificIdentifier}`;
      });

      afterEach(() => {
        nock.cleanAll();
        jest.restoreAllMocks();
      });

      it("should throw an error if code_verifier is missing", async () => {
        expect.assertions(2);

        const code = randomUUID();
        const codeVerifier = randomBytes(50).toString("base64url");
        const codeChallenge = base64url.baseEncode(
          createHash("sha256").update(codeVerifier).digest()
        );

        const tokenRequestQueryParams: Partial<PostTokenPkceDto> = {
          grant_type: "authorization_code",
          code,
          client_id: clientDid, // When client ID is a Key DID, code_verifier is required
        };

        // Mock authentication request in cache
        const authorizationDetails: AuthorizationDetails = [
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [issuerMockUri],
            types,
          },
        ];

        const authMockKeyPair = await generateKeyPair("ES256");
        const authMockPublicKeyJwk = await exportJWK(authMockKeyPair.publicKey);
        const authMockPrivateKeyJwk = await exportJWK(
          authMockKeyPair.privateKey
        );
        const thumbprint = await calculateJwkThumbprint(authMockPublicKeyJwk);

        jest
          .spyOn(Level.prototype, "get")
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          .mockImplementation((k: unknown): Promise<unknown> => {
            const key = k as LevelDbKeyAuth;

            // Requesting Auth Mock private key
            if (key.did === "" && key.jwks === true) {
              return Promise.resolve([
                {
                  ...authMockPrivateKeyJwk,
                  kid: thumbprint,
                },
              ]);
            }

            if (key.code !== code) {
              return Promise.reject(new Error("not found"));
            }

            return Promise.resolve({
              clientId: clientDid,
              did: clientDid,
              authorizationDetails,
              jwks: {
                keys: [{ ...clientPublicKeyJwk, kid: clientKid }],
              },
              codeChallenge,
            } satisfies CachedCodeResponse);
          });

        const response = await request(server)
          .post("/auth-mock/token")
          .send(new URLSearchParams(tokenRequestQueryParams).toString());

        expect(response.status).toBe(400);
        expect(response.body).toStrictEqual({
          error: "invalid_request",
          error_description: "code_verifier must be a string",
        });
      });

      it("should throw an error if no code_challenge has been stored in cache for this request", async () => {
        expect.assertions(2);

        const code = randomUUID();
        const codeVerifier = randomBytes(50).toString("base64url");

        const tokenRequestQueryParams = {
          grant_type: "authorization_code",
          code,
          client_id: clientDid,
          code_verifier: codeVerifier,
        } satisfies PostTokenPkceDto;

        // Mock authentication request in cache
        const authorizationDetails: AuthorizationDetails = [
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [issuerMockUri],
            types,
          },
        ];

        const authMockKeyPair = await generateKeyPair("ES256");
        const authMockPublicKeyJwk = await exportJWK(authMockKeyPair.publicKey);
        const authMockPrivateKeyJwk = await exportJWK(
          authMockKeyPair.privateKey
        );
        const thumbprint = await calculateJwkThumbprint(authMockPublicKeyJwk);

        jest
          .spyOn(Level.prototype, "get")
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          .mockImplementation((k: unknown): Promise<unknown> => {
            const key = k as LevelDbKeyAuth;

            // Requesting Auth Mock private key
            if (key.did === "" && key.jwks === true) {
              return Promise.resolve([
                {
                  ...authMockPrivateKeyJwk,
                  kid: thumbprint,
                },
              ]);
            }

            if (key.code !== code) {
              return Promise.reject(new Error("not found"));
            }

            return Promise.resolve({
              clientId: clientDid,
              did: clientDid,
              authorizationDetails,
              jwks: {
                keys: [{ ...clientPublicKeyJwk, kid: clientKid }],
              },
              // codeChallenge, // No code_challenge stored in cache
            } satisfies CachedCodeResponse);
          });

        const response = await request(server)
          .post("/auth-mock/token")
          .send(new URLSearchParams(tokenRequestQueryParams).toString());

        expect(response.status).toBe(400);
        expect(response.body).toStrictEqual({
          error: "invalid_request",
          error_description: "No code_challenge found for this request",
        });
      });

      it("should throw an error if code_verifier doesn't correspond to the code_challenge", async () => {
        expect.assertions(2);

        const code = randomUUID();
        const codeVerifier = randomBytes(50).toString("base64url");
        const codeChallenge = base64url.baseEncode(
          createHash("sha256").update(codeVerifier).digest()
        );
        const randomCodeVerifier = randomBytes(50).toString("base64url");

        const tokenRequestQueryParams = {
          grant_type: "authorization_code",
          code,
          client_id: clientDid,
          code_verifier: randomCodeVerifier,
        } satisfies PostTokenPkceDto;

        // Mock authentication request in cache
        const authorizationDetails: AuthorizationDetails = [
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [issuerMockUri],
            types,
          },
        ];

        const authMockKeyPair = await generateKeyPair("ES256");
        const authMockPublicKeyJwk = await exportJWK(authMockKeyPair.publicKey);
        const authMockPrivateKeyJwk = await exportJWK(
          authMockKeyPair.privateKey
        );
        const thumbprint = await calculateJwkThumbprint(authMockPublicKeyJwk);

        jest
          .spyOn(Level.prototype, "get")
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          .mockImplementation((k: unknown): Promise<unknown> => {
            const key = k as LevelDbKeyAuth;

            // Requesting Auth Mock private key
            if (key.did === "" && key.jwks === true) {
              return Promise.resolve([
                {
                  ...authMockPrivateKeyJwk,
                  kid: thumbprint,
                },
              ]);
            }

            if (key.code !== code) {
              return Promise.reject(new Error("not found"));
            }

            return Promise.resolve({
              clientId: clientDid,
              did: clientDid,
              authorizationDetails,
              jwks: {
                keys: [{ ...clientPublicKeyJwk, kid: clientKid }],
              },
              codeChallenge,
            } satisfies CachedCodeResponse);
          });

        const response = await request(server)
          .post("/auth-mock/token")
          .send(new URLSearchParams(tokenRequestQueryParams).toString());

        expect(response.status).toBe(400);
        expect(response.body).toStrictEqual({
          error: "invalid_request",
          error_description: "Invalid code_verifier",
        });
      });

      it("should get an access token (PKCE)", async () => {
        expect.assertions(7);

        const code = randomUUID();
        const codeVerifier = randomBytes(50).toString("base64url");
        const codeChallenge = base64url.baseEncode(
          createHash("sha256").update(codeVerifier).digest()
        );

        const tokenRequestQueryParams = {
          grant_type: "authorization_code",
          code,
          client_id: clientDid,
          code_verifier: codeVerifier,
        } satisfies PostTokenPkceDto;

        // Mock authentication request in cache
        const authorizationDetails: AuthorizationDetails = [
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [issuerMockUri],
            types,
          },
        ];

        const authMockKeyPair = await generateKeyPair("ES256");
        const authMockPublicKeyJwk = await exportJWK(authMockKeyPair.publicKey);
        const authMockPrivateKeyJwk = await exportJWK(
          authMockKeyPair.privateKey
        );
        const thumbprint = await calculateJwkThumbprint(authMockPublicKeyJwk);

        jest
          .spyOn(Level.prototype, "get")
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          .mockImplementation((k: unknown): Promise<unknown> => {
            const key = k as LevelDbKeyAuth;

            // Requesting Auth Mock private key
            if (key.did === "" && key.jwks === true) {
              return Promise.resolve([
                {
                  ...authMockPrivateKeyJwk,
                  kid: thumbprint,
                },
              ]);
            }

            if (key.code !== code) {
              return Promise.reject(new Error("not found"));
            }

            return Promise.resolve({
              clientId: clientDid,
              did: clientDid,
              authorizationDetails,
              jwks: {
                keys: [{ ...clientPublicKeyJwk, kid: clientKid }],
              },
              codeChallenge,
            } satisfies CachedCodeResponse);
          });

        const response = await request(server)
          .post("/auth-mock/token")
          .send(new URLSearchParams(tokenRequestQueryParams).toString());

        expect(response.status).toBe(200);
        expect(response.body).toStrictEqual({
          access_token: expect.any(String),
          c_nonce: expect.any(String),
          c_nonce_expires_in: 86400,
          expires_in: 86400,
          id_token: expect.any(String),
          token_type: "Bearer",
        });
        expect(response.headers).toStrictEqual(
          expect.objectContaining({
            "cache-control": "no-store",
            pragma: "no-cache",
          })
        );

        const {
          c_nonce: cNonce,
          c_nonce_expires_in: cNonceExpiresIn,
          id_token: idToken,
          access_token: accessToken,
        } = response.body as TokenResponse;

        // Verify ID Token
        expect(decodeProtectedHeader(idToken)).toStrictEqual({
          alg: "ES256",
          kid: expect.any(String),
          typ: "JWT",
        });
        expect(decodeJwt(idToken)).toStrictEqual({
          iss: authMockUri,
          aud: clientDid,
          sub: clientDid,
          exp: expect.any(Number),
          iat: expect.any(Number),
          nonce: expect.any(String),
        });

        // Verify Access Token
        expect(decodeProtectedHeader(accessToken)).toStrictEqual({
          alg: "ES256",
          kid: expect.any(String),
          typ: "JWT",
        });
        expect(decodeJwt(accessToken)).toStrictEqual({
          iss: authMockUri,
          aud: [issuerMockUri],
          sub: clientDid,
          exp: expect.any(Number),
          iat: expect.any(Number),
          claims: {
            authorization_details: authorizationDetails,
            c_nonce: cNonce,
            c_nonce_expires_in: cNonceExpiresIn,
            client_id: clientDid,
          },
          nonce: expect.any(String),
        });
      });
    });

    describe("Pre-Authorized code", () => {
      let clientKeyPair: GenerateKeyPairResult<KeyLike>;
      let clientPublicKeyJwk: JWK;
      let clientDid: string;
      let issuerMockKeyPair: KeyPair;
      let issuerMockSigningKey: KeyLike | Uint8Array;

      beforeAll(async () => {
        issuerMockKeyPair = await getKeyPair(issuerMockPrivateKeyHex, "ES256");
        issuerMockSigningKey = await importJWK(issuerMockKeyPair.privateKeyJwk);
      });

      beforeEach(async () => {
        clientKeyPair = await generateKeyPair("ES256");
        clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
        clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
      });

      afterEach(() => {
        nock.cleanAll();
        jest.restoreAllMocks();
      });

      it("should return an error if the request is missing pre-authorized_code or user_pin", async () => {
        expect.assertions(4);

        let queryParams: Partial<PostTokenPreAuthorizedCodeDto> = {
          grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
          user_pin: getUserPin(clientDid),
        };

        let response = await request(server)
          .post("/auth-mock/token")
          .send(new URLSearchParams(queryParams).toString());

        expect(response.status).toBe(400);
        expect(response.body).toStrictEqual({
          error: "invalid_request",
          error_description: "pre-authorized_code must be a jwt string",
        });

        // Create fake pre-authorized code issued by Issuer Mock
        const authorizationDetails = [
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [issuerMockUri],
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              "CTWalletSamePreAuthorised",
            ],
          },
        ];
        const preAuthorizedCode = await new SignJWT({
          client_id: clientDid,
          authorization_details: authorizationDetails,
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: issuerMockKeyPair.publicKeyJwk.kid,
          })
          .setIssuedAt()
          .setExpirationTime("5m")
          .setIssuer(issuerMockUri)
          .setAudience(authMockUri)
          .setSubject(clientDid)
          .sign(issuerMockSigningKey);

        queryParams = {
          grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
          "pre-authorized_code": preAuthorizedCode,
        };

        response = await request(server)
          .post("/auth-mock/token")
          .send(new URLSearchParams(queryParams).toString());

        expect(response.status).toBe(400);
        expect(response.body).toStrictEqual({
          error: "invalid_request",
          error_description: "user_pin must be a string",
        });
      });

      it("should return an error if the pre-authorized_code is not a JWT", async () => {
        expect.assertions(2);

        const queryParams: Partial<PostTokenPreAuthorizedCodeDto> = {
          grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
          "pre-authorized_code": "not a jwt",
          user_pin: getUserPin(clientDid),
        };

        const response = await request(server)
          .post("/auth-mock/token")
          .send(new URLSearchParams(queryParams).toString());

        expect(response.status).toBe(400);
        expect(response.body).toStrictEqual({
          error: "invalid_request",
          error_description: "pre-authorized_code must be a jwt string",
        });
      });

      it("should return an error if the pre-authorized_code doesn't contain a client_id", async () => {
        expect.assertions(2);

        // Create fake pre-authorized code issued by Issuer Mock
        const authorizationDetails = [
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [issuerMockUri],
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              "CTWalletSamePreAuthorised",
            ],
          },
        ];
        const preAuthorizedCode = await new SignJWT({
          // client_id: clientDid, // Missing param
          authorization_details: authorizationDetails,
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: issuerMockKeyPair.publicKeyJwk.kid,
          })
          .setIssuedAt()
          .setExpirationTime("5m")
          .setIssuer(issuerMockUri)
          .setAudience(authMockUri)
          .setSubject(clientDid)
          .sign(issuerMockSigningKey);

        const queryParams = {
          grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
          "pre-authorized_code": preAuthorizedCode,
          user_pin: getUserPin(clientDid),
        } satisfies PostTokenPreAuthorizedCodeDto;

        const response = await request(server)
          .post("/auth-mock/token")
          .send(new URLSearchParams(queryParams).toString());

        expect(response.status).toBe(400);
        expect(response.body).toStrictEqual({
          error: "invalid_request",
          error_description:
            "invalid pre-authorised code: client_id is missing",
        });
      });

      it("should return an error if the pre-authorized_code doesn't contain authorization_details", async () => {
        expect.assertions(2);

        // Create fake pre-authorized code issued by Issuer Mock
        const preAuthorizedCode = await new SignJWT({
          client_id: clientDid,
          // authorization_details: authorizationDetails, // Missing param
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: issuerMockKeyPair.publicKeyJwk.kid,
          })
          .setIssuedAt()
          .setExpirationTime("5m")
          .setIssuer(issuerMockUri)
          .setAudience(authMockUri)
          .setSubject(clientDid)
          .sign(issuerMockSigningKey);

        const queryParams = {
          grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
          "pre-authorized_code": preAuthorizedCode,
          user_pin: getUserPin(clientDid),
        } satisfies PostTokenPreAuthorizedCodeDto;

        const response = await request(server)
          .post("/auth-mock/token")
          .send(new URLSearchParams(queryParams).toString());

        expect(response.status).toBe(400);
        expect(response.body).toStrictEqual({
          error: "invalid_request",
          error_description:
            "invalid pre-authorised code: authorization_details is missing",
        });
      });

      it("should return an error if the pre-authorized_code authorization_details is invalid", async () => {
        expect.assertions(2);

        // Create fake pre-authorized code issued by Issuer Mock
        const authorizationDetails = [
          {
            type: "openid_credential",
            // format: "jwt_vc", // Missing "format"
            locations: [issuerMockUri],
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              "CTWalletSamePreAuthorised",
            ],
          },
        ];
        const preAuthorizedCode = await new SignJWT({
          client_id: clientDid,
          authorization_details: authorizationDetails,
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: issuerMockKeyPair.publicKeyJwk.kid,
          })
          .setIssuedAt()
          .setExpirationTime("5m")
          .setIssuer(issuerMockUri)
          .setAudience(authMockUri)
          .setSubject(clientDid)
          .sign(issuerMockSigningKey);

        const queryParams = {
          grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
          "pre-authorized_code": preAuthorizedCode,
          user_pin: getUserPin(clientDid),
        } satisfies PostTokenPreAuthorizedCodeDto;

        const response = await request(server)
          .post("/auth-mock/token")
          .send(new URLSearchParams(queryParams).toString());

        expect(response.status).toBe(400);
        expect(response.body).toStrictEqual({
          error: "invalid_request",
          error_description:
            "invalid pre-authorised code: invalid authorization_details",
        });
      });

      it("should return an error if the server can't get the JWKS to validate the pre-authorized_code", async () => {
        expect.assertions(2);

        const signer = (await generateKeyPair("ES256")).privateKey;

        const jwksUri = `${issuerMockUri}/jwks`;
        const jwksUrlInstance = new URL(jwksUri);
        nock(jwksUrlInstance.origin)
          .get(jwksUrlInstance.pathname)
          .reply(404, "Not Found")
          .persist();

        // Create fake pre-authorized code issued by Issuer Mock
        const authorizationDetails = [
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [issuerMockUri],
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              "CTWalletSamePreAuthorised",
            ],
          },
        ];
        const preAuthorizedCode = await new SignJWT({
          client_id: clientDid,
          authorization_details: authorizationDetails,
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: issuerMockKeyPair.publicKeyJwk.kid,
          })
          .setIssuedAt()
          .setExpirationTime("5m")
          .setIssuer(issuerMockUri)
          .setAudience(authMockUri)
          .setSubject(clientDid)
          .sign(signer);

        const queryParams = {
          grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
          "pre-authorized_code": preAuthorizedCode,
          user_pin: getUserPin(clientDid),
        } satisfies PostTokenPreAuthorizedCodeDto;

        const response = await request(server)
          .post("/auth-mock/token")
          .send(new URLSearchParams(queryParams).toString());

        expect(response.status).toBe(400);
        expect(response.body).toStrictEqual({
          error: "invalid_request",
          error_description: `error while verifying the signature of pre-authorized_code: couldn't fetch JWKS from URL: ${jwksUri}`,
        });
      });

      it("should return an error if the pre-authorized_code is not signed by the Issuer Mock", async () => {
        expect.assertions(2);

        const jwksUri = `${issuerMockUri}/jwks`;
        const jwksUrlInstance = new URL(jwksUri);
        nock(jwksUrlInstance.origin)
          .get(jwksUrlInstance.pathname)
          .reply(200, { keys: [issuerMockKeyPair.publicKeyJwk] })
          .persist();

        const signer = (await generateKeyPair("ES256")).privateKey;

        // Create fake pre-authorized code issued by Issuer Mock
        const authorizationDetails = [
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [issuerMockUri],
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              "CTWalletSamePreAuthorised",
            ],
          },
        ];
        const preAuthorizedCode = await new SignJWT({
          client_id: clientDid,
          authorization_details: authorizationDetails,
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: issuerMockKeyPair.publicKeyJwk.kid,
          })
          .setIssuedAt()
          .setExpirationTime("5m")
          .setIssuer(issuerMockUri)
          .setAudience(authMockUri)
          .setSubject(clientDid)
          .sign(signer);

        const queryParams = {
          grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
          "pre-authorized_code": preAuthorizedCode,
          user_pin: getUserPin(clientDid),
        } satisfies PostTokenPreAuthorizedCodeDto;

        const response = await request(server)
          .post("/auth-mock/token")
          .send(new URLSearchParams(queryParams).toString());

        expect(response.status).toBe(400);
        expect(response.body).toStrictEqual({
          error: "invalid_request",
          error_description:
            "error while verifying the signature of pre-authorized_code: signature verification failed",
        });
      });

      it("should return an error if the user PIN is invalid", async () => {
        expect.assertions(2);

        const jwksUri = `${issuerMockUri}/jwks`;
        const jwksUrlInstance = new URL(jwksUri);
        nock(jwksUrlInstance.origin)
          .get(jwksUrlInstance.pathname)
          .reply(200, { keys: [issuerMockKeyPair.publicKeyJwk] })
          .persist();

        // Create fake pre-authorized code issued by Issuer Mock
        const authorizationDetails = [
          {
            type: "openid_credential",
            format: "jwt_vc", // Missing "format"
            locations: [issuerMockUri],
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              "CTWalletSamePreAuthorised",
            ],
          },
        ];
        const preAuthorizedCode = await new SignJWT({
          client_id: clientDid,
          authorization_details: authorizationDetails,
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: issuerMockKeyPair.publicKeyJwk.kid,
          })
          .setIssuedAt()
          .setExpirationTime("5m")
          .setIssuer(issuerMockUri)
          .setAudience(authMockUri)
          .setSubject(clientDid)
          .sign(issuerMockSigningKey);

        const expectedUserPin = getUserPin(clientDid);
        const invalidUserPin = expectedUserPin === "1234" ? "5678" : "1234"; // Make sure to use an invalid PIN

        const queryParams = {
          grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
          "pre-authorized_code": preAuthorizedCode,
          user_pin: invalidUserPin,
        } satisfies PostTokenPreAuthorizedCodeDto;

        const response = await request(server)
          .post("/auth-mock/token")
          .send(new URLSearchParams(queryParams).toString());

        expect(response.status).toBe(400);
        expect(response.body).toStrictEqual({
          error: "invalid_request",
          error_description: "Invalid PIN",
        });
      });

      it("should get an access token", async () => {
        expect.assertions(7);

        const jwksUri = `${issuerMockUri}/jwks`;
        const jwksUrlInstance = new URL(jwksUri);
        nock(jwksUrlInstance.origin)
          .get(jwksUrlInstance.pathname)
          .reply(200, { keys: [issuerMockKeyPair.publicKeyJwk] })
          .persist();

        // Create fake pre-authorized code issued by Issuer Mock
        const authorizationDetails = [
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [issuerMockUri],
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              "CTWalletSamePreAuthorised",
            ],
          },
        ];
        const preAuthorizedCode = await new SignJWT({
          client_id: clientDid,
          authorization_details: authorizationDetails,
        })
          .setProtectedHeader({
            typ: "JWT",
            alg: "ES256",
            kid: issuerMockKeyPair.publicKeyJwk.kid,
          })
          .setIssuedAt()
          .setExpirationTime("5m")
          .setIssuer(issuerMockUri)
          .setAudience(authMockUri)
          .setSubject(clientDid)
          .sign(issuerMockSigningKey);

        const queryParams = {
          grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
          "pre-authorized_code": preAuthorizedCode,
          user_pin: getUserPin(clientDid),
        } satisfies PostTokenPreAuthorizedCodeDto;

        const response = await request(server)
          .post("/auth-mock/token")
          .send(new URLSearchParams(queryParams).toString());

        expect(response.status).toBe(200);
        expect(response.body).toStrictEqual({
          access_token: expect.any(String),
          c_nonce: expect.any(String),
          c_nonce_expires_in: 86400,
          expires_in: 86400,
          id_token: expect.any(String),
          token_type: "Bearer",
        });
        expect(response.headers).toStrictEqual(
          expect.objectContaining({
            "cache-control": "no-store",
            pragma: "no-cache",
          })
        );

        const {
          c_nonce: cNonce,
          c_nonce_expires_in: cNonceExpiresIn,
          id_token: idToken,
          access_token: accessToken,
        } = response.body as TokenResponse;

        // Verify ID Token
        expect(decodeProtectedHeader(idToken)).toStrictEqual({
          alg: "ES256",
          kid: expect.any(String),
          typ: "JWT",
        });
        expect(decodeJwt(idToken)).toStrictEqual({
          iss: authMockUri,
          aud: clientDid,
          sub: clientDid,
          exp: expect.any(Number),
          iat: expect.any(Number),
          nonce: expect.any(String),
        });

        // Verify Access Token
        expect(decodeProtectedHeader(accessToken)).toStrictEqual({
          alg: "ES256",
          kid: expect.any(String),
          typ: "JWT",
        });
        expect(decodeJwt(accessToken)).toStrictEqual({
          iss: authMockUri,
          aud: [issuerMockUri],
          sub: clientDid,
          exp: expect.any(Number),
          iat: expect.any(Number),
          claims: {
            authorization_details: authorizationDetails,
            c_nonce: cNonce,
            c_nonce_expires_in: cNonceExpiresIn,
            client_id: clientDid,
          },
          nonce: expect.any(String),
        });
      });
    });
  });
});
