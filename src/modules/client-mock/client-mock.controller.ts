import {
  Body,
  Controller,
  Get,
  Header,
  HttpCode,
  Post,
  Param,
  Query,
  Response,
  Headers,
} from "@nestjs/common";
import type { FastifyReply } from "fastify";
import { BadRequestError } from "@cef-ebsi/problem-details-errors";
import {
  OPMetadata,
  GetAuthorizeDto,
  GetRequestUriDto,
  TokenResponse,
} from "../../shared/auth-server";
import type {
  CredentialResponse,
  DeferredCredentialResponse,
} from "../../shared/credential-issuer";
import type {
  JsonWebKeySet,
  CredentialIssuerMetadata,
} from "../../shared/interfaces";
import { ClientMockService } from "./client-mock.service";
import { InitiateDto, UpdateListDto } from "./dto";
import { OAuth2Error, OAuth2TokenError } from "../../shared/errors";

@Controller("/client-mock")
export class ClientMockController {
  constructor(private clientMockService: ClientMockService) {}

  @HttpCode(204)
  @Post("/initiate")
  initiate(@Body() body: InitiateDto): Promise<void> {
    return this.clientMockService.initiate(body);
  }

  @HttpCode(204)
  @Post("/updateList")
  updateList(@Body() body: UpdateListDto): Promise<void> {
    return this.clientMockService.updateList(body);
  }

  @HttpCode(200)
  @Get("/:did/.well-known/openid-configuration")
  getOPMetadata(@Param("did") did: string): OPMetadata {
    return this.clientMockService.getOPMetadata(did);
  }

  @HttpCode(200)
  @Get("/:did/jwks")
  @Header("Content-type", "application/jwk-set+json")
  async getJwks(@Param("did") did: string): Promise<JsonWebKeySet> {
    return this.clientMockService.getJwks(did);
  }

  @Get("/:did/authorize")
  async getAuthorize(
    @Query() query: GetAuthorizeDto,
    @Response({ passthrough: true }) res: FastifyReply,
    @Param("did") did: string
  ): Promise<void> {
    const location = await this.clientMockService.authorize(did, query);

    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    res.code(302).header("Location", location).send();
  }

  @Get("/:did/request_uri/:requestId")
  @Header("Content-type", "application/jwt")
  async getRequestById(@Param() params: GetRequestUriDto): Promise<string> {
    return this.clientMockService.getRequestById(params.did, params.requestId);
  }

  @Post("/:did/direct_post")
  async directPost(
    @Headers("content-type") contentType: string,
    @Param("did") did: string,
    @Body() body: unknown, // Validate DTO within the service method so we can properly handle the error response
    @Response({ passthrough: true }) res: FastifyReply
  ): Promise<void> {
    // Only accept application/x-www-form-urlencoded
    // https://openid.net/specs/openid-4-verifiable-presentations-1_0-15.html#name-response-mode-direct_post
    if (
      !contentType.toLowerCase().includes("application/x-www-form-urlencoded")
    ) {
      throw new BadRequestError(BadRequestError.defaultTitle, {
        detail: "Content-type must be application/x-www-form-urlencoded",
      });
    }

    const location = await this.clientMockService.directPost(did, body);

    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    res.code(302).header("Location", location).send();
  }

  @HttpCode(200)
  @Post("/:did/token")
  @Header("Cache-Control", "no-store")
  @Header("Pragma", "no-cache")
  async postToken(
    @Headers("content-type") contentType: string,
    @Param("did") did: string,
    @Body() body: unknown // Validate DTO within the service method so we can properly handle the error response
  ): Promise<TokenResponse> {
    // Only accept application/x-www-form-urlencoded
    // https://www.rfc-editor.org/rfc/rfc6749#section-4.1.3
    if (
      !contentType.toLowerCase().includes("application/x-www-form-urlencoded")
    ) {
      throw new OAuth2TokenError("invalid_request", {
        errorDescription:
          "Content-type must be application/x-www-form-urlencoded",
      });
    }

    return this.clientMockService.token(did, body);
  }

  @HttpCode(200)
  @Get("/:did/.well-known/openid-credential-issuer")
  getCredentialIssuerMetadata(
    @Param("did") did: string
  ): CredentialIssuerMetadata {
    return this.clientMockService.getCredentialIssuerMetadata(did);
  }

  @HttpCode(200)
  @Post("/:did/credential")
  postCredential(
    @Headers("content-type") contentType: string,
    @Headers("authorization") authorizationHeader: string,
    @Param("did") did: string,
    @Body() body: unknown
  ): Promise<CredentialResponse | DeferredCredentialResponse> {
    // Only accept application/json
    // https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-11.html#section-7.2
    if (!contentType.toLowerCase().includes("application/json")) {
      throw new OAuth2Error("invalid_request", {
        errorDescription: "Content-type must be application/json",
      });
    }

    return this.clientMockService.postCredential(
      did,
      authorizationHeader,
      body
    );
  }

  @HttpCode(200)
  @Post("/:did/credential_deferred")
  postCredentialDeferred(
    @Headers("authorization") authorizationHeader: string,
    @Param("did") did: string
  ): Promise<CredentialResponse> {
    return this.clientMockService.postCredentialDeferred(
      did,
      authorizationHeader
    );
  }

  @HttpCode(200)
  @Get("/:did/credentials/status/:id")
  async getCredentialStatusListJwt(
    @Param() params: { did: string; id: string }
  ): Promise<string> {
    return this.clientMockService.getCredentialStatusListJwt(
      params.did,
      params.id
    );
  }
}

export default ClientMockController;
