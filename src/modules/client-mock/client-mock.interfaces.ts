import type {
  LevelDbKeyAuth,
  LevelDbObjectAuth,
} from "../../shared/auth-server";
import type {
  LevelDbKeyIssuer,
  LevelDbObjectIssuer,
} from "../../shared/credential-issuer";

export interface AttributeUrl {
  attributeUrl: string;
}

export interface ProxyId {
  proxyId: string;
}

export interface IssuerState {
  issuerState: string;
}

export interface EncodedList {
  encodedList: string;
}

export type LevelDbKey = (LevelDbKeyAuth | LevelDbKeyIssuer) & {
  attributeUrl?: true;
  proxyId?: true;
  issuerState?: true;
  statusList2021Id?: string;
};
export type LevelDbObject =
  | LevelDbObjectAuth
  | LevelDbObjectIssuer
  | AttributeUrl
  | ProxyId
  | IssuerState
  | EncodedList;
