import { CacheModule, Module } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { ApiConfigModule, cacheConfig } from "../../config/configuration";
import { ClientMockController } from "./client-mock.controller";
import { ClientMockService } from "./client-mock.service";
import { EbsiModule } from "../ebsi/ebsi.module";

@Module({
  imports: [ApiConfigModule, CacheModule.register(cacheConfig), EbsiModule],
  controllers: [ClientMockController],
  providers: [ConfigService, ClientMockService],
  exports: [ClientMockService],
})
export class ClientMockModule {}

export default ClientMockModule;
