import {
  Injectable,
  OnModuleDestroy,
  OnModuleInit,
  Logger,
} from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { createHash, randomBytes } from "node:crypto";
import {
  BadRequestError,
  InternalServerError,
  NotFoundError,
} from "@cef-ebsi/problem-details-errors";
import { Level } from "level";
import { gzip, Inflate } from "pako";
import { base64, base64url } from "multiformats/bases/base64";
import {
  createVerifiableCredentialJwt,
  CreateVerifiableCredentialOptions,
  EbsiIssuer,
  EbsiVerifiableAttestation,
} from "@cef-ebsi/verifiable-credential";
import { importJWK } from "jose";
import { PEXv2 } from "@sphereon/pex";
import { Resolver } from "did-resolver";
import { getResolver as getEbsiDidResolver } from "@cef-ebsi/ebsi-did-resolver";
import { getResolver as getKeyDidResolver } from "@cef-ebsi/key-did-resolver";
import type { ApiConfig } from "../../config/configuration";
import type { InitiateDto, UpdateListDto } from "./dto";
import {
  OPMetadata,
  TokenResponse,
  authorize,
  directPost,
  getOPMetadata,
  getPrivateJwks,
  getPublicJwks,
  getPrivateKeyJwk,
  getRequestById,
  token,
  GetAuthorizeDto,
  LevelDbKeyAuth,
  LevelDbObjectAuth,
} from "../../shared/auth-server";
import type {
  LevelDbKeyIssuer,
  LevelDbObjectIssuer,
  CredentialResponse,
  DeferredCredentialResponse,
  CachedVcJwt,
} from "../../shared/credential-issuer";
import {
  validatePostCredential,
  issueCredential,
  preregisterAttribute,
} from "../../shared/credential-issuer";
import { JWKWithKid, getKeyPair } from "../../shared/utils";
import type {
  CredentialIssuerMetadata,
  JsonWebKeySet,
} from "../../shared/interfaces";
import type {
  LevelDbKey,
  LevelDbObject,
  EncodedList,
  AttributeUrl,
  ProxyId,
  IssuerState,
} from "./client-mock.interfaces";
import { EBSIAuthorisationService } from "../ebsi/authorisation.service";
import { CredentialError } from "../../shared/errors";

type LevelAuth = Level<LevelDbKeyAuth, LevelDbObjectAuth>;
type LevelIssuer = Level<LevelDbKeyIssuer, LevelDbObjectIssuer>;

@Injectable()
export class ClientMockService implements OnModuleInit, OnModuleDestroy {
  private readonly logger = new Logger(ClientMockService.name);

  private db: Level<LevelDbKey, LevelDbObject>;

  private clientMockUri: string;

  private ebsiAuthority: string;

  private timeout: number;

  private trustedIssuersRegistryApiUrl: string;

  private statusList2021CredentialSchemaUrl: string;

  private createVerifiableCredentialOptions: CreateVerifiableCredentialOptions;

  private cacheManager: {
    interval: ReturnType<typeof setInterval>;
    keys: { key: LevelDbKey; expiration: number }[];
  };

  /**
   * EBSI DID Resolver (did:ebsi v1)
   */
  private readonly ebsiResolver: Resolver;

  /**
   * Key DID Resolver (did:key)
   */
  private readonly keyResolver: Resolver;

  /**
   * EBSI VA schema URI
   */
  private readonly authorisationCredentialSchema: string;

  /**
   * TIR API /jsonrpc endpoint
   */
  private readonly trustedIssuersRegistryApiJsonrpcUrl: string;

  /**
   * Ledger API /besu endpoint
   */
  private readonly ledgerApiUrl: string;

  /**
   * Issuer Mock public key JWK (to verify the signature of issuer_state)
   * Note: in practice, the Auth Server would load the JWKS from the Issuer Mock in order to get the public key.
   */
  private issuerMockPrivateKey: string;

  constructor(
    configService: ConfigService<ApiConfig, true>,
    private ebsiAuthorisationService: EBSIAuthorisationService
  ) {
    const conformanceDomain = configService.get<string>("conformanceDomain");
    const domain = configService.get<string>("domain");
    const apiUrlPrefix = configService.get<string>("apiUrlPrefix");
    const didRegistryApiUrl = configService.get<string>("didRegistryApiUrl");
    this.trustedIssuersRegistryApiUrl = configService.get<string>(
      "trustedIssuersRegistryApiUrl"
    );
    this.trustedIssuersRegistryApiJsonrpcUrl = configService.get<string>(
      "trustedIssuersRegistryApiJsonrpcUrl"
    );
    const trustedPoliciesRegistryApiUrl = configService.get<string>(
      "trustedPoliciesRegistryApiUrl"
    );
    this.ledgerApiUrl = configService.get<string>("ledgerApiUrl");
    this.timeout = configService.get<number>("requestTimeout");
    this.clientMockUri = `${conformanceDomain}${apiUrlPrefix}/client-mock`;
    this.statusList2021CredentialSchemaUrl = configService.get<string>(
      "statusList2021CredentialSchemaUrl"
    );
    this.db = new Level<LevelDbKey, LevelDbObject>("db/clientMock", {
      keyEncoding: "json",
      valueEncoding: "json",
    });
    this.cacheManager = {
      interval: setInterval(() => {
        this.manageCache().catch(() => {});
      }, 120_000),
      keys: [],
    };
    this.ebsiAuthority = domain.replace(/^https?:\/\//, "");
    this.ebsiResolver = new Resolver(
      getEbsiDidResolver({ registry: didRegistryApiUrl })
    );
    this.createVerifiableCredentialOptions = {
      ebsiAuthority: this.ebsiAuthority,
      timeout: this.timeout,
      ebsiEnvConfig: {
        didRegistry: didRegistryApiUrl,
        trustedIssuersRegistry: this.trustedIssuersRegistryApiUrl,
        trustedPoliciesRegistry: trustedPoliciesRegistryApiUrl,
      },
    };
    this.keyResolver = new Resolver(getKeyDidResolver());
    this.issuerMockPrivateKey = configService.get<string>(
      "issuerMockPrivateKey"
    );
    this.authorisationCredentialSchema = configService.get<string>(
      "authorisationCredentialSchema"
    );
  }

  async onModuleInit() {
    await this.db.open();
  }

  async onModuleDestroy() {
    clearInterval(this.cacheManager.interval);
    // remove data from cache
    try {
      await this.db.batch(
        this.cacheManager.keys.map((k) => ({
          type: "del",
          key: k.key,
        }))
      );
    } catch {
      // empty
    }
    await this.db.close();
  }

  private addKeyToCacheManager(
    key: LevelDbKey,
    ttl: number = Number.MAX_SAFE_INTEGER
  ) {
    this.cacheManager.keys.push({ key, expiration: Date.now() + ttl });
    // sort by expiration: The first ones to
    // expire are at the beginning of the list
    this.cacheManager.keys.sort((a, b) => a.expiration - b.expiration);
  }

  private async manageCache() {
    const now = Date.now();
    // search which ID has not expired yet
    const id = this.cacheManager.keys.findIndex((a) => now < a.expiration);
    // remove the expired keys (from 0 until id-1)
    const expiredKeys = this.cacheManager.keys.splice(0, id);
    // remove data from database
    await this.db.batch(
      expiredKeys.map((k) => ({
        type: "del",
        key: k.key,
      }))
    );
  }

  async initiate(body: InitiateDto): Promise<void> {
    const { did, keys, attributeUrl, proxyId, issuerState } = body;
    await this.db.put({ did, jwks: true }, keys as JWKWithKid[]);

    if (attributeUrl) {
      await this.db.put({ did, attributeUrl: true }, { attributeUrl });
    }

    if (proxyId) {
      await this.db.put({ did, proxyId: true }, { proxyId });
    }

    if (issuerState) {
      await this.db.put({ did, issuerState: true }, { issuerState });
    }
  }

  getOPMetadata(did: string): OPMetadata {
    const url = `${this.clientMockUri}/${did}`;
    return getOPMetadata(url);
  }

  /**
   * Expose Client Mock's public keys.
   *
   * @returns Client Mock's JWKS
   */
  async getJwks(did: string): Promise<JsonWebKeySet> {
    try {
      const privateJwks = await getPrivateJwks(this.db as LevelAuth, did);
      return getPublicJwks(privateJwks);
    } catch (error) {
      throw new NotFoundError(NotFoundError.defaultTitle, {
        detail: `No JWKS found for ${did}`,
      });
    }
  }

  /**
   * Process client's auth request.
   *
   * @returns The redirect location.
   */
  async authorize(did: string, query: GetAuthorizeDto): Promise<string> {
    const url = `${this.clientMockUri}/${did}`;
    let privateKeyJwk: JWKWithKid;
    try {
      const privateJwks = await getPrivateJwks(this.db as LevelAuth, did);
      privateKeyJwk = getPrivateKeyJwk(privateJwks);
    } catch (error) {
      throw new NotFoundError(NotFoundError.defaultTitle, {
        detail: (error as Error).message,
      });
    }
    const privateKey = await importJWK(privateKeyJwk);
    const { kid } = privateKeyJwk;
    const issuerMockKeyPair = await getKeyPair(this.issuerMockPrivateKey);

    let issuerState: string | undefined;
    try {
      issuerState = (
        (await this.db.get({ did, issuerState: true })) as IssuerState
      ).issuerState;
    } catch {
      // do nothing
    }

    return authorize(
      this.db as LevelAuth,
      (key: LevelDbKey, ttl: number = Number.MAX_SAFE_INTEGER) => {
        this.addKeyToCacheManager(key, ttl);
      },
      did,
      url,
      kid,
      privateKey,
      query,
      issuerMockKeyPair.publicKeyJwk,
      issuerState
    );
  }

  /**
   * Get Authorization Request by ID.
   */
  async getRequestById(did: string, requestId: string): Promise<string> {
    try {
      return await getRequestById(this.db as LevelAuth, did, requestId);
    } catch (error) {
      throw new NotFoundError(NotFoundError.defaultTitle, {
        detail: `No Authorization Request found with the ID ${requestId}`,
      });
    }
  }

  /**
   * Process /direct_post request.
   *
   * @returns The location URI based on the Authentication Request `redirect_uri`, with `code` and `state` params.
   */
  async directPost(did: string, query: unknown): Promise<string> {
    const url = `${this.clientMockUri}/${did}`;
    try {
      return await directPost(
        this.db as LevelAuth,
        (key: LevelDbKey, ttl: number = Number.MAX_SAFE_INTEGER) => {
          this.addKeyToCacheManager(key, ttl);
        },
        did,
        url,
        this.ebsiAuthority,
        new PEXv2(),
        this.ebsiResolver,
        this.keyResolver,
        this.timeout,
        query
      );
    } catch (error) {
      if (error instanceof Error) {
        throw new BadRequestError(BadRequestError.defaultTitle, {
          detail: error.message,
        });
      }
      throw error;
    }
  }

  /**
   * Process /token request.
   * Access Token is delivered as a response payload from a successful Token Endpoint initiation.
   * `c_nonce` (Challenge Nonce) must be stored until a new one is given.
   *
   * @see https://www.rfc-editor.org/rfc/rfc6749#section-4.1.4
   *
   * @param body The POST /token request payload
   * @returns A token response.
   */
  async token(did: string, query: unknown): Promise<TokenResponse> {
    const url = `${this.clientMockUri}/${did}`;
    let privateKeyJwk: JWKWithKid;
    try {
      const privateJwks = await getPrivateJwks(this.db as LevelAuth, did);
      privateKeyJwk = getPrivateKeyJwk(privateJwks);
    } catch (error) {
      throw new NotFoundError(NotFoundError.defaultTitle, {
        detail: (error as Error).message,
      });
    }
    const privateKey = await importJWK(privateKeyJwk);
    const { kid } = privateKeyJwk;

    return token(
      this.db as LevelAuth,
      did,
      url,
      kid,
      privateKey,
      query,
      this.timeout
    );
  }

  async preregisterAttribute(
    serverDid: string,
    privateKeyES256: JWKWithKid,
    privateKeyES256K: JWKWithKid,
    accreditationUrl: string,
    subjectDid: string,
    issuerType: number,
    attributeId: string
  ): Promise<void> {
    const privateKeyHex = Buffer.from(
      base64url.baseDecode(privateKeyES256K.d ?? "")
    ).toString("hex");
    const { d, ...publicKey256 } = privateKeyES256;
    const subject: EbsiIssuer = {
      did: serverDid,
      kid: `${serverDid}#${publicKey256.kid}`,
      publicKeyJwk: publicKey256,
      privateKeyJwk: privateKeyES256,
      alg: "ES256",
    };

    const accessToken = await this.ebsiAuthorisationService.getAccessToken(
      "tir_write",
      subject
    );

    let taoAttributeId = accreditationUrl.slice(
      accreditationUrl.lastIndexOf("/") + 1
    );

    if (!taoAttributeId.startsWith("0x"))
      taoAttributeId = `0x${taoAttributeId}`;

    try {
      await preregisterAttribute(
        serverDid,
        taoAttributeId,
        privateKeyHex,
        this.trustedIssuersRegistryApiJsonrpcUrl,
        this.ledgerApiUrl,
        this.logger,
        subjectDid,
        issuerType,
        attributeId,
        accessToken
      );
    } catch (e) {
      throw new InternalServerError(undefined, {
        detail: e instanceof Error ? e.message : "unknown error",
      });
    }
  }

  getCredentialIssuerMetadata(did: string): CredentialIssuerMetadata {
    return {
      credential_issuer: `${this.clientMockUri}/${did}`,
      credential_endpoint: `${this.clientMockUri}/${did}/credential`,
      deferred_credential_endpoint: `${this.clientMockUri}/${did}/credential_deferred`,
      credentials_supported: [
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          display: [
            {
              name: "Verifiable Authorisation to onboard",
              locale: "en-GB",
            },
          ],
        },
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAccreditation",
            "VerifiableAccreditationToAttest",
          ],
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          display: [
            {
              name: "Verifiable Accreditation to attest",
              locale: "en-GB",
            },
          ],
        },
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAccreditation",
            "VerifiableAccreditationToAccredit",
          ],
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          display: [
            {
              name: "Verifiable Accreditation to accredit",
              locale: "en-GB",
            },
          ],
        },
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "CTRevocable",
          ],
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          display: [
            {
              name: "Verifiable Accreditation to attest",
              locale: "en-GB",
            },
          ],
        },
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "CTWalletSameInTime",
          ],
          display: [
            {
              name: "Conformance Testing CTWalletSameInTime credential",
              locale: "en-GB",
            },
          ],
        },
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "CTWalletSameDeferred",
          ],
          display: [
            {
              name: "Conformance Testing CTWalletSameDeferred credential",
              locale: "en-GB",
            },
          ],
        },
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "CTWalletSamePreAuthorised",
          ],
          display: [
            {
              name: "Conformance Testing CTWalletSamePreAuthorised credential",
              locale: "en-GB",
            },
          ],
        },
      ],
    };
  }

  async postCredential(
    did: string,
    authorizationHeader: string,
    rawRequestBody: unknown
  ): Promise<CredentialResponse | DeferredCredentialResponse> {
    const url = `${this.clientMockUri}/${did}`;
    let privateJwks: JWKWithKid[];
    let privateKeyES256: JWKWithKid;
    try {
      privateJwks = await getPrivateJwks(this.db as LevelAuth, did);
      privateKeyES256 = getPrivateKeyJwk(privateJwks, "ES256");
    } catch (error) {
      throw new NotFoundError(NotFoundError.defaultTitle, {
        detail: (error as Error).message,
      });
    }

    let attributeUrl = "";
    try {
      attributeUrl = (
        (await this.db.get({ did, attributeUrl: true })) as AttributeUrl
      ).attributeUrl;
    } catch {
      // No accreditation has been registered. Create VC without any accreditation.
    }

    const { d, ...publicKeyES256 } = privateKeyES256;
    const { credentialRequest, accessTokenPayload } =
      await validatePostCredential(
        this.db as LevelIssuer,
        did,
        url,
        publicKeyES256,
        this.ebsiResolver,
        this.keyResolver,
        this.timeout,
        authorizationHeader,
        rawRequestBody
      );

    // Store c_nonce to prevent replay attacks
    const dbKey1 = {
      did,
      nonceAccessToken: accessTokenPayload.claims.c_nonce,
    };
    await this.db.put(dbKey1, { nonce: accessTokenPayload.claims.c_nonce });
    const now = Math.floor(Date.now() / 1000);
    this.addKeyToCacheManager(dbKey1, accessTokenPayload.exp - now); // TTL = remaining time before AT expires

    const additionalVcPayload: Pick<
      EbsiVerifiableAttestation,
      "credentialStatus"
    > = {};
    if (credentialRequest.types.includes("CTRevocable")) {
      let proxyId = "";
      try {
        proxyId = ((await this.db.get({ did, proxyId: true })) as ProxyId)
          .proxyId;
      } catch (error) {
        throw new NotFoundError(NotFoundError.defaultTitle, {
          detail: `No proxy found for ${did}`,
        });
      }
      const statusListCredential = `${this.trustedIssuersRegistryApiUrl}/${did}/proxies/${proxyId}/credentials/status/1`;
      const statusListIndex = (
        createHash("sha256")
          .update(accessTokenPayload.sub, "utf8")
          .digest()
          .slice(0, 6)
          .readUInt32BE() % 131072
      ).toString();
      additionalVcPayload.credentialStatus = {
        id: statusListCredential,
        type: "StatusList2021Entry",
        statusPurpose: "revocation",
        statusListIndex,
        statusListCredential,
      };
    }

    let kid = "";
    if (did.startsWith("did:key:")) {
      const methodSpecificIdentifier = did.replace("did:key:", "");
      kid = `${did}#${methodSpecificIdentifier}`;
    } else {
      kid = `${did}#${privateKeyES256.kid}`;
    }

    const { reservedAttributeId, vcJwt } = await issueCredential(
      did,
      kid,
      privateKeyES256,
      attributeUrl,
      this.authorisationCredentialSchema,
      this.createVerifiableCredentialOptions,
      additionalVcPayload,
      credentialRequest,
      accessTokenPayload
    );

    if (
      (
        [
          "VerifiableAuthorisationForTrustChain",
          "VerifiableAccreditationToAccredit",
          "VerifiableAccreditationToAttest",
        ] as const
      ).some((type) => credentialRequest.types.includes(type))
    ) {
      let privateKeyES256K: JWKWithKid;
      try {
        privateKeyES256K = getPrivateKeyJwk(privateJwks, "ES256K");
      } catch (error) {
        throw new NotFoundError(NotFoundError.defaultTitle, {
          detail: (error as Error).message,
        });
      }

      if (
        credentialRequest.types.includes("VerifiableAuthorisationForTrustChain")
      ) {
        await this.preregisterAttribute(
          did,
          privateKeyES256,
          privateKeyES256K,
          attributeUrl,
          accessTokenPayload.sub,
          1,
          reservedAttributeId
        );

        // Create random acceptance token
        const acceptanceToken = Buffer.from(randomBytes(32)).toString(
          "base64url"
        );

        // Store VC JWT in cache
        const dbKey2 = { did, deferredCredential: acceptanceToken };
        await this.db.put(dbKey2, { vcJwt });
        this.addKeyToCacheManager(dbKey2, 300_000); // 5 minutes

        // Return acceptance token
        return {
          acceptance_token: acceptanceToken,
        };
      }

      if (
        credentialRequest.types.includes("VerifiableAccreditationToAccredit")
      ) {
        await this.preregisterAttribute(
          did,
          privateKeyES256,
          privateKeyES256K,
          attributeUrl,
          accessTokenPayload.sub,
          2,
          reservedAttributeId
        );
      } else if (
        credentialRequest.types.includes("VerifiableAccreditationToAttest")
      ) {
        await this.preregisterAttribute(
          did,
          privateKeyES256,
          privateKeyES256K,
          attributeUrl,
          accessTokenPayload.sub,
          3,
          reservedAttributeId
        );
      }
    }

    if (credentialRequest.types.includes("CTWalletSameDeferred")) {
      // Create random acceptance token
      const acceptanceToken = Buffer.from(randomBytes(32)).toString(
        "base64url"
      );

      // Store VC JWT in cache
      const dbKey2 = { did, deferredCredential: acceptanceToken };
      await this.db.put(dbKey2, {
        vcJwt,
        notBefore: Date.now() + 5000, // Available in 5 seconds
      });
      this.addKeyToCacheManager(dbKey2, 300_000); // 5 minutes

      // Return acceptance token
      return {
        acceptance_token: acceptanceToken,
      };
    }

    return {
      format: "jwt_vc",
      credential: vcJwt,
    };
  }

  async postCredentialDeferred(
    did: string,
    authorizationHeader: string
  ): Promise<CredentialResponse> {
    if (!authorizationHeader) {
      throw new CredentialError("invalid_token", {
        errorDescription: "Authorization header is missing",
      });
    }

    if (!authorizationHeader.startsWith("Bearer ")) {
      throw new CredentialError("invalid_token", {
        errorDescription: "Authorization header must contain a Bearer token",
      });
    }

    // Get the deferred credential from the cache
    const accessToken = authorizationHeader.replace("Bearer ", "");
    const dbKey2 = { did, deferredCredential: accessToken };
    const deferredCredential = (await this.db.get(dbKey2)) as CachedVcJwt;

    if (!deferredCredential) {
      throw new CredentialError("invalid_token", {
        errorDescription: "Deferred credential not found",
      });
    }

    if (
      deferredCredential.notBefore &&
      Date.now() < deferredCredential.notBefore
    ) {
      throw new CredentialError("invalid_request", {
        errorDescription: "Deferred credential not available yet",
      });
    }

    await this.db.del(dbKey2);

    return {
      format: "jwt_vc",
      credential: deferredCredential.vcJwt,
    };
  }

  async updateList(args: UpdateListDto): Promise<void> {
    const { did, id, position, value } = args;

    // get list from the database
    let encodedList: string;
    try {
      const data = (await this.db.get({
        did,
        statusList2021Id: id,
      })) as EncodedList;
      if (!data.encodedList) throw new Error();
      encodedList = data.encodedList;
    } catch (error) {
      const list = new Uint8Array(16384);
      encodedList = base64.baseEncode(gzip(list));
    }

    const decodedList = base64.baseDecode(encodedList);

    // Inflate decodedList
    const inflator = new Inflate();
    inflator.push(decodedList);

    if (inflator.err) {
      throw new BadRequestError(BadRequestError.defaultTitle, {
        detail:
          "The StatusList2021Credential is not a valid GZIP-compressed bitstring",
      });
    }

    const inflatedList = inflator.result as Uint8Array;

    // update bit
    const bytePosition = Math.floor(position / 8);
    const bitPosition = position % 8;

    const byte = inflatedList.at(bytePosition);

    if (byte === undefined) {
      throw new BadRequestError(BadRequestError.defaultTitle, {
        detail: `Invalid position ${position}`,
      });
    }

    const byteAsBitstring = byte.toString(2).padStart(8, "0");
    const bitstringStart = byteAsBitstring.substring(0, bitPosition);
    const bitstringEnd = byteAsBitstring.substring(bitPosition + 1);
    const updatedByteAsBitstring = `${bitstringStart}${value}${bitstringEnd}`;
    inflatedList[bytePosition] = parseInt(updatedByteAsBitstring, 2);

    // save list
    encodedList = base64.baseEncode(gzip(inflatedList));
    await this.db.put(
      {
        did,
        statusList2021Id: id,
      },
      { encodedList }
    );
  }

  async getCredentialStatusListJwt(did: string, id: string): Promise<string> {
    const jwks = await getPrivateJwks(this.db as LevelAuth, did);
    const privateKeyJwk = jwks.find(
      (k) => k.kty === "EC" && k.crv === "P-256" && !!k.d
    );

    if (!privateKeyJwk) {
      throw new NotFoundError(NotFoundError.defaultTitle, {
        detail: `No ES256 private key found for ${did}`,
      });
    }

    const { d, ...publicKeyJwk } = privateKeyJwk;

    const issuer: EbsiIssuer = {
      did,
      kid: `${did}#${(privateKeyJwk as { kid: string }).kid}`,
      publicKeyJwk,
      privateKeyJwk,
      alg: "ES256",
    };

    const options = {
      ebsiAuthority: this.ebsiAuthority,
      skipValidation: true,
    };

    // get list from the database
    let encodedList: string;
    try {
      const data = (await this.db.get({
        did,
        statusList2021Id: id,
      })) as EncodedList;
      if (!data.encodedList) throw new Error();
      encodedList = data.encodedList;
    } catch (error) {
      throw new NotFoundError(NotFoundError.defaultTitle, {
        detail: `No list stored for ${did} id: ${id}`,
      });
    }

    // issued 1 minute in the past to avoid issues with clocks
    const iat = Math.round(Date.now() / 1000 - 60);
    const exp = iat + 365 * 24 * 3600;
    const issuanceDate = `${new Date(iat * 1000).toISOString().slice(0, -5)}Z`;
    const expirationDate = `${new Date(exp * 1000)
      .toISOString()
      .slice(0, -5)}Z`;
    const credentialStatusListPayload = {
      "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://w3id.org/vc/status-list/2021/v1",
      ],
      id: `${this.clientMockUri}/${did}/credentials/status/${id}`,
      type: [
        "VerifiableCredential",
        "VerifiableAttestation",
        "StatusList2021Credential",
      ],
      issuer: issuer.did,
      issuanceDate,
      issued: issuanceDate,
      validFrom: issuanceDate,
      expirationDate,
      validUntil: expirationDate,
      credentialSubject: {
        id: `${this.clientMockUri}/${did}/credentials/status/${id}#list`,
        type: "StatusList2021",
        statusPurpose: "revocation",
        encodedList,
      },
      credentialSchema: [
        {
          id: this.statusList2021CredentialSchemaUrl,
          type: "FullJsonSchemaValidator2021",
        },
      ],
    };

    return createVerifiableCredentialJwt(
      credentialStatusListPayload,
      issuer,
      options
    );
  }
}

export default ClientMockService;
