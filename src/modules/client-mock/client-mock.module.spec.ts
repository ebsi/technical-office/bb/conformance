import {
  jest,
  describe,
  beforeAll,
  afterEach,
  afterAll,
  it,
  expect,
} from "@jest/globals";
import { Test, TestingModule } from "@nestjs/testing";
import { Logger } from "@nestjs/common";
import type { INestApplication, HttpServer } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { decodeJwt } from "jose";
import type { FastifyInstance } from "fastify";
import nock from "nock";
import request from "supertest";
import { ClientMockModule } from "./client-mock.module";
import { configureApp } from "../../../tests/utils/app";
import type { ApiConfig } from "../../config/configuration";

describe("Client Mock Module", () => {
  let app: INestApplication;
  let server: HttpServer;
  let configService: ConfigService<ApiConfig, true>;
  let conformanceDomain: string;
  let apiUrlPrefix: string;

  const did = "did:ebsi:zugd8WN9Q3W2nCLC7ZMMUEK";
  const publicKeyJwk = {
    kty: "EC",
    x: "a50FlP4lKXxxEDfzP6mBCy7lye8eOH-wXTzbzywFnfk",
    y: "SvmxVSt296G6YjybxtehZHtMgHDSFRDnbC8R9YbRGCw",
    crv: "P-256",
    kid: `${did}#PQqRfHDc6fDBdQkWdkkbPz9FVi7K_Mva1kgIE1ybiqc`,
  };
  const privateKeyJwk = {
    ...publicKeyJwk,
    d: "vjKf6akzWkmZt6wnugMEze5XylwBHKrjNB_3oACbmHU",
  };

  beforeAll(async () => {
    // Disable external requests
    nock.disableNetConnect();
    // Allow localhost connections so we can test local routes and mock servers.
    nock.enableNetConnect("127.0.0.1");

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [ClientMockModule],
    }).compile();

    app = await configureApp(moduleFixture);

    Logger.overrideLogger(false);

    await app.init();
    await (app.getHttpAdapter().getInstance() as FastifyInstance).ready();
    server = app.getHttpServer() as HttpServer;

    configService =
      moduleFixture.get<ConfigService<ApiConfig, true>>(ConfigService);
    conformanceDomain = configService.get<string>("conformanceDomain");
    apiUrlPrefix = configService.get<string>("apiUrlPrefix");
  });

  afterEach(() => {
    nock.cleanAll();
    jest.restoreAllMocks();
  });

  afterAll(async () => {
    nock.restore();

    // Avoid jest open handle error
    await new Promise((r) => {
      setTimeout(r, 500);
    });
    await app.close();
  });

  describe("GET /client-mock/jwks", () => {
    it("should return the OP's JWKS", async () => {
      expect.assertions(4);

      const responseInitiate = await request(server)
        .post("/client-mock/initiate")
        .send({
          did,
          keys: [privateKeyJwk],
        });
      expect(responseInitiate.status).toBe(204);

      const response = await request(server).get(`/client-mock/${did}/jwks`);

      expect(response.body).toStrictEqual({
        keys: [publicKeyJwk],
      });

      expect(response.status).toBe(200);
      expect(
        (response.headers as Record<string, unknown>)["content-type"]
      ).toBe("application/jwk-set+json; charset=utf-8");
    });

    it("should return an error 404 when the did is not found", async () => {
      const response = await request(server).get(
        "/client-mock/did:ebsi:unknown/jwks"
      );

      expect(response.body).toStrictEqual({
        detail: "No JWKS found for did:ebsi:unknown",
        status: 404,
        title: "Not Found",
        type: "about:blank",
      });
      expect(response.status).toBe(404);
    });
  });

  describe("Credential Status List 2021", () => {
    it("should get a credential status list 2021", async () => {
      expect.assertions(5);

      const responseInitiate = await request(server)
        .post("/client-mock/initiate")
        .send({
          did,
          keys: [privateKeyJwk],
        });
      expect(responseInitiate.status).toBe(204);

      const responseUpdateList = await request(server)
        .post("/client-mock/updateList")
        .send({
          did,
          id: "1",
          position: 50,
          value: 1,
        });
      expect(responseUpdateList.status).toBe(204);

      const response = await request(server).get(
        `/client-mock/${did}/credentials/status/1`
      );
      expect(response.text).toBeDefined();
      expect(response.status).toBe(200);

      expect(decodeJwt(response.text)).toStrictEqual(
        expect.objectContaining({
          iss: did,
          vc: expect.objectContaining({
            "@context": [
              "https://www.w3.org/2018/credentials/v1",
              "https://w3id.org/vc/status-list/2021/v1",
            ],
            credentialSchema: expect.arrayContaining([]),
            credentialSubject: {
              encodedList: expect.any(String),
              id: expect.any(String),
              statusPurpose: "revocation",
              type: "StatusList2021",
            },
            issuer: did,
            type: [
              "VerifiableCredential",
              "VerifiableAttestation",
              "StatusList2021Credential",
            ],
          }),
        })
      );
    });
  });

  describe("GET /client-mock/:did/.well-known/openid-configuration", () => {
    it("should return the well-known OpenID configuration", async () => {
      expect.assertions(2);

      const response = await request(server).get(
        `/client-mock/${did}/.well-known/openid-configuration`
      );

      const clientServerUrl = `${conformanceDomain}${apiUrlPrefix}/client-mock/${did}`;

      expect(response.body).toStrictEqual({
        redirect_uris: [`${clientServerUrl}/direct_post`],
        issuer: `${clientServerUrl}`,
        authorization_endpoint: `${clientServerUrl}/authorize`,
        token_endpoint: `${clientServerUrl}/token`,
        jwks_uri: `${clientServerUrl}/jwks`,
        scopes_supported: ["openid"],
        response_types_supported: ["vp_token", "id_token"],
        response_modes_supported: ["query"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
        request_object_signing_alg_values_supported: ["ES256"],
        request_parameter_supported: true,
        request_uri_parameter_supported: true,
        token_endpoint_auth_methods_supported: ["private_key_jwt"],
        request_authentication_methods_supported: {
          authorization_endpoint: ["request_object"],
        },
        vp_formats_supported: {
          jwt_vp: {
            alg_values_supported: ["ES256"],
          },
          jwt_vc: {
            alg_values_supported: ["ES256"],
          },
        },
        subject_syntax_types_supported: ["did:key", "did:ebsi"],
        subject_syntax_types_discriminations: [
          "did:key:jwk_jcs-pub",
          "did:ebsi:v1",
        ],
        subject_trust_frameworks_supported: ["ebsi"],
        id_token_types_supported: [
          "subject_signed_id_token",
          "attester_signed_id_token",
        ],
      });

      expect(response.status).toBe(200);
    });
  });
});
