import { CacheModule, Module } from "@nestjs/common";
import { CheckService } from "./check.service";
import { CheckController } from "./check.controller";
import { DataStoreModule } from "../data-store/data-store.module";
import { IssuerMockModule } from "../issuer-mock/issuer-mock.module";
import { ApiConfigModule, cacheConfig } from "../../config/configuration";

@Module({
  imports: [
    ApiConfigModule,
    CacheModule.register(cacheConfig),
    DataStoreModule,
    IssuerMockModule,
  ],
  controllers: [CheckController],
  providers: [CheckService],
  exports: [CheckService],
})
export class CheckModule {}

export default CheckModule;
