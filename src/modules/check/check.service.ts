import { Inject, Injectable, Logger } from "@nestjs/common";
import { Resolver } from "did-resolver";
import { getResolver } from "@cef-ebsi/ebsi-did-resolver";
import { ConfigService } from "@nestjs/config";
import axios from "axios";
import type { ApiConfig } from "../../config/configuration";
import { DataStoreService } from "../data-store/data-store.service";
import { formatZodError } from "../../shared/utils";
import {
  RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
  RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
  TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
  TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
  TAO_REVOKE_RIGHTS_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
  TI_REGISTER_DID,
  TI_REGISTER_VERIFIABLE_ACCREDITATION_TO_ATTEST,
  TI_REQUEST_CT_REVOCABLE,
  TI_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST,
  TI_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD,
  TI_REVOKE_CT_REVOCABLE,
  TI_VALIDATE_CT_REVOCABLE,
  REQUEST_CTAA_QUALIFICATION_CREDENTIAL,
  CT_WALLET_CROSS_IN_TIME,
  CT_WALLET_CROSS_DEFERRED,
  CT_WALLET_SAME_IN_TIME,
  CT_WALLET_SAME_DEFERRED,
  CT_WALLET_CROSS_PRE_AUTHORISED,
  CT_WALLET_SAME_PRE_AUTHORISED,
  REQUEST_CT_WALLET_QUALIFICATION_CREDENTIAL,
  ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_IN_TIME,
  ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_IN_TIME,
  ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_DEFERRED,
  ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_DEFERRED,
  ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_PRE_AUTHORISED,
  ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_PRE_AUTHORISED,
  REQUEST_CT_ISSUE_TO_HOLDER_QUALIFICATION_CREDENTIAL,
  VERIFIER_ID_TOKEN_EXCHANGE,
  VERIFIER_VP_VALID_VC,
  VERIFIER_VP_EXPIRED_VC,
  VERIFIER_VP_REVOKED_VC,
  VERIFIER_VP_NOT_YET_VALID_VC,
} from "../../shared/constants";
import type {
  CheckResult,
  IntentName,
  IssuerAttribute,
} from "../../shared/interfaces";
import { didSchema, requestSchema } from "./validators";
import { IssuerMockService } from "../issuer-mock/issuer-mock.service";
import { ISSUER_MOCK_CHECKS_LIST } from "../issuer-mock/issuer-mock.constants";
import type { ChecksMap } from "./check.interface";

@Injectable()
export class CheckService {
  /**
   * Mapping between intents and their corresponding check functions
   */
  private readonly checksMap: ChecksMap;

  private readonly logger = new Logger(CheckService.name);

  constructor(
    private dataStoreService: DataStoreService,
    @Inject(IssuerMockService) private issuerMockService: IssuerMockService,
    @Inject(ConfigService)
    private configService: ConfigService<ApiConfig, true>
  ) {
    this.checksMap = {
      [TI_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD]:
        this.checkEventsForIntent.bind(this),
      [TI_REGISTER_DID]: this.checkIfDidExists.bind(this),
      [TI_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST]:
        this.checkEventsForIntent.bind(this),
      [TI_REGISTER_VERIFIABLE_ACCREDITATION_TO_ATTEST]:
        this.checkRegister.bind(this),
      [TI_REQUEST_CT_REVOCABLE]: this.callIssuerMock.bind(this),
      [TI_VALIDATE_CT_REVOCABLE]: this.callIssuerMock.bind(this),
      [TI_REVOKE_CT_REVOCABLE]: this.callIssuerMock.bind(this),
      [TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT]:
        this.checkRegister.bind(this),
      [TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT]:
        this.checkEventsForIntent.bind(this),
      [TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT]:
        this.callIssuerMock.bind(this),
      [TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT]:
        this.callIssuerMock.bind(this),
      [TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT]:
        this.callIssuerMock.bind(this),
      [TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT]:
        this.callIssuerMock.bind(this),
      [TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT]:
        this.callIssuerMock.bind(this),
      [TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT]:
        this.callIssuerMock.bind(this),
      [TAO_REVOKE_RIGHTS_SUBACCOUNT]: this.callIssuerMock.bind(this),
      [RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN]:
        this.checkEventsForIntent.bind(this),
      [RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN]:
        this.checkRegister.bind(this),
      [REQUEST_CTAA_QUALIFICATION_CREDENTIAL]:
        this.checkEventsForIntent.bind(this),
      [CT_WALLET_CROSS_IN_TIME]: this.checkEventsForIntent.bind(this),
      [CT_WALLET_CROSS_DEFERRED]: this.checkEventsForIntent.bind(this),
      [CT_WALLET_SAME_IN_TIME]: this.checkEventsForIntent.bind(this),
      [CT_WALLET_SAME_DEFERRED]: this.checkEventsForIntent.bind(this),
      [CT_WALLET_CROSS_PRE_AUTHORISED]: this.checkEventsForIntent.bind(this),
      [CT_WALLET_SAME_PRE_AUTHORISED]: this.checkEventsForIntent.bind(this),
      [REQUEST_CT_WALLET_QUALIFICATION_CREDENTIAL]:
        this.checkEventsForIntent.bind(this),
      [ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_IN_TIME]:
        this.callIssuerMock.bind(this),
      [ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_IN_TIME]:
        this.callIssuerMock.bind(this),
      [ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_DEFERRED]:
        this.callIssuerMock.bind(this),
      [ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_DEFERRED]:
        this.callIssuerMock.bind(this),
      [ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_PRE_AUTHORISED]:
        this.callIssuerMock.bind(this),
      [ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_PRE_AUTHORISED]:
        this.callIssuerMock.bind(this),
      [REQUEST_CT_ISSUE_TO_HOLDER_QUALIFICATION_CREDENTIAL]:
        this.checkEventsForIntent.bind(this),
      [VERIFIER_ID_TOKEN_EXCHANGE]: this.callIssuerMock.bind(this),
      [VERIFIER_VP_VALID_VC]: this.callIssuerMock.bind(this),
      [VERIFIER_VP_EXPIRED_VC]: this.callIssuerMock.bind(this),
      [VERIFIER_VP_REVOKED_VC]: this.callIssuerMock.bind(this),
      [VERIFIER_VP_NOT_YET_VALID_VC]: this.callIssuerMock.bind(this),
    };
  }

  async dispatch(body: unknown): Promise<CheckResult> {
    const parsedCheckBody = requestSchema.safeParse(body);
    if (!parsedCheckBody.success) {
      return {
        success: false,
        errors: formatZodError(parsedCheckBody.error),
      };
    }
    const { intent, data } = parsedCheckBody.data;

    const result = await this.checksMap[intent](intent, data);

    this.logger.log(
      `Test Data {"intent": "${intent}", "data": ${JSON.stringify(
        data
      )}, "result": ${JSON.stringify(result)} } End Test Data`
    );

    return result;
  }

  resolveDid(did: string) {
    const resolverConfig = {
      registry: this.configService.get<string>("didRegistryApiUrl"),
    };
    const ebsiResolver = getResolver(resolverConfig);
    const didResolver = new Resolver(ebsiResolver);
    return didResolver.resolve(did);
  }

  async checkIfDidExists(
    _intent: string,
    data: Record<string, unknown>
  ): Promise<CheckResult> {
    const parsedData = didSchema.safeParse(data);

    if (!parsedData.success) {
      return {
        success: false,
        errors: formatZodError(parsedData.error, "data"),
      };
    }

    const { did } = parsedData.data;

    const { didResolutionMetadata } = await this.resolveDid(did);

    if (didResolutionMetadata.error) {
      return {
        success: false,
        errors: [didResolutionMetadata.error],
      };
    }

    return { success: true };
  }

  async checkRegister(
    intent: string,
    data: Record<string, unknown>
  ): Promise<CheckResult> {
    const parsedData = didSchema.safeParse(data);

    if (!parsedData.success) {
      return {
        success: false,
        errors: formatZodError(parsedData.error, "data"),
      };
    }

    const { did } = parsedData.data;
    const storedCachedResult = await this.dataStoreService.getVCWithLinkedAttr(
      `${intent}_${did}`
    );

    if (!storedCachedResult) {
      return {
        success: false,
        errors: ["No stored VC was found for the DID"],
      };
    }

    try {
      const attributeResponse = await axios.get<IssuerAttribute>(
        `${this.configService.get<string>(
          "trustedIssuersRegistryApiUrl"
        )}/${did}/attributes/${storedCachedResult.attributeId}`
      );

      if (attributeResponse.data.attribute.body !== storedCachedResult.vc) {
        return {
          success: false,
          errors: ["Stored VC does not match VC from issuer registry"],
        };
      }
    } catch (ex) {
      return {
        success: false,
        errors: ["VC was not found in registry"],
      };
    }

    return { success: true };
  }

  async checkEventsForIntent(
    intent: string,
    data: Record<string, unknown>
  ): Promise<CheckResult> {
    const parsedData = didSchema.safeParse(data);

    if (!parsedData.success) {
      return {
        success: false,
        errors: formatZodError(parsedData.error, "data"),
      };
    }

    const { did } = parsedData.data;

    const events = await this.dataStoreService.getEvents(did);

    if (events === undefined || events.length === 0) {
      return {
        success: false,
        errors: ["No events for intents registered!"],
      };
    }

    const event = events
      ?.sort((a, b) => b.timestamp - a.timestamp)
      .find((item) => item.intent === intent);

    if (!event) {
      return {
        success: false,
        errors: ["No events for intents registered!"],
      };
    }

    if (!event.success) {
      return {
        success: false,
        errors: event.errors,
      };
    }

    return { success: true };
  }

  async callIssuerMock(
    intent: IntentName,
    data: Record<string, unknown>
  ): Promise<CheckResult> {
    if (!ISSUER_MOCK_CHECKS_LIST.includes(intent)) {
      return {
        success: false,
        errors: ["Not implemented"],
      };
    }

    const method =
      this.issuerMockService.checksMap[
        intent as (typeof ISSUER_MOCK_CHECKS_LIST)[number]
      ];

    return method(data);
  }
}

export default CheckService;
