import {
  afterAll,
  afterEach,
  beforeAll,
  describe,
  expect,
  it,
  jest,
} from "@jest/globals";
import { randomBytes } from "node:crypto";
import type { NestFastifyApplication } from "@nestjs/platform-fastify";
import { HttpServer, Logger } from "@nestjs/common";
import { Test, TestingModule } from "@nestjs/testing";
import type { FastifyInstance } from "fastify";
import request from "supertest";
import nock from "nock";
import { util } from "@cef-ebsi/ebsi-did-resolver";
import { ConfigService } from "@nestjs/config";
import {
  ApiConfig,
  DIDR_API_PATH,
  TIR_API_PATH,
} from "../../config/configuration";
import { CheckModule } from "./check.module";
import { configureApp } from "../../../tests/utils/app";
import { DataStoreService } from "../data-store/data-store.service";
import {
  INTENTS_LIST,
  TI_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD,
  TI_REGISTER_DID,
  TI_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST,
  TI_REGISTER_VERIFIABLE_ACCREDITATION_TO_ATTEST,
  TI_REQUEST_CT_REVOCABLE,
  TI_VALIDATE_CT_REVOCABLE,
  TI_REVOKE_CT_REVOCABLE,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
  TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
  TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
  RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
  RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
  REQUEST_CTAA_QUALIFICATION_CREDENTIAL,
  TAO_REVOKE_RIGHTS_SUBACCOUNT,
  CT_WALLET_CROSS_IN_TIME,
  CT_WALLET_CROSS_DEFERRED,
  CT_WALLET_SAME_IN_TIME,
  CT_WALLET_SAME_DEFERRED,
  CT_WALLET_CROSS_PRE_AUTHORISED,
  CT_WALLET_SAME_PRE_AUTHORISED,
  REQUEST_CT_WALLET_QUALIFICATION_CREDENTIAL,
  ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_IN_TIME,
  ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_IN_TIME,
  ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_DEFERRED,
  ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_DEFERRED,
  ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_PRE_AUTHORISED,
  ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_PRE_AUTHORISED,
  REQUEST_CT_ISSUE_TO_HOLDER_QUALIFICATION_CREDENTIAL,
  VERIFIER_ID_TOKEN_EXCHANGE,
  VERIFIER_VP_VALID_VC,
  VERIFIER_VP_EXPIRED_VC,
  VERIFIER_VP_REVOKED_VC,
  VERIFIER_VP_NOT_YET_VALID_VC,
} from "../../shared/constants";
import { IssuerMockService } from "../issuer-mock/issuer-mock.service";

describe("Check Module", () => {
  let app: NestFastifyApplication;
  let server: HttpServer;
  let dataStoreService: DataStoreService;
  let issuerMockService: IssuerMockService;
  let configService: ConfigService<ApiConfig, true>;

  beforeAll(async () => {
    nock.disableNetConnect();
    nock.enableNetConnect("127.0.0.1");

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [CheckModule],
    }).compile();

    Logger.overrideLogger(false);

    app = await configureApp(moduleFixture);

    await app.init();
    await (app.getHttpAdapter().getInstance() as FastifyInstance).ready();
    dataStoreService = moduleFixture.get<DataStoreService>(DataStoreService);
    configService =
      moduleFixture.get<ConfigService<ApiConfig, true>>(ConfigService);
    server = app.getHttpServer() as HttpServer;

    issuerMockService = moduleFixture.get<IssuerMockService>(IssuerMockService);
  });

  afterAll(async () => {
    await new Promise<void>((resolve) => {
      setTimeout(() => resolve(), 500);
    });
    nock.restore();
    await app.close();
  });

  afterEach(() => {
    nock.cleanAll();
    jest.restoreAllMocks();
  });

  describe("POST /check", () => {
    describe("with any intent", () => {
      it("should return { success: false } and errors if data is missing and intent is invalid", async () => {
        const response = await request(server).post("/check").send({
          intent: "unknown",
          // Data is missing
        });

        expect(response.body).toEqual({
          success: false,
          errors: [
            `Validation error. Path: 'intent'. Reason: Invalid enum value. Expected '${INTENTS_LIST.join(
              "' | '"
            )}', received 'unknown'`,
            "Validation error. Path: 'data'. Reason: Required",
          ],
        });
      });
    });

    describe.each([
      TI_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD,
      TI_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST,
      TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
      REQUEST_CTAA_QUALIFICATION_CREDENTIAL,
      CT_WALLET_CROSS_IN_TIME,
      CT_WALLET_CROSS_DEFERRED,
      CT_WALLET_SAME_IN_TIME,
      CT_WALLET_SAME_DEFERRED,
      CT_WALLET_CROSS_PRE_AUTHORISED,
      CT_WALLET_SAME_PRE_AUTHORISED,
      REQUEST_CT_WALLET_QUALIFICATION_CREDENTIAL,
      REQUEST_CT_ISSUE_TO_HOLDER_QUALIFICATION_CREDENTIAL,
    ] as const)(`with intent %s`, (intent) => {
      const did = "did:ebsi:za6JmV84EuYeNirrqpjYcPB";

      it("should return { success: false } for no data found for a did (that exists)", async () => {
        jest
          .spyOn(dataStoreService, "getEvents")
          .mockImplementation(() => Promise.resolve([]));
        const response = await request(server).post("/check").send({
          intent,
          data: {
            did,
          },
        });

        expect(response.body).toEqual({
          success: false,
          errors: ["No events for intents registered!"],
        });
      });

      it("should return latest event registered for a DID and intent", async () => {
        jest.spyOn(dataStoreService, "getEvents").mockImplementation(() =>
          Promise.resolve([
            {
              intent,
              success: true,
              errors: [],
              timestamp: 0,
            },
            {
              intent,
              success: false,
              errors: ["ERROR"],
              timestamp: 1,
            },
          ])
        );
        const response = await request(server).post("/check").send({
          intent,
          data: {
            did,
          },
        });

        expect(response.body).toStrictEqual({
          success: false,
          errors: ["ERROR"],
        });
      });

      it("should return success true for existing intent", async () => {
        jest.spyOn(dataStoreService, "getEvents").mockImplementation(() =>
          Promise.resolve([
            {
              intent,
              success: true,
              timestamp: 0,
            },
          ])
        );
        const response = await request(server).post("/check").send({
          intent,
          data: {
            did,
          },
        });

        expect(response.body).toStrictEqual({
          success: true,
        });
      });
    });

    describe(`with intent ${TI_REGISTER_DID}`, () => {
      it("should return { success: false } if the DID does not resolve", async () => {
        const response = await request(server)
          .post("/check")
          .send({
            intent: TI_REGISTER_DID,
            data: {
              did: "WRONG_DID",
            },
          });

        expect(response.body).toStrictEqual({
          success: false,
          errors: [`invalidDid`],
        });
        expect(response.status).toBe(200);
      });

      it("should return { success: true } if the DID does resolve", async () => {
        const did = util.createDid(randomBytes(16));

        nock(configService.get<string>("domain"))
          .get(`${DIDR_API_PATH}/${did}`)
          .reply(200, {})
          .persist();

        const response = await request(server).post("/check").send({
          intent: TI_REGISTER_DID,
          data: {
            did,
          },
        });

        expect(response.body).toStrictEqual({
          success: true,
        });
        expect(response.status).toBe(200);
      });
    });

    describe.each([
      TI_REGISTER_VERIFIABLE_ACCREDITATION_TO_ATTEST,
      TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
      RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
    ] as const)(`with intent %s`, (intent) => {
      const did = "did:ebsi:za6JmV84EuYeNirrqpjYcPB";

      it("should return { success: false } if no result is found in cache", async () => {
        jest
          .spyOn(dataStoreService, "getVCWithLinkedAttr")
          .mockImplementation(() => Promise.resolve(undefined));

        const response = await request(server).post("/check").send({
          intent,
          data: {
            did,
          },
        });

        expect(response.body).toStrictEqual({
          success: false,
          errors: ["No stored VC was found for the DID"],
        });
      });

      it("should return { success: false } if stored VC does not match VC from issuer registry", async () => {
        jest
          .spyOn(dataStoreService, "getVCWithLinkedAttr")
          .mockImplementation(() =>
            Promise.resolve({
              attributeId: "ATTRIBUTE_ID",
              vc: "VC",
            })
          );

        nock(configService.get<string>("domain"))
          .get(`${TIR_API_PATH}/${did}/attributes/ATTRIBUTE_ID`)
          .reply(200, {
            did,
            attribute: {
              hash: "",
              body: "OTHER_VC",
              issuerType: "",
              tao: "",
              rootTao: "",
            },
          })
          .persist();

        const response = await request(server).post("/check").send({
          intent,
          data: {
            did,
          },
        });

        expect(response.body).toStrictEqual({
          success: false,
          errors: ["Stored VC does not match VC from issuer registry"],
        });
      });

      it("should return { success: false } if TIR API does not return a VC", async () => {
        jest
          .spyOn(dataStoreService, "getVCWithLinkedAttr")
          .mockImplementation(() =>
            Promise.resolve({
              attributeId: "ATTRIBUTE_ID",
              vc: "VC",
            })
          );

        nock(configService.get<string>("domain"))
          .get(`${TIR_API_PATH}/${did}/attributes/ATTRIBUTE_ID`)
          .reply(404)
          .persist();

        const response = await request(server).post("/check").send({
          intent,
          data: {
            did,
          },
        });

        expect(response.body).toStrictEqual({
          success: false,
          errors: ["VC was not found in registry"],
        });
      });

      it("should return success true for existing intent", async () => {
        jest
          .spyOn(dataStoreService, "getVCWithLinkedAttr")
          .mockImplementation(() =>
            Promise.resolve({
              attributeId: "ATTRIBUTE_ID",
              vc: "VC",
            })
          );

        nock(configService.get<string>("domain"))
          .get(`${TIR_API_PATH}/${did}/attributes/ATTRIBUTE_ID`)
          .reply(200, {
            did,
            attribute: {
              hash: "",
              body: "VC",
              issuerType: "",
              tao: "",
              rootTao: "",
            },
          })
          .persist();

        const response = await request(server).post("/check").send({
          intent,
          data: {
            did,
          },
        });

        expect(response.body).toStrictEqual({
          success: true,
        });
      });
    });

    describe(`with intent ${RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN}`, () => {
      const did = "did:ebsi:za6JmV84EuYeNirrqpjYcPB";

      it("should return { success: false } for no data found for a DID (that exists)", async () => {
        jest
          .spyOn(dataStoreService, "getEvents")
          .mockImplementation(() => Promise.resolve([]));
        const response = await request(server).post("/check").send({
          intent: RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
          data: {
            did,
          },
        });

        expect(response.body).toEqual({
          success: false,
          errors: ["No events for intents registered!"],
        });
      });

      it("should return latest event registered for a DID and intent", async () => {
        jest.spyOn(dataStoreService, "getEvents").mockImplementation(() =>
          Promise.resolve([
            {
              intent: RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
              success: true,
              errors: [],
              timestamp: 0,
            },
            {
              intent: RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
              success: false,
              errors: ["ERROR"],
              timestamp: 1,
            },
          ])
        );
        const response = await request(server).post("/check").send({
          intent: RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
          data: {
            did,
          },
        });

        expect(response.body).toStrictEqual({
          success: false,
          errors: ["ERROR"],
        });
      });

      it("should return success true for existing intent", async () => {
        jest.spyOn(dataStoreService, "getEvents").mockImplementation(() =>
          Promise.resolve([
            {
              intent: RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
              success: true,
              timestamp: 0,
            },
          ])
        );
        const response = await request(server).post("/check").send({
          intent: RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
          data: {
            did,
          },
        });

        expect(response.body).toStrictEqual({
          success: true,
        });
      });
    });

    // Checks passed to IssuerMockService
    describe.each([
      TI_REQUEST_CT_REVOCABLE,
      TI_VALIDATE_CT_REVOCABLE,
      TI_REVOKE_CT_REVOCABLE,
      TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
      TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
      TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
      TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
      TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
      TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
      TAO_REVOKE_RIGHTS_SUBACCOUNT,
      ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_IN_TIME,
      ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_IN_TIME,
      ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_DEFERRED,
      ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_DEFERRED,
      ISSUE_TO_HOLDER_INITIATE_CT_WALLET_SAME_PRE_AUTHORISED,
      ISSUE_TO_HOLDER_VALIDATE_CT_WALLET_SAME_PRE_AUTHORISED,
      VERIFIER_ID_TOKEN_EXCHANGE,
      VERIFIER_VP_VALID_VC,
      VERIFIER_VP_EXPIRED_VC,
      VERIFIER_VP_REVOKED_VC,
      VERIFIER_VP_NOT_YET_VALID_VC,
    ] as const)(`with intent %s`, (intent) => {
      it("should return what IssuerMockService returns", async () => {
        expect.assertions(2);

        jest.spyOn(issuerMockService.checksMap, intent).mockImplementation(() =>
          Promise.resolve({
            success: false,
            errors: ["ERROR"],
          })
        );

        const response = await request(server).post("/check").send({
          intent,
          data: {},
        });

        expect(response.body).toStrictEqual({
          success: false,
          errors: ["ERROR"],
        });
        expect(response.status).toBe(200);
      });
    });
  });
});
