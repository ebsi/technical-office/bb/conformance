import { Body, Controller, HttpCode, Post } from "@nestjs/common";
import type { CheckResult } from "../../shared/interfaces";
import { CheckService } from "./check.service";

@Controller("/check")
export class CheckController {
  constructor(private checkService: CheckService) {}

  @HttpCode(200)
  @Post("/")
  check(@Body() body: unknown): Promise<CheckResult> {
    return this.checkService.dispatch(body);
  }
}

export default CheckController;
