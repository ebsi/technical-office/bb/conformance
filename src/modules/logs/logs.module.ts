import { Module } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { ApiConfigModule } from "../../config/configuration";
import { LogsService } from "./logs.service";
import { LogsController } from "./logs.controller";

@Module({
  imports: [ApiConfigModule],
  controllers: [LogsController],
  providers: [ConfigService, LogsService],
  exports: [LogsService],
})
export class LogsModule {}

export default LogsModule;
