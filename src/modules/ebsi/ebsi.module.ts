import { CacheModule, Module } from "@nestjs/common";
import { EBSIAuthorisationService } from "./authorisation.service";
import { ApiConfigModule, cacheConfig } from "../../config/configuration";

@Module({
  imports: [ApiConfigModule, CacheModule.register(cacheConfig)],
  controllers: [],
  providers: [EBSIAuthorisationService],
  exports: [EBSIAuthorisationService],
})
export class EbsiModule {}

export default EbsiModule;
