import { CacheModule, Module } from "@nestjs/common";
import { DataStoreService } from "./data-store.service";
import { cacheConfig } from "../../config/configuration";

@Module({
  imports: [CacheModule.register(cacheConfig)],
  controllers: [],
  providers: [DataStoreService],
  exports: [DataStoreService],
})
export class DataStoreModule {}

export default DataStoreModule;
