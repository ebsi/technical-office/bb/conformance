import { CACHE_MANAGER, Inject, Injectable, Logger } from "@nestjs/common";
import { Cron, CronExpression } from "@nestjs/schedule";
import type { Cache } from "cache-manager";
import type { IntentEvent, VCWithLinkedAttr } from "./data-store.interface";

@Injectable()
export class DataStoreService {
  private readonly logger = new Logger(DataStoreService.name);

  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}

  async setVCWithLinkedAttr(key: string, value: VCWithLinkedAttr) {
    this.logger.verbose(`Pushing data for ${key}: ${JSON.stringify(value)}}`);
    return this.cacheManager.set(key, value);
  }

  async getVCWithLinkedAttr(key: string) {
    return this.cacheManager.get<VCWithLinkedAttr>(key);
  }

  async pushEvent(key: string, event: IntentEvent) {
    const existingEvents = await this.getEvents(key);

    let events: IntentEvent[];
    if (existingEvents) {
      events = [event, ...existingEvents];
    } else {
      events = [event];
    }

    return this.cacheManager.set(key, events, 0);
  }

  getEvents(key: string) {
    return this.cacheManager.get<IntentEvent[]>(key);
  }

  @Cron(CronExpression.EVERY_DAY_AT_3AM)
  // @ts-expect-error TS will warn about unused private method, but it is used by the @Cron decorator
  private resetStoreData() {
    this.logger.verbose("Resetting data store.");
    return this.cacheManager.reset();
  }
}

export default DataStoreService;
