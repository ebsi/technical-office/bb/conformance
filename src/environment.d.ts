// Provide typings for process.env
declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NODE_ENV: "development" | "production" | "test";
      EBSI_ENV: "test" | "conformance";
      API_PORT?: string;
      API_URL_PREFIX?: string;
      LOG_LEVEL?: "silent" | "error" | "warn" | "info" | "verbose" | "debug";
      DOMAIN: string;
      CONFORMANCE_DOMAIN?: string;
      LOCAL_ORIGIN?: string;
      REQUEST_TIMEOUT?: string;
      AUTH_MOCK_PRIVATE_KEY: string;
      TEST_CLIENT_MOCK_PRIVATE_KEY: string;
      TEST_CLIENT_MOCK_KID: string;
      TEST_CLIENT_MOCK_ACCREDITATION_URL: string;
      TEST_CLIENT_MOCK_PROXY_URL: string;
      ISSUER_MOCK_PRIVATE_KEY: string;
      ISSUER_MOCK_KID: string;
      ISSUER_MOCK_ACCREDITATION_URL: string;
      ISSUER_MOCK_PROXY_URL: string;
      AUTHORISATION_CREDENTIAL_SCHEMA_ID: string;
      PDA1_CREDENTIAL_SCHEMA_ID: string;
      STATUSLIST2021_CREDENTIAL_SCHEMA_ID: string;
      TEST_ENV?: string;
      LOKI_AUTH_TOKEN: string;
      LOKI_URL: string;
      LOKI_LOGS_LIFETIME: string;
    }
  }
}

export {};
