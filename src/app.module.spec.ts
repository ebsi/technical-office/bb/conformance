import { createHash, randomBytes, randomUUID } from "node:crypto";
import { URLSearchParams } from "node:url";
import {
  jest,
  describe,
  beforeAll,
  afterAll,
  it,
  expect,
  afterEach,
} from "@jest/globals";
import request from "supertest";
import { Test, TestingModule } from "@nestjs/testing";
import { Logger, HttpServer } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import type { FastifyInstance } from "fastify";
import type { NestFastifyApplication } from "@nestjs/platform-fastify";
import qs from "qs";
import { exportJWK, generateKeyPair, importJWK, SignJWT } from "jose";
import { util as ebsiDidHelpers } from "@cef-ebsi/ebsi-did-resolver";
import { util as keyDidHelpers } from "@cef-ebsi/key-did-resolver";
import {
  EbsiIssuer,
  createVerifiableCredentialJwt,
  verifyCredentialJwt,
} from "@cef-ebsi/verifiable-credential";
import type { VerifyCredentialOptions } from "@cef-ebsi/verifiable-credential";
import {
  EbsiVerifiablePresentation,
  createVerifiablePresentationJwt,
} from "@cef-ebsi/verifiable-presentation";
import * as verifiablePresentationLib from "@cef-ebsi/verifiable-presentation";
import nock from "nock";
import { base64url } from "multiformats/bases/base64";
import type { PresentationSubmission } from "@sphereon/pex-models";
import { AppModule } from "./app.module";
import { configureApp } from "../tests/utils/app";
import { EBSIAuthorisationService } from "./modules/ebsi/authorisation.service";
import {
  ClientProps,
  createClient,
  createDidDocument,
  createLegalEntity,
  createLokiLogsResponse,
  createQueryParams,
  createTokenRequestParams,
  mockClientServer,
  mockConformanceIssuerRecords,
  mockSchemas,
} from "../tests/utils/data";
import { ApiConfig, DIDR_API_PATH, TIR_API_PATH } from "./config/configuration";
import {
  GetAuthorizeHolderWallerDto,
  HOLDER_WALLET_QUALIFICATION_PRESENTATION_DEFINITION,
  VA_TO_ONBOARD_PRESENTATION_DEFINITION,
} from "./shared/auth-server";
import type {
  IdTokenRequest,
  VpTokenRequest,
  TokenResponse,
  OPMetadata,
  PostTokenPkceDto,
  PostTokenPreAuthorizedCodeDto,
} from "./shared/auth-server";
import type { CredentialResponse } from "./shared/credential-issuer";
import type { AuthorizationDetails } from "./shared/validators/authorization-details.validator";
import type {
  CredentialOffer,
  CredentialOfferPayload,
} from "./modules/issuer-mock/issuer-mock.interface";
import type { CredentialIssuerMetadata } from "./shared/interfaces";
import {
  VALID_CREDENTIAL_TYPES,
  CT_WALLET_CROSS_DEFERRED,
  CT_WALLET_CROSS_IN_TIME,
  CT_WALLET_CROSS_PRE_AUTHORISED,
  CT_WALLET_SAME_DEFERRED,
  CT_WALLET_SAME_IN_TIME,
  CT_WALLET_SAME_PRE_AUTHORISED,
  REQUEST_CT_WALLET_QUALIFICATION_CREDENTIAL,
  RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
  RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
  TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
  TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
  TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
  TAO_REVOKE_RIGHTS_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
  TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
  TI_REGISTER_DID,
  TI_REGISTER_VERIFIABLE_ACCREDITATION_TO_ATTEST,
  TI_REQUEST_CT_REVOCABLE,
  TI_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST,
  TI_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD,
  TI_REVOKE_CT_REVOCABLE,
  TI_VALIDATE_CT_REVOCABLE,
} from "./shared/constants";
import { getUserPin } from "./shared/utils";

describe("App Module", () => {
  let app: NestFastifyApplication;
  let server: HttpServer;
  let ebsiAuthorisationService: EBSIAuthorisationService;
  let conformanceDomain: string;
  let apiUrlPrefix: string;
  let authMockUri: string;
  let issuerMockUri: string;
  let configService: ConfigService<ApiConfig, true>;
  let ebsiAuthority: string;
  let didRegistry: string;
  let trustedIssuersRegistry: string;
  let trustedPoliciesRegistry: string;

  function getPathnameWithoutPrefix(uri: string) {
    return new URL(uri).pathname.replace(apiUrlPrefix, "");
  }

  /**
   * Re-route requests to Conformance API to local server
   */
  function interceptConformanceDomainRequests() {
    nock(conformanceDomain)
      .get(() => true)
      .reply(async (uri, _, cb) => {
        try {
          const response = await request(server).get(
            uri.replace(apiUrlPrefix, "")
          );
          cb(null, [response.status, response.body]);
        } catch (e) {
          // eslint-disable-next-line no-console
          console.error(e);
          cb(null, [500]);
        }
      });
  }

  beforeAll(async () => {
    // Disable external requests
    nock.disableNetConnect();
    // Allow localhost connections so we can test local routes and mock servers.
    nock.enableNetConnect("127.0.0.1");

    // Start server
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    ebsiAuthorisationService = moduleFixture.get<EBSIAuthorisationService>(
      EBSIAuthorisationService
    );
    jest
      .spyOn(ebsiAuthorisationService, "getAccessToken")
      .mockImplementation(() => Promise.resolve("token"));

    app = await configureApp(moduleFixture);

    // Turn off logger
    Logger.overrideLogger(false);

    await app.init();
    await (app.getHttpAdapter().getInstance() as FastifyInstance).ready();
    server = app.getHttpServer() as HttpServer;

    configService =
      moduleFixture.get<ConfigService<ApiConfig, true>>(ConfigService);
    conformanceDomain = configService.get<string>("conformanceDomain");
    apiUrlPrefix = configService.get<string>("apiUrlPrefix");
    authMockUri = `${conformanceDomain}${apiUrlPrefix}/auth-mock`;
    issuerMockUri = `${conformanceDomain}${apiUrlPrefix}/issuer-mock`;
    didRegistry = configService.get<string>("didRegistryApiUrl");
    trustedIssuersRegistry = configService.get<string>(
      "trustedIssuersRegistryApiUrl"
    );
    trustedPoliciesRegistry = configService.get<string>(
      "trustedPoliciesRegistryApiUrl"
    );
    ebsiAuthority = configService
      .get<string>("domain")
      .replace(/^https?:\/\//, ""); // remove http protocol scheme
  });

  afterEach(() => {
    nock.cleanAll();
    jest.restoreAllMocks();
    jest
      .spyOn(ebsiAuthorisationService, "getAccessToken")
      .mockImplementation(() => Promise.resolve("token"));
  });

  afterAll(async () => {
    nock.restore();

    // Avoid jest open handle error
    await new Promise((r) => {
      setTimeout(r, 500);
    });
    await app.close();
  });

  describe("GET /unknown-route", () => {
    it("should return an error", async () => {
      expect.assertions(2);

      const response = await request(server).get("/unknown-route").send();

      expect(response.body).toStrictEqual({
        detail: "Cannot GET /unknown-route",
        status: 404,
        title: "Not Found",
        type: "about:blank",
      });
      expect(response.status).toBe(404);
    });
  });

  /**
   * @see https://ec.europa.eu/digital-building-blocks/wikis/pages/viewpage.action?pageId=622624898#CTAccreditandAuthoriseFunctionalFlows(2023)-RequestingandIssuingVerifiableCredentials
   */
  describe("A&A - test the different functional flows", () => {
    afterEach(() => {
      nock.cleanAll();
      jest.restoreAllMocks();
    });

    const itShouldObtainAVerifiableAuthorisationToOnboard = async (
      client: ClientProps,
      didToOnboard: string
    ) => {
      // Mock TIR and DIDR for trusted issuer
      await mockConformanceIssuerRecords(configService);

      // 1. GET /issuer-mock/.well-known/openid-credential-issuer
      let response = await request(server).get(
        "/issuer-mock/.well-known/openid-credential-issuer"
      );

      expect(response.status).toBe(200);
      const credentialIssuerConfig = response.body as CredentialIssuerMetadata;

      // 2. GET /auth-mock/.well-known/openid-configuration
      response = await request(server).get(
        "/auth-mock/.well-known/openid-configuration"
      );

      expect(response.status).toBe(200);
      const authMockConfig = response.body as OPMetadata;

      // 3. GET /auth-mock/authorize
      const authenticationRequestParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri
      );

      response = await request(server)
        .get(
          `${getPathnameWithoutPrefix(
            authMockConfig.authorization_endpoint
          )}?${new URLSearchParams({
            ...authenticationRequestParams,
            // Test getting request object by reference (for this test only)
            request_object: "reference",
          }).toString()}`
        )
        .send();

      expect(response.status).toBe(302);

      let { location } = response.headers as { location: string };
      const idTokenRequestParams = qs.parse(
        new URL(location).search.substring(1)
      ) as unknown as IdTokenRequest;

      expect(idTokenRequestParams.request_uri).toStrictEqual(
        expect.stringContaining(`${authMockUri}/request_uri/`)
      );
      expect(idTokenRequestParams.request).toBeUndefined();

      // 4. POST /auth-mock/direct_post
      const privateKey = await importJWK(client.privateKeyJwk);
      const idToken = await new SignJWT({
        nonce: idTokenRequestParams.nonce,
        sub: didToOnboard,
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: didToOnboard,
        })
        .setIssuer(didToOnboard)
        .setAudience(authMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(privateKey);

      response = await request(server)
        .post(`${getPathnameWithoutPrefix(idTokenRequestParams.redirect_uri)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token: idToken,
            state: idTokenRequestParams.state,
          }).toString()
        );

      expect(response.status).toBe(302);
      location = (response.headers as { location: string }).location;
      expect(location).toStrictEqual(
        expect.stringContaining(authenticationRequestParams.redirect_uri)
      );
      const authenticationResponseQueryParams = qs.parse(
        new URL(location).search.substring(1)
      ) as unknown as { code: string; state: string };

      expect(authenticationResponseQueryParams).toStrictEqual({
        state: authenticationRequestParams.state,
        code: expect.any(String),
      });

      // 5. POST /auth-mock/token
      const { code } = authenticationResponseQueryParams;
      const tokenRequestParams = await createTokenRequestParams(
        client,
        authMockUri,
        code
      );
      response = await request(server)
        .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(new URLSearchParams(tokenRequestParams).toString());

      expect(response.status).toBe(200);
      const { access_token: accessToken, c_nonce: cNonce } =
        response.body as TokenResponse;
      expect(accessToken).toBeDefined();

      // 6. POST /issuer-mock/credential
      const proofJwt = await new SignJWT({
        nonce: cNonce,
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
        })
        .setIssuer(client.clientId)
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(privateKey);

      const credentialRequestParams = {
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAuthorisationToOnboard",
        ],
        format: "jwt_vc",
        proof: {
          proof_type: "jwt",
          jwt: proofJwt,
        },
      };

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${accessToken}`)
        .send(credentialRequestParams);

      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        format: "jwt_vc",
        credential: expect.any(String),
      });

      // Check VC
      const { credential } = response.body as CredentialResponse;
      const options: VerifyCredentialOptions = {
        ebsiAuthority,
        ebsiEnvConfig: {
          didRegistry,
          trustedIssuersRegistry,
          trustedPoliciesRegistry,
        },
      };
      const vcPayload = await verifyCredentialJwt(credential, options);

      expect(vcPayload).toStrictEqual({
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: expect.stringContaining("vc:ebsi:conformance#"),
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          "VerifiableAuthorisationToOnboard",
        ],
        issuer: configService.get<string>("issuerMockKid").split("#")[0],
        issuanceDate: expect.any(String),
        issued: expect.any(String),
        validFrom: expect.any(String),
        expirationDate: expect.any(String),
        credentialSubject: { id: didToOnboard },
        credentialSchema: {
          id: configService.get<string>("authorisationCredentialSchema"),
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: configService.get<string>("issuerMockAccreditationUrl"),
          type: "IssuanceCertificate",
        },
      });

      return credential;
    };

    it("should obtain a VerifiableAuthorisationToOnboard", async () => {
      expect.assertions(13);
      const client = await createClient();
      const clientDid = ebsiDidHelpers.createDid(randomBytes(16));
      mockClientServer(client);
      mockSchemas();
      await itShouldObtainAVerifiableAuthorisationToOnboard(client, clientDid);
    });

    it.each([
      "VerifiableAccreditationToAccredit",
      "VerifiableAccreditationToAttest",
      "CTAAQualificationCredential",
    ] as const)("should obtain a %s", async (requestedType) => {
      expect.assertions(14);

      const requestedTypes: (typeof VALID_CREDENTIAL_TYPES)[number][] =
        requestedType === "CTAAQualificationCredential"
          ? [requestedType]
          : ["VerifiableAccreditation", requestedType];

      const client = await createClient();
      const clientDid = ebsiDidHelpers.createDid(randomBytes(16));
      const clientKid = `${clientDid}#key-1`;
      const clientDidDocument = createDidDocument(
        clientDid,
        clientKid,
        client.publicKeyJwk
      );

      mockClientServer(client);
      mockSchemas();

      if (requestedType === "CTAAQualificationCredential") {
        const lokiLogsResponse = createLokiLogsResponse(
          clientDid,
          client.clientId,
          [
            {
              intent: TI_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD,
              result: { success: true },
            },
            {
              intent: TI_REGISTER_DID,
              result: { success: true },
            },
            {
              intent: TI_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST,
              result: { success: true },
            },
            {
              intent: TI_REGISTER_VERIFIABLE_ACCREDITATION_TO_ATTEST,
              result: { success: true },
            },
            {
              intent: TI_REQUEST_CT_REVOCABLE,
              result: { success: true },
            },
            {
              intent: TI_VALIDATE_CT_REVOCABLE,
              result: { success: true },
            },
            {
              intent: TI_REVOKE_CT_REVOCABLE,
              result: { success: true },
            },
            {
              intent: TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
              result: { success: true },
            },
            {
              intent: TAO_REGISTER_VERIFIABLE_ACCREDITATION_TO_ACCREDIT,
              result: { success: true },
            },
            {
              intent:
                TAO_REQUEST_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
              result: { success: true },
            },
            {
              intent:
                TAO_VALIDATE_VERIFIABLE_AUTHORISATION_TO_ONBOARD_SUBACCOUNT,
              result: { success: true },
            },
            {
              intent: TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
              result: { success: true },
            },
            {
              intent:
                TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ATTEST_SUBACCOUNT,
              result: { success: true },
            },
            {
              intent:
                TAO_REQUEST_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
              result: { success: true },
            },
            {
              intent:
                TAO_VALIDATE_VERIFIABLE_ACCREDITATION_TO_ACCREDIT_SUBACCOUNT,
              result: { success: true },
            },
            {
              intent: TAO_REVOKE_RIGHTS_SUBACCOUNT,
              result: { success: true },
            },
            {
              intent: RTAO_REQUEST_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
              result: { success: true },
            },
            {
              intent: RTAO_REGISTER_VERIFIABLE_AUTHORISATION_FOR_TRUST_CHAIN,
              result: { success: true },
            },
          ]
        );

        nock("https://loki.ebsi.xyz")
          .get("/loki/api/v1/query_range")
          .query(true)
          .reply(200, lokiLogsResponse);
      }

      // Mock DID Registry
      nock(configService.get<string>("domain"))
        .get(`${DIDR_API_PATH}/${clientDid}`)
        .reply(200, clientDidDocument)
        .persist();

      // Mock TIR
      nock(configService.get<string>("domain"))
        .get(`${TIR_API_PATH}/${clientDid}`)
        .reply(200, {})
        .persist();

      // Mock TIR and DIDR for trusted issuer
      await mockConformanceIssuerRecords(configService);

      // 1. GET /issuer-mock/.well-known/openid-credential-issuer
      let response = await request(server).get(
        "/issuer-mock/.well-known/openid-credential-issuer"
      );

      expect(response.status).toBe(200);
      const credentialIssuerConfig = response.body as CredentialIssuerMetadata;

      // 2. GET /auth-mock/.well-known/openid-configuration
      response = await request(server).get(
        "/auth-mock/.well-known/openid-configuration"
      );

      expect(response.status).toBe(200);
      const authMockConfig = response.body as OPMetadata;

      // 3. GET /auth-mock/authorize
      const authenticationRequestParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri,
        {
          customJwtPayload: {
            authorization_details: [
              {
                type: "openid_credential",
                format: "jwt_vc",
                locations: [issuerMockUri],
                types: [
                  "VerifiableCredential",
                  "VerifiableAttestation",
                  ...requestedTypes,
                ],
              },
            ] satisfies AuthorizationDetails,
          },
        }
      );
      response = await request(server)
        .get(
          `${getPathnameWithoutPrefix(
            authMockConfig.authorization_endpoint
          )}?${new URLSearchParams(authenticationRequestParams).toString()}`
        )
        .send();

      expect(response.status).toBe(302);

      let { location } = response.headers as { location: string };
      const idTokenRequestParams = qs.parse(
        new URL(location).search.substring(1)
      ) as unknown as IdTokenRequest;

      expect(idTokenRequestParams.request).toBeUndefined();
      expect(idTokenRequestParams.request_uri).toBeDefined();

      // 4. POST /auth-mock/direct_post
      const privateKey = await importJWK(client.privateKeyJwk);
      const idToken = await new SignJWT({
        nonce: idTokenRequestParams.nonce,
        sub: clientDid,
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(clientDid)
        .setAudience(authMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(privateKey);

      response = await request(server)
        .post(`${getPathnameWithoutPrefix(idTokenRequestParams.redirect_uri)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token: idToken,
            state: idTokenRequestParams.state,
          }).toString()
        );

      expect(response.status).toBe(302);
      location = (response.headers as { location: string }).location;
      expect(location).toStrictEqual(
        expect.stringContaining(authenticationRequestParams.redirect_uri)
      );

      const authenticationResponseQueryParams = qs.parse(
        new URL(location).search.substring(1)
      ) as unknown as { code: string; state: string };

      expect(authenticationResponseQueryParams.state).toBe(
        authenticationRequestParams.state
      );
      expect(authenticationResponseQueryParams.code).toBeTruthy();

      // 5. POST /auth-mock/token
      const { code } = authenticationResponseQueryParams;
      const tokenRequestParams = await createTokenRequestParams(
        client,
        authMockUri,
        code
      );
      response = await request(server)
        .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(new URLSearchParams(tokenRequestParams).toString());

      expect(response.status).toBe(200);
      const { access_token: accessToken, c_nonce: cNonce } =
        response.body as TokenResponse;
      expect(accessToken).toBeDefined();

      // 6. POST /issuer-mock/credential
      const proofJwt = await new SignJWT({
        nonce: cNonce,
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(client.clientId)
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(privateKey);

      const credentialRequestParams = {
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          ...requestedTypes,
        ],
        format: "jwt_vc",
        proof: {
          proof_type: "jwt",
          jwt: proofJwt,
        },
      };

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${accessToken}`)
        .send(credentialRequestParams);

      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        format: "jwt_vc",
        credential: expect.any(String),
      });

      // Check VC
      const { credential } = response.body as CredentialResponse;
      const options: VerifyCredentialOptions = {
        ebsiAuthority,
        ebsiEnvConfig: {
          didRegistry,
          trustedIssuersRegistry,
          trustedPoliciesRegistry,
        },
      };
      const vcPayload = await verifyCredentialJwt(credential, options);

      expect(vcPayload).toStrictEqual({
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: expect.stringContaining("vc:ebsi:conformance#"),
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          ...requestedTypes,
        ],
        issuer: configService.get<string>("issuerMockKid").split("#")[0],
        issuanceDate: expect.any(String),
        issued: expect.any(String),
        validFrom: expect.any(String),
        expirationDate: expect.any(String),
        credentialSubject: {
          id: clientDid,
          ...([
            "VerifiableAccreditationToAccredit",
            "VerifiableAccreditationToAttest",
          ].includes(requestedType) && {
            // eslint-disable-next-line jest/no-conditional-expect
            reservedAttributeId: expect.any(String),
            accreditedFor: [
              {
                schemaId: configService.get<string>(
                  "authorisationCredentialSchema"
                ),
                types: [
                  "VerifiableCredential",
                  "VerifiableAttestation",
                  "CTRevocable",
                ],
                limitJurisdiction:
                  "https://publications.europa.eu/resource/authority/atu/FIN",
              },
            ],
          }),
        },
        credentialSchema: {
          id: configService.get<string>("authorisationCredentialSchema"),
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: configService.get<string>("issuerMockAccreditationUrl"),
          type: "IssuanceCertificate",
        },
      });
    });

    it("should obtain a VerifiableAuthorisationForTrustChain", async () => {
      expect.assertions(30);

      const requestedTypes = ["VerifiableAuthorisationForTrustChain"] as const;

      const client = await createClient();
      const clientDid = ebsiDidHelpers.createDid(randomBytes(16));
      const clientKid = `${clientDid}#key-1`;
      const clientDidDocument = createDidDocument(
        clientDid,
        clientKid,
        client.publicKeyJwk
      );

      mockClientServer(client);
      mockSchemas();

      // 0. Get a VerifiableAuthorisationToOnboard
      const vcToOnboard = await itShouldObtainAVerifiableAuthorisationToOnboard(
        client,
        clientDid
      );

      // Mock DID Registration
      nock(configService.get<string>("domain"))
        .get(`${DIDR_API_PATH}/${clientDid}`)
        .reply(200, clientDidDocument)
        .persist();

      // Mock TIR and DIDR for trusted issuer
      await mockConformanceIssuerRecords(configService);

      // 1. GET /issuer-mock/.well-known/openid-credential-issuer
      let response = await request(server).get(
        "/issuer-mock/.well-known/openid-credential-issuer"
      );

      expect(response.status).toBe(200);
      const credentialIssuerConfig = response.body as CredentialIssuerMetadata;

      // 2. GET /auth-mock/.well-known/openid-configuration
      response = await request(server).get(
        "/auth-mock/.well-known/openid-configuration"
      );

      expect(response.status).toBe(200);
      const authMockConfig = response.body as OPMetadata;

      // 3. GET /auth-mock/authorize
      const authenticationRequestParams = await createQueryParams(
        client,
        authMockUri,
        issuerMockUri,
        {
          customJwtPayload: {
            authorization_details: [
              {
                type: "openid_credential",
                format: "jwt_vc",
                locations: [issuerMockUri],
                types: [
                  "VerifiableCredential",
                  "VerifiableAttestation",
                  ...requestedTypes,
                ],
              },
            ] satisfies AuthorizationDetails,
          },
        }
      );
      response = await request(server)
        .get(
          `${getPathnameWithoutPrefix(
            authMockConfig.authorization_endpoint
          )}?${new URLSearchParams(authenticationRequestParams).toString()}`
        )
        .send();

      expect(response.status).toBe(302);

      let { location } = response.headers as { location: string };
      const vpTokenRequestParams = qs.parse(
        new URL(location).search.substring(1),
        {
          depth: 50,
          parameterLimit: 1000,
        }
      ) as unknown as VpTokenRequest;

      expect(vpTokenRequestParams).toStrictEqual({
        state: expect.any(String),
        client_id: expect.any(String),
        redirect_uri: `${authMockUri}/direct_post`,
        response_type: "vp_token",
        response_mode: "direct_post",
        scope: "openid",
        nonce: expect.any(String),
        presentation_definition: expect.any(String),
        request: expect.any(String),
      });

      expect(
        JSON.parse(vpTokenRequestParams.presentation_definition)
      ).toStrictEqual(
        expect.objectContaining({
          id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.id,
        })
      );

      // 4. POST /auth-mock/direct_post
      const privateKey = await importJWK(client.privateKeyJwk);

      const vpPayload = {
        id: `urn:did:${randomUUID()}`,
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        type: ["VerifiablePresentation"],
        holder: clientDid,
        verifiableCredential: [vcToOnboard],
      };

      const credentialSubject: EbsiIssuer = {
        did: clientDid,
        kid: clientKid,
        privateKeyJwk: client.privateKeyJwk,
        publicKeyJwk: client.publicKeyJwk,
        alg: "ES256",
      };

      const vpToken = await createVerifiablePresentationJwt(
        vpPayload,
        credentialSubject,
        authMockUri,
        {
          ebsiAuthority: "example.net",
          skipValidation: true,
          nonce: randomUUID(),
          exp: Math.floor(Date.now() / 1000) + 100,
          nbf: Math.floor(Date.now() / 1000) - 100,
        }
      );

      const presentationSubmission: PresentationSubmission = {
        id: randomUUID(),
        definition_id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.id,
        descriptor_map: [
          {
            id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.input_descriptors[0].id,
            format: "jwt_vp",
            path: "$",
            path_nested: {
              id: VA_TO_ONBOARD_PRESENTATION_DEFINITION.input_descriptors[0].id,
              format: "jwt_vc",
              path: "$.verifiableCredential[0]",
            },
          },
        ],
      };

      response = await request(server)
        .post(`${getPathnameWithoutPrefix(vpTokenRequestParams.redirect_uri)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            vp_token: vpToken,
            state: vpTokenRequestParams.state,
            presentation_submission: JSON.stringify(presentationSubmission),
          }).toString()
        );

      expect(response.status).toBe(302);
      location = (response.headers as { location: string }).location;
      expect(location).toStrictEqual(
        expect.stringContaining(authenticationRequestParams.redirect_uri)
      );

      const authenticationResponseQueryParams = qs.parse(
        new URL(location).search.substring(1)
      ) as unknown as { code: string; state: string };

      expect(authenticationResponseQueryParams).toStrictEqual({
        state: authenticationRequestParams.state,
        code: expect.any(String),
      });

      // 5. POST /auth-mock/token
      const { code } = authenticationResponseQueryParams;
      const tokenRequestParams = await createTokenRequestParams(
        client,
        authMockUri,
        code
      );
      response = await request(server)
        .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(new URLSearchParams(tokenRequestParams).toString());

      expect(response.status).toBe(200);
      const { access_token: accessToken, c_nonce: cNonce } =
        response.body as TokenResponse;
      expect(accessToken).toBeDefined();

      // 6. POST /issuer-mock/credential
      const proofJwt = await new SignJWT({
        nonce: cNonce,
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(client.clientId)
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(privateKey);

      const credentialRequestParams = {
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          ...requestedTypes,
        ],
        format: "jwt_vc",
        proof: {
          proof_type: "jwt",
          jwt: proofJwt,
        },
      };

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${accessToken}`)
        .send(credentialRequestParams);

      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        acceptance_token: expect.any(String),
      });

      const { acceptance_token: acceptanceToken } = response.body as {
        acceptance_token: string;
      };

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.deferred_credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${acceptanceToken}`)
        .send();

      expect(response.status).toBe(400);
      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description: "Deferred credential not available yet",
      });

      // Wait 5.5 seconds (> 5 seconds)
      await new Promise((r) => {
        setTimeout(r, 5500);
      });

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.deferred_credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${acceptanceToken}`)
        .send();

      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        format: "jwt_vc",
        credential: expect.any(String),
      });

      // Check VC
      const { credential } = response.body as CredentialResponse;
      const options: VerifyCredentialOptions = {
        ebsiAuthority,
        ebsiEnvConfig: {
          didRegistry,
          trustedIssuersRegistry,
          trustedPoliciesRegistry,
        },
      };
      const vcPayload = await verifyCredentialJwt(credential, options);

      expect(vcPayload).toStrictEqual({
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: expect.stringContaining("vc:ebsi:conformance#"),
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          ...requestedTypes,
        ],
        issuer: configService.get<string>("issuerMockKid").split("#")[0],
        issuanceDate: expect.any(String),
        issued: expect.any(String),
        validFrom: expect.any(String),
        expirationDate: expect.any(String),
        credentialSubject: {
          id: clientDid,
          reservedAttributeId: expect.any(String),
        },
        credentialSchema: {
          id: configService.get<string>("authorisationCredentialSchema"),
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: configService.get<string>("issuerMockAccreditationUrl"),
          type: "IssuanceCertificate",
        },
      });
    });
  });

  /**
   * @see https://ec.europa.eu/digital-building-blocks/wikis/pages/viewpage.action?pageId=613548327#CTHolderWalletFunctionalFlows(2023)-Tests
   */
  describe("Holder wallet - test the different functional flows", () => {
    afterEach(() => {
      nock.cleanAll();
      jest.restoreAllMocks();
    });

    it("should obtain a CTWalletCrossInTime credential", async () => {
      mockSchemas();
      await mockConformanceIssuerRecords(configService);

      const clientKeyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
      const clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
      const methodSpecificIdentifier = clientDid.replace("did:key:", "");
      const clientKid = `${clientDid}#${methodSpecificIdentifier}`;
      const { privateKey: clientPrivateKey } = clientKeyPair;

      // Initiate credential offer from Issuer Mock
      const requestParams = {
        credential_type: "CTWalletCrossInTime",
        credential_offer_endpoint: "openid-credential-offer://",
        client_id: clientDid,
      } as const;

      let response = await request(server).get(
        `/issuer-mock/initiate-credential-offer?${new URLSearchParams(
          requestParams
        ).toString()}`
      );

      expect(response.text).toStrictEqual(
        expect.stringContaining(requestParams.credential_offer_endpoint)
      );
      expect(response.status).toBe(200);

      const { search } = new URL(response.text);

      const parsedCredentialOffer = qs.parse(
        search.slice(1)
      ) as unknown as CredentialOffer;

      expect(parsedCredentialOffer).toStrictEqual({
        credential_offer_uri: expect.any(String),
      });

      // Get credential_offer_uri
      response = await request(server)
        .get(
          getPathnameWithoutPrefix(
            parsedCredentialOffer.credential_offer_uri as string
          )
        )
        .send();

      const credentialOfferPayload = response.body as CredentialOfferPayload;

      expect(credentialOfferPayload).toStrictEqual({
        credential_issuer: issuerMockUri,
        credentials: [
          {
            format: "jwt_vc",
            trust_framework: {
              name: "ebsi",
              type: "Accreditation",
              uri: "TIR link towards accreditation",
            },
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              requestParams.credential_type,
            ],
          },
        ],
        grants: {
          authorization_code: {
            issuer_state: expect.any(String),
          },
        },
      });

      // GET /issuer-mock/.well-known/openid-credential-issuer
      response = await request(server).get(
        "/issuer-mock/.well-known/openid-credential-issuer"
      );

      expect(response.status).toBe(200);
      const credentialIssuerConfig = response.body as CredentialIssuerMetadata;

      // GET AUthorization Server (Auth Mock) /.well-known/openid-configuration
      if (!credentialIssuerConfig.authorization_server)
        throw new Error("authorization_server not defined");
      const authorizationServerUri = getPathnameWithoutPrefix(
        credentialIssuerConfig.authorization_server
      );

      response = await request(server).get(
        `${authorizationServerUri}/.well-known/openid-configuration`
      );

      expect(response.status).toBe(200);
      const authMockConfig = response.body as OPMetadata;

      // GET /auth-mock/authorize
      // Code verifier: cryptographically random string using the characters A-Z, a-z, 0-9, and the
      // punctuation characters -._~ (hyphen, period, underscore, and tilde), between 43 and 128
      // characters long.
      // Example: https://github.com/jaredhanson/passport-oauth2/blob/master/lib/strategy.js#L239
      const codeVerifier = randomBytes(50).toString("base64url");
      const codeChallenge = base64url.baseEncode(
        createHash("sha256").update(codeVerifier).digest()
      );

      const authenticationRequestParams = {
        scope: "openid",
        client_id: clientDid,
        response_type: "code",
        redirect_uri: "openid://callback",
        code_challenge: codeChallenge,
        code_challenge_method: "S256",
        state: randomUUID(),
        ...(credentialOfferPayload.grants.authorization_code?.issuer_state && {
          issuer_state:
            credentialOfferPayload.grants.authorization_code.issuer_state,
        }),
        client_metadata: JSON.stringify({
          authorization_endpoint: "openid:",
        }),
        authorization_details: JSON.stringify([
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [issuerMockUri],
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              requestParams.credential_type,
            ],
          },
        ]),
      } satisfies GetAuthorizeHolderWallerDto;

      response = await request(server)
        .get(
          `${getPathnameWithoutPrefix(
            authMockConfig.authorization_endpoint
          )}?${new URLSearchParams(authenticationRequestParams).toString()}`
        )
        .send();

      expect(response.status).toBe(302);

      let { location } = response.headers as { location: string };

      const idTokenRequestParams = qs.parse(
        new URL(location).search.substring(1)
      ) as unknown as IdTokenRequest;

      expect(idTokenRequestParams.request).toBeUndefined();
      expect(idTokenRequestParams.request_uri).toBeDefined();

      // POST /auth-mock/direct_post
      const idToken = await new SignJWT({
        nonce: idTokenRequestParams.nonce,
        sub: clientDid,
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(clientDid)
        .setAudience(authMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(clientPrivateKey);

      response = await request(server)
        .post(`${getPathnameWithoutPrefix(idTokenRequestParams.redirect_uri)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token: idToken,
            state: idTokenRequestParams.state,
          }).toString()
        );

      expect(response.status).toBe(302);
      location = (response.headers as { location: string }).location;
      expect(location).toStrictEqual(
        expect.stringContaining(authenticationRequestParams.redirect_uri)
      );

      const authenticationResponseQueryParams = qs.parse(
        new URL(location).search.substring(1)
      ) as unknown as { code: string; state: string };

      expect(authenticationResponseQueryParams.state).toBe(
        authenticationRequestParams.state
      );
      expect(authenticationResponseQueryParams.code).toBeTruthy();

      // POST /auth-mock/token
      const { code } = authenticationResponseQueryParams;
      const tokenRequestQueryParams = {
        grant_type: "authorization_code",
        code,
        client_id: clientDid,
        code_verifier: codeVerifier,
      } satisfies PostTokenPkceDto;

      response = await request(server)
        .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(new URLSearchParams(tokenRequestQueryParams).toString());

      expect(response.status).toBe(200);
      const { access_token: accessToken, c_nonce: cNonce } =
        response.body as TokenResponse;
      expect(accessToken).toBeDefined();

      // POST /issuer-mock/credential
      const proofJwt = await new SignJWT({
        nonce: cNonce,
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(clientDid)
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(clientPrivateKey);

      const credentialRequestParams = {
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          requestParams.credential_type,
        ],
        format: "jwt_vc",
        proof: {
          proof_type: "jwt",
          jwt: proofJwt,
        },
      };

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${accessToken}`)
        .send(credentialRequestParams);

      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        format: "jwt_vc",
        credential: expect.any(String),
      });

      // Check VC
      const { credential } = response.body as CredentialResponse;
      const options: VerifyCredentialOptions = {
        ebsiAuthority,
        ebsiEnvConfig: {
          didRegistry,
          trustedIssuersRegistry,
          trustedPoliciesRegistry,
        },
      };
      const vcPayload = await verifyCredentialJwt(credential, options);

      expect(vcPayload).toStrictEqual({
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: expect.stringContaining("vc:ebsi:conformance#"),
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          requestParams.credential_type,
        ],
        issuer: configService.get<string>("issuerMockKid").split("#")[0],
        issuanceDate: expect.any(String),
        issued: expect.any(String),
        validFrom: expect.any(String),
        expirationDate: expect.any(String),
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: configService.get<string>("authorisationCredentialSchema"),
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: configService.get<string>("issuerMockAccreditationUrl"),
          type: "IssuanceCertificate",
        },
      });

      // Check if the test is completed
      response = await request(server)
        .post("/check")
        .send({
          intent: CT_WALLET_CROSS_IN_TIME,
          data: {
            did: clientDid,
          },
        });

      expect(response.body).toStrictEqual({
        success: true,
      });
    });

    it("should obtain a CTWalletCrossDeferred credential", async () => {
      mockSchemas();
      await mockConformanceIssuerRecords(configService);

      const clientKeyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
      const clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
      const methodSpecificIdentifier = clientDid.replace("did:key:", "");
      const clientKid = `${clientDid}#${methodSpecificIdentifier}`;
      const { privateKey: clientPrivateKey } = clientKeyPair;

      // Initiate credential offer from Issuer Mock
      const requestParams = {
        credential_type: "CTWalletCrossDeferred",
        credential_offer_endpoint: "openid-credential-offer://",
        client_id: clientDid,
      } as const;

      let response = await request(server).get(
        `/issuer-mock/initiate-credential-offer?${new URLSearchParams(
          requestParams
        ).toString()}`
      );

      expect(response.text).toStrictEqual(
        expect.stringContaining(requestParams.credential_offer_endpoint)
      );
      expect(response.status).toBe(200);

      const { search } = new URL(response.text);

      const parsedCredentialOffer = qs.parse(
        search.slice(1)
      ) as unknown as CredentialOffer;

      expect(parsedCredentialOffer).toStrictEqual({
        credential_offer_uri: expect.any(String),
      });

      // Get credential_offer_uri
      response = await request(server)
        .get(
          getPathnameWithoutPrefix(
            parsedCredentialOffer.credential_offer_uri as string
          )
        )
        .send();

      const credentialOfferPayload = response.body as CredentialOfferPayload;

      expect(credentialOfferPayload).toStrictEqual({
        credential_issuer: issuerMockUri,
        credentials: [
          {
            format: "jwt_vc",
            trust_framework: {
              name: "ebsi",
              type: "Accreditation",
              uri: "TIR link towards accreditation",
            },
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              requestParams.credential_type,
            ],
          },
        ],
        grants: {
          authorization_code: {
            issuer_state: expect.any(String),
          },
        },
      });

      // GET /issuer-mock/.well-known/openid-credential-issuer
      response = await request(server).get(
        "/issuer-mock/.well-known/openid-credential-issuer"
      );

      expect(response.status).toBe(200);
      const credentialIssuerConfig = response.body as CredentialIssuerMetadata;

      // GET AUthorization Server (Auth Mock) /.well-known/openid-configuration
      if (!credentialIssuerConfig.authorization_server)
        throw new Error("authorization_server not defined");
      const authorizationServerUri = getPathnameWithoutPrefix(
        credentialIssuerConfig.authorization_server
      );

      response = await request(server).get(
        `${authorizationServerUri}/.well-known/openid-configuration`
      );

      expect(response.status).toBe(200);
      const authMockConfig = response.body as OPMetadata;

      // GET /auth-mock/authorize
      // Code verifier: cryptographically random string using the characters A-Z, a-z, 0-9, and the
      // punctuation characters -._~ (hyphen, period, underscore, and tilde), between 43 and 128
      // characters long.
      // Example: https://github.com/jaredhanson/passport-oauth2/blob/master/lib/strategy.js#L239
      const codeVerifier = randomBytes(50).toString("base64url");
      const codeChallenge = base64url.baseEncode(
        createHash("sha256").update(codeVerifier).digest()
      );

      const authenticationRequestParams = {
        scope: "openid",
        client_id: clientDid,
        response_type: "code",
        redirect_uri: "openid://callback",
        code_challenge: codeChallenge,
        code_challenge_method: "S256",
        state: randomUUID(),
        ...(credentialOfferPayload.grants.authorization_code?.issuer_state && {
          issuer_state:
            credentialOfferPayload.grants.authorization_code.issuer_state,
        }),
        client_metadata: JSON.stringify({
          authorization_endpoint: "openid:",
        }),
        authorization_details: JSON.stringify([
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [issuerMockUri],
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              requestParams.credential_type,
            ],
          },
        ]),
      } satisfies GetAuthorizeHolderWallerDto;

      response = await request(server)
        .get(
          `${getPathnameWithoutPrefix(
            authMockConfig.authorization_endpoint
          )}?${new URLSearchParams(authenticationRequestParams).toString()}`
        )
        .send();

      expect(response.status).toBe(302);

      let { location } = response.headers as { location: string };

      const idTokenRequestParams = qs.parse(
        new URL(location).search.substring(1)
      ) as unknown as IdTokenRequest;

      expect(idTokenRequestParams.request).toBeUndefined();
      expect(idTokenRequestParams.request_uri).toBeDefined();

      // POST /auth-mock/direct_post
      const idToken = await new SignJWT({
        nonce: idTokenRequestParams.nonce,
        sub: clientDid,
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(clientDid)
        .setAudience(authMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(clientPrivateKey);

      response = await request(server)
        .post(`${getPathnameWithoutPrefix(idTokenRequestParams.redirect_uri)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token: idToken,
            state: idTokenRequestParams.state,
          }).toString()
        );

      expect(response.status).toBe(302);
      location = (response.headers as { location: string }).location;
      expect(location).toStrictEqual(
        expect.stringContaining(authenticationRequestParams.redirect_uri)
      );

      const authenticationResponseQueryParams = qs.parse(
        new URL(location).search.substring(1)
      ) as unknown as { code: string; state: string };

      expect(authenticationResponseQueryParams.state).toBe(
        authenticationRequestParams.state
      );
      expect(authenticationResponseQueryParams.code).toBeTruthy();

      // POST /auth-mock/token
      const { code } = authenticationResponseQueryParams;
      const tokenRequestQueryParams = {
        grant_type: "authorization_code",
        code,
        client_id: clientDid,
        code_verifier: codeVerifier,
      } satisfies PostTokenPkceDto;

      response = await request(server)
        .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(new URLSearchParams(tokenRequestQueryParams).toString());

      expect(response.status).toBe(200);
      const { access_token: accessToken, c_nonce: cNonce } =
        response.body as TokenResponse;
      expect(accessToken).toBeDefined();

      // POST /issuer-mock/credential
      const proofJwt = await new SignJWT({
        nonce: cNonce,
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(clientDid)
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(clientPrivateKey);

      const credentialRequestParams = {
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          requestParams.credential_type,
        ],
        format: "jwt_vc",
        proof: {
          proof_type: "jwt",
          jwt: proofJwt,
        },
      };

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${accessToken}`)
        .send(credentialRequestParams);

      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        acceptance_token: expect.any(String),
      });

      const { acceptance_token: acceptanceToken } = response.body as {
        acceptance_token: string;
      };

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.deferred_credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${acceptanceToken}`)
        .send();

      expect(response.status).toBe(400);
      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description: "Deferred credential not available yet",
      });

      // Wait 5.5 seconds (> 5 seconds)
      await new Promise((r) => {
        setTimeout(r, 5500);
      });

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.deferred_credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${acceptanceToken}`)
        .send();

      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        format: "jwt_vc",
        credential: expect.any(String),
      });

      // Check VC
      const { credential } = response.body as CredentialResponse;
      const options: VerifyCredentialOptions = {
        ebsiAuthority,
        ebsiEnvConfig: {
          didRegistry,
          trustedIssuersRegistry,
          trustedPoliciesRegistry,
        },
      };
      const vcPayload = await verifyCredentialJwt(credential, options);

      expect(vcPayload).toStrictEqual({
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: expect.stringContaining("vc:ebsi:conformance#"),
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          requestParams.credential_type,
        ],
        issuer: configService.get<string>("issuerMockKid").split("#")[0],
        issuanceDate: expect.any(String),
        issued: expect.any(String),
        validFrom: expect.any(String),
        expirationDate: expect.any(String),
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: configService.get<string>("authorisationCredentialSchema"),
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: configService.get<string>("issuerMockAccreditationUrl"),
          type: "IssuanceCertificate",
        },
      });

      // Check if the test is completed
      response = await request(server)
        .post("/check")
        .send({
          intent: CT_WALLET_CROSS_DEFERRED,
          data: {
            did: clientDid,
          },
        });

      expect(response.body).toStrictEqual({
        success: true,
      });
    });

    it("should obtain a CTWalletCrossPreAuthorised credential", async () => {
      mockSchemas();
      await mockConformanceIssuerRecords(configService);
      interceptConformanceDomainRequests();

      const clientKeyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
      const clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
      const methodSpecificIdentifier = clientDid.replace("did:key:", "");
      const clientKid = `${clientDid}#${methodSpecificIdentifier}`;
      const { privateKey: clientPrivateKey } = clientKeyPair;

      // Initiate credential offer from Issuer Mock
      const requestParams = {
        credential_type: "CTWalletCrossPreAuthorised",
        credential_offer_endpoint: "openid-credential-offer://",
        client_id: clientDid,
      } as const;

      let response = await request(server).get(
        `/issuer-mock/initiate-credential-offer?${new URLSearchParams(
          requestParams
        ).toString()}`
      );

      expect(response.text).toStrictEqual(
        expect.stringContaining(requestParams.credential_offer_endpoint)
      );
      expect(response.status).toBe(200);

      const { search } = new URL(response.text);

      const parsedCredentialOffer = qs.parse(
        search.slice(1)
      ) as unknown as CredentialOffer;

      expect(parsedCredentialOffer).toStrictEqual({
        credential_offer_uri: expect.any(String),
      });

      // Get credential_offer_uri
      response = await request(server)
        .get(
          getPathnameWithoutPrefix(
            parsedCredentialOffer.credential_offer_uri as string
          )
        )
        .send();

      const credentialOfferPayload = response.body as CredentialOfferPayload;

      expect(credentialOfferPayload).toStrictEqual({
        credential_issuer: issuerMockUri,
        credentials: [
          {
            format: "jwt_vc",
            trust_framework: {
              name: "ebsi",
              type: "Accreditation",
              uri: "TIR link towards accreditation",
            },
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              requestParams.credential_type,
            ],
          },
        ],
        grants: {
          "urn:ietf:params:oauth:grant-type:pre-authorized_code": {
            "pre-authorized_code": expect.any(String),
            user_pin_required: true,
          },
        },
      });

      // GET /issuer-mock/.well-known/openid-credential-issuer
      response = await request(server).get(
        "/issuer-mock/.well-known/openid-credential-issuer"
      );

      expect(response.status).toBe(200);
      const credentialIssuerConfig = response.body as CredentialIssuerMetadata;

      // GET AUthorization Server (Auth Mock) /.well-known/openid-configuration
      if (!credentialIssuerConfig.authorization_server)
        throw new Error("authorization_server not defined");
      const authorizationServerUri = getPathnameWithoutPrefix(
        credentialIssuerConfig.authorization_server
      );

      response = await request(server).get(
        `${authorizationServerUri}/.well-known/openid-configuration`
      );

      expect(response.status).toBe(200);
      const authMockConfig = response.body as OPMetadata;

      // POST /auth-mock/token
      const preAuthorizedCode = credentialOfferPayload.grants[
        "urn:ietf:params:oauth:grant-type:pre-authorized_code"
      ]?.["pre-authorized_code"] as string;

      const tokenRequestQueryParams = {
        grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
        "pre-authorized_code": preAuthorizedCode,
        user_pin: getUserPin(clientDid),
      } satisfies PostTokenPreAuthorizedCodeDto;

      response = await request(server)
        .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(new URLSearchParams(tokenRequestQueryParams).toString());

      expect(response.status).toBe(200);
      const { access_token: accessToken, c_nonce: cNonce } =
        response.body as TokenResponse;
      expect(accessToken).toBeDefined();

      // POST /issuer-mock/credential
      const proofJwt = await new SignJWT({
        nonce: cNonce,
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(clientDid)
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(clientPrivateKey);

      const credentialRequestParams = {
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          requestParams.credential_type,
        ],
        format: "jwt_vc",
        proof: {
          proof_type: "jwt",
          jwt: proofJwt,
        },
      };

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${accessToken}`)
        .send(credentialRequestParams);

      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        format: "jwt_vc",
        credential: expect.any(String),
      });

      // Check VC
      const { credential } = response.body as CredentialResponse;
      const options: VerifyCredentialOptions = {
        ebsiAuthority,
        ebsiEnvConfig: {
          didRegistry,
          trustedIssuersRegistry,
          trustedPoliciesRegistry,
        },
      };
      const vcPayload = await verifyCredentialJwt(credential, options);

      expect(vcPayload).toStrictEqual({
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: expect.stringContaining("vc:ebsi:conformance#"),
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          requestParams.credential_type,
        ],
        issuer: configService.get<string>("issuerMockKid").split("#")[0],
        issuanceDate: expect.any(String),
        issued: expect.any(String),
        validFrom: expect.any(String),
        expirationDate: expect.any(String),
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: configService.get<string>("authorisationCredentialSchema"),
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: configService.get<string>("issuerMockAccreditationUrl"),
          type: "IssuanceCertificate",
        },
      });

      // Check if the test is completed
      response = await request(server)
        .post("/check")
        .send({
          intent: CT_WALLET_CROSS_PRE_AUTHORISED,
          data: {
            did: clientDid,
          },
        });

      expect(response.body).toStrictEqual({
        success: true,
      });
    });

    it("should obtain a CTWalletSameInTime credential", async () => {
      mockSchemas();
      await mockConformanceIssuerRecords(configService);

      const clientKeyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
      const clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
      const methodSpecificIdentifier = clientDid.replace("did:key:", "");
      const clientKid = `${clientDid}#${methodSpecificIdentifier}`;
      const { privateKey: clientPrivateKey } = clientKeyPair;

      // Initiate credential offer from Issuer Mock
      const requestParams = {
        credential_type: "CTWalletSameInTime",
        credential_offer_endpoint: "openid-credential-offer://",
        client_id: clientDid,
      } as const;

      let response = await request(server).get(
        `/issuer-mock/initiate-credential-offer?${new URLSearchParams(
          requestParams
        ).toString()}`
      );

      expect(response.status).toBe(302);
      let { location } = response.headers as { location: string };
      expect(location).toStrictEqual(
        expect.stringContaining(requestParams.credential_offer_endpoint)
      );

      const { search } = new URL(location);

      const parsedCredentialOffer = qs.parse(
        search.slice(1)
      ) as unknown as CredentialOffer;

      expect(parsedCredentialOffer).toStrictEqual({
        credential_offer_uri: expect.any(String),
      });

      // Get credential_offer_uri
      response = await request(server)
        .get(
          getPathnameWithoutPrefix(
            parsedCredentialOffer.credential_offer_uri as string
          )
        )
        .send();

      const credentialOfferPayload = response.body as CredentialOfferPayload;

      expect(credentialOfferPayload).toStrictEqual({
        credential_issuer: issuerMockUri,
        credentials: [
          {
            format: "jwt_vc",
            trust_framework: {
              name: "ebsi",
              type: "Accreditation",
              uri: "TIR link towards accreditation",
            },
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              requestParams.credential_type,
            ],
          },
        ],
        grants: {
          authorization_code: {
            issuer_state: expect.any(String),
          },
        },
      });

      // GET /issuer-mock/.well-known/openid-credential-issuer
      response = await request(server).get(
        "/issuer-mock/.well-known/openid-credential-issuer"
      );

      expect(response.status).toBe(200);
      const credentialIssuerConfig = response.body as CredentialIssuerMetadata;

      // GET AUthorization Server (Auth Mock) /.well-known/openid-configuration
      if (!credentialIssuerConfig.authorization_server)
        throw new Error("authorization_server not defined");
      const authorizationServerUri = getPathnameWithoutPrefix(
        credentialIssuerConfig.authorization_server
      );

      response = await request(server).get(
        `${authorizationServerUri}/.well-known/openid-configuration`
      );

      expect(response.status).toBe(200);
      const authMockConfig = response.body as OPMetadata;

      // GET /auth-mock/authorize
      // Code verifier: cryptographically random string using the characters A-Z, a-z, 0-9, and the
      // punctuation characters -._~ (hyphen, period, underscore, and tilde), between 43 and 128
      // characters long.
      // Example: https://github.com/jaredhanson/passport-oauth2/blob/master/lib/strategy.js#L239
      const codeVerifier = randomBytes(50).toString("base64url");
      const codeChallenge = base64url.baseEncode(
        createHash("sha256").update(codeVerifier).digest()
      );

      const authenticationRequestParams = {
        scope: "openid",
        client_id: clientDid,
        response_type: "code",
        redirect_uri: "openid://callback",
        code_challenge: codeChallenge,
        code_challenge_method: "S256",
        state: randomUUID(),
        ...(credentialOfferPayload.grants.authorization_code?.issuer_state && {
          issuer_state:
            credentialOfferPayload.grants.authorization_code.issuer_state,
        }),
        client_metadata: JSON.stringify({
          authorization_endpoint: "openid:",
        }),
        authorization_details: JSON.stringify([
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [issuerMockUri],
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              requestParams.credential_type,
            ],
          },
        ]),
      } satisfies GetAuthorizeHolderWallerDto;

      response = await request(server)
        .get(
          `${getPathnameWithoutPrefix(
            authMockConfig.authorization_endpoint
          )}?${new URLSearchParams(authenticationRequestParams).toString()}`
        )
        .send();

      expect(response.status).toBe(302);

      location = (response.headers as { location: string }).location;

      const idTokenRequestParams = qs.parse(
        new URL(location).search.substring(1)
      ) as unknown as IdTokenRequest;

      expect(idTokenRequestParams.request).toBeUndefined();
      expect(idTokenRequestParams.request_uri).toBeDefined();

      // POST /auth-mock/direct_post
      const idToken = await new SignJWT({
        nonce: idTokenRequestParams.nonce,
        sub: clientDid,
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(clientDid)
        .setAudience(authMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(clientPrivateKey);

      response = await request(server)
        .post(`${getPathnameWithoutPrefix(idTokenRequestParams.redirect_uri)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token: idToken,
            state: idTokenRequestParams.state,
          }).toString()
        );

      expect(response.status).toBe(302);
      location = (response.headers as { location: string }).location;
      expect(location).toStrictEqual(
        expect.stringContaining(authenticationRequestParams.redirect_uri)
      );

      const authenticationResponseQueryParams = qs.parse(
        new URL(location).search.substring(1)
      ) as unknown as { code: string; state: string };

      expect(authenticationResponseQueryParams.state).toBe(
        authenticationRequestParams.state
      );
      expect(authenticationResponseQueryParams.code).toBeTruthy();

      // POST /auth-mock/token
      const { code } = authenticationResponseQueryParams;
      const tokenRequestQueryParams = {
        grant_type: "authorization_code",
        code,
        client_id: clientDid,
        code_verifier: codeVerifier,
      } satisfies PostTokenPkceDto;

      response = await request(server)
        .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(new URLSearchParams(tokenRequestQueryParams).toString());

      expect(response.status).toBe(200);
      const { access_token: accessToken, c_nonce: cNonce } =
        response.body as TokenResponse;
      expect(accessToken).toBeDefined();

      // POST /issuer-mock/credential
      const proofJwt = await new SignJWT({
        nonce: cNonce,
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(clientDid)
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(clientPrivateKey);

      const credentialRequestParams = {
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          requestParams.credential_type,
        ],
        format: "jwt_vc",
        proof: {
          proof_type: "jwt",
          jwt: proofJwt,
        },
      };

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${accessToken}`)
        .send(credentialRequestParams);

      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        format: "jwt_vc",
        credential: expect.any(String),
      });

      // Check VC
      const { credential } = response.body as CredentialResponse;
      const options: VerifyCredentialOptions = {
        ebsiAuthority,
        ebsiEnvConfig: {
          didRegistry,
          trustedIssuersRegistry,
          trustedPoliciesRegistry,
        },
      };
      const vcPayload = await verifyCredentialJwt(credential, options);

      expect(vcPayload).toStrictEqual({
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: expect.stringContaining("vc:ebsi:conformance#"),
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          requestParams.credential_type,
        ],
        issuer: configService.get<string>("issuerMockKid").split("#")[0],
        issuanceDate: expect.any(String),
        issued: expect.any(String),
        validFrom: expect.any(String),
        expirationDate: expect.any(String),
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: configService.get<string>("authorisationCredentialSchema"),
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: configService.get<string>("issuerMockAccreditationUrl"),
          type: "IssuanceCertificate",
        },
      });

      // Check if the test is completed
      response = await request(server)
        .post("/check")
        .send({
          intent: CT_WALLET_SAME_IN_TIME,
          data: {
            did: clientDid,
          },
        });

      expect(response.body).toStrictEqual({
        success: true,
      });
    });

    it("should obtain a CTWalletSameDeferred credential", async () => {
      mockSchemas();
      await mockConformanceIssuerRecords(configService);

      const clientKeyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
      const clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
      const methodSpecificIdentifier = clientDid.replace("did:key:", "");
      const clientKid = `${clientDid}#${methodSpecificIdentifier}`;
      const { privateKey: clientPrivateKey } = clientKeyPair;

      // Initiate credential offer from Issuer Mock
      const requestParams = {
        credential_type: "CTWalletSameDeferred",
        credential_offer_endpoint: "openid-credential-offer://",
        client_id: clientDid,
      } as const;

      let response = await request(server).get(
        `/issuer-mock/initiate-credential-offer?${new URLSearchParams(
          requestParams
        ).toString()}`
      );

      expect(response.status).toBe(302);
      let { location } = response.headers as { location: string };
      expect(location).toStrictEqual(
        expect.stringContaining(requestParams.credential_offer_endpoint)
      );

      const { search } = new URL(location);

      const parsedCredentialOffer = qs.parse(
        search.slice(1)
      ) as unknown as CredentialOffer;

      expect(parsedCredentialOffer).toStrictEqual({
        credential_offer_uri: expect.any(String),
      });

      // Get credential_offer_uri
      response = await request(server)
        .get(
          getPathnameWithoutPrefix(
            parsedCredentialOffer.credential_offer_uri as string
          )
        )
        .send();

      const credentialOfferPayload = response.body as CredentialOfferPayload;

      expect(credentialOfferPayload).toStrictEqual({
        credential_issuer: issuerMockUri,
        credentials: [
          {
            format: "jwt_vc",
            trust_framework: {
              name: "ebsi",
              type: "Accreditation",
              uri: "TIR link towards accreditation",
            },
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              requestParams.credential_type,
            ],
          },
        ],
        grants: {
          authorization_code: {
            issuer_state: expect.any(String),
          },
        },
      });

      // GET /issuer-mock/.well-known/openid-credential-issuer
      response = await request(server).get(
        "/issuer-mock/.well-known/openid-credential-issuer"
      );

      expect(response.status).toBe(200);
      const credentialIssuerConfig = response.body as CredentialIssuerMetadata;

      // GET AUthorization Server (Auth Mock) /.well-known/openid-configuration
      if (!credentialIssuerConfig.authorization_server)
        throw new Error("authorization_server not defined");
      const authorizationServerUri = getPathnameWithoutPrefix(
        credentialIssuerConfig.authorization_server
      );

      response = await request(server).get(
        `${authorizationServerUri}/.well-known/openid-configuration`
      );

      expect(response.status).toBe(200);
      const authMockConfig = response.body as OPMetadata;

      // GET /auth-mock/authorize
      // Code verifier: cryptographically random string using the characters A-Z, a-z, 0-9, and the
      // punctuation characters -._~ (hyphen, period, underscore, and tilde), between 43 and 128
      // characters long.
      // Example: https://github.com/jaredhanson/passport-oauth2/blob/master/lib/strategy.js#L239
      const codeVerifier = randomBytes(50).toString("base64url");
      const codeChallenge = base64url.baseEncode(
        createHash("sha256").update(codeVerifier).digest()
      );

      const authenticationRequestParams = {
        scope: "openid",
        client_id: clientDid,
        response_type: "code",
        redirect_uri: "openid://callback",
        code_challenge: codeChallenge,
        code_challenge_method: "S256",
        state: randomUUID(),
        ...(credentialOfferPayload.grants.authorization_code?.issuer_state && {
          issuer_state:
            credentialOfferPayload.grants.authorization_code.issuer_state,
        }),
        client_metadata: JSON.stringify({
          authorization_endpoint: "openid:",
        }),
        authorization_details: JSON.stringify([
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [issuerMockUri],
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              requestParams.credential_type,
            ],
          },
        ]),
      } satisfies GetAuthorizeHolderWallerDto;

      response = await request(server)
        .get(
          `${getPathnameWithoutPrefix(
            authMockConfig.authorization_endpoint
          )}?${new URLSearchParams(authenticationRequestParams).toString()}`
        )
        .send();

      expect(response.status).toBe(302);

      location = (response.headers as { location: string }).location;

      const idTokenRequestParams = qs.parse(
        new URL(location).search.substring(1)
      ) as unknown as IdTokenRequest;

      expect(idTokenRequestParams.request).toBeUndefined();
      expect(idTokenRequestParams.request_uri).toBeDefined();

      // POST /auth-mock/direct_post
      const idToken = await new SignJWT({
        nonce: idTokenRequestParams.nonce,
        sub: clientDid,
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(clientDid)
        .setAudience(authMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(clientPrivateKey);

      response = await request(server)
        .post(`${getPathnameWithoutPrefix(idTokenRequestParams.redirect_uri)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            id_token: idToken,
            state: idTokenRequestParams.state,
          }).toString()
        );

      expect(response.status).toBe(302);
      location = (response.headers as { location: string }).location;
      expect(location).toStrictEqual(
        expect.stringContaining(authenticationRequestParams.redirect_uri)
      );

      const authenticationResponseQueryParams = qs.parse(
        new URL(location).search.substring(1)
      ) as unknown as { code: string; state: string };

      expect(authenticationResponseQueryParams.state).toBe(
        authenticationRequestParams.state
      );
      expect(authenticationResponseQueryParams.code).toBeTruthy();

      // POST /auth-mock/token
      const { code } = authenticationResponseQueryParams;
      const tokenRequestQueryParams = {
        grant_type: "authorization_code",
        code,
        client_id: clientDid,
        code_verifier: codeVerifier,
      } satisfies PostTokenPkceDto;

      response = await request(server)
        .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(new URLSearchParams(tokenRequestQueryParams).toString());

      expect(response.status).toBe(200);
      const { access_token: accessToken, c_nonce: cNonce } =
        response.body as TokenResponse;
      expect(accessToken).toBeDefined();

      // POST /issuer-mock/credential
      const proofJwt = await new SignJWT({
        nonce: cNonce,
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(clientDid)
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(clientPrivateKey);

      const credentialRequestParams = {
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          requestParams.credential_type,
        ],
        format: "jwt_vc",
        proof: {
          proof_type: "jwt",
          jwt: proofJwt,
        },
      };

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${accessToken}`)
        .send(credentialRequestParams);

      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        acceptance_token: expect.any(String),
      });

      const { acceptance_token: acceptanceToken } = response.body as {
        acceptance_token: string;
      };

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.deferred_credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${acceptanceToken}`)
        .send();

      expect(response.status).toBe(400);
      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description: "Deferred credential not available yet",
      });

      // Wait 5.5 seconds (> 5 seconds)
      await new Promise((r) => {
        setTimeout(r, 5500);
      });

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.deferred_credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${acceptanceToken}`)
        .send();

      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        format: "jwt_vc",
        credential: expect.any(String),
      });

      // Check VC
      const { credential } = response.body as CredentialResponse;
      const options: VerifyCredentialOptions = {
        ebsiAuthority,
        ebsiEnvConfig: {
          didRegistry,
          trustedIssuersRegistry,
          trustedPoliciesRegistry,
        },
      };
      const vcPayload = await verifyCredentialJwt(credential, options);

      expect(vcPayload).toStrictEqual({
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: expect.stringContaining("vc:ebsi:conformance#"),
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          requestParams.credential_type,
        ],
        issuer: configService.get<string>("issuerMockKid").split("#")[0],
        issuanceDate: expect.any(String),
        issued: expect.any(String),
        validFrom: expect.any(String),
        expirationDate: expect.any(String),
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: configService.get<string>("authorisationCredentialSchema"),
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: configService.get<string>("issuerMockAccreditationUrl"),
          type: "IssuanceCertificate",
        },
      });

      // Check if the test is completed
      response = await request(server)
        .post("/check")
        .send({
          intent: CT_WALLET_SAME_DEFERRED,
          data: {
            did: clientDid,
          },
        });

      expect(response.body).toStrictEqual({
        success: true,
      });
    });

    it("should obtain a CTWalletSamePreAuthorised credential", async () => {
      mockSchemas();
      await mockConformanceIssuerRecords(configService);
      interceptConformanceDomainRequests();

      const clientKeyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
      const clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
      const methodSpecificIdentifier = clientDid.replace("did:key:", "");
      const clientKid = `${clientDid}#${methodSpecificIdentifier}`;
      const { privateKey: clientPrivateKey } = clientKeyPair;

      // Initiate credential offer from Issuer Mock
      const requestParams = {
        credential_type: "CTWalletSamePreAuthorised",
        credential_offer_endpoint: "openid-credential-offer://",
        client_id: clientDid,
      } as const;

      let response = await request(server).get(
        `/issuer-mock/initiate-credential-offer?${new URLSearchParams(
          requestParams
        ).toString()}`
      );

      expect(response.status).toBe(302);
      const { location } = response.headers as { location: string };
      expect(location).toStrictEqual(
        expect.stringContaining(requestParams.credential_offer_endpoint)
      );

      const { search } = new URL(location);

      const parsedCredentialOffer = qs.parse(
        search.slice(1)
      ) as unknown as CredentialOffer;

      expect(parsedCredentialOffer).toStrictEqual({
        credential_offer_uri: expect.any(String),
      });

      // Get credential_offer_uri
      response = await request(server)
        .get(
          getPathnameWithoutPrefix(
            parsedCredentialOffer.credential_offer_uri as string
          )
        )
        .send();

      const credentialOfferPayload = response.body as CredentialOfferPayload;

      expect(credentialOfferPayload).toStrictEqual({
        credential_issuer: issuerMockUri,
        credentials: [
          {
            format: "jwt_vc",
            trust_framework: {
              name: "ebsi",
              type: "Accreditation",
              uri: "TIR link towards accreditation",
            },
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              requestParams.credential_type,
            ],
          },
        ],
        grants: {
          "urn:ietf:params:oauth:grant-type:pre-authorized_code": {
            "pre-authorized_code": expect.any(String),
            user_pin_required: true,
          },
        },
      });

      // GET /issuer-mock/.well-known/openid-credential-issuer
      response = await request(server).get(
        "/issuer-mock/.well-known/openid-credential-issuer"
      );

      expect(response.status).toBe(200);
      const credentialIssuerConfig = response.body as CredentialIssuerMetadata;

      // GET AUthorization Server (Auth Mock) /.well-known/openid-configuration
      if (!credentialIssuerConfig.authorization_server)
        throw new Error("authorization_server not defined");
      const authorizationServerUri = getPathnameWithoutPrefix(
        credentialIssuerConfig.authorization_server
      );

      response = await request(server).get(
        `${authorizationServerUri}/.well-known/openid-configuration`
      );

      expect(response.status).toBe(200);
      const authMockConfig = response.body as OPMetadata;

      // POST /auth-mock/token
      const preAuthorizedCode = credentialOfferPayload.grants[
        "urn:ietf:params:oauth:grant-type:pre-authorized_code"
      ]?.["pre-authorized_code"] as string;

      const tokenRequestQueryParams = {
        grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
        "pre-authorized_code": preAuthorizedCode,
        user_pin: getUserPin(clientDid),
      } satisfies PostTokenPreAuthorizedCodeDto;

      response = await request(server)
        .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(new URLSearchParams(tokenRequestQueryParams).toString());

      expect(response.status).toBe(200);
      const { access_token: accessToken, c_nonce: cNonce } =
        response.body as TokenResponse;
      expect(accessToken).toBeDefined();

      // POST /issuer-mock/credential
      const proofJwt = await new SignJWT({
        nonce: cNonce,
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(clientDid)
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(clientPrivateKey);

      const credentialRequestParams = {
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          requestParams.credential_type,
        ],
        format: "jwt_vc",
        proof: {
          proof_type: "jwt",
          jwt: proofJwt,
        },
      };

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${accessToken}`)
        .send(credentialRequestParams);

      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        format: "jwt_vc",
        credential: expect.any(String),
      });

      // Check VC
      const { credential } = response.body as CredentialResponse;
      const options: VerifyCredentialOptions = {
        ebsiAuthority,
        ebsiEnvConfig: {
          didRegistry,
          trustedIssuersRegistry,
          trustedPoliciesRegistry,
        },
      };
      const vcPayload = await verifyCredentialJwt(credential, options);

      expect(vcPayload).toStrictEqual({
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: expect.stringContaining("vc:ebsi:conformance#"),
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          requestParams.credential_type,
        ],
        issuer: configService.get<string>("issuerMockKid").split("#")[0],
        issuanceDate: expect.any(String),
        issued: expect.any(String),
        validFrom: expect.any(String),
        expirationDate: expect.any(String),
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: configService.get<string>("authorisationCredentialSchema"),
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: configService.get<string>("issuerMockAccreditationUrl"),
          type: "IssuanceCertificate",
        },
      });

      // Check if the test is completed
      response = await request(server)
        .post("/check")
        .send({
          intent: CT_WALLET_SAME_PRE_AUTHORISED,
          data: {
            did: clientDid,
          },
        });

      expect(response.body).toStrictEqual({
        success: true,
      });
    });

    it("should obtain a CTWalletQualificationCredential credential", async () => {
      mockSchemas();
      await mockConformanceIssuerRecords(configService);

      const clientKeyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
      const clientPrivateKeyJwk = await exportJWK(clientKeyPair.privateKey);
      const clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
      const methodSpecificIdentifier = clientDid.replace("did:key:", "");
      const clientKid = `${clientDid}#${methodSpecificIdentifier}`;
      const { privateKey: clientPrivateKey } = clientKeyPair;

      // Initiate credential offer from Issuer Mock
      const requestParams = {
        credential_type: "CTWalletQualificationCredential",
        credential_offer_endpoint: "openid-credential-offer://",
        client_id: clientDid,
      } as const;

      let response = await request(server).get(
        `/issuer-mock/initiate-credential-offer?${new URLSearchParams(
          requestParams
        ).toString()}`
      );

      expect(response.text).toStrictEqual(
        expect.stringContaining(requestParams.credential_offer_endpoint)
      );
      expect(response.status).toBe(200);

      const { search } = new URL(response.text);

      const parsedCredentialOffer = qs.parse(
        search.slice(1)
      ) as unknown as CredentialOffer;

      expect(parsedCredentialOffer).toStrictEqual({
        credential_offer_uri: expect.any(String),
      });

      // Get credential_offer_uri
      response = await request(server)
        .get(
          getPathnameWithoutPrefix(
            parsedCredentialOffer.credential_offer_uri as string
          )
        )
        .send();

      const credentialOfferPayload = response.body as CredentialOfferPayload;

      expect(credentialOfferPayload).toStrictEqual({
        credential_issuer: issuerMockUri,
        credentials: [
          {
            format: "jwt_vc",
            trust_framework: {
              name: "ebsi",
              type: "Accreditation",
              uri: "TIR link towards accreditation",
            },
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              requestParams.credential_type,
            ],
          },
        ],
        grants: {
          authorization_code: {
            issuer_state: expect.any(String),
          },
        },
      });

      // GET /issuer-mock/.well-known/openid-credential-issuer
      response = await request(server).get(
        "/issuer-mock/.well-known/openid-credential-issuer"
      );

      expect(response.status).toBe(200);
      const credentialIssuerConfig = response.body as CredentialIssuerMetadata;

      // GET AUthorization Server (Auth Mock) /.well-known/openid-configuration
      if (!credentialIssuerConfig.authorization_server)
        throw new Error("authorization_server not defined");
      const authorizationServerUri = getPathnameWithoutPrefix(
        credentialIssuerConfig.authorization_server
      );

      response = await request(server).get(
        `${authorizationServerUri}/.well-known/openid-configuration`
      );

      expect(response.status).toBe(200);
      const authMockConfig = response.body as OPMetadata;

      // GET /auth-mock/authorize
      // Code verifier: cryptographically random string using the characters A-Z, a-z, 0-9, and the
      // punctuation characters -._~ (hyphen, period, underscore, and tilde), between 43 and 128
      // characters long.
      // Example: https://github.com/jaredhanson/passport-oauth2/blob/master/lib/strategy.js#L239
      const codeVerifier = randomBytes(50).toString("base64url");
      const codeChallenge = base64url.baseEncode(
        createHash("sha256").update(codeVerifier).digest()
      );

      const authenticationRequestParams = {
        scope: "openid",
        client_id: clientDid,
        response_type: "code",
        redirect_uri: "openid://callback",
        code_challenge: codeChallenge,
        code_challenge_method: "S256",
        state: randomUUID(),
        ...(credentialOfferPayload.grants.authorization_code?.issuer_state && {
          issuer_state:
            credentialOfferPayload.grants.authorization_code.issuer_state,
        }),
        client_metadata: JSON.stringify({
          authorization_endpoint: "openid:",
        }),
        authorization_details: JSON.stringify([
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [issuerMockUri],
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              requestParams.credential_type,
            ],
          },
        ]),
      } satisfies GetAuthorizeHolderWallerDto;

      response = await request(server)
        .get(
          `${getPathnameWithoutPrefix(
            authMockConfig.authorization_endpoint
          )}?${new URLSearchParams(authenticationRequestParams).toString()}`
        )
        .send();

      expect(response.status).toBe(302);

      let { location } = response.headers as { location: string };

      const vpTokenRequestParams = qs.parse(
        new URL(location).search.substring(1),
        {
          depth: 50,
          parameterLimit: 1000,
        }
      ) as unknown as VpTokenRequest;

      expect(vpTokenRequestParams).toStrictEqual({
        state: expect.any(String),
        client_id: expect.any(String),
        redirect_uri: `${authMockUri}/direct_post`,
        response_type: "vp_token",
        response_mode: "direct_post",
        scope: "openid",
        nonce: expect.any(String),
        presentation_definition: expect.any(String),
        request: expect.any(String),
      });

      expect(
        JSON.parse(vpTokenRequestParams.presentation_definition)
      ).toStrictEqual(
        expect.objectContaining({
          id: HOLDER_WALLET_QUALIFICATION_PRESENTATION_DEFINITION.id,
        })
      );

      // POST /auth-mock/direct_post

      // Fake credential issuer
      const credentialIssuer = await createLegalEntity("ES256K");
      const credentialIssuerAccreditationUrl = `${configService.get<string>(
        "domain"
      )}${TIR_API_PATH}/${credentialIssuer.did}/attributes/${randomBytes(
        16
      ).toString("hex")}`;
      jest
        .spyOn(verifiablePresentationLib, "verifyPresentationJwt")
        .mockImplementation(() =>
          Promise.resolve({} as EbsiVerifiablePresentation)
        );

      const createVcJwt = async (type: string) => {
        const issuanceDate = new Date();
        // JWT access token must have 2 hours expiration time and there are no Refresh Tokens.
        const expirationDate = new Date(
          issuanceDate.getTime() + 2 * 60 * 60 * 1000
        );
        const vcPayload = {
          "@context": ["https://www.w3.org/2018/credentials/v1"],
          id: `urn:uuid:${randomUUID()}`,
          type: ["VerifiableCredential", "VerifiableAttestation", type],
          issuer: credentialIssuer.did,
          issuanceDate: `${issuanceDate.toISOString().slice(0, -5)}Z`,
          issued: `${issuanceDate.toISOString().slice(0, -5)}Z`,
          validFrom: `${issuanceDate.toISOString().slice(0, -5)}Z`,
          expirationDate: `${expirationDate.toISOString().slice(0, -5)}Z`,
          credentialSubject: {
            id: clientDid,
          },
          credentialSchema: {
            id: configService.get<string>("authorisationCredentialSchema"),
            type: "FullJsonSchemaValidator2021",
          },
          termsOfUse: {
            id: credentialIssuerAccreditationUrl,
            type: "IssuanceCertificate",
          },
        };

        return createVerifiableCredentialJwt(vcPayload, credentialIssuer, {
          ebsiAuthority: "example.net",
          skipValidation: true,
        });
      };

      // Create fake credentials
      const credentials = [
        await createVcJwt("CTWalletSameInTime"),
        await createVcJwt("CTWalletCrossInTime"),
        await createVcJwt("CTWalletSameDeferred"),
        await createVcJwt("CTWalletCrossDeferred"),
        await createVcJwt("CTWalletSamePreAuthorised"),
        await createVcJwt("CTWalletCrossPreAuthorised"),
      ];

      const vpPayload = {
        id: `urn:did:${randomUUID()}`,
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        type: ["VerifiablePresentation"],
        holder: clientDid,
        verifiableCredential: credentials,
      };

      const credentialSubject: EbsiIssuer = {
        did: clientDid,
        kid: clientKid,
        privateKeyJwk: clientPrivateKeyJwk,
        publicKeyJwk: clientPublicKeyJwk,
        alg: "ES256",
      };

      const vpToken = await createVerifiablePresentationJwt(
        vpPayload,
        credentialSubject,
        authMockUri,
        {
          ebsiAuthority: "example.net",
          skipValidation: true,
          nonce: randomUUID(),
          exp: Math.floor(Date.now() / 1000) + 100,
          nbf: Math.floor(Date.now() / 1000) - 100,
        }
      );

      const presentationSubmission: PresentationSubmission = {
        id: randomUUID(),
        definition_id: HOLDER_WALLET_QUALIFICATION_PRESENTATION_DEFINITION.id,
        descriptor_map: [
          {
            id: "same-device-in-time-credential",
            path: "$",
            format: "jwt_vp",
            path_nested: {
              id: "same-device-in-time-credential",
              format: "jwt_vc",
              path: "$.verifiableCredential[0]",
            },
          },
          {
            id: "cross-device-in-time-credential",
            path: "$",
            format: "jwt_vp",
            path_nested: {
              id: "cross-device-in-time-credential",
              format: "jwt_vc",
              path: "$.verifiableCredential[1]",
            },
          },
          {
            id: "same-device-deferred-credential",
            path: "$",
            format: "jwt_vp",
            path_nested: {
              id: "same-device-deferred-credential",
              format: "jwt_vc",
              path: "$.verifiableCredential[2]",
            },
          },
          {
            id: "cross-device-deferred-credential",
            path: "$",
            format: "jwt_vp",
            path_nested: {
              id: "cross-device-deferred-credential",
              format: "jwt_vc",
              path: "$.verifiableCredential[3]",
            },
          },
          {
            id: "same-device-pre_authorised-credential",
            path: "$",
            format: "jwt_vp",
            path_nested: {
              id: "same-device-pre_authorised-credential",
              format: "jwt_vc",
              path: "$.verifiableCredential[4]",
            },
          },
          {
            id: "cross-device-pre_authorised-credential",
            path: "$",
            format: "jwt_vp",
            path_nested: {
              id: "cross-device-pre_authorised-credential",
              format: "jwt_vc",
              path: "$.verifiableCredential[5]",
            },
          },
        ],
      };

      response = await request(server)
        .post(`${getPathnameWithoutPrefix(vpTokenRequestParams.redirect_uri)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(
          new URLSearchParams({
            vp_token: vpToken,
            state: vpTokenRequestParams.state,
            presentation_submission: JSON.stringify(presentationSubmission),
          }).toString()
        );

      expect(response.status).toBe(302);
      location = (response.headers as { location: string }).location;
      expect(location).toStrictEqual(
        expect.stringContaining(authenticationRequestParams.redirect_uri)
      );

      const authenticationResponseQueryParams = qs.parse(
        new URL(location).search.substring(1)
      ) as unknown as { code: string; state: string };

      expect(authenticationResponseQueryParams.state).toBe(
        authenticationRequestParams.state
      );
      expect(authenticationResponseQueryParams.code).toBeTruthy();

      // POST /auth-mock/token
      const { code } = authenticationResponseQueryParams;
      const tokenRequestQueryParams = {
        grant_type: "authorization_code",
        code,
        client_id: clientDid,
        code_verifier: codeVerifier,
      } satisfies PostTokenPkceDto;

      response = await request(server)
        .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(new URLSearchParams(tokenRequestQueryParams).toString());

      expect(response.status).toBe(200);
      const { access_token: accessToken, c_nonce: cNonce } =
        response.body as TokenResponse;
      expect(accessToken).toBeDefined();

      // POST /issuer-mock/credential
      const proofJwt = await new SignJWT({
        nonce: cNonce,
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(clientDid)
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(clientPrivateKey);

      const credentialRequestParams = {
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          requestParams.credential_type,
        ],
        format: "jwt_vc",
        proof: {
          proof_type: "jwt",
          jwt: proofJwt,
        },
      };

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${accessToken}`)
        .send(credentialRequestParams);

      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        format: "jwt_vc",
        credential: expect.any(String),
      });

      // Check VC
      const { credential } = response.body as CredentialResponse;
      const options: VerifyCredentialOptions = {
        ebsiAuthority,
        ebsiEnvConfig: {
          didRegistry,
          trustedIssuersRegistry,
          trustedPoliciesRegistry,
        },
      };
      const credentialPayload = await verifyCredentialJwt(credential, options);

      expect(credentialPayload).toStrictEqual({
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: expect.stringContaining("vc:ebsi:conformance#"),
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          requestParams.credential_type,
        ],
        issuer: configService.get<string>("issuerMockKid").split("#")[0],
        issuanceDate: expect.any(String),
        issued: expect.any(String),
        validFrom: expect.any(String),
        expirationDate: expect.any(String),
        credentialSubject: {
          id: clientDid,
        },
        credentialSchema: {
          id: configService.get<string>("authorisationCredentialSchema"),
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: configService.get<string>("issuerMockAccreditationUrl"),
          type: "IssuanceCertificate",
        },
      });

      // Check if the test is completed
      response = await request(server)
        .post("/check")
        .send({
          intent: REQUEST_CT_WALLET_QUALIFICATION_CREDENTIAL,
          data: {
            did: clientDid,
          },
        });

      expect(response.body).toStrictEqual({
        success: true,
      });
    });
  });

  describe("PDA1 flow", () => {
    it("should obtain a VerifiablePortableDocumentA1 credential", async () => {
      mockSchemas();
      await mockConformanceIssuerRecords(configService);
      interceptConformanceDomainRequests();

      const clientKeyPair = await generateKeyPair("ES256");
      const clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
      const clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
      const methodSpecificIdentifier = clientDid.replace("did:key:", "");
      const clientKid = `${clientDid}#${methodSpecificIdentifier}`;
      const { privateKey: clientPrivateKey } = clientKeyPair;

      // Initiate credential offer from Issuer Mock
      const requestParams = {
        credential_type: "VerifiablePortableDocumentA1",
        credential_offer_endpoint: "openid-credential-offer://",
        client_id: clientDid,
      } as const;

      let response = await request(server).get(
        `/issuer-mock/initiate-credential-offer?${new URLSearchParams(
          requestParams
        ).toString()}`
      );

      expect(response.text).toStrictEqual(
        expect.stringContaining(requestParams.credential_offer_endpoint)
      );
      expect(response.status).toBe(200);

      const { search } = new URL(response.text);

      const parsedCredentialOffer = qs.parse(
        search.slice(1)
      ) as unknown as CredentialOffer;

      expect(parsedCredentialOffer).toStrictEqual({
        credential_offer_uri: expect.any(String),
      });

      // Get credential_offer_uri
      response = await request(server)
        .get(
          getPathnameWithoutPrefix(
            parsedCredentialOffer.credential_offer_uri as string
          )
        )
        .send();

      const credentialOfferPayload = response.body as CredentialOfferPayload;

      expect(credentialOfferPayload).toStrictEqual({
        credential_issuer: issuerMockUri,
        credentials: [
          {
            format: "jwt_vc",
            trust_framework: {
              name: "ebsi",
              type: "Accreditation",
              uri: "TIR link towards accreditation",
            },
            types: [
              "VerifiableCredential",
              "VerifiableAttestation",
              requestParams.credential_type,
            ],
          },
        ],
        grants: {
          "urn:ietf:params:oauth:grant-type:pre-authorized_code": {
            "pre-authorized_code": expect.any(String),
            user_pin_required: true,
          },
        },
      });

      // GET /issuer-mock/.well-known/openid-credential-issuer
      response = await request(server).get(
        "/issuer-mock/.well-known/openid-credential-issuer"
      );

      expect(response.status).toBe(200);
      const credentialIssuerConfig = response.body as CredentialIssuerMetadata;

      // GET AUthorization Server (Auth Mock) /.well-known/openid-configuration
      if (!credentialIssuerConfig.authorization_server)
        throw new Error("authorization_server not defined");
      const authorizationServerUri = getPathnameWithoutPrefix(
        credentialIssuerConfig.authorization_server
      );

      response = await request(server).get(
        `${authorizationServerUri}/.well-known/openid-configuration`
      );

      expect(response.status).toBe(200);
      const authMockConfig = response.body as OPMetadata;

      // POST /auth-mock/token
      const preAuthorizedCode = credentialOfferPayload.grants[
        "urn:ietf:params:oauth:grant-type:pre-authorized_code"
      ]?.["pre-authorized_code"] as string;

      const tokenRequestQueryParams = {
        grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
        "pre-authorized_code": preAuthorizedCode,
        user_pin: getUserPin(clientDid),
      } satisfies PostTokenPreAuthorizedCodeDto;

      response = await request(server)
        .post(`${getPathnameWithoutPrefix(authMockConfig.token_endpoint)}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send(new URLSearchParams(tokenRequestQueryParams).toString());

      expect(response.status).toBe(200);
      const { access_token: accessToken, c_nonce: cNonce } =
        response.body as TokenResponse;
      expect(accessToken).toBeDefined();

      // POST /issuer-mock/credential
      const proofJwt = await new SignJWT({
        nonce: cNonce,
      })
        .setProtectedHeader({
          typ: "openid4vci-proof+jwt",
          alg: "ES256",
          kid: clientKid,
        })
        .setIssuer(clientDid)
        .setAudience(issuerMockUri)
        .setIssuedAt()
        .setExpirationTime("5m")
        .sign(clientPrivateKey);

      const credentialRequestParams = {
        types: [
          "VerifiableCredential",
          "VerifiableAttestation",
          requestParams.credential_type,
        ],
        format: "jwt_vc",
        proof: {
          proof_type: "jwt",
          jwt: proofJwt,
        },
      };

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${accessToken}`)
        .send(credentialRequestParams);

      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        acceptance_token: expect.any(String),
      });

      const { acceptance_token: acceptanceToken } = response.body as {
        acceptance_token: string;
      };

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.deferred_credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${acceptanceToken}`)
        .send();

      expect(response.status).toBe(400);
      expect(response.body).toStrictEqual({
        error: "invalid_request",
        error_description: "Deferred credential not available yet",
      });

      // Wait 5.5 seconds (> 5 seconds)
      await new Promise((r) => {
        setTimeout(r, 5500);
      });

      response = await request(server)
        .post(
          `${getPathnameWithoutPrefix(
            credentialIssuerConfig.deferred_credential_endpoint
          )}`
        )
        .set("Authorization", `Bearer ${acceptanceToken}`)
        .send();

      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        format: "jwt_vc",
        credential: expect.any(String),
      });

      // Check VC
      const { credential } = response.body as CredentialResponse;
      const options: VerifyCredentialOptions = {
        ebsiAuthority,
        ebsiEnvConfig: {
          didRegistry,
          trustedIssuersRegistry,
          trustedPoliciesRegistry,
        },
      };
      const vcPayload = await verifyCredentialJwt(credential, options);

      expect(vcPayload).toStrictEqual({
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: expect.stringContaining("vc:ebsi:conformance#"),
        type: [
          "VerifiableCredential",
          "VerifiableAttestation",
          requestParams.credential_type,
        ],
        issuer: configService.get<string>("issuerMockKid").split("#")[0],
        issuanceDate: expect.any(String),
        issued: expect.any(String),
        validFrom: expect.any(String),
        expirationDate: expect.any(String),
        credentialSubject: {
          id: clientDid,
          section1: {
            personalIdentificationNumber: "1",
            sex: "01",
            surname: "Dalton",
            forenames: "Joe Jack William Averell",
            dateBirth: "1985-08-15",
            nationalities: ["BE"],
            stateOfResidenceAddress: {
              streetNo: "sss, nnn ",
              postCode: "ppp",
              town: "ccc",
              countryCode: "BE",
            },
            stateOfStayAddress: {
              streetNo: "sss, nnn ",
              postCode: "ppp",
              town: "ccc",
              countryCode: "BE",
            },
          },
          section2: {
            memberStateWhichLegislationApplies: "DE",
            startingDate: "2022-10-09",
            endingDate: "2022-10-29",
            certificateForDurationActivity: true,
            determinationProvisional: false,
            transitionRulesApplyAsEC8832004: false,
          },
          section3: {
            postedEmployedPerson: false,
            employedTwoOrMoreStates: false,
            postedSelfEmployedPerson: true,
            selfEmployedTwoOrMoreStates: true,
            civilServant: true,
            contractStaff: false,
            mariner: false,
            employedAndSelfEmployed: false,
            civilAndEmployedSelfEmployed: true,
            flightCrewMember: false,
            exception: false,
            exceptionDescription: "",
            workingInStateUnder21: false,
          },
          section4: {
            employee: false,
            selfEmployedActivity: true,
            nameBusinessName: "1",
            registeredAddress: {
              streetNo: "1, 1 1",
              postCode: "1",
              town: "1",
              countryCode: "DE",
            },
          },
          section5: {
            noFixedAddress: true,
          },
          section6: {
            name: "National Institute for the Social Security of the Self-employed (NISSE)",
            address: {
              streetNo: "Quai de Willebroeck 35",
              postCode: "1000",
              town: "Bruxelles",
              countryCode: "BE",
            },
            institutionID: "NSSIE/INASTI/RSVZ",
            officeFaxNo: "",
            officePhoneNo: "0800 12 018",
            email: "info@rsvz-inasti.fgov.be",
            date: "2022-10-28",
            signature: "Official signature",
          },
        },
        credentialSchema: {
          id: configService.get<string>("pda1CredentialSchema"),
          type: "FullJsonSchemaValidator2021",
        },
        termsOfUse: {
          id: configService.get<string>("issuerMockAccreditationUrl"),
          type: "IssuanceCertificate",
        },
      });
    });
  });
});
