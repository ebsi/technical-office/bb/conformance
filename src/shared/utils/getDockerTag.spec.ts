import { describe, expect, it } from "@jest/globals";
import { getDockerTag } from "./index";

describe("getDockerTag", () => {
  it("should return an empty string on unsupported environment", () => {
    const tag = getDockerTag("foo");
    expect(tag).toBe("");
  });
});
