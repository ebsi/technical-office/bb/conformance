import {
  createVerifiableCredentialJwt,
  CreateVerifiableCredentialOptions,
  EbsiIssuer,
  EbsiVerifiableAttestation,
} from "@cef-ebsi/verifiable-credential";
import { randomBytes, randomUUID } from "node:crypto";
import type { JWKWithKid } from "../utils";
import type { CredentialRequest, AccessTokenPayload } from "./validators";

export async function issueCredential(
  serverDid: string,
  serverKid: string,
  issuerPrivateKeyJwk: JWKWithKid,
  issuerAccreditationUrl: string,
  authorisationCredentialSchema: string,
  createVerifiableCredentialOptions: CreateVerifiableCredentialOptions,
  additionalVcPayload: Partial<EbsiVerifiableAttestation>,
  credentialRequest: CredentialRequest,
  accessTokenPayload: AccessTokenPayload
): Promise<{
  reservedAttributeId: string;
  vcJwt: string;
}> {
  /**
   * Reserved Attribute ID
   *
   * When the Verifiable Credential is intended to be in the Trusted
   * Issuers Registry the issuer mock makes a preregistration (aka invitation)
   * defining the metadata, and later on the subject registers the
   * content of the credential (aka acceptance). The "reserved attribute
   * ID" acts an identifier to be able to update or consult this credential
   * in the Trusted Issuers Registry.
   */
  let reservedAttributeId = "";
  if (
    credentialRequest.types.some((t) =>
      [
        "VerifiableAuthorisationForTrustChain",
        "VerifiableAccreditationToAccredit",
        "VerifiableAccreditationToAttest",
      ].includes(t)
    )
  ) {
    reservedAttributeId = `0x${randomBytes(32).toString("hex")}`;
  }

  // Issue VC
  const { d, ...publicKeyJwk } = issuerPrivateKeyJwk;
  const vcIssuer: EbsiIssuer = {
    kid: serverKid,
    did: serverDid,
    publicKeyJwk,
    privateKeyJwk: issuerPrivateKeyJwk,
    alg: "ES256",
  };
  const issuedAt = new Date();
  const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
  const expiresAt = new Date(
    issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
  );
  const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;
  const vcPayload: EbsiVerifiableAttestation = {
    "@context": ["https://www.w3.org/2018/credentials/v1"],
    id: `vc:ebsi:conformance#${randomUUID()}`,
    type: credentialRequest.types,
    issuer: vcIssuer.did,
    issuanceDate,
    issued: issuanceDate,
    validFrom: issuanceDate,
    expirationDate,
    credentialSubject: {
      id: accessTokenPayload.sub,
      ...(issuerAccreditationUrl &&
        [
          "VerifiableAccreditationToAttest",
          "VerifiableAccreditationToAccredit",
        ].some((type) => credentialRequest.types.includes(type)) && {
          accreditedFor: [
            {
              schemaId: authorisationCredentialSchema,
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "CTRevocable", // Is allowed to issue CTRevocable VCs
              ],
              limitJurisdiction:
                "https://publications.europa.eu/resource/authority/atu/FIN",
            },
          ],
        }),
      ...(reservedAttributeId && { reservedAttributeId }),
      ...(credentialRequest.types.includes("VerifiablePortableDocumentA1") && {
        section1: {
          personalIdentificationNumber: "1",
          sex: "01",
          surname: "Dalton",
          forenames: "Joe Jack William Averell",
          dateBirth: "1985-08-15",
          nationalities: ["BE"],
          stateOfResidenceAddress: {
            streetNo: "sss, nnn ",
            postCode: "ppp",
            town: "ccc",
            countryCode: "BE",
          },
          stateOfStayAddress: {
            streetNo: "sss, nnn ",
            postCode: "ppp",
            town: "ccc",
            countryCode: "BE",
          },
        },
        section2: {
          memberStateWhichLegislationApplies: "DE",
          startingDate: "2022-10-09",
          endingDate: "2022-10-29",
          certificateForDurationActivity: true,
          determinationProvisional: false,
          transitionRulesApplyAsEC8832004: false,
        },
        section3: {
          postedEmployedPerson: false,
          employedTwoOrMoreStates: false,
          postedSelfEmployedPerson: true,
          selfEmployedTwoOrMoreStates: true,
          civilServant: true,
          contractStaff: false,
          mariner: false,
          employedAndSelfEmployed: false,
          civilAndEmployedSelfEmployed: true,
          flightCrewMember: false,
          exception: false,
          exceptionDescription: "",
          workingInStateUnder21: false,
        },
        section4: {
          employee: false,
          selfEmployedActivity: true,
          nameBusinessName: "1",
          registeredAddress: {
            streetNo: "1, 1 1",
            postCode: "1",
            town: "1",
            countryCode: "DE",
          },
        },
        section5: {
          noFixedAddress: true,
        },
        section6: {
          name: "National Institute for the Social Security of the Self-employed (NISSE)",
          address: {
            streetNo: "Quai de Willebroeck 35",
            postCode: "1000",
            town: "Bruxelles",
            countryCode: "BE",
          },
          institutionID: "NSSIE/INASTI/RSVZ",
          officeFaxNo: "",
          officePhoneNo: "0800 12 018",
          email: "info@rsvz-inasti.fgov.be",
          date: "2022-10-28",
          signature: "Official signature",
        },
      }),
    },
    credentialSchema: {
      id: authorisationCredentialSchema,
      type: "FullJsonSchemaValidator2021",
    },
    ...(issuerAccreditationUrl && {
      termsOfUse: {
        id: issuerAccreditationUrl,
        type: "IssuanceCertificate",
      },
    }),
    ...additionalVcPayload,
  };

  const vcJwt = await createVerifiableCredentialJwt(
    vcPayload,
    vcIssuer,
    createVerifiableCredentialOptions
  );
  return { vcJwt, reservedAttributeId };
}

export default issueCredential;
