/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  testTimeout: 60_000,
  rootDir: ".",
  roots: ["<rootDir>/src/", "<rootDir>/tests/"],
  injectGlobals: false,
  testMatch: ["**/?(*.|*-)+(spec|test).ts"],
  transform: {
    "^.+\\.(t|j)s$": ["ts-jest", { isolatedModules: true }],
  },
  moduleFileExtensions: ["js", "json", "ts"],
  coverageDirectory: "./coverage/",
  collectCoverageFrom: ["src/**/*.(t|j)s", "!src/main.ts", "!**/*.d.ts"],
  coverageReporters: ["text", "lcov", "json", "clover", "cobertura"],
};
